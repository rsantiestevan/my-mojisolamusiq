<?php
	global $wp_embed, $blog_arr;
	
	// Check post thumb
	$thumbnail = (has_post_thumbnail(get_the_ID())) ? true : false;
	
	// Get category of post
	$categories = get_the_terms(get_the_ID(), 'gallery-category');
	
	$gallery_size = (function_exists('get_field')) ? get_field('gallery_size', get_the_ID()) : ''; 
	$gallery_size = empty($gallery_size) ? 'small' : $gallery_size;

	// Get HTML 
	$title 				= get_the_title();
	$post_link 			= get_permalink( get_the_ID() );
	$date 				= get_the_date();
	$post_thumb_size = 'thumb_960x615';
	if( $gallery_size == 'horizontal' ){
		$post_thumb_size = 'thumb_480x402';
	}
	elseif( $gallery_size == 'vertical' ){
		$post_thumb_size = 'thumb_480x615';
	}
	elseif( $gallery_size == 'big' ){
		$post_thumb_size = 'thumb_960x615';
	}
	elseif ( $gallery_size == 'style1' ) {
		$post_thumb_size = 'thumb_480x352';
	}
	elseif ( $gallery_size == 'style2' ) {
		$post_thumb_size = 'thumb_480x313';
	}


	$post_thumb 		= get_the_post_thumbnail( get_the_ID(), $post_thumb_size, array( 'alt' => trim( get_the_title() ) ) );
	$post_thumb_url 	= wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
	
	// Post Class
	$post_classes = array('article','post','project','isotope-selector','free-style');	
	if($thumbnail) $post_classes[] = 'has-post-thumbnail'; else $post_classes[] = 'no-post-thumbnail';
	$post_classes[] = 'has-hover';
	if ( $gallery_size == 'small' || $gallery_size == 'vertical' || $gallery_size == 'style1' || $gallery_size == 'style2' ) $post_classes[] = 'gallery-1';
	elseif ( $gallery_size == 'big' ) $post_classes[] = 'gallery-2';
	//
	if(count($categories) > 0 && is_array($categories)){
		foreach ($categories as $key => $category) {
			$post_classes[] = 'k2t-cat-'.$category->slug;
		}
	}
	$post_classes = implode(' ',$post_classes);
	
?>

<article class="<?php echo esc_attr( $post_classes ) ;?>"><div class="article-inner">
	<div class="view view-first">
	    <?php if ( $thumbnail ): echo ( $post_thumb ); else:?>
	        <img src="<?php echo K_GALLERY_BASE_PATH . '/assets/images/thumb-430x430.png' ?>" />
	    <?php endif;?>
	    <a href="<?php echo esc_url( $post_thumb_url );?>" class="k2t-popup-link"></a>
	</div>
</div></article><!-- .article -->