<?php
// Register custom post type "Page Section"
add_action( 'init', 'k2t_register_gallery' ); 
function k2t_register_gallery ()  { 
	global $smof_data;

$gallery_slug = ( get_option( 'gallery_slug' ) != '' ) ?  get_option( 'gallery_slug' ) : esc_html__( 'k-gallery', 'k-gallery' );

	$labels = array(  
		'name' => esc_html__('K-Gallery', 'k-gallery'),  
		'singular_name' => esc_html__('K-Gallery', 'k-gallery'),  
		'add_new' => esc_html__('Add New Gallery', 'k-gallery'),  
		'add_new_item' => esc_html__('Add New Gallery', 'k-gallery'),  
		'edit_item' => esc_html__('Edit Gallery', 'k-gallery'),  
		'new_item' => esc_html__('New Gallery', 'k-gallery'),  
		'view_item' => esc_html__('View Gallery', 'k-gallery'),  
		'search_items' => esc_html__('Search Gallery', 'k-gallery'),  
		'not_found' => esc_html__('No Gallery found', 'k-gallery'),  
		'not_found_in_trash' => esc_html__('No Gallery found in Trash', 'k-gallery'),  
		'parent_item_colon' => '' 
	);  

	$args = array(  
		'labels' 				=> $labels,  
		'menu_position' 		=> 5, 
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'has_archive' 			=> true,
		'hierarchical' 			=> false,
		'supports' 				=> array( 'title', 'author', 'thumbnail' )
	);
	if(!empty($gallery_slug)){
		$args['rewrite'] = array('slug' => $gallery_slug);
	} else{
		$args['rewrite'] = array('slug' => 'gallery');
	}
	register_post_type('post-gallery',$args);  
}

/* --------------------------------------------------- */
/* Register gallery Category
/* --------------------------------------------------- */

add_action( 'init', 'k2t_register_gallery_category', 0 );
if ( ! function_exists('k2t_register_gallery_category') ) {
	function k2t_register_gallery_category(){
		global $smof_data;
		$labels = array(
			'name'                => _x( 'K-Gallery Categories', 'taxonomy general name','k-gallery'),
			'singular_name'       => _x( 'K-Gallery Category', 'taxonomy singular name','k-gallery'),
			'search_items'        => esc_html__( 'Search K-Gallery Categories','k-gallery'),
			'all_items'           => esc_html__( 'All K-Gallery Categories','k-gallery'),
			'parent_item'         => esc_html__( 'Parent K-Gallery Category','k-gallery'),
			'parent_item_colon'   => esc_html__( 'Parent K-Gallery Category:','k-gallery'),
			'edit_item'           => esc_html__( 'Edit K-Gallery Category','k-gallery'), 
			'update_item'         => esc_html__( 'Update K-Gallery Category','k-gallery'),
			'add_new_item'        => esc_html__( 'Add New K-Gallery Category','k-gallery'),
			'new_item_name'       => esc_html__( 'New K-Gallery Category Name','k-gallery'),
			'menu_name'           => esc_html__( 'K-Gallery Category','k-gallery')
		); 	
		
		$args = array(
			'hierarchical'        => true,
			'labels'              => $labels,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'query_var'           => true,
		);
		$args['rewrite'] = array('slug' => 'gallery-category');
		register_taxonomy( 'gallery-category', array('post-gallery'), $args );
	}
}
?>