<?php
if( function_exists('register_field_group') ):

register_field_group( array(
	'key' => 'group_54227be67e225',
	'title' => 'Gallery Options',
	'fields' => array(
		array(
			'key' => 'field_54227ccb3b6d2',
			'label' => 'Gallery size',
			'name' => 'gallery_size',
			'prefix' => '',
			'type' => 'select',
			'instructions' => 'Only support Gallery Masonry Free Style',
			'required' => 0,
			'conditional_logic' => 0,
			'choices' => array(
				'horizontal' => 'Horizontal',
				'vertical' 	 => 'Vertical',
				'big' 		 => 'Big',
				'style1'	 => 'Free Style 1',
				'style2'	 => 'Free Style 2',
			),
			'default_value' => array(
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post-gallery',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;
?>