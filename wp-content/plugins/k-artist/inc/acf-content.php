<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_55b9e575baf55',
	'title' => 'Artist',
	'fields' => array (
		array(
			'key'               => 'field_53df40c5583c3',
			'label'             => esc_html__('Artist Layout', 'k-artist'),
			'name'              => '',
			'prefix'            => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'placement' 		=> 'left',
		),
		array(
			'key'               => 'field_53df40dd593c5',
			'label'             => esc_html__('Artist layout', 'k-artist'),
			'name'              => 'teacher_layout',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'choices' => array(
				'default'       => 'Default',
				'right_sidebar' => 'Right Sidebar',
				'left_sidebar'  => 'Left Sidebar',
				'no_sidebar'    => 'No Sidebar',
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		// array(
		// 	'key'               => 'field_53df4176c939b',
		// 	'label'             => esc_html__('Custom sidebar name', 'k-artist'),
		// 	'name'              => 'teacher_custom_sidebar',
		// 	'prefix'            => '',
		// 	'type'              => 'text',
		// 	'instructions'      => '',
		// 	'required'          => 0,
		// 	'conditional_logic' => 0,
		// 	'default_value'     => '',
		// 	'placeholder'       => '',
		// 	'prepend'           => '',
		// 	'prepend'           => '',
		// 	'maxlength'         => '',
		// 	'readonly'          => 0,
		// 	'disabled'          => 0,
		// ),
		array(
			'key'               => 'field_53fee97493014',
			'label'             => esc_html__('Artist Titlebar', 'k-artist'),
			'name'              => '',
			'prefix'            => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'placement' 		=> 'left',
		),
		array(
			'key'               => 'field_53fd4b6093834',
			'label'             => esc_html__('Show/Hide Title Bar', 'k-artist'),
			'name'              => 'teacher_display_titlebar',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'choices' => array(
				'default'=>'Default',
				'show'  => 'Show',
				'hided' => 'Hide',
			),
			'default_value' => 'show',
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_542e57d793106',
			'label'             => esc_html__('Artist titlebar font size', 'k-artist'),
			'name'              => 'teacher_titlebar_font_size',
			'prefix'            => '',
			'type'              => 'text',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'prepend'           => '',
			'maxlength'         => '',
			'readonly'          => 0,
			'disabled'          => 0,
		),
		array(
			'key'               => 'field_53fftww693019',
			'label'             => esc_html__('Artist title bar color', 'k-artist'),
			'name'              => 'teacher_titlebar_color',
			'prefix'            => '',
			'type'              => 'color_picker',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
		),
		array(
			'key'               => 'field_53feec3f93016',
			'label'             => esc_html__('Padding top', 'k-artist'),
			'name'              => 'teacher_pading_top',
			'prefix'            => '',
			'type'              => 'text',
			'instructions'      => 'Unit: px (Ex: 10px)',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				)
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'prepend'           => '',
			'maxlength'         => '',
			'readonly'          => 0,
			'disabled'          => 0,
		),
		array(
			'key'               => 'field_53feec3f93127',
			'label'             => esc_html__('Padding bottom', 'k-artist'),
			'name'              => 'teacher_pading_bottom',
			'prefix'            => '',
			'type'              => 'text',
			'instructions'      => 'Unit: px (Ex: 10px)',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				)
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'prepend'           => '',
			'maxlength'         => '',
			'readonly'          => 0,
			'disabled'          => 0,
		),
		array(
			'key'               => 'field_53feecea93019',
			'label'             => esc_html__('Background color', 'k-artist'),
			'name'              => 'teacher_background_color',
			'prefix'            => '',
			'type'              => 'color_picker',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
		),
		array(
			'key'               => 'field_53feecb293018',
			'label'             => esc_html__('Background image', 'k-artist'),
			'name'              => 'teacher_background_image',
			'prefix'            => '',
			'type'              => 'image',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'return_format' 	=> 'url',
			'preview_size'      => 'thumbnail',
			'library'           => 'all',
		),
		array(
			'key'               => 'field_53feeda09301b',
			'label'             => esc_html__('Background position', 'k-artist'),
			'name'              => 'teacher_background_position',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				''      		=> esc_html__('None', 'k-artist'),
				'left top'      => esc_html__('Left Top', 'k-artist'),
				'left center'   => esc_html__('Left Center', 'k-artist'),
				'left bottom'   => esc_html__('Left Bottom', 'k-artist'),
				'right top'     => esc_html__('Right Top', 'k-artist'),
				'right center'  => esc_html__('Right Center', 'k-artist'),
				'right bottom'  => esc_html__('Right Bottom', 'k-artist'),
				'center top'    => esc_html__('Center top', 'k-artist'),
				'center center' => esc_html__('Center Center', 'k-artist'),
				'center bottom' => esc_html__('Center Bottom', 'k-artist'),
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_53fuida09367b',
			'label'             => esc_html__('Background size', 'k-artist'),
			'name'              => 'teacher_background_size',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				''		  => esc_html__('None', 'k-artist'),
				'inherit' => esc_html__('Inherit', 'k-artist'),
				'cover'   => esc_html__('Cover', 'k-artist'),
				'contain' => esc_html__('Contain', 'k-artist'),
				'full'    => esc_html__('100%', 'k-artist'),
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_52rguda09301b',
			'label'             => esc_html__('Background repeat', 'k-artist'),
			'name'              => 'teacher_background_repeat',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				''		  	=> esc_html__('None', 'k-artist'),
				'no-repeat' => esc_html__('No Repeat', 'k-artist'),
				'repeat'    => esc_html__('Repeat', 'k-artist'),
				'repeat-x'  => esc_html__('Repeat X', 'k-artist'),
				'repeat-y'  => esc_html__('Repeat Y', 'k-artist'),
			),
			'default_value' => 'repeat',
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_54336341939f7',
			'label'             => esc_html__('Background parallax', 'k-artist'),
			'name'              => 'teacher_background_parallax',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				'1' => 'True',
				'0' => 'False',
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_53fef06093020',
			'label'             => esc_html__('Titlebar overlay opacity', 'k-artist'),
			'name'              => 'teacher_titlebar_overlay_opacity',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => esc_html__('Set your overlay opacity in titlebar', 'k-artist'),
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				0   => 0,
				1 	=> 1,
				2 	=> 2,
				3 	=> 3,
				4 	=> 4,
				5 	=> 5,
				6 	=> 6,
				7 	=> 7,
				8 	=> 8,
				9 	=> 9,
				10  => 10,
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_53fef07c93021',
			'label'             => esc_html__('Titlebar clipmask opacity', 'k-artist'),
			'name'              => 'teacher_titlebar_clipmask_opacity',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => esc_html__('Set your clipmask opacity in titlebar', 'k-artist'),
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				0   => 0,
				1 	=> 1,
				2 	=> 2,
				3 	=> 3,
				4 	=> 4,
				5 	=> 5,
				6 	=> 6,
				7 	=> 7,
				8 	=> 8,
				9 	=> 9,
				10  => 10,
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_53fef0eb93023',
			'label'             => esc_html__('Custom titlebar content', 'k-artist'),
			'name'              => 'teacher_titlebar_custom_content',
			'prefix'            => '',
			'type'              => 'wysiwyg',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b6093834',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
			'tabs'              => 'all',
			'toolbar'           => 'full',
			'media_upload'      => 1,
		),
		array (
			'key' => 'field_55b9e56e0bec7',
			'label' => esc_html__('Artist Information', 'k-artist'),
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'left',
		),
		array (
			'key' => 'field_55cc5d9f666df',
			'label' => esc_html__('Artist Data', 'k-artist'),
			'name' => 'teacher_artist_data',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Row',
			'sub_fields' => array (
				array (
					'key' => 'field_55cc5dc86626e1',
					'label' => esc_html__('Data 1', 'k-artist'),
					'name' => 'data_1',
					'type' => 'image',
					'instructions' => esc_html__('File Images Size < 1MB','k-artist'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'url',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array (
					'key' => 'field_55cc5dc8666e1',
					'label' => esc_html__('Data 2', 'k-artist'),
					'name' => 'data_2',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_55cc5dc86661',
					'label' => esc_html__('Data 3', 'k-artist'),
					'name' => 'data_3',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		),
		// array (
		// 	'key' => 'field_55b9e734102jw',
		// 	'label' => esc_html__('Number of events show on page', 'k-artist'),
		// 	'name' => 'teacher_related_events',
		// 	'type' => 'number',
		// 	'min'  => 0,
		// 	'max' => 100,
		// 	'step' => '',
		// 	'default_value' => 5,
		// ),
		array (
			'key' => 'field_55b9e7c5e3ae5',
			'label' => esc_html__('Facebook URL', 'k-artist'),
			'name' => 'teacher_facebook',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e7d6e3ae6',
			'label' => esc_html__('Instagram URL', 'k-artist'),
			'name' => 'teacher_instagram',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e7dfe3ae7',
			'label' => esc_html__('Email URL', 'k-artist'),
			'name' => 'teacher_email',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e7f4e3ae8',
			'label' => esc_html__('Twitter URL', 'k-artist'),
			'name' => 'teacher_twitter',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e802e3ae9',
			'label' => esc_html__('LinkedIn URL', 'k-artist'),
			'name' => 'teacher_linkedin',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e836e3aea',
			'label' => esc_html__('Tumblr URL', 'k-artist'),
			'name' => 'teacher_tumblr',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e83ee3aeb',
			'label' => esc_html__('Google Plus URL', 'k-artist'),
			'name' => 'teacher_google_plus',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e84de3aec',
			'label' => esc_html__('Pinterest URL', 'k-artist'),
			'name' => 'teacher_pinterest',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e885e3aed',
			'label' => esc_html__('YouTube URL', 'k-artist'),
			'name' => 'teacher_youtube',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e892e3aee',
			'label' => esc_html__('Flickr URL', 'k-artist'),
			'name' => 'teacher_flickr',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e8a8e3aef',
			'label' => esc_html__('GitHub URL', 'k-artist'),
			'name' => 'teacher_github',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e8b4e3af0',
			'label' => esc_html__('Dribbble URL', 'k-artist'),
			'name' => 'teacher_dribbble',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b9e8c0e3af1',
			'label' => esc_html__('VK URL', 'k-artist'),
			'name' => 'teacher_vk',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post-k-teacher',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

acf_add_local_field_group(
	array(
		'key' => 'group_55b9e57123456',
		'title' => 'Artist',
		'fields' => array (
			array(
				'key'               => 'field_53df40c5583c3',
				'label'             => esc_html__('Artist Layout', 'k-artist'),
				'name'              => '',
				'prefix'            => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'placement' 		=> 'left',
			),
			array(
				'key'               => 'field_53df40dd593c5',
				'label'             => esc_html__('Artist layout', 'k-artist'),
				'name'              => 'teacher_layout',
				'prefix'            => '',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'choices' => array(
					'default'       => 'Default',
					'right_sidebar' => 'Right Sidebar',
					'left_sidebar'  => 'Left Sidebar',
					'no_sidebar'    => 'No Sidebar',
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'k-teacher-setting',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	)
);
endif;

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page( array(
		'page_title' => 'K teacher setting',
		'menu_slug' => 'K-teacher-setting',
	));
}

function k_teacher_edit_admin_menus() {
    global $menu;
    global $submenu;

    $submenu['edit.php?post_type=post-k-teacher'][16][2] = 'admin.php?page=K-teacher-setting';

    remove_menu_page('K-teacher-setting'); // Remove the Tools Menu
}
add_action( 'admin_menu', 'k_teacher_edit_admin_menus', 99 );
