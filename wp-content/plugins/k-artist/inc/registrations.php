<?php
// Include register factory
include_once dirname( __FILE__) . '/class.register_factory.php';

if( empty($k2t_register) || !$k2t_register instanceof K2T_Teacher_Register ){
	$k2t_register = new K2T_Teacher_Register();
}
$slug = ( get_option( 'k2t_Teacher_slug' ) != '' ) ?  get_option( 'k2t_Teacher_slug' ) : esc_html__( 'k-artist', 'k-artist' );
$slug_cat = ( get_option( 'k2t_tag_category_slug' ) != '' ) ?  get_option( 'k2t_tag_category_slug' ) : esc_html__( 'k-tag-category', 'k-artist' );
// Register teacher post type
$k2t_register->register_post_type(array(
	array(
		'post-type' => 'post-k-teacher',
		'args' => array(
			'labels' => array(
				'name' => esc_html__('K-Artist', 'k-artist'),  
				'singular_name' => esc_html__('K-Artist', 'k-artist'),  
				'add_new' => esc_html__('Add New Artist', 'k-artist'),  
				'add_new_item' => esc_html__('Add New Artist', 'k-artist'),  
				'edit_item' => esc_html__('Edit Artist', 'k-artist'),  
				'new_item' => esc_html__('New Artist', 'k-artist'),  
				'view_item' => esc_html__('View Artist', 'k-artist'),  
				'all_items' => esc_html__('All Artist', 'k-artist'),
				'search_items' => esc_html__('Search Artist', 'k-artist'),  
				'not_found' =>  esc_html__('No Artist found', 'k-artist'),  
				'not_found_in_trash' => esc_html__('No Artist found in Trash', 'k-artist'),  
				'parent_item_colon' => ''
			),
			'rewrite' => array( 'slug' => ( !empty($smof_data['teacher-slug']) ? $smof_data['teacher-slug'] : 'k-artist'), 'with_front' => FALSE )
		)
	)
));
// Register Teacher category taxonomy
$k2t_register->register_taxonomy(array(
	array(
		'taxonomy' => 'k-teacher-category',
		'args' => array(
			'labels' => array(
				'name'                => _x( 'Artist Categories', 'taxonomy general name','k-artist'),
				'singular_name'       => _x( 'K-Artist Category', 'taxonomy singular name','k-artist'),
				'search_items'        => esc_html__( 'Search K-Artist Categories','k-artist'),
				'all_items'           => esc_html__( 'All K-Artist Categories','k-artist'),
				'parent_item'         => esc_html__( 'Parent K-Artist Category','k-artist'),
				'parent_item_colon'   => esc_html__( 'Parent K-Artist Category:','k-artist'),
				'edit_item'           => esc_html__( 'Edit K-Artist Category','k-artist'), 
				'update_item'         => esc_html__( 'Update K-Artist Category','k-artist'),
				'add_new_item'        => esc_html__( 'Add New K-Artist Category','k-artist'),
				'new_item_name'       => esc_html__( 'New K-Artist Category Name','k-artist'),
				'menu_name'           => esc_html__( 'K-Artist Categories','k-artist')
			),
			'rewrite' => array('slug' => $slug_cat ),
			'post-type' => 'post-k-teacher'
		)
	),

	// Settings

	// array(
	// 	'taxonomy' => 'k-teacher-setting',
	// 	'args' => array(
	// 		'labels' => array(
	// 			'name'                => _x( 'Artist Setting', 'taxonomy general name','k-artist'),
	// 			'singular_name'       => _x( 'K-Artist Setting', 'taxonomy singular name','k-artist'),
	// 			'search_items'        => esc_html__( 'Search K-Artist Setting','k-artist'),
	// 			'all_items'           => esc_html__( 'All K-Artist Setting','k-artist'),
	// 			'parent_item'         => esc_html__( 'Parent K-Artist Setting','k-artist'),
	// 			'parent_item_colon'   => esc_html__( 'Parent K-Artist Setting:','k-artist'),
	// 			'edit_item'           => esc_html__( 'Edit K-Artist Setting','k-artist'),
	// 			'update_item'         => esc_html__( 'Update K-Artist Setting','k-artist'),
	// 			'add_new_item'        => esc_html__( 'Add New K-Artist Setting','k-artist'),
	// 			'new_item_name'       => esc_html__( 'New K-Artist Setting Name','k-artist'),
	// 			'menu_name'           => esc_html__( 'K-Artist Setting','k-artist')
	// 		),
	// 		'post-type' => 'post-k-teacher'
	// 	)
	// ),
));

