<?php

if( function_exists( 'register_field_group' ) ):

	register_field_group( array(
		'key' => 'group_52227bedfegdasd',
		'title' => 'k_teacher Setting',

		// k_teacher SETTIG

		'fields' => array(

			// k_teacher TAXONOMY

			array(
				'key'             => 'field_52227be6ddfwa',
				'label'           => 'k_teacher taxonomy',
				'type'            => 'tab',
				'instructions'    => '',
				'placement' 	  => 'top',
			),
			array(
				'key'     => 'field_52227bekmj56',
				'label'   => 'k_teacher Single slug',
				'name'    => 'k_teacher_slug',
				'type'    => 'text',
			),
			array(
				'key'     => 'field_52227b2d4563',
				'label'   => 'k_teacher Category slug',
				'name'    => 'k_teacher_cat_slug',
				'type'    => 'text',
			),
			array(
				'key'              => 'field_52227b2d45ss',
				'label'            => 'k_teacher layout',
				'name'             => 'k_teacher_layout',
				'type' 			   => 'select',
				'choices' 		   => array(
					'no_sidebar'   => 'No Sidebar',
					'left_sidebar' => 'Left Sidebar',
					'right_sidebar'=> 'Right Sidebar',
				),
			),
			array(
				'key'   => 'field_5222892f9bc5f',
				'label' => 'Custom sidebar',
				'name'  => 'k_teacher_custom_sidebar',
				'type'  => 'text',
			),
			array(
				'key'   => 'field_5222892kdmjnd',
				'label' => 'Number teacher display',
				'name'  => 'k_teacher_cat_litmit',
				'type'  => 'text',
			),
			array(
				'key'     => 'field_52227bed23536',
				'label'   => 'Style',
				'name' 	  => 'k_teacher_style',
				'type'    => 'select',
				'choices' => array(
					'teacher-grid-style' 	  => 'Grid',
					'teacher-listing-style'   => 'Listing',
				),
			),
			array(
				'key' 		=> 'field_52227bedd2de',
				'label' 	=> 'Column :',
				'name' 		=> 'k_teacher_column',
				'type' 		=> 'select',
				'choices' 	=> array(
					'2' 	=> '2 Column',
					'3' 	=> '3 Column',
					'4' 	=> '4 Column',
				),
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52227bed23536',
							'operator' => '==',
							'value'    => 'teacher-grid-style',
						),
					),
				),
			),

			// k_teacher TITLEBAR

			array(
				'key' => 'field_52339f231D456',
				'label' => 'k_teacher Category title bar',
				'type' => 'tab',
			),
			array(
				'key'               => 'field_52339fDKE216',
				'label'             => 'Show/Hide Title Bar',
				'name'              => 'k_teacher_display_titlebar',
				'type'              => 'select',
				'instructions'      => 'Only support k_teacher layout "Full Width" and "No Sidebar"',
				'choices' => array(
					'show'  => 'Show',
					'hided' => 'Hide',
				),
			),
			array(
				'key'               => 'field_522e57d394EK2',
				'label'             => 'k_teacher titlebar font size',
				'name'              => 'k_teacher_titlebar_font_size',
				'type'              => 'text',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_53fftww39DJGNW',
				'label'             => 'k_teacher title bar color',
				'name'              => 'k_teacher_titlebar_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_53feec3KDME203',
				'label'             => 'Padding top',
				'name'              => 'k_teacher_pading_top',
				'type'              => 'text',
				'instructions'      => 'Unit: px (Ex: 10px)',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					)
				),
			),
			array(
				'key'               => 'field_53feec4KM3495',
				'label'             => 'Padding bottom',
				'name'              => 'k_teacher_pading_bottom',
				'type'              => 'text',
				'instructions'      => 'Unit: px (Ex: 10px)',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					)
				),
			),
			array(
				'key'               => 'field_53feeceWKDCV8',
				'label'             => 'Background color',
				'name'              => 'k_teacher_background_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_53feecb32SDF',
				'label'             => 'Background image',
				'name'              => 'k_teacher_background_image',
				'type'              => 'image',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'return_format' 	=> 'url',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_53feedDKDMW9',
				'label'             => 'Background position',
				'name'              => 'k_teacher_background_position',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					''      => 'None',
					'left top'      => 'Left Top',
					'left center'   => 'Left Center',
					'left bottom'   => 'Left Bottom',
					'right top'     => 'Right Top',
					'right center'  => 'Right Center',
					'right bottom'  => 'Right Bottom',
					'center top'    => 'Center top',
					'center center' => 'Center Center',
					'center bottom' => 'Center Bottom',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_53fuida3JFCM5',
				'label'             => 'Background size',
				'name'              => 'k_teacher_background_size',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					''		  => 'None',
					'inherit' => 'Inherit',
					'cover'   => 'Cover',
					'contain' => 'Contain',
					'full'    => '100%',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_52rguda215DDF',
				'label'             => 'Background repeat',
				'name'              => 'k_teacher_background_repeat',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					''		  	=> 'None',
					'no-repeat' => 'No Repeat',
					'repeat'    => 'Repeat',
					'repeat-x'  => 'Repeat X',
					'repeat-y'  => 'Repeat Y',
				),
				'default_value' => 'no-repeat',
			),
			array(
				'key'               => 'field_523363434JDCF',
				'label'             => 'Background parallax',
				'name'              => 'k_teacher_background_parallax',
				'type'              => 'select',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					'1' => 'True',
					'0' => 'False',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_53fef06232654',
				'label'             => 'Titlebar overlay opacity',
				'name'              => 'k_teacher_titlebar_overlay_opacity',
				'type'              => 'select',
				'instructions'      => 'Set your overlay opacity in titlebar',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					0   => 0,
					1 	=> 1,
					2 	=> 2,
					3 	=> 3,
					4 	=> 4,
					5 	=> 5,
					6 	=> 6,
					7 	=> 7,
					8 	=> 8,
					9 	=> 9,
					10  => 10,
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_522fftww3sjwmns1',
				'label'             => 'title bar overlay color',
				'name'              => 'k_teacher_overlay_background_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_53fef07K334524',
				'label'             => 'Titlebar clipmask opacity',
				'name'              => 'k_teacher_titlebar_clipmask_opacity',
				'type'              => 'select',
				'instructions'      => 'Set your clipmask opacity in titlebar',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					0   => 0,
					1 	=> 1,
					2 	=> 2,
					3 	=> 3,
					4 	=> 4,
					5 	=> 5,
					6 	=> 6,
					7 	=> 7,
					8 	=> 8,
					9 	=> 9,
					10  => 10,
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_522feecejmsnes',
				'label'             => 'Titlebar mask pattern',
				'name'              => 'k_teacher_clipmask_background_img',
				'type'              => 'image',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'return_format' 	=> 'url',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_53fef0Dlmknb',
				'label'             => 'Custom titlebar content',
				'name'              => 'k_teacher_titlebar_custom_content',
				'type'              => 'wysiwyg',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52339fDKE216',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'tabs'              => 'all',
				'toolbar'           => 'full',
				'media_upload'      => 1,
			),

		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'K-teacher-setting',
				),
			),
		),

		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

endif;