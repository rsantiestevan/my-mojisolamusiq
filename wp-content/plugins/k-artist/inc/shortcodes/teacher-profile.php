<?php

if( !function_exists('k_teacher_profile_shortcode') ){
	function k_teacher_profile_shortcode($atts, $content = null){
		extract( shortcode_atts( array(
			'style' 				=> 'dark-style',
			'id'					=> '',
			'bio'					=> 'show',
			'skill'					=> 'show',
			'certi'					=> 'show',
		), $atts));

		$args = array(
			'post_type'			=> 'post-k-teacher',
			'p'					=> $id,
		);

		$the_query = new WP_Query( $args );

		if ( $the_query->have_posts() ) :
			while( $the_query->have_posts() ) :	$the_query->the_post();

				// Get metadata of teacher in single

				$single_pre = 'teacher_';

				$arr_teacher_meta_val  	= array();
				$arr_teacher_meta 		= array(
					// Layout
					'layout'						=> 'no_sidebar',
					'custom_sidebar' 				=> '',

					// Location
					'related_courses'				=> '',
					'position'						=> '',
					'skill'							=> '',
					'circle_label'					=> '',
					'skill_circle'					=> '',
					'certifications'				=> '',
					'line_label'					=> '',
					'data_1_lb'						=> '',
					'data_2_lb'						=> '',
					'graph'							=> '',
					'facebook'						=> '',
					'instagram'						=> '',
					'email'							=> '',
					'twitter'						=> '',
					'linkedin'						=> '',
					'tumblr'						=> '',
					'google_plus'					=> '',
					'pinterest'						=> '',
					'youtube'						=> '',
					'flickr'						=> '',
					'github'						=> '',
					'dribbble'						=> '',
					'vk'							=> '',
				);

				foreach ( $arr_teacher_meta as $meta => $val ) {
					if ( function_exists( 'get_field' ) ) {
						if ( get_field( $single_pre . $meta, get_the_ID() ) ) {
							$arr_teacher_meta_val[$meta] = get_field( $single_pre . $meta, get_the_ID() );
						}
					}
				}

				extract( shortcode_atts( $arr_teacher_meta, $arr_teacher_meta_val ) ); 

				$categories = get_the_terms( get_the_ID(), 'k-teacher-category' );?>

				<div class="single-post-k-teacher k-teacher-profile-shortcode <?php echo esc_attr( $style);?> ">

					<div class="k2t-row teacher-top-info">
						<div class="teacher-entry-thumb col-6">
							<div class="teacher-thumb" style="background-image: url('<?php echo the_post_thumbnail_url('full');?>');">
							</div>
						</div>
						<div class="teacher-entry-meta	col-6">

						 	<!--teacher top meta-->

							<div class="teacher-top-meta">

								<!--teacher title-->

								<div class="teacher-title">
									<h2><a href="<?php echo get_permalink();?>"> <?php the_title(); ?></a></h2>
								</div>

								<!--teacher position-->

								<div class="teacher-position">
									<?php
										if (is_array($categories)) {
											foreach ( $categories as $key => $category ) {
												if ( $key == count( $categories ) - 1 ) {
													echo '<a href="'. esc_url( get_term_link( $category->term_id, 'k-course-category' ) ) .'" title="'. esc_attr( $category->name ) .'">'. esc_html( $category->name ) .'</a>';
												} else {
													echo '<a href="'. esc_url( get_term_link( $category->term_id, 'k-course-category' ) ) .'" title="'. esc_attr( $category->name ) .'">'. esc_html( $category->name ) .'</a>, ';
												}
											}
										}
									?>
								</div>

								<!--teacher social-->

								<div class="teacher-social k2t-social-share social">
									<ul class="">
					                	<?php if(!empty($facebook) || !empty($facebook_text)) : ?>
					                		<li>
					                			<a target='_blank' href="<?php echo esc_url($facebook);?>">
					                				<i class='fa fa-facebook'></i>
					                			</a>
					                		</li>
					               		<?php endif; ?>
					               		<?php if(!empty($instagram) || !empty($instagram_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($instagram);?>">
						                			<i class='fa fa-instagram'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($email) || !empty($email_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($email);?>">
						                			<i class="fa fa-envelope"></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($twitter) || !empty($twitter_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($twitter);?>">
						                			<i class='fa fa-twitter'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($linkedin) || !empty($linkedin_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($linkedin);?>">
						                			<i class='fa fa-linkedin'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($tumblr) || !empty($tumblr_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($tumblr);?>">
						                			<i class='fa fa-tumblr'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($google_plus) || !empty($google_plus_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($google_plus);?>">
						                			<i class='fa fa-google-plus'>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($pinterest) || !empty($pinterest_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($pinterest);?>">
						                			<i class='fa fa-pinterest'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($youtube) || !empty($youtube_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($youtube);?>">
						                			<i class='fa fa-youtube'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($flickr) || !empty($flickr_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($flickr);?>">
						                			<i class='fa fa-flickr'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($github) || !empty($github_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($github);?>">
						                			<i class='fa fa-github'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($dribbble) || !empty($dribbble_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($dribbble);?>">
						                			<i class='fa fa-dribbble'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					               		<?php if(!empty($vk) || !empty($vk_text)) : ?>
						               		<li>
						                		<a target='_blank' href="<?php echo esc_url($vk);?>">
						                			<i class='fa fa-vk'></i>
						                		</a>
						                	</li>
					               		<?php endif; ?>
					                </ul>
								</div>

							</div> <!--teacher top meta-->

							<!--teacher content-->

							<div class="teacher-content">
								<p class="top-part-label"><i class="zmdi zmdi-account"></i><span><?php esc_html_e( 'Bio', 'k-artist' )?></span></p>
								<div class="content">
									<?php the_content();?>
								</div>
							</div><!--teacher content-->

							<!--teacher skill-->

							<div class="teacher-skill">
								<p class="top-part-label"><i class="zmdi zmdi-settings"></i><span><?php esc_html_e( 'Skill', 'k-artist' )?></span></p>
								<?php
								if ( !empty($skill) && is_array( $skill ) ) :
									foreach ($skill as $key => $value) :
										$progress ='[progress percent="' . $value['percentage'] . '" title="' . $value['label'] . '" height="10"]';
										echo do_shortcode($progress);
									endforeach;
								endif;

								?>

							</div><!--teacher skill-->

							<!--teacher certification-->

							<div class="teacher-certificate">
								<p class="top-part-label"><i class="zmdi zmdi-settings"></i><span><?php esc_html_e( 'CERTIFICATIONS', 'k-artist' )?></span></p>
								<?php
								if( ! empty( $certifications ) ) :
									echo '<ul>';
									foreach ($certifications as $key => $value) :
										echo '<li>';
										echo $value['label'];
										echo '</li>';
									endforeach;
									echo '</ul>';
								endif;
								?>
							</div>

						</div><!--teacher-entry-meta-->

					</div><!--k2t-row-->

				</div><!--single-post-k-teacher-->

				<?php 

			endwhile;

		endif;

		wp_reset_postdata();

		return $html;
	}

	add_shortcode('k_teacher_profile', 'k_teacher_profile_shortcode');
}