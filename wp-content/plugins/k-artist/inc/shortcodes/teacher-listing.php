<?php

if( !function_exists('k_teacher_listing_shortcode') ){
	function k_teacher_listing_shortcode($atts, $content = null){
		extract( shortcode_atts( array(
			'style' 				=> 'teacher-grid-style',
			'column' 		        => '2',
			'teacher_per_page'      => '12',
			'cat'					=> '',
			'navigation'			=> '',
			'items_desktop'			=> '3',
			'items_tablet'			=> '2',
			'items_mobile'			=> '1',
			'filter'				=> 'show',
			'show_hide_pagination'	=> 'show',
			'page_nav'				=> '',
		), $atts));

		$html = k2t_get_template_part( 'teacher', 'listing', array(
            'style' 				=> $style,
            'column' 		        => $column,
            'teacher_per_page'      => $teacher_per_page,
			'cat'					=> $cat,
			'filter'				=> $filter,
			'show_hide_pagination'	=> $show_hide_pagination,
			'page_nav'				=> $page_nav,
			'navigation'			=> $navigation,
			'items_desktop'			=> $items_desktop,
			'items_tablet'			=> $items_tablet,
			'items_mobile'			=> $items_mobile,
            )
        );

		wp_reset_postdata();

		return $html;
	}

	add_shortcode('k_teacher_listing', 'k_teacher_listing_shortcode');
}