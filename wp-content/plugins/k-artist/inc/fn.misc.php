<?php
/* ------------------------------------------------------------------------------ */



/**
 * Get related post
 *
 * @link http://wordpress.org/support/topic/custom-query-related-posts-by-common-tag-amount
 * @link http://pastebin.com/NnDzdSLd
 */
if ( ! function_exists( 'get_related_tag_posts_ids' ) ) {
	function get_related_tag_posts_ids( $post_id, $number = 5, $taxonomy = 'post_tag', $post_type = 'post' ) {

		$related_ids = false;

		$post_ids = array();
		// get tag ids belonging to $post_id
		$tag_ids = wp_get_post_terms( $post_id, $taxonomy, array( 'fields' => 'ids' ) );
		if ( $tag_ids ) {
			// get all posts that have the same tags
			$tag_posts = get_posts(
				array(
					'post_type'   => $post_type,
					'posts_per_page' => -1, // return all posts
					'no_found_rows'  => true, // no need for pagination
					'fields'         => 'ids', // only return ids
					'post__not_in'   => array( $post_id ), // exclude $post_id from results
					'tax_query'      => array(
						array(
							'taxonomy' => $taxonomy,
							'field'    => 'id',
							'terms'    => $tag_ids,
							'operator' => 'IN'
						)
					)
				)
			);

			// loop through posts with the same tags
			if ( $tag_posts ) {
				$score = array();
				$i = 0;
				foreach ( $tag_posts as $tag_post ) {
					// get tags for related post
					$terms = wp_get_post_terms( $tag_post, $taxonomy, array( 'fields' => 'ids' ) );
					$total_score = 0;

					foreach ( $terms as $term ) {
						if ( in_array( $term, $tag_ids ) ) {
							++$total_score;
						}
					}

					if ( $total_score > 0 ) {
						$score[$i]['ID'] = $tag_post;
						// add number $i for sorting
						$score[$i]['score'] = array( $total_score, $i );
					}
					++$i;
				}

				// sort the related posts from high score to low score
				uasort( $score, 'stevecadey_sort_tag_score' );
				// get sorted related post ids
				$related_ids = wp_list_pluck( $score, 'ID' );
				// limit ids
				$related_ids = array_slice( $related_ids, 0, (int) $number );
			}
		}
		return $related_ids;
	}
}


/* ------------------------------------------------------------------------------ */
/* Pagination
/* ------------------------------------------------------------------------------ */
if ( !function_exists('k2t_pagination') ) {
 function k2t_pagination( $custom_query = false ){
  global $wp_query;

  if ( !$custom_query ) $custom_query = $wp_query;

  $big = 999999999; // need an unlikely integer
  echo '<div class="k2t-pagination">';
   echo paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $custom_query->max_num_pages,
    'type'   => 'list',
    'prev_text'    => sprintf( __('%s Previous','wi'), '<i class="icon-angle-left"></i>' ),
    'next_text'    => sprintf( __('Next %s','wi'), '<i class="icon-angle-right"></i>' ),
   ) );
  echo '<div class="clearfix"></div></div>';
 }
}

/* ------------------------------------------------------------------------------ */
// Get a real label
if( !function_exists('k2t_get_object_label') ){
	function check_matches($matches){ return ' '.ucfirst($matches[1]); }
	function k2t_get_object_label($object_name){
		$object_name = preg_replace_callback('/[^a-z0-9]+(\w{1,})/i', check_matches($matches), strtolower(trim($object_name)));
		return ucfirst($object_name);
	}
}

// Get all values of an associated array by a specified key
function k2t_teacher_array_values_deep($elem, $field = null){
	$holder = array();
	k2t_teacher_array_values_deep_($elem, $field, null, $holder);
	return $holder;
}

function k2t_teacher_array_values_deep_($elem, $field, $pre_field, &$holder){
	if( !is_array($elem) && !is_object($elem) ){
		if( !$field || ($field && $pre_field === $field) ){
			$holder[] = $elem;
		}
	}
	else{
		foreach($elem as $k => $e){
			k2t_teacher_array_values_deep_($e, $field, $k, $holder);
		}
	}

	return $holder;
}


/**
 * Function k2t_get_template_part
 * Like wordpress function get_template_part with override templates file.
 * All file in k-teacher/templates can be override in themes/theme_name/k-teacher/
 *
 */
if (!function_exists('k2t_get_template_part')) {
	function k2t_get_template_part($slug = null, $name = null, array $params = array())
	{
		global $wp_query;
		$template_slug = K_TEACHER_PLG_DIRECTORY . '/' . $slug;
		do_action("get_template_part_{$template_slug}", $template_slug, $name);

		$templates = array();
		if (isset($name)){
			$templates[] = "{$template_slug}-{$name}.php";
		}

		$templates[] = "{$template_slug}.php";

		$_template_file = locate_template($templates, false, false);

		if (is_array($wp_query->query_vars)) {
			extract($wp_query->query_vars, EXTR_SKIP);
		}
		extract($params, EXTR_SKIP);

		ob_start();
		if (file_exists($_template_file)) {
			include($_template_file);
		} else  {
			$templates = array();
			if (isset($name)){
				$templates[] = "{$slug}-{$name}.php";
			}
			$templates[] = "{$slug}.php";

			if((file_exists(K_TEACHER_PLG_PATH . '/templates/' . $templates[0])))
				include(K_TEACHER_PLG_PATH . '/templates/' . $templates[0]);
		}
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}


if(!function_exists('k_teacher_get_events')){
	function k_teacher_get_events($teacherId){
		$event_args = array(
			'post_type'			=> 'post-k-event',
			'posts_per_page'	=> -1,
			'orderby'			=> 'date',
			'order'				=> 'DESC'
		);

		$eventsJoined = array();

		$events = get_posts($event_args);

		if(is_array($events) && count($events) > 0){
			foreach($events as $event){
				$teachers = get_field('event_teacher', $event->ID);
				if( is_array($teachers) &&  count($teachers) > 0){
					foreach($teachers as $teacher){
						if($teacher->ID == $teacherId){
							$eventsJoined[] = $event;
						}
					}
				}
			}
		}
		return $eventsJoined;
	}
}

if( !function_exists('k_teacher_get_courses') ){
	function k_teacher_get_courses( $teacherId, $course_limit ){

		// args
		$args = array(
			'post_type'	=> 'post-k-course',
			'meta_query' => array(
				'relation'		=> 'OR',
				array(
					'key' => 'mon_%_w_teacher',
					'value' => '"' . $teacherId . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'tue_%_w_teacher',
					'value' => '"' . $teacherId . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'wed_%_w_teacher',
					'value' => '"' . $teacherId . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'thu_%_w_teacher',
					'value' => '"' . $teacherId . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'fri_%_w_teacher',
					'value' => '"' . $teacherId . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'sat_%_w_teacher',
					'value' => '"' . $teacherId . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'san_%_w_teacher',
					'value' => '"' . $teacherId . '"',
					'compare' => 'LIKE'
				)
			)
		);

		// get results
		$the_query = new WP_Query( $args );

		$week =	array(
					'mon'		=> '',
					'tue'		=> '',
					'wed'		=> '',
					'thu'		=> '',
					'fri'		=> '',
					'sat'		=> '',
					'sun'		=> '',
				);

		// set up course for save data of course

		$courses = array();

		foreach ($week as $key => $value) {
			$courses[$key] = array();
		}

		$courses['wait_time'] = array(); // wait_time = start_day - today;

		if ( $the_query->have_posts() ) :

			while( $the_query->have_posts() ) :	$the_query->the_post();

				$id = get_the_ID();

				//get date

				$date = array(
					'course_start_date' 	=> '',
					'course_end_date' 		=> '',
				);

				if ( function_exists('stevecadey_get_acf_field') ) :
					$date = stevecadey_get_acf_field( $date, $id );
					$week = stevecadey_get_acf_field( $week, $id );
				endif;

				// Get date duration

				$start_date = strtotime( str_replace( '/', '-', $date['course_start_date'] ) );

				$end_date 	= strtotime( str_replace( '/', '-', $date['course_end_date'] ) );

				$duration 	= round( ( $end_date - $start_date ) / 86400 ) ;

				$re_time  	= round( ( $end_date - time() ) / 86400 );



				if ( $re_time  >=  0 && $duration >= 0 ) :

					$wait_time = array();

					$wait_time['wait'] = round( ( $start_date - time() ) / 86400 );

					$wait_time['duration'] = $duration;

					$wait_time['re_time'] = $re_time;

					$courses['wait_time'][] = $wait_time;

					foreach ( $week as $day => $value ) :

						if ( is_array( $value ) ) :

							foreach ( $value as $k => $row ) :

								if ( is_array( $row['w_teacher'] ) && in_array( $teacherId, $row['w_teacher'] ) ) :

									$row['class'] = get_the_title();

									$row['duration'] = $duration;

									$row['re_time'] = $re_time;

									$row['wait_time'] = $wait_time;

									array_splice( $row, 1, 1 );

									$courses[$day][] = $row; // get schedule of day

								endif;

							endforeach;

						endif;

					endforeach;

				endif; // Remaining time of course

			endwhile;

		endif;

		// Sort course schedule

		foreach ( $courses as $key => $value ) {
			if ( $key == 'wait_time' ) :
				usort( $value, 'stevecadey_compare_day');
				$courses[$key] =  $value;
			else :
				usort( $value, 'stevecadey_compare_day');
				$courses[$key] =  $value;
			endif;
		};

		// Start out put

		$num_related  = $course_limit; // show the related course

		if ( $courses['wait_time'][0]['wait'] <= 0 )
			$next_related = 0; // start from today;
		else
			$next_related = $courses['wait_time']['wait'];

		stevecadey_output_related_course( '0', $next_related, $num_related, $courses);

		wp_reset_query();  // Restore global post data stomped by the_post().

	}
}

// Loop get related course

function stevecadey_output_related_course( $total_next, $next_related, $num_related ='10', &$courses = array() ) {

	// get next day to jump closest course;

	$total_next += $next_related;

	$the_day = strtotime( '+' . $total_next . ' days ' . date('d-m-Y', time()) );
	$jday 	 = date('w', $the_day);				// get julian day
	$jday 	 = stevecadey_get_jday_key($jday);	// get key

	// html output

	foreach ( $courses[$jday] as $key => $value ) {

		if ( $value['wait_time']['wait'] <= 0 && $num_related > 0) :

			$time = strtotime( $value['time'] );

			echo '<div class="course-item">' .
					'<h4 class="course-title">' . $value['class'] . '</h4>' .
					'<p class="course-datetime">' .
						'<span class="course-date"><i class="zmdi zmdi-calendar-note"></i>' . date( 'M d,Y', $the_day ) . '</span>' .
						'<span class="course-time"><i class="zmdi zmdi-time"></i>' . date('g:i A', $time ) . '</span>' .
					'<p>' .
				 '</div>';
			$num_related--;
		endif;
	}

	if( count( $courses['wait_time'] ) < 1 ) return false; //  break if done all schedule

	if ( $courses['wait_time'][0]['wait'] <= 0 )
		$next_related = 1;
	else
		$next_related = $courses['wait_time'][0]['wait'];

	stevecadey_update_related_course( $next_related , $courses );	// update course index

	if ( $num_related <= 0 ) return false; // break

	stevecadey_output_related_course( $total_next, $next_related, $num_related, $courses );
}

// update related course array

function stevecadey_update_related_course( $next_day, &$courses ) {

	foreach ( $courses as $k => &$v ) {

		if ( $k != 'wait_time' ) :		// data of week
			$index = 0 ;
			foreach ($v as $key => &$value) :

				$value['wait_time']['wait']		 = $value['wait_time']['wait'] - $next_day;
				$value['wait_time']['re_time']   = $value['wait_time']['re_time'] - $next_day;

				if ( $value['wait_time']['re_time'] < 0 ) :
					array_splice( $v, $index, 1 );
				endif;

				$index++;

			endforeach;
		else :					// data of wait_time for get flag of next day
			$index = 0;
			foreach ($v as $key => &$value) {

				$value['wait'] = $value['wait'] - $next_day;
				$value['re_time'] = $value['re_time'] - $next_day;

				if ( $value['re_time'] < 0 ) :
					array_splice( $v, $index, 1 );
				endif;
				$index++;
			}
		endif;
	}
}


// get key of jday

function stevecadey_get_jday_key( $int ){
	switch ($int) {
		case '1':
			return 'mon';
			break;
		case '2':
			return 'tue';
			break;
		case '3':
			return 'wed';
			break;
		case '4':
			return 'thu';
			break;
		case '5':
			return 'fri';
			break;
		case '6':
			return 'sat';
			break;
		default:
			return 'sun';
			break;
	}
}

// custom filter to replace '=' with 'LIKE'

function stevecadey_my_posts_where( $where )
{
	$week =	array(
		'mon'		=> '',
		'tue'		=> '',
		'wed'		=> '',
		'thu'		=> '',
		'fri'		=> '',
		'sat'		=> '',
		'sun'		=> '',
	);

	foreach ($week as $key => $value) {
		$where = str_replace("meta_key = '".$key."_%_w_teacher'", "meta_key LIKE '".$key."_%_w_teacher'", $where);
	}

	return $where;
}

add_filter('posts_where', 'stevecadey_my_posts_where');


// function compare time for courses

if ( ! function_exists( 'stevecadey_compare_time' ) ) :

	function stevecadey_compare_time( $row1, $row2 ){

		return strtotime( $row1['time'] ) - strtotime( $row2['time'] );

	}

endif;

// function compare days for courses

if ( ! function_exists( 'stevecadey_compare_day' ) ) :

	function stevecadey_compare_day( $row1, $row2 ){

		return  $row1['wait'] - $row2['wait'];

	}

endif;

// Set query limit

function set_posts_per_page_for_k_teacher( $query ) {
	
	$cat = is_tax('k-teacher-category');
	
  	if ( !is_admin() && $query->is_main_query() && $cat ) {
  		if ( function_exists('get_field') ) $limit = get_field('k_teacher_cat_limit','option');
  		if ( is_numeric($limit) ) $query->set( 'posts_per_page', '1' );
  	}
}
add_filter( 'pre_get_posts', 'set_posts_per_page_for_k_teacher' );
