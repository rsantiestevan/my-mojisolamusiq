(function($) {

	function isotopeTeacher(target){
		if(jQuery('.t-grid').length){
			var $container = $('.t-grid');
			var $this = $(this);
			$this.imagesLoaded( function() {
				$container.isotope({
					itemSelector: '.post-k-teacher',
					layoutMode: 'fitRows',
					filter: target ,
				});
			});
		}
	}

	$(function() {

		$('.teacher-listing.owl-carousel').owlCarousel({
		    loop: true,
		    margin: 30,
		    autoplay: true,
    		autoplayTimeout: 3000,
    		autoplayHoverPause: true,
		    nav: true,
		    dots: false,
		    responsive: {
				320: {
					items: $('.teacher-listing.owl-carousel').attr('data-mobile')
				},
				480: {
					items: $('.teacher-listing.owl-carousel').attr('data-mobile')
				},
				568: {
					items: $('.teacher-listing.owl-carousel').attr('data-tablet')
				},
				600: {
					items: $('.teacher-listing.owl-carousel').attr('data-tablet')
				},
				768: {
					items: $('.teacher-listing.owl-carousel').attr('data-tablet')
				},
				992: {
					items: $('.teacher-listing.owl-carousel').attr('data-items')
				},
				1200: {
					items: $('.teacher-listing.owl-carousel').attr('data-items')
				}
			},
		    navText: [
				'<i class="zmdi zmdi-chevron-left"></i>',
				'<i class="zmdi zmdi-chevron-right"></i>'
			],

		});
	});

	$(document).ready(function () {

		isotopeTeacher('*');

	});

	$(window).load(function(){

		// if ( $().mCustomScrollbar ) $(".teacher-entry-meta").mCustomScrollbar();

	});

})(jQuery);

