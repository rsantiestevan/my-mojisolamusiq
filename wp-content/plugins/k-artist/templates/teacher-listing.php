<?php
wp_enqueue_script( 'isotope-js');
wp_enqueue_script( 'teacher-js');
wp_enqueue_script( 'jquery-imagesloaded' );

//data
$single_pre 			= 'teacher_';

$arr_teacher_meta 		= array(
	// Location
	'avatar'						=> '',
	'skill'							=> '',
	'circle_label'					=> '',
	'facebook'						=> '',
	'instagram'						=> '',
	'email'							=> '',
	'twitter'						=> '',
	'linkedin'						=> '',
	'tumblr'						=> '',
	'google_plus'					=> '',
	'pinterest'						=> '',
	'youtube'						=> '',
	'flickr'						=> '',
	'github'						=> '',
	'dribbble'						=> '',
	'vk'							=> '',
);

$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );
$arr = array(
    'post_type' 		=> 'post-k-teacher',
    'posts_per_page' 	=> (int)$teacher_per_page,
    'order'				=> 'DESC',
    'post_status'		=> 'publish',
    'paged'				=> $paged,
    'orderby'			=> 'date',
);
if ( !empty( $params['cat'] ) ) :
    $arr['tax_query'] = array(
                            array(
                                'taxonomy' => 'k-teacher-category',
                                'field'    => 'id',
                                'terms'    => explode(',', $params['cat']),
                            )
                        );
endif;

$query = new WP_Query( $arr );

// filter

if( $style == 'teacher-carousel' ){ ?>
<div class="teacher-listing owl-carousel teacher-grid-style" data-items="<?php echo esc_attr($items_desktop); ?>" data-mobile="<?php echo esc_attr($items_mobile); ?>" data-tablet="<?php echo esc_attr($items_tablet); ?>" data-desktop="<?php echo $items_desktop; ?>" >
<?php } else { include ( 'teacher-filter-bar.php' ); ?>
<div class="teacher-listing t-grid teacher-grid-style <?php echo 'columns-' . esc_attr($column) ;?>">
<?php } ?>
    <?php

    if( count( $query->posts ) > 0 ):
        while( $query->have_posts() ) : $query->the_post();

    		$arr_teacher_meta_val  	= array();

    		foreach ( $arr_teacher_meta as $meta => $val ) {
				if ( function_exists( 'get_field' ) ) {
					if ( get_field( $single_pre . $meta, get_the_ID() ) ) {
						$arr_teacher_meta_val[$meta] = get_field( $single_pre . $meta, get_the_ID() );
					}
				}
			}
			extract( shortcode_atts( $arr_teacher_meta, $arr_teacher_meta_val ) );

			// Get categories

			$categories = get_the_terms( get_the_ID(), 'k-teacher-category' );



            ?>
            <article <?php post_class(); ?>>
            	<div class="wrap-inner">
	            	<!-- Thumnail -->
	            	<div class="artist-mask"></div>
	            	<div class="teacher-thumb" style="background-image: url('<?php echo the_post_thumbnail_url('thumb_400x800');?>');">
	            		<div class="grid-thumb">
	            			<?php
	                			if ( $avatar ) :
	                				echo wp_get_attachment_image($avatar,'thumb_130x130');
	                			endif;
	                		?>
	            		</div><!-- grid-thumb -->
	            	</div><!-- teacher-thumb -->

	            	<!-- teacher-Meta -->

	            	<div class="teacher-meta">

	            		<!-- Title  -->

	            		<div class="teacher-title">
	            			<h2>
	                			<a href="<?php echo get_permalink();?>">
	                				<?php the_title();?>
	                			</a>
	                		</h2>
	            		</div>

	            		<!-- teacher-content  -->

	            		<div class="teacher-main-content">

	            			<!--teacher content-->

							<div class="teacher-content">

								<div class="content">

									<div class="grid-excerpt">
										<?php
											$excerpt = get_the_excerpt();
											if ( !empty( $excerpt ) )
											 	echo wp_trim_words( get_the_excerpt(), 20 );
											else
												echo wp_trim_words( get_the_content(), 20 );
										?>
									</div>
								</div>
							</div><!--teacher content-->

	            		</div> <!-- teacher-content  -->



	            		<!--teacher social-->

						<div class="teacher-social k2t-social-share social">
							<ul class="">
			                	<?php if(!empty($facebook) || !empty($facebook_text)) : ?>
			                		<li>
			                			<a target='_blank' title='Facebook' href="<?php echo esc_url($facebook);?>">
			                				<i class='fa fa-facebook'></i>
			                			</a>
			                		</li>
			               		<?php endif; ?>
			               		<?php if(!empty($twitter) || !empty($twitter_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Twitter" href="<?php echo esc_url($twitter);?>">
				                			<i class='fa fa-twitter'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($instagram) || !empty($instagram_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Instagram" href="<?php echo esc_url($instagram);?>">
				                			<i class='fa fa-instagram'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($google_plus) || !empty($google_plus_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Google Plus" href="<?php echo esc_url($google_plus);?>">
				                			<i class='fa fa-google-plus'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>

			               		<?php if(!empty($email) || !empty($email_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Email" href="<?php echo esc_url($email);?>">
				                			<i class="fa fa-envelope"></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		
			               		<?php if(!empty($linkedin) || !empty($linkedin_text)) : ?>
				               		<li>
				                		<a target='_blank' title="LinkedIn" href="<?php echo esc_url($linkedin);?>">
				                			<i class='fa fa-linkedin'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($tumblr) || !empty($tumblr_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Tumblr" href="<?php echo esc_url($tumblr);?>">
				                			<i class='fa fa-tumblr'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		
			               		<?php if(!empty($pinterest) || !empty($pinterest_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Pinterest" href="<?php echo esc_url($pinterest);?>">
				                			<i class='fa fa-pinterest'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($youtube) || !empty($youtube_text)) : ?>
				               		<li>
				                		<a target='_blank' title="YouTube" href="<?php echo esc_url($youtube);?>">
				                			<i class='fa fa-youtube'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($flickr) || !empty($flickr_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Flickr" href="<?php echo esc_url($flickr);?>">
				                			<i class='fa fa-flickr'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($github) || !empty($github_text)) : ?>
				               		<li>
				                		<a target='_blank' title="GitHub" href="<?php echo esc_url($github);?>">
				                			<i class='fa fa-github'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($dribbble) || !empty($dribbble_text)) : ?>
				               		<li>
				                		<a target='_blank' title="Dribbble" href="<?php echo esc_url($dribbble);?>">
				                			<i class='fa fa-dribbble'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			               		<?php if(!empty($vk) || !empty($vk_text)) : ?>
				               		<li>
				                		<a target='_blank' title="VK" href="<?php echo esc_url($vk);?>">
				                			<i class='fa fa-vk'></i>
				                		</a>
				                	</li>
			               		<?php endif; ?>
			                </ul>
						</div>

	            	</div><!-- teacher-Meta -->
	            </div> <!-- wrap inner -->
            </article> <!--post-k-teacher -->
        <?php
        endwhile;
    endif; ?>
</div> <!-- teacher-listing -->
<?php

// navigation
if( $show_hide_pagination == 'show' )
include ( 'navigation.php');

