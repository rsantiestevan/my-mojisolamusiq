<?php
/**
 * The template display blog navigation.
 *
 * @package K Teacher
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Prepare variables
$max          = $query->max_num_pages;
$current_page = max( 1, get_query_var( 'paged' ) );
$big          = 99999;

// Get type of page navigation if necessary

if ( $max > 1 ) : ?>
		<div class="k2t-navigation">
			<?php
			echo '' . paginate_links(
					array(
						'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format'    => '?paged=%#%',
						'current'   => $current_page,
						'total'     => $max,
						'type'      => 'list',
						'prev_text' => 'Prev',
						'next_text' => 'Next',
					)
				) . ' ';
			?>
		</div>
<?php
endif;
