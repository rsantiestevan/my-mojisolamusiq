<?php
/**
 * The template for displaying Category pages
 */
get_header();


// Register variables
$classes 						= array();

// Get metadata of categories setting

$setting 		= array(
	// Layout
	'layout'				=> 'no_sidebar',
	'custom_sidebar' 		=> '',
	'cat_litmit'			=> '',
	'style'					=> 'teacher-listing-style',
	'column'				=> '1',
);

// get field

if ( function_exists('stevecadey_get_acf_field') ) {
	$setting = stevecadey_get_acf_field( $setting, 'option', false, 'k_teacher_' );
}

extract( $setting, EXTR_OVERWRITE );


$classes[] = str_replace('_', '-', $layout);

// Get current term

$query = get_queried_object();

// get col

$column = ( $style == 'teacher-listing-style' ) ? '1' : $column;

$teacher_listing = array(
						'style' 				=> $style,
						'column' 		        => $column,
						'teacher_per_page'      => $cat_litmit,
						'cat'					=> $query->term_id,
						'filter'				=> '',
						'page_nav'				=> 'show',
					);

// create shortcode

$shortcode = '[k_teacher_listing ';

foreach ($teacher_listing as $key => $value) {
	$shortcode .= $key . '="' . $value . '" ';
}

$shortcode .= ']';

?>
<div class="k2t-content <?php echo esc_attr( implode( ' ', $classes ) ) ?>" >

	<div class="container k2t-wrap">

		<!-- Main -->
		<main class="k2t-main page">

			<?php echo do_shortcode($shortcode);?>

		</main>

		<!-- Sidebar -->
		<?php
			if ( $layout != 'no_sidebar' ) {
				if ( is_active_sidebar( $custom_sidebar ) ) :
					get_sidebar( $custom_sidebar );
				else:
					get_sidebar();
				endif;
			}
		?>
	</div>
</div>

<?php get_footer();
