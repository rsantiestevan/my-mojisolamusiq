<?php

get_header();

// Register variables

$classes 						= array();
$single_pre 					= 'teacher_';

// Get metadata of teacher in single
$arr_teacher_meta_val  	= array();
$arr_teacher_meta 		= array(
	// Layout
	'layout'						=> 'no_sidebar',
	//'custom_sidebar' 				=> '',
	// 'related_events'				=> '',
	'artist_data'					=> '',
	'facebook'						=> '',
	'instagram'						=> '',
	'email'							=> '',
	'twitter'						=> '',
	'linkedin'						=> '',
	'tumblr'						=> '',
	'google_plus'					=> '',
	'pinterest'						=> '',
	'youtube'						=> '',
	'flickr'						=> '',
	'github'						=> '',
	'dribbble'						=> '',
	'vk'							=> '',
);

foreach ( $arr_teacher_meta as $meta => $val ) {
	if ( function_exists( 'get_field' ) ) {
		if ( get_field( $single_pre . $meta, $id ) ) {
			$arr_teacher_meta_val[$meta] = get_field( $single_pre . $meta, $id );
		}
	}
}
extract( shortcode_atts( $arr_teacher_meta, $arr_teacher_meta_val ) );


// layout sidebar

$classes[] = 'no-sidebar';

// xAsis label, data line 1, data line 2

wp_enqueue_script('k2t-scroll-bar');
wp_enqueue_style('k2t-scroll-bar-css');

wp_enqueue_script( 'teacher-js' );
wp_enqueue_script( 'k2t-countTo' );
wp_enqueue_script( 'k2t-inview' );

// get categories

$categories = get_the_terms( get_the_ID(), 'k-teacher-category' );

if ( is_plugin_active('k-shortcodes/init.php') ) 
	wp_enqueue_script( 'k2t-tabslet' );

?>

	<div class="k2t-content <?php echo implode( ' ', $classes ) ?>">
		<div class="k2t-wrap">
			<main class="k2t-main k2t-teacher">

				<?php while ( have_posts() ) : the_post(); ?>

					<div id="main-col" <?php post_class(); ?>>

						<div class="k2t-row teacher-top-info">
							<div class="teacher-entry-thumb">
								<div class="teacher-thumb" style="background-image: url('<?php echo the_post_thumbnail_url('thumb_270x480');?>')">
								</div>
							</div>
							<div class="teacher-entry-meta">

							 	<!--teacher top meta-->

								<div class="teacher-top-meta">

									<!--teacher title-->

									<div class="teacher-title">
										<h2> <?php the_title(); ?> </h2>
									</div>

									<!-- Teacher Description -->

									<div class="teacher-description">
										<?php the_content(); ?>
									</div>

									<!--teacher social-->

									<div class="teacher-social k2t-social-share social">
										<ul class="t-social">
						                	<?php if(!empty($facebook) || !empty($facebook_text)) : ?>
						                		<li>
						                			<a target='_blank' title="Facebook" href="<?php echo esc_url($facebook);?>">
						                				<i class='fa fa-facebook'></i>
						                			</a>
						                		</li>
						               		<?php endif; ?>

						               		<?php if(!empty($twitter) || !empty($twitter_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Twitter" href="<?php echo esc_url($twitter);?>">
							                			<i class='fa fa-twitter'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($instagram) || !empty($instagram_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Instagram" href="<?php echo esc_url($instagram);?>">
							                			<i class='fa fa-instagram'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($google_plus) || !empty($google_plus_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Google Plus" href="<?php echo esc_url($google_plus);?>">
							                			<i class='fa fa-google-plus'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($email) || !empty($email_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Email" href="<?php echo esc_url($email);?>">
							                			<i class="fa fa-envelope"></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		
						               		<?php if(!empty($linkedin) || !empty($linkedin_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Linkedin" href="<?php echo esc_url($linkedin);?>">
							                			<i class='fa fa-linkedin'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($tumblr) || !empty($tumblr_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Tumblr" href="<?php echo esc_url($tumblr);?>">
							                			<i class='fa fa-tumblr'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		
						               		<?php if(!empty($pinterest) || !empty($pinterest_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Pinterest" href="<?php echo esc_url($pinterest);?>">
							                			<i class='fa fa-pinterest'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($youtube) || !empty($youtube_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Youtube" href="<?php echo esc_url($youtube);?>">
							                			<i class='fa fa-youtube'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($flickr) || !empty($flickr_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Flickr" href="<?php echo esc_url($flickr);?>">
							                			<i class='fa fa-flickr'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($github) || !empty($github_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Github" href="<?php echo esc_url($github);?>">
							                			<i class='fa fa-github'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($dribbble) || !empty($dribbble_text)) : ?>
							               		<li>
							                		<a target='_blank' title="Dribbble" href="<?php echo esc_url($dribbble);?>">
							                			<i class='fa fa-dribbble'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						               		<?php if(!empty($vk) || !empty($vk_text)) : ?>
							               		<li>
							                		<a target='_blank' title="VK" href="<?php echo esc_url($vk);?>">
							                			<i class='fa fa-vk'></i>
							                		</a>
							                	</li>
						               		<?php endif; ?>
						                </ul>
									</div>

								</div> <!--teacher top meta-->

							</div><!--teacher-entry-meta-->

						</div><!--k2t-row-->


						<div class="teacher-skill">
							<?php
								if( !empty( $artist_data) ):
									foreach ( $artist_data as $key => $artist ): 

									?>
									<div class="k2t-counter col-3" data-delay="0" >
										<div class="counter-inner">
											<div class="wrap-thumb">
												<p>
													<img src=<?php echo esc_url( $artist['data_1'] ) ?>>
												</p>
												<div class="counter-number">
													<p class="number artist-number" data-from="0" data-to="<?php echo $artist['data_2']; ?>" data-speed="1000" data-delayrefresh-interval="10">
													</p>
												</div>
											</div>
											<p class="artist-name">
												<?php echo esc_html( $artist['data_3'] ); ?>
											</p>
										</div>
									</div>
									<?php endforeach;
								endif;
							?>
						</div>

						<div class="k2t-row teacher-bot-info">
							<!-- Event -->				
							<div class="teacher-event k-event-page">
								<?php
									$events = k_teacher_get_events( get_the_ID() );

									if( count($events) > 0  ) : ?>

										<div class="k2t-heading align-left three_dotted">
											<div class="k2t-heading-inner">
												<h2 class="heading"><span><span style="color: #cb2d88;">U</span>pcoming Even<span style="color: #3cbfae;">t</span></span></h2>
											</div>
										</div>


										<?php foreach( $events as $event ){
											$eventID = (function_exists('get_field')) ? get_field('event_event_id',  $event->ID) : '';
											$eventID = empty($eventID) ? '' : $eventID;

											$eventAddr = (function_exists('get_field')) ? get_field('event_address',  $event->ID) : '';
											$eventAddr = empty($eventAddr) ? '' : $eventAddr;

											$eventName = $event->post_title;
											$link = get_the_permalink($event->ID);

											$startDate = (function_exists('get_field')) ? get_field('event_start_date', $event->ID) : '';
											$startDate = empty($startDate) ? '' : $startDate;

										?>

										<!-- Event Meta --> 
										<div class="event-meta">

											<!-- Event Date --> 

												<div class="date">
												<?php
													$new_start_date = date_i18n('M ,d', strtotime($startDate) );
													$time_up  = date_i18n( 'g:i A', strtotime( $startDate ) );
													$date = explode(',', $new_start_date);
													echo '<span>' . $date[1] . '</span>';
													echo '<span>' . $date[0] . '</span>';
												?>
												</div>

											<!-- Event title --> 
											<div class="event-meta-inner">
												<h3 class="title">
													<a href="<?php echo esc_url( get_permalink( $event->ID ) );?>" title="<?php echo get_the_title( $event->ID );?>">
														<?php echo $eventName; ?>
													</a>
												</h3>

												<!-- Time -->
												<div class="event-meta-sub">
													<div class="event-time">
														<span><i class="zmdi zmdi-time"></i></span>
														<?php
															echo '<span>' . $time_up . '</span>';
														?>
													</div>

													<!-- Address -->

													<div class="event-address">
														<span><i class="zmdi zmdi-pin"></i></span>
														<span><?php echo esc_html( $eventAddr ); ?></span>
													</div>

													<!-- Excerpt -->
												</div>
												<div class="event-excerpt">
													<?php
														echo get_the_excerpt( $event->ID );
													?>
												</div>
											</div>

										</div><!-- Event Meta --> 

										<?php } ?>
									</div><!--event-info-->
								<?php endif; ?>
							</div>	<!-- END Event -->
					</div><!-- #main-col -->

				<?php endwhile; ?>
				<div class="clear"></div>

				<?php 
					if ( comments_open() ) :
						comments_template( '', false );
					endif;
				?>

			</main><!-- .k2t-teacher -->

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->
<?php get_footer(); ?>