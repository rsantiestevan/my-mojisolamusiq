<?php

/**
 * Teacher filter navigation.
 *
 * @package K Teacher
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get filter list

$filter_list = array();

$cat = $params['cat'];

if ( !empty( $cat ) ) :	// Have select categories

	$cat = explode( ',', $cat );

	foreach( $cat as $key => $value ) :
		$cat_name = get_cat_name( $value );
		$cat_name = strtolower( $cat_name );
		$filter_list[ str_replace(' ', '-', $cat_name) ] = get_cat_name( $value ) ;
	endforeach;

else:	// Get all categories

	$categories = get_categories( array( 'taxonomy' => 'k-teacher-category' ) );
	if ( count( $categories ) > 0 ) :
		foreach ($categories as $key => $value) {
			$filter_list[ $value->slug ] =  $value->cat_name;
		}
	endif;

endif;	?>

<!-- Output -->
<?php

	if ( $filter && $filter == 'show' ) : ?>
		<div class="k2t-isotop-filter k-teacher-filter">

			<!-- Filter bar -->

			<?php if ( $filter && count($filter_list) > 0 ) : ?>
				<ul class="menu-filter">
					<li>
						<a class="active" href='#'><?php echo esc_html__('All Trainer','aslan');?></a>
					</li>
					<?php
						foreach ($filter_list as $key => $value) :
							echo
								'<li>' .
									'<a href="#" data-filter=".k-teacher-category-' . $key . '">' .
										$value .
									'</a>' .
								'</li>';
						endforeach;
					?>
				</ul>
			<?php endif;?>

			<!-- Switch layout bar  -->

			<?php if ( $switch_layout ) :

				$col_grid = ( $column == 1 ) ? '3' : $column;

				$grid_active = $list_active = '';

				if ( $column > 1 ) $grid_active = 'sl-active'; else $list_active = 'sl-active';

			?>
				<div class="t-switch-layout">
					<span class="t-grid-layout <?php echo esc_attr( $grid_active );?>" data-layout="teacher-grid-style" data-col='columns-<?php echo esc_attr($col_grid);?>'><i class="zmdi zmdi-view-module"></i></span>
					<span class="t-listing <?php echo esc_attr( $list_active );?>" data-layout="teacher-listing-style" data-col='columns-1'><i class="zmdi zmdi-view-stream"></i></span>
				</div>
			<?php endif;?>
		</div>
	<?php
	endif;
