<?php
/**
 * Misc funtions
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

/*-------------------------------------------------------------------
	Social channel.
--------------------------------------------------------------------*/
if ( ! function_exists( 'k2t_social_array' ) ) {
	function k2t_social_array() {
		return array(
			'facebook'		=>	esc_html__( 'Facebook', 'k-shortcodes' ),
			'twitter'		=>	esc_html__( 'Twitter', 'k-shortcodes' ),
			'google-plus'	=>	esc_html__( 'Google+', 'k-shortcodes' ),
			'linkedin'	 	=>	esc_html__( 'LinkedIn', 'k-shortcodes' ),
			'tumblr'	 	=>	esc_html__( 'Tumblr', 'k-shortcodes' ),
			'pinterest'	 	=>	esc_html__( 'Pinterest', 'k-shortcodes' ),
			'youtube'	 	=>	esc_html__( 'YouTube', 'k-shortcodes' ),
			'skype'	 		=>	esc_html__( 'Skype', 'k-shortcodes' ),
			'instagram'	 	=>	esc_html__( 'Instagram', 'k-shortcodes' ),
			'delicious'	 	=>	esc_html__( 'Delicious', 'k-shortcodes' ),
			'reddit'		=>	esc_html__( 'Reddit', 'k-shortcodes' ),
			'stumbleupon'	=>	esc_html__( 'StumbleUpon', 'k-shortcodes' ),
			'wordpress'	 	=>	esc_html__( 'Wordpress', 'k-shortcodes' ),
			'joomla'		=>	esc_html__( 'Joomla', 'k-shortcodes' ),
			'blogger'	 	=>	esc_html__( 'Blogger', 'k-shortcodes' ),
			'vimeo'	 		=>	esc_html__( 'Vimeo', 'k-shortcodes' ),
			'yahoo'	 		=>	esc_html__( 'Yahoo!', 'k-shortcodes' ),
			'flickr'	 	=>	esc_html__( 'Flickr', 'k-shortcodes' ),
			'picasa'	 	=>	esc_html__( 'Picasa', 'k-shortcodes' ),
			'deviantart'	=>	esc_html__( 'DeviantArt', 'k-shortcodes' ),
			'github'	 	=>	esc_html__( 'GitHub', 'k-shortcodes' ),
			'stackoverflow'	=>	esc_html__( 'StackOverFlow', 'k-shortcodes' ),
			'xing'	 		=>	esc_html__( 'Xing', 'k-shortcodes' ),
			'flattr'	 	=>	esc_html__( 'Flattr', 'k-shortcodes' ),
			'foursquare'	=>	esc_html__( 'Foursquare', 'k-shortcodes' ),
			'paypal'	 	=>	esc_html__( 'Paypal', 'k-shortcodes' ),
			'yelp'	 		=>	esc_html__( 'Yelp', 'k-shortcodes' ),
			'soundcloud'	=>	esc_html__( 'SoundCloud', 'k-shortcodes' ),
			'lastfm'	 	=>	esc_html__( 'Last.fm', 'k-shortcodes' ),
			'lanyrd'	 	=>	esc_html__( 'Lanyrd', 'k-shortcodes' ),
			'dribbble'	 	=>	esc_html__( 'Dribbble', 'k-shortcodes' ),
			'forrst'	 	=>	esc_html__( 'Forrst', 'k-shortcodes' ),
			'steam'	 		=>	esc_html__( 'Steam', 'k-shortcodes' ),
			'behance'		=>	esc_html__( 'Behance', 'k-shortcodes' ),
			'mixi'			=>	esc_html__( 'Mixi', 'k-shortcodes' ),
			'weibo'			=>	esc_html__( 'Weibo', 'k-shortcodes' ),
			'renren'		=>	esc_html__( 'Renren', 'k-shortcodes' ),
			'evernote'		=>	esc_html__( 'Evernote', 'k-shortcodes' ),
			'dropbox'		=>	esc_html__( 'Dropbox', 'k-shortcodes' ),
			'bitbucket'		=>	esc_html__( 'Bitbucket', 'k-shortcodes' ),
			'trello'		=>	esc_html__( 'Trello', 'k-shortcodes' ),
			'vk'			=>	esc_html__( 'VKontakte', 'k-shortcodes' ),
			'home'			=>	esc_html__( 'Homepage', 'k-shortcodes' ),
			'envelope-alt'	=>	esc_html__( 'Email', 'k-shortcodes' ),
			'rss'			=>	esc_html__( 'RSS', 'k-shortcodes' ),
		);
	}
}

// Custom Gallery shortcode 

remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'stevecadey_gallery_shortcode' );

// Custom gallery shortcode 

function stevecadey_gallery_shortcode( $attr ) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}

	/**
	 * Filter the default gallery shortcode output.
	 *
	 * If the filtered output isn't empty, it will be used instead of generating
	 * the default gallery template.
	 *
	 * @since 2.5.0
	 * @since 4.2.0 The `$instance` parameter was added.
	 *
	 * @see gallery_shortcode()
	 *
	 * @param string $output   The gallery output. Default empty.
	 * @param array  $attr     Attributes of the gallery shortcode.
	 * @param int    $instance Unique numeric ID of this gallery shortcode instance.
	 */
	$output = apply_filters( 'post_gallery', '', $attr, $instance );
	if ( $output != '' ) {
		return $output;
	}
	$html5 = current_theme_supports( 'html5', 'gallery' );
	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => $html5 ? 'figure'     : 'dl',
		'icontag'    => $html5 ? 'div'        : 'dt',
		'captiontag' => $html5 ? 'figcaption' : 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => '',
		'link'       => ''
	), $attr, 'gallery' );

	if ( $attr['style'] != 'owl-slider' ) : // default wp style 

		$id = intval( $atts['id'] );

		if ( ! empty( $atts['include'] ) ) {
			$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

			$attachments = array();
			foreach ( $_attachments as $key => $val ) {
				$attachments[$val->ID] = $_attachments[$key];
			}
		} elseif ( ! empty( $atts['exclude'] ) ) {
			$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
		} else {
			$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
		}

		if ( empty( $attachments ) ) {
			return '';
		}

		if ( is_feed() ) {
			$output = "\n";
			foreach ( $attachments as $att_id => $attachment ) {
				$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
			}
			return $output;
		}

		$itemtag = tag_escape( $atts['itemtag'] );
		$captiontag = tag_escape( $atts['captiontag'] );
		$icontag = tag_escape( $atts['icontag'] );
		$valid_tags = wp_kses_allowed_html( 'post' );
		if ( ! isset( $valid_tags[ $itemtag ] ) ) {
			$itemtag = 'dl';
		}
		if ( ! isset( $valid_tags[ $captiontag ] ) ) {
			$captiontag = 'dd';
		}
		if ( ! isset( $valid_tags[ $icontag ] ) ) {
			$icontag = 'dt';
		}

		$columns = intval( $atts['columns'] );
		$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
		$float = is_rtl() ? 'right' : 'left';

		$selector = "gallery-{$instance}";

		$gallery_style = '';

		/**
		 * Filter whether to print default gallery styles.
		 *
		 * @since 3.1.0
		 *
		 * @param bool $print Whether to print default gallery styles.
		 *                    Defaults to false if the theme supports HTML5 galleries.
		 *                    Otherwise, defaults to true.
		 */
		if ( apply_filters( 'use_default_gallery_style', ! $html5 ) ) {
			$gallery_style = "
			<style type='text/css'>
				#{$selector} {
					margin: auto;
				}
				#{$selector} .gallery-item {
					float: {$float};
					margin-top: 10px;
					text-align: center;
					width: {$itemwidth}%;
				}
				#{$selector} img {
					border: 2px solid #cfcfcf;
				}
				#{$selector} .gallery-caption {
					margin-left: 0;
				}
				/* see gallery_shortcode() in wp-includes/media.php */
			</style>\n\t\t";
		}

		$size_class = sanitize_html_class( $atts['size'] );
		$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

		/**
		 * Filter the default gallery shortcode CSS styles.
		 *
		 * @since 2.5.0
		 *
		 * @param string $gallery_style Default CSS styles and opening HTML div container
		 *                              for the gallery shortcode output.
		 */
		$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

		$i = 0;
		foreach ( $attachments as $id => $attachment ) {

			$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
			if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
				$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
			} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
				$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
			} else {
				$image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
			}
			$image_meta  = wp_get_attachment_metadata( $id );

			$orientation = '';
			if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
				$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
			}
			$output .= "<{$itemtag} class='gallery-item'>";
			$output .= "
				<{$icontag} class='gallery-icon {$orientation}'>
					$image_output
				</{$icontag}>";
			if ( $captiontag && trim($attachment->post_excerpt) ) {
				$output .= "
					<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
					" . wptexturize($attachment->post_excerpt) . "
					</{$captiontag}>";
			}
			$output .= "</{$itemtag}>";
			if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
				$output .= '<br style="clear: both" />';
			}
		}

		if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
			$output .= "
				<br style='clear: both' />";
		}

		$output .= "
			</div>\n";

		return $output;

	else : 

		if ( ! isset( $atts['columns'] ) ) $atts['columns'] = 3;

		$items = $atts['columns'];

		$item_mobile  = '1';
		$item_tablet  = '2';
		$item_desktop = $atts['columns'];

		switch ( $atts['columns'] ) {
			case '1':
					$item_tablet = $item_desktop = '1';
				break;
			
			default:
				# code...
				break;
		}


		$slices = explode( ',', $attr['ids'] );

		ob_start();

		echo '<div class="owl-carousel" data-items="' . esc_attr( $items ) . '" data-autoPlay="false" data-margin="15" data-loop="true" data-nav="true" data-dots="true" data-mobile="' . esc_attr( $item_mobile ) . '" data-tablet="' . esc_attr( $item_tablet ) . '" data-desktop="' . esc_attr( $item_desktop ) . '">';
		foreach ( $slices as $slide ) :
			$image = wp_get_attachment_image( $slide, 'stevecadey_blog_single' ); 
			echo '<div class="item">';
			echo ( $image );
			echo '</div>';
		endforeach;
		echo '</div>';
		$output .= ob_get_clean();

		return $output;
	endif;
}


add_action('print_media_templates', function(){

  // define your backbone template;
  // the "tmpl-" prefix is required,
  // and your input field should have a data-setting attribute
  // matching the shortcode name
  ?>
  <script type="text/html" id="tmpl-gallery-style">
    <label class="setting">
      <span><?php _e('Gallery style'); ?></span>
      <select data-setting="style">
        <option value="default"> Default </option>
        <option value="owl-slider"> owl-slider </option>
      </select>
    </label>
  </script>

  <script>

    jQuery(document).ready(function(){

      // add your shortcode attribute and its default value to the
      // gallery settings list; $.extend should work as well...
      _.extend(wp.media.gallery.defaults, {
        my_custom_attr: 'default'
      });

      // merge default gallery settings template with yours
      wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
        template: function(view){
          return wp.media.template('gallery-settings')(view)
               + wp.media.template('gallery-style')(view);
        }
      });

    });

  </script>
  <?php

});