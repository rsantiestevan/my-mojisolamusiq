(function($) {
	"use strict";

	$(function() {

		if ($().magnificPopup) {

			$('.mask-item a').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false
			});

			$('.slick-slider').slick({
				autoplay: false,
				autoplaySpeed: 3000,
				vertical: true,
				arrows: false,
				responsive: [
				    {
				    	breakpoint: 800,
				      	settings: {
				      		vertical: false,
				        	slidesToShow: 3,
				        	slidesToScroll: 1,
				        	verticalSwiping: false,
							vertialScroll: false,
				    	}
				    },
				],
				verticalSwiping: true,
				vertialScroll: true,
				focusOnSelect: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				nextArrow : '<i class="zmdi zmdi-chevron-left"></i>',
				prevArrow : '<i class="zmdi zmdi-chevron-right"></i>'
			});
		}
	});
})(jQuery);