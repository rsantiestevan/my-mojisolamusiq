/**
 * Script trigger shortcodes.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

(function($) {
	"use strict";

	var K2T = K2T || {};

	/*  [ Mobile Check ]
	- - - - - - - - - - - - - - - - - - - */
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	// get image in shortcode blog large
	$('.shortcode-blog-large').each(function(){
		var $this = $(this);
		var $img = $this.find('.flx-entry-thumb').find('img').attr('src');
		$this.find('.b-large-mark').css('background','url('+$img+') no-repeat');
	});

	/*  [ Animation ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.animated = function() {
		$('.animated').each(function() {

			var $this = $(this);
			var delay = $this.data('animation-delay');
			var animation = $this.data('animation');
			$this.one('inview', function(event, isInView, visiblePartX, visiblePartY) {
				setTimeout(function(){
					$this.addClass(animation + ' run_animation');
				}, delay);
			}); // inview

		});
	}

	/*  [ Parallax ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.parallax = function() {
		$('.k2t-parallax').each(function() {
			var $this = $(this);
			$this.find('.bg-element').parallax();
		});
	}


	/*  [ Tipsy ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.tipsy = function() {
		if ($().tipsy) {
			$('.hastip.tooltip-top').tipsy({
				gravity: 's',
				opacity: 1,
				fade: true,
			});
			$('.hastip.tooltip-bottom').tipsy({
				gravity: 'n',
				opacity: 1,
				fade: true,
			});
			$('.hastip.tooltip-left').tipsy({
				gravity: 'e',
				opacity: 1,
				fade: true,
			});
			$('.hastip.tooltip-right').tipsy({
				gravity: 'w',
				opacity: 1,
				fade: true,
			});
		};
		if ($().tooltipster) {

			$('.k2t-tooltipster').each(function() {
				var $this = $(this);
				var args = {
					content: $this.find('.tooltipster-markup'),
					animation: $this.data('effect'),
					delay: 0,
					maxWidth: $this.data('max-width') ? $this.data('max-width') : 300,
					theme: $this.data('theme'),
					trigger: $this.data('trigger'),
					position: $this.data('position'),
				}
				$this.tooltipster(args);
			});

		}
	}

	/*  [ Pie Chart ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.piechart = function() {
		if ($().easyPieChart) {
			$('.k2t-piechart').each(function() {
				var $this = $(this);
				$this.one('inview', function(event, isInView, visiblePartX, visiblePartY) {
					//alert('ok');
					var chart_args = {
						barColor: $this.data('color'),
						trackColor: $this.data('trackcolor'),
						scaleColor: false,
						lineCap: $this.data('linecap'),
						lineWidth: $this.data('thickness'),
						size: $this.data('size'),
						animate: $this.data('speed') ? $this.data('speed') : 600,
						onStep: function(value) {
							this.$el.find('span.number').text(~~value);
						},
						onStop: function() {
							var percent = this.$el.data('percent');
							this.$el.find('span.number').text(percent);
						}
					};
					var delay = parseInt($this.data('delay'));
					setTimeout(function() {
						$this.find('.chart').find('.percent').css({
							visibility: 'visible'
						}).end().easyPieChart(chart_args);
					}, delay);
				}); // bind inview
			}); // each
		} // if easyPieChart
	} // piechart

	/*  [ Progress ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.progress = function() {
		$('.k2t-progress').each(function() {
			var $this = $(this);
			var percent = $(this).attr('data-percent');
			$this.one('inview', function(event, isInView, visiblePartX, visiblePartY) {
				$this.find('.bar').animate({
					left: 0,
					opacity: '1',
				}, $this.data('speed') ? $this.data('speed') : 1500, $this.data('easing') ? $this.data('easing') : 'easeOutExpo'); // animate
				$this.find('.percent').animate({
					left: percent,
					opacity: '1',
				}, $this.data('speed') ? $this.data('speed') : 1500, $this.data('easing') ? $this.data('easing') : 'easeOutExpo'); // animate
			}); // bind inview
		}); // each
	} // progress

	/*  [ Toggle ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.toggle = function() {
			if ($().collapse) {

				/* TOGGLE */
				$('.k2t-toggle').each(function() {
					var $this = $(this);
					var collapse_args = {
						query: '.toggle-title',
						open: function() {
							this.slideDown($this.data('speed') ? $this.data('speed') : 250, 'easeOutExpo');
						},
						close: function() {
							this.slideUp($this.data('speed') ? $this.data('speed') : 250, 'easeOutExpo');
						},
					};
					$this.collapse(collapse_args);
				}); // each

				/* ACCORDION */
				$('.k2t-accordion').each(function() {
					var $this = $(this);
					var collapse_args = {
						accordion: true,
						open: function() {
							this.slideDown($this.data('speed') ? $this.data('speed') : 250, 'easeOutExpo');
						},
						close: function() {
							this.slideUp($this.data('speed') ? $this.data('speed') : 250, 'easeOutExpo');
						},
					};
					$this.collapse(collapse_args);
				}); // each

			} // if collapse
		} // toggle

	/*  [ Tabs ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.tab = function() {
		if ($().tabslet) {
			$('.k2t-tab').each(function() {
				var $this = $(this);
				var tab_args = {
					active: $this.data('active'),
					attribute: 'data-href',
					mouseevent: $this.data('mouse'),
					animation: $this.data('animation'),
				};
				$this.tabslet(tab_args);
			}); // each

		} // if tabslet
	} // tab

	/*  [ Counter ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.counter = function() {
		if ( $().countTo ) {
			$('.k2t-counter').each(function() {
				var $this = $(this);
				var delay = parseInt($this.data('delay'));
				var counter_args = {};
				$this.one('inview', function(event, isInView, visiblePartX, visiblePartY) {
					setTimeout(function() {
						$this.find('.number').css({
							opacity: 1
						}).countTo(counter_args);
					}, delay);
				}); // inview
			}); // counter
		} // if countTo
	} // counter

	/*  [ Lightbox ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.lightbox = function() {
		if ($().magnificPopup) {
			/* general */
			$('.k2t-popup-link').magnificPopup({
				type: 'image'
			});

			/* gallery */
			$('.k2t-popup-gallery').magnificPopup({
				delegate: 'a', // the selector for gallery item
				type: 'image',
				gallery: {
					enabled: true
				}
			});

		}; // if magnificPopup exists
	}; // lightbox

	/*  [ iView ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.iview = function() {
		if ($().iView) {
			$('.iview').each(function(){
				$(this).iView({
					pauseTime                : 7000,
					pauseOnHover             : true,
					directionNav             : true,
					directionNavHide         : false,
					directionNavHoverOpacity : 0,
					controlNav               : false,
					nextLabel                : "Nächste",
					previousLabel            : "Vorherige",
					playLabel                : "Spielen",
					pauseLabel               : "Pause",
					timer                    : "360Bar",
					timerPadding             : 3,
					timerColor               : "#0F0"
				});
			});
		}; // if ivew exists
	}; // iview

	/*  [ eislideshow ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.eislideshow = function() {
		if ($().eislideshow) {
			$('.ei-slider').each(function(){
				$(this).eislideshow({
					animation           : 'center',
					autoplay            : true,
					slideshow_interval  : 3000,
					titlesFactor        : 0
				});
			});
		}; // if eislideshow exists
	}; // eislideshow

	/*  [ flexslider ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.flexslider = function() {
		if ($().flexslider) {
			$('.flexslider').each(function(){
				$(this).flexslider({
					animation: "slide",
					start: function(slider){
					  jQuery('body').removeClass('loading');
					}
				});
			});
		}; // if flexslider exists
	}; // flexslider

	/*  [ Sticky ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.sticky = function(){
		$('.k2t-tab-sticky').each(function(){
			var $this = $(this);
			var ID = $this.data('id');
			$this.find('.tabsticky-nav').stickyMojo({
				footerID: '#k2t-tabsticky-footer-'+ID,
				contentID: '#k2t-tabsticky-content-'+ID,
			}); // stickyMojo
		}); // k2t-tab-sticky
	};

	K2T.shortcode_isotope = function() {
		if ( $().isotope && $().imagesLoaded ) {
			$( '.k2t-shortcode-isotope' ).each( function() {

				var $this = $(this);
				var $container = $(this);
				// initialize Isotope + Masonry after all images have loaded
				$this.imagesLoaded( function() {

					$container.addClass('loaded');
					var isotope_args = {
						itemSelector: 'article',
						transitionDuration	: '.55s',
						masonry: {
							gutter	: '.gutter-sizer',
							//columnWidth:
						},
					};
					if ( true ) {
						isotope_args['layoutMode'] = 'fitRows';
					}
					var $grid = $container.isotope(isotope_args);

					// animation
					var animation = $grid.data('animation');
					if (animation = true) {
						$container.find('.isotope-selector').find('.article-inner').each(function(){
							var $this=$(this);
							$this.parent().one('inview', function(event, isInView, visiblePartX, visiblePartY) {
								if (isInView) {
									$this.addClass('run_animation');
								} // inview
							});// bind
						}); // each

					} // endif animation

				}); // imagesLoaded

			}); // each .k2t-isotope-wrapper
		} // if isotope
	}
	/*  [ Countdown ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.countdown = function(){
		if(jQuery('.k2t-countdown .countdown-container').length){

			jQuery(window).on('load', function() {

	            $('.k2t-countdown .countdown-container').each(function(index){
	                var labels = ['week','days', 'hours', 'minutes', 'seconds'],
	                    template = _.template($(this).parent().next().find('script.countdown-template').html()),
	                    currDate = '00:00:00:00:00',
	                    nextDate = '00:00:00:00:00',
	                    parser = /([0-9]{2})/gi,
	                    $example = $(this);
	                // Parse countdown string to an object
	                function strfobj(str) {
	                    var parsed = str.match(parser),
	                        obj = {};
	                    labels.forEach(function(label, i) {
	                        obj[label] = parsed[i]
	                    });
	                    return obj;
	                }
	                // Return the time components that diffs
	                function diff(obj1, obj2) {
	                    var diff = [];
	                    labels.forEach(function(key) {
	                        if (obj1[key] !== obj2[key]) {
	                            diff.push(key);
	                        }
	                    });
	                    return diff;
	                }
	                // Build the layout
	                var initData = strfobj(currDate);
	                labels.forEach(function(label, i) {
	                    $example.append(template({
	                        curr: initData[label],
	                        next: initData[label],
	                        label: label
	                    }));
	                });
	                // Starts the countdown

	                $example.countdown($example.parent().next().find('script.countdown-template').data('startdate'), function(event) {
	                    var newDate = event.strftime('%w:%d:%H:%M:%S'),
	                        data;
	                    if (newDate !== nextDate) {
	                        currDate = nextDate;
	                        nextDate = newDate;
	                        // Setup the data
	                        data = {
	                            'curr': strfobj(currDate),
	                            'next': strfobj(nextDate)
	                        };
	                        // Apply the new values to each node that changed
	                        diff(data.curr, data.next).forEach(function(label) {
	                            var selector = '.%s'.replace(/%s/, label),
	                                $node = $example.find(selector);
	                            // Update the node
	                            $node.removeClass('flip');
	                            $node.find('.curr').text(data.curr[label]);
	                            $node.find('.next').text(data.next[label]);
	                            // Wait for a repaint to then flip
	                            _.delay(function($node) {
	                                $node.addClass('flip');
	                            }, 50, $node);
	                        });
	                    }
	                });
	            });

			});
		}
	}

		/*  [ testimonial istope ]
	- - - - - - - - - - - - - - - - - - - */

	K2T.testimonial = function(){
		if ( $().isotope && $().imagesLoaded ) {
			if ( $('.k2t-testi-isotope').length > 0 ) {
				var $container = $('.k2t-testi-isotope');
				var $this = $(this);
				$this.imagesLoaded( function() {
					$container.isotope({
						itemSelector: '.testi-item',
						filter: '.testi-show',
						masonry: {
						    columnWidth: '.testi-item',
						}
					});
				});
			}

			$('.k2t-testimonial .filter-all a').on('click',function(){
				$container.isotope({
					filter: '*',
				});
				$(this).css('display','none');
				return false;
			});
		}
	}

	/*  [ Init Function ]
	- - - - - - - - - - - - - - - - - - - */
	K2T.countdown();
	$(document).ready(function() {
		K2T.animated();
		K2T.parallax();
		K2T.tipsy();
		K2T.piechart();
		K2T.progress();
		K2T.toggle();
		K2T.tab();
		K2T.counter();
		K2T.lightbox();
		K2T.iview();
		K2T.eislideshow();
		K2T.flexslider();
		K2T.shortcode_isotope();
		K2T.testimonial();

		// google map 

		$('.map-select').on('change',function(){
			var $popup = $(this).closest('.map-pop-up');
			$popup.find('.map-btn').attr('data-link', $(this).val());
		});

		$('.map-btn').on('click',function(){
			$(this).closest('.map-pop-up').addClass('hidden');
			return false;
		});
		$('.open-btn').on('click',function(){
			$(this).closest('.map-pop-up').removeClass('hidden');
			return false;
		});	


	}); // ready
	$(window).load(function(){
		K2T.sticky();
	});
})(jQuery);