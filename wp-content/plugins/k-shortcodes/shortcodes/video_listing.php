<?php
/**
 * Shortcode k2t slider.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_video_listing_shortcode' ) ) {
	function k2t_video_listing_shortcode( $atts, $content ) {
		$html = $i = $style = $cat = $column = $limit = $items = $items_desktop = $items_tablet = $items_mobile = $navigation = $pagination = $p_class = $data_name = $data_delay = $anm = $anm_name = $anm_delay = $cat_anm = $exc_anm = $footer_anm = $thum_anm = $title_anm = $id = $class = '';
		extract( shortcode_atts( array(
			'style'         => 'video',
			'limit'	        => '-1',
			'cat'           => '',
			'slider'        => '',
			'items'         => '1',
			'items_desktop' => '1',
			'items_tablet'  => '1',
			'items_mobile'  => '1',
			'navigation'    => '',
			'pagination'    => '',
			'auto_play'     => '',
			'anm'           => '',
			'anm_name'      => '',
			'anm_delay'     => '',
			'id'            => '',
			'class'         => '',
		), $atts));

	if ( $anm ) {
		$anm        = ' animated';
		$data_name  = ' data-animation="' . $anm_name . '"';
		$data_delay = ' data-animation-delay="' . $anm_delay . '"';
	}

	$id    = ( $id != '' ) ? ' id="' . $id . '"' : '';
	$class  = ( $class != '' ) ? ' ' . $class . '' : '';
	$class .= ' ' . 'style-' . $style;

	if ( in_array( $style, array( 'video' ) ) ) $class .= ' ' . 'b-large'; // setup like blog taxonomy;

	// Global variables
	global $post, $smof_data;

	// Filter post type
	$args = array(
		'post_type'      => 'post',
		'posts_per_page' => $limit,
		'cat'            => $cat,
		'tax_query' => array(
			array(
			    'taxonomy' => 'post_format',
			    'field'    => 'slug',
			    'terms'    => 'post-format-video',
		    )
		)
    );

	wp_enqueue_style('style-slick');
	wp_enqueue_script('jquery-slick');
	wp_enqueue_script('video-slider');
	wp_enqueue_script('magnific-popup');

	// The query
	$blog = new WP_query( $args );

	$post_format = get_post_format();

	$html .= '<div class="k2t-blog-post '. $class .'">';

	$html .= '<div class="col-9">';

	while ( $blog->have_posts() ) : $blog->the_post();

		$crr = $blog->current_post;

		switch ( $crr ) {
			case '0':
				$image_size = 'thumb_900x565';
				break;
			
			default:
				$image_size = 'thumb_200x200';
				break;
		}
		
		$video = array(
			'video_format_source'			=> '',
			'video_url'						=> '',
			'video_code'					=> '',
			'video_local'					=> '',
		);

		$video = stevecadey_get_acf_field( $video, get_the_ID() );

		$output_link = '';

		switch ( $video['video_format_source'] ) {
			case 'link':
				$output_link = $video['video_url'];
				break;

			case 'embed':
				$output_link = $video['code'];
				break;

			case 'local':
				$output_link = $video['video_local']['url'];
				break;
		}

		ob_start(); 

		if( $crr == 1 )

			echo '</div><div class="col-3"><div class="k-blog-carousel slick-slider">'; ?>
			<article <?php post_class( 'video-listing element hentry post-item ' . $post_format . '-post' . $anm );?> <?php echo esc_attr( $data_name ) .  esc_attr( $data_delay );?> >
				<div class="post-inner post-video">
					<div class="flx-entry-thumb">
						<?php
							if ( has_post_thumbnail() ) :
								echo get_the_post_thumbnail( get_the_ID(), $image_size ) .'<div class="mask-item"><a title="' . get_the_title()  .'" href="'. esc_url( $output_link ) .'"><i class="mask-icon"></i></a></div>';
							endif;

						?>
					</div>
				</div><!--End:post-inner-->
			</article><!--End:post-item-->

		<?php //include  'blog/content-video.php';	?>

		<?php $html .= ob_get_clean();

	endwhile;
	
	$html .= '</div></div></div>'; // End Masonry-layout, slider

	$html = stevecadey_mini_output($html);

	// Restore original Post Data
	wp_reset_postdata();

	// Apply filters return
	$html = apply_filters( 'k2t_video_listing_return', $html );

	return $html;
	}
}