<?php
/**
 * The template for displaying content large image thumbnail.
 *
 * @package K Shortcodes
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// get theme option

if ( empty( $smof_data ) )
	$smof_data = of_get_options();

// Get single setting

$single_stt = array(
					'link'					=> '',
					'single_layout'			=> '',
					'link_format_url'		=> '',
					'display_categories'	=> '',
					'display_posted_author'	=> '',
					'display_post_date'		=> '',
					'display_tags'			=> '',
					'display_authorbox'		=> '',
					'display_related_post'	=> '',
			  	);

$single_stt	= stevecadey_get_acf_field( $single_stt, get_the_ID() );

// Get post format
$post_format = get_post_format();

// animation

$anm = $data_name = $data_delay = '';

if ( isset( $smof_data['blog_anm'] ) && $smof_data['blog_anm'] == '1' ) :
	$anm        = ' animated';
	$data_name  = ' data-animation=' . $smof_data['blog_type_anm'];
	$data_delay = ' data-animation-delay=' . $smof_data['blog_anm_delay'];
endif;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'element hentry post-item ' . $post_format . '-post' . $anm );?> <?php echo esc_attr( $data_name ) .  esc_attr( $data_delay );?> >
	<div class="post-inner">

	<?php
		if ( 'quote' == $post_format ) :		// format quote

			include get_template_directory() . '/templates/blog/post-format.php';

		else :		// other formats

			include get_template_directory() . '/templates/blog/post-format.php';	?>

			<div class="entry-content clearfix">

				<div class="top-meta">

					<!-- Categories -->

					<?php if ( true ) : ;?>
						<div class="categories-list">
							<?php $categories_list = get_the_category_list( esc_html__( '  ', 'k-shortcodes' ) );
								if ( $categories_list ) :
									printf( esc_html__( '%1$s', 'k-shortcodes' ), $categories_list ) ;
								endif;
							?>
						</div>
					<?php endif;?>

	        		<!-- Title -->

	        		<?php
						// title

						if ( get_post_format() == 'link' ) :
							the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $single_stt['link_format_url'] ) ), '</a></h2>' );
						else :
							if ( $smof_data['blog_post_link'] ) {
								the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
							}
						endif;
					?>

	        		<!-- Date -->

					<?php if ( $single_stt['display_post_date'] ) : ?>
						<div class="date-meta">
							<?php echo '<i class="zmdi zmdi-time"></i><span>' . get_the_date('d/m/Y \a\t g:i A') . '</span>';?>
						</div>
					<?php endif;?>

        			<!-- Author -->

					<?php if ( $single_stt['display_posted_author'] ) : ?>
						<div class="author-meta">
						<?php
							echo '<i class="zmdi zmdi-account"></i><a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"> ' . get_the_author() . '</a>';
						?>
						</div>
					<?php endif;?>
				</div>

				<!-- Blog excerpt -->
				<?php
					//stevecadey_blog_excerpt();
				?>

				<!-- Readmore btn -->
				<?php
					//k2t_aslan_readmore_btn();
				?>

			</div><!--End:entry-content-->
		<?php endif;?>
	</div><!--End:post-inner-->
</article><!--End:post-item-->