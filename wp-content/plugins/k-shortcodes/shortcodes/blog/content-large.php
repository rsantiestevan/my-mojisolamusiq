<?php
/**
 * The template for displaying content large image thumbnail.
 *
 * @package K Shortcodes
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// get theme option

if ( empty( $smof_data ) )
	$smof_data = of_get_options();

// Get single setting

$single_stt = array(
					'single_layout'			=> '',
					'link_format_url'		=> '',
					'display_categories'	=> '',
					'display_posted_author'	=> '',
					'display_post_date'		=> '',
					'display_tags'			=> '',
					'display_authorbox'		=> '',
					'display_related_post'	=> '',
			  );

$single_stt	= stevecadey_get_acf_field( $single_stt, get_the_ID() );

// Get post format
$post_format = get_post_format();

// animation

$anm = $data_name = $data_delay = '';

if ( isset( $smof_data['blog_anm'] ) && $smof_data['blog_anm'] == '1' ) :
	$anm        = ' animated';
	$data_name  = ' data-animation=' . $smof_data['blog_type_anm'];
	$data_delay = ' data-animation-delay=' . $smof_data['blog_anm_delay'];
endif;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'element hentry post-item ' . $post_format . '-post' . $anm );?> <?php echo esc_attr( $data_name ) .  esc_attr( $data_delay );?> >
	<div class="post-inner shortcode-blog-large">

	<?php
		if ( 'quote' == $post_format ) :		// format quote

			include 'post-format.php';

		else :		// other formats

			//include 'post-format.php';

			$image_size = 'thumb_1200x240';

			if ( has_post_thumbnail() ) :
					echo '<div class="flx-entry-thumb flx-large"><a href="'. esc_url( get_permalink() ) .'"><i class="mask-item"></i>'. get_the_post_thumbnail( get_the_ID(), $image_size ) .'</a></div>';
				else :
					//stevecadey_place_holder( $image_size );
			endif;

			?>
			<div class="b-large-mark"></div>
			<div class="entry-content clearfix">

				<div class="top-meta">

	        		<!-- Title -->

	        		<?php
						// title

						if ( get_post_format() == 'link' ) :
							the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $single_stt['link_format_url'] ) ), '</a></h2>' );
						else :
							if ( $smof_data['blog_post_link'] ) {
								the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
							}
						endif;
					?>

	        		<!-- Date -->

					<?php if ( $single_stt['display_post_date'] ) : ?>
						<div class="date-meta">
							<?php echo '<i class="zmdi zmdi-calendar-note"></i><span>' . get_the_date('d M Y') . '</span>';?>
						</div>
					<?php endif;?>

        			<!-- Author -->

					<?php if ( $single_stt['display_posted_author'] ) : ?>
						<div class="author-meta">
						<?php
							echo '<i class="zmdi zmdi-account"></i><a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"> ' . get_the_author() . '</a>';
						?>
						</div>
					<?php endif;?>
				</div>

				<!-- Blog excerpt -->
				<?php
					echo '<div class="blog-content">';
					if ( 'excerpts' == $smof_data['blog_display'] ) {
						$excerpt = $blog->post->post_excerpt;
						if( empty( $excerpt ) ) {
							$excerpt = '<p class="excerpt">' . wp_trim_words( get_the_content(),  $smof_data[ 'excerpt_length' ] ) . '</p>';
						}
						echo $excerpt;
					} else {
						the_content();
					}
					echo '</div>';
				?>

			</div><!--End:entry-content-->
		<?php endif;?>
	</div><!--End:post-inner-->
</article><!--End:post-item-->


