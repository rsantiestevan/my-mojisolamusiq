<?php
/**
 * The template for displaying content large image thumbnail.
 *
 * @package K Shortcodes
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
global $smof_data, $post;
$blog_style = $smof_data['blog_style'];
$layout = $smof_data['blog_layout'];

// Get post format
$post_format = get_post_format();
$link        = ( function_exists( 'get_field' ) ) ? get_field( 'link_format_url', get_the_ID() ) : '';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'element hentry post-item ' . $post_format . '-post' . $anm ); echo $data_name . $data_delay; ?>>
	<div class="post-inner">

		<?php if ( 'quote' == $post_format ) : ?>
			<header>
				<?php if ( $smof_data['blog-date'] ):?>
					<span class="entry-date"><?php the_time( 'j M Y' ); ?></span>
				<?php endif;?>
				<?php if ( $smof_data['blog-author'] ) : ?>
					<span class="entry-author"><a class="" href="<?php echo esc_url($author_link); ?>"><?php the_author(); ?></a></span>
				<?php endif;?>
			   
			</header>
		<?php endif;?>

		<!-- Include thumb -->
		<?php include get_template_directory() . '/templates/blog/post-format.php'; ?>

		<!-- Content will display if not format quote -->
		<?php if ( 'quote' != $post_format ) : ?>

			<div class="entry-content clearfix">
				<?php 
					if ( 'link' == $post_format ) {
						the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $link ) ), '</a></h2>' );
					} else {
						if ( $smof_data['blog-post-link'] ) {
							the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
						}
					}
				?>
				<div class="top-meta">
					<?php if ( $display_posted_author != 'hide' ) : ?>
						<div class="author-meta">
						<?php
							echo '<span>' . esc_html__( 'POSTED BY', 'k-shortcodes' ) . ' &nbsp;</span>';
							echo '<a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"> ' . get_the_author() . '</a>';
						?>
						</div>
					<?php endif;?>
					<?php if ( $smof_data['blog_date'] ) : ?>
						<div class="date-meta">
							<?php echo '<span>' . get_the_date() . '</span>';?>
						</div>
					<?php endif;?>

					<?php if ( $smof_data['blog_categories'] ) : ?>
						<?php $categories_list = get_the_category_list( __( ', ', 'k-shortcodes' ) );
							if ( $categories_list ) :
								printf( __( '%1$s', 'k-shortcodes' ), $categories_list ) ;
							endif;
						?>
					<?php endif;?>
				</div>
				
				<?php
					if ( true ) {
						$excerpt = $blog->post->post_excerpt;
						if ( ! empty( $excerpt ) )
							echo '<p class="excerpt">' . $excerpt . '</p>';
						else 
							echo $trimmed_content = '<p class="excerpt">' . wp_trim_words( get_the_content(), $smof_data['excerpt-length'] ) . '</p>';
					}
				?>
				<div class= "footer-content clearfix">
					<?php 
						if ( $smof_data['blog_readmore'] ) {
							echo '<a class="more-link" href="' . get_permalink() . '">'. esc_html__( 'Continue', 'k-shortcodes' ) .' <span aria-hidden="true" class="arrow_right"></span></a>';
						}
					?>

					<?php if ( $smof_data['blog_number_comment'] ) :?>
						<span class="entry-comment"><span aria-hidden="true" class="icon_comment_alt"></span><a href="<?php comments_link(); ?>"><?php comments_number( '0', '1', '%' ); ?></a></span>
					<?php endif;?>
				</div>
				
			</div><!--end:entry-content-->

		<?php endif;?>

		<?php if ( 'quote' == $post_format ) : ?>

			<span title="<?php echo get_cat_name($post_categories[0]); ?>" class="cat-icon" style = "background-color : <?php function_exists( 'get_field' ) ? the_field('category_color', 'category_' . $post_categories[0]) : '';?>">
				<?php function_exists( 'get_field' ) ? the_field('category_icon', 'category_' . $post_categories[0]) : '';?>
			</span>
			<div class= "footer-content clearfix">
				<?php 
					if ( $smof_data['blog_readmore'] ) {
						echo '<a class="more-link btn-ripple" href="' . get_permalink() . '">'. esc_html__( 'Read more', 'k-shortcodes' ) .'</a>';
					}
				?>

				<?php if ( $smof_data['blog_number_comment'] ) :?>
					<span class="entry-comment"><a href="<?php comments_link(); ?>"><?php comments_number( '0', '1', '%' ); ?></a></span>
				<?php endif;?>
			</div>

		<?php endif ?>
	</div>
</article><!--end:post-item-->


