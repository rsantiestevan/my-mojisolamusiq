<?php
/**
 * The template for displaying post formats.
 *
 * @package K Shortcodes
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

global $post;

if ( empty( $smof_data ) )
	$smof_data = of_get_options();

// Post format video

$video = array(
			'video_format_source'			=> '',
			'video_url'						=> '',
			'video_code'					=> '',
			'video_local'					=> '',
		);

$video = stevecadey_get_acf_field( $video, get_the_ID() );

// Post format audio

$audio = array(
			'audio_format_source'			=> '',
			'audio_url'						=> '',
			'audio_local'					=> '',
		);

$audio = stevecadey_get_acf_field( $audio, get_the_ID() );

// Post format gallery

$gallery = array(
				'post_gallery'					=> '',
				'gallery_auto'					=> '',
				'gallery_auto_time_wait'		=> '',
				'gallery_speed'					=> '',
				'gallery_pagination'			=> '',
				'gallery_navigation'			=> '',
				'gallery_mousewheel'			=> '',
			);

$gallery = stevecadey_get_acf_field( $gallery, get_the_ID() );

// Post format quote

$quote = array(
			'quote_author'					=> '',
			'author_quote_url'				=> '',
			'quote_content'					=> '',
		);

$quote = stevecadey_get_acf_field( $quote, get_the_ID() );

//set image size for single

if ( isset( $archive_stt ) ) :	// listing blog

	switch ( $archive_stt['blog_style'] ) :

		case 'large':
			if ( $archive_stt['blog_layout'] == 'no_sidebar' ) :
				$image_size = 'stevecadey_blog_large';
			else:
				$image_size = 'stevecadey_blog_large';
			endif;
		break;

		case 'grid':

			switch ( $archive_stt['blog_grid_column'] ) :

				case 'column-2':
					$image_size =	'stevecadey_blog_grid_2';
					break;
				case 'column-3':
					$image_size =	'stevecadey_blog_grid_2';
					break;
				case 'column-4':
					$image_size =	'stevecadey_blog_grid_2';
					break;
			endswitch;
		break;

		case 'masonry':

			switch ($archive_stt['blog_masonry_column']) :
				case 'column-2':
					$image_size =	'stevecadey_blog_masonry_2';
					break;
				case 'column-3':
					$image_size =	'stevecadey_blog_masonry_2';
					break;
				case 'column-4':
					$image_size =	'stevecadey_blog_masonry_2';
					break;
			endswitch;
		break;
	endswitch; // End set image size for each style

else :	// single post
	$image_size = 'stevecadey_blog_large';
endif;

$image_thum = $image_size;

?>

<div class="flx-entry-thumb">

 <?php
	switch ( get_post_format() ) :

		case 'video':
			if ( is_single() ) :
				if ( 'link' == $video['video_format_source'] ) :
					echo do_shortcode( '[vc_video link="' . esc_url( $video['video_url'] ) . '"/]' );
				elseif ( 'embed' == $video_source ) :
					echo ( $video['code'] );
				elseif ( 'local' == $video_source ) :
					echo do_shortcode('[video src="' . esc_url( $video['video_local']['url'] ) . '"/]');
				endif;
			else :
				if ( has_post_thumbnail() ) :
					echo '<a href="'. esc_url( get_permalink() ) .'"><div class="mask-item"></div>'. get_the_post_thumbnail( get_the_ID(), $image_size ) .'</a>';
				else :
					stevecadey_place_holder( $image_size );
				endif;
			endif;
		break;
		case 'audio':
			if ( 'link' == $audio['audio_format_source'] ) :
				stevecadey_place_holder( $image_size, true );
				stevecadey_audio( '[embed]' . esc_url( $audio['audio_url'] ) . '[/embed]' );
			elseif ( 'local' == $audio['audio_format_source'] ) :
				echo do_shortcode('[audio src="' . esc_url( $audio['audio_source_local']['url'] ) . '"/]');
			endif;
		break;
		case 'gallery':
			if ( count( $gallery['post_gallery'] ) > 0 && is_array( $gallery['post_gallery'] ) ) :
				echo '<div class="owl-carousel"
					data-items="1" data-autoPlay="false" data-margin="0" data-loop="true" data-nav="'. esc_attr( $gallery['gallery_navigation'] ) .'"
					data-dots="'. esc_attr( $gallery['gallery_pagination'] ) .'" data-mobile="1" data-tablet="1" data-desktop="1">';
					foreach ( $gallery['post_gallery'] as $slide ):
						if ( is_array( $slide ) && ! empty( $slide['ID'] ) ) :
							$image = wp_get_attachment_image( $slide['ID'], $image_thum ); ?>
							<div class="item">
								<?php echo ( $image ); ?>
							</div>

						<?php
						elseif ( ! empty( $slide ) ) :
							$image = wp_get_attachment_image( $slide, $image_thum ); ?>
							<div class="item">
								<?php echo ( $image ); ?>
							</div>
						<?php endif;

					endforeach;
				echo '</div>';
			else :
				the_post_thumbnail( $image_size );
			endif;
		break;
		case 'quote':
			include K2T_TEMPLATE_PATH . 'blog/content-quote.php';
			break;
		default:
			if ( is_single() ) {
				if ( has_post_thumbnail() ) :
					echo get_the_post_thumbnail( get_the_ID(), $image_size );
				else :
					stevecadey_place_holder( $image_size );
				endif;
			} else {
				if ( has_post_thumbnail() ) :
					echo '<a href="'. esc_url( get_permalink() ) .'"><div class="mask-item"></div>'. get_the_post_thumbnail( get_the_ID(), $image_size ) .'</a>';
				else :
					stevecadey_place_holder( $image_size );
				endif;
			}
	endswitch;	?>

</div><!--end:flx-entry-thumb-->