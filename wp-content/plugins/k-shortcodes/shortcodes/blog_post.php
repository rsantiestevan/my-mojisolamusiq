<?php
/**
 * Shortcode k2t slider.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_blog_post_shortcode' ) ) {
	function k2t_blog_post_shortcode( $atts, $content ) {
		$html = $i = $style = $cat = $column = $limit = $items = $items_desktop = $items_tablet = $items_mobile = $navigation = $pagination = $p_class = $anm = $anm_name = $anm_delay = $cat_anm = $exc_anm = $footer_anm = $thum_anm = $title_anm = $id = $class = '';
		extract( shortcode_atts( array(
			'style'         => 'blog-masonry',
			'column'		=> '2',
			'thumb_align'	=> 'top',
			'limit'	        => '-1',
			'cat'           => '',
			'slider'        => '',
			'items'         => '1',
			'items_desktop' => '1',
			'items_tablet'  => '1',
			'items_mobile'  => '1',
			'navigation'    => '',
			'pagination'    => '',
			'auto_play'     => '',
			'anm'           => '',
			'anm_name'      => '',
			'anm_delay'     => '',
			'id'            => '',
			'class'         => '',
		), $atts));

	if ( $anm ) {
		$anm        = ' animated';
		$data_name  = ' data-animation="' . $anm_name . '"';
		$data_delay = ' data-animation-delay="' . $anm_delay . '"';
	}

	$id    = ( $id != '' ) ? ' id="' . $id . '"' : '';
	$class  = ( $class != '' ) ? ' ' . $class . '' : '';
	$class .= ' ' . 'style-' . $style;

	if ( in_array( $style, array( 'blog-masonry', 'blog-grid', 'blog-medium', 'blog-large') ) ) 
	$class .= ' ' . 'k2t-blog'; // setup like blog taxonomy;
	if ( in_array( $style, array( 'blog-masonry', 'blog-time', 'blog-2'  ) ) ) $class .= ' ' . 'b-masonry';
	if ( $style == 'blog-grid' ) $class .= ' ' . 'b-grid';

	// Global variables
	global $post, $smof_data;

	// Filter post type
	$args = array(
		'post_type'      => 'post',
		'posts_per_page' => $limit,
		'cat'            => $cat,
	);

	// The query
	$blog = new WP_query( $args );


	switch ( $style ) : // set up like template blog for style large, medium 
		case 'blog-large':
			$html .= '<div class="b-large">';
			break;
		case 'blog-medium':
			$html .= '<div class="b-medium">';
			break;
	endswitch;

	$html .= '<div class="k2t-blog-post  '. $class .'">';

	// STYLES start ouput
	
	switch ( $style ) :
		case 'blog-grid-2':

			$html .= '<div class="grid-layout column-2">';
			$html .= '<div class="grid-sizer"></div>';
			break;
		case 'blog-2':

			wp_enqueue_script( 'k2t-furion-isotope' );
			wp_enqueue_script( 'k2t-furion-imagesloaded' );

			$html .= '<div class="masonry-layout column-2">';
			$html .= '<div class="grid-sizer"></div>';
			break;
		case 'blog-masonry':

			wp_enqueue_script( 'k2t-furion-isotope' );
			wp_enqueue_script( 'k2t-furion-imagesloaded' );
			$html .= '<div class="masonry-layout column-' . $column . '">';
			$html .= 	'<div class="grid-sizer"></div>';
			break;
		case 'blog-grid':

			wp_enqueue_script( 'k2t-furion-isotope' );
			wp_enqueue_script( 'k2t-furion-imagesloaded' );

			$html .= '<div class="grid-layout column-' . $column . '">';
			break;
		case 'blog-slider':

			wp_enqueue_script( 'k2t-furion-owlcarousel' );
			$html .= '<div class="owl-carousel' . $anm . '"' . $data_name . $data_delay . ' data-items="'. esc_attr( $items ) .'" data-autoPlay="'. esc_attr( ( $auto_play ) ? 'true' : 'false' ) .'" data-margin="30" data-nav="'. esc_attr( ( $navigation ) ? 'true' : 'false' ) .'" data-dots="'. esc_attr( ( $pagination ) ? 'true' : 'false' ) .'" data-mobile="'. esc_attr( $items_mobile ) .'" data-tablet="'. esc_attr( $items_tablet ) .'" data-desktop="'. esc_attr( $items_desktop ) .'">';
			break;
		default:
			# code...
			break;
	endswitch;

	if ( $slider ) {
		$html .= '<div class="owl-carousel" 
			data-items="'. esc_attr( $items ) .'" data-autoPlay="'. esc_attr( ( $auto_play ) ? 'true' : 'false' ) .'" data-margin="30" data-nav="'. esc_attr( ( $navigation ) ? 'true' : 'false' ) .'"
			data-dots="'. esc_attr( ( $pagination ) ? 'true' : 'false' ) .'" data-mobile="'. esc_attr( $items_mobile ) .'" data-tablet="'. esc_attr( $items_tablet ) .'" data-desktop="'. esc_attr( $items_desktop ) .'">';
	}
	

	while ( $blog->have_posts() ) : $blog->the_post();
		
		ob_start(); 

		switch ( $style ) :
			case 'blog-time':
				include  'blog/content-grid-2.php';
				break;
			case 'blog-2':
				include  'blog/content-grid.php';
				break;
			case 'blog-masonry':
				include  'blog/content-grid.php';
				break;
			case 'blog-grid':
				include  'blog/content-grid.php';
				break;
			case 'blog-medium':
				include  'blog/content-medium.php';
				break;
			case 'blog-large':
				include  'blog/content-large.php';
				break;
			case 'blog-slider':
				include  'blog/content-slider.php';
				break;
		endswitch;
		

		$html .= ob_get_clean();

	endwhile;
	
	
	//$html .= '</div>'; // End Masonry-layout, slider

	$html .= '</div>'; // end k2t-blog

	if ( in_array( $style, array( 'blog-large', 'blog-medium' ) ) ) 
	
	$html .= '</div>'; // End for large, medium style 

	$html = stevecadey_mini_output($html);

	// Restore original Post Data
	wp_reset_postdata();

	// Apply filters return
	$html = apply_filters( 'k2t_blog_post_return', $html );

	return $html;
	}
}