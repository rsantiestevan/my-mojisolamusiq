<?php
/**
 * Shortcode brands.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_brands_shortcode' ) ) {
	$html = $column = $enable_slider = $items_tablet = $items_mobile = $enable_nav = $align = $padding = $grayscale = $title = $tooltip = $link = $target = $tooltip = $class_tip = $anm = $anm_name = $anm_delay = $data_name = $id = $class = $style = '';
	function k2t_brands_shortcode( $args, $content ) {
		extract( shortcode_atts( array(
			'column'    => '0',
			'enable_column'  => '',
			'enable_slider'  => '',
			'items_tablet'  => '3',
			'items_mobile'  => '2',
			'enable_nav'  => '',
			'align'		=> 'align-left',
			'padding'   => '',
			'opacity_hover' => '',
			'grayscale' => 'false',
			'title'     => '',
			'tooltip'   => '',
			'link'      => '',
			'target'    => '',
			'tooltip'   => '',
			'anm'       => '',
			'anm_name'  => '',
			'anm_delay' => '2000',
			'id'        => '',
			'class'     => '',
		), $args ) );

		//Global $cl
		$cl = array( 'k2t-brands' );

		// Animation

		if ( ! preg_match_all( "/(.?)\[(brand)\b(.*?)(?:(\/ ) )?\]/s", $content, $matches ) ) :
			return do_shortcode( $content );
		else :
			//Numbers band element
			$number_band = count( $matches[0] );

		/*----------------Brands column-----------------*/

		if ( $column != '0' ) $cl[] = 'brands-'. $column .'-columns' . ' brand-grid';

		/*---------------- padding ----------------*/
		$style = isset( $style ) ? $style : ''; 

		if ( !empty( $padding ) ) $style = 'style="padding-right: ' . $padding . ';"' ; 


		/*----------------Grayscale----------------*/
		if ( trim( $grayscale ) == 'true' ) { $cl[] = 'enable-grayscale';}

		$cl[] = $opacity_hover;

		$cl[] =  $align;

		$cl[] = $class;


		// Slider 

		$data = $owl = '';

		if ( $enable_slider ) :
			$owl = 'owl-carousel';

			$data = 'data-items="' . $column . '" data-autoPlay="false" data-margin="0" data-loop="true" data-nav="'. $enable_nav .'"
					data-dots="false" data-mobile="' . $items_mobile . '" data-tablet="' . $items_tablet . '" data-desktop="' . $column . '"';

		endif;

		//

		$id    = ( $id != '' ) ? ' id="' . $id . '"' : '';

		$html = '<div ' . $id . ' class="' . implode( ' ', $cl ) . '">';

		$html .= do_action( 'k2t_brands_open' );

		$html .= '<div class="brand-table"><div class="brand-row ' . esc_attr( $owl ) . '" ' . $data . ' >';

		for ( $i = 0; $i < count( $matches[0] ); $i++ ):

			$matches[3][$i] = shortcode_parse_atts( $matches[3][$i] );

			$title     = isset( $matches[3][$i]['title'] ) ? trim( $matches[3][$i]['title'] ) : '';
			$tooltip   = isset( $matches[3][$i]['tooltip'] ) ? trim( $matches[3][$i]['tooltip'] ) : 'false';
			$link      = isset( $matches[3][$i]['link'] ) ? trim( $matches[3][$i]['link'] ) : '';
			$size      = isset( $matches[3][$i]['size'] ) ? trim( $matches[3][$i]['size'] ) : 'full';
			$target    = isset( $matches[3][$i]['target'] ) ? trim( $matches[3][$i]['target'] ) : '_self';
			$anm       = isset( $matches[3][$i]['anm'] ) ? trim( $matches[3][$i]['anm'] ) : '';
			$anm_name  = isset( $matches[3][$i]['anm_name'] ) ? $matches[3][$i]['anm_name'] : '';
			$anm_delay = isset( $matches[3][$i]['anm_delay'] ) ? trim( $matches[3][$i]['anm_delay'] ) : '';
			$data_name = $data_delay = '';
			if ( !empty( $anm ) ) {
				$anm        = !empty( $anm ) ? ' animated' : '';
				$data_name  = !empty( $anm_name ) ? ' data-animation="' . $anm_name . '"' : '';
				$data_delay = !empty( $anm_delay ) ? ' data-animation-delay="' . $anm_delay . '"' : '';
			}

			/*-----------Title-------------*/
			if ( $title == '' ) {
				$title_html = 'Brand '.( $i+1 ).' title';
				$alt_html = 'Brand '.( $i+1 );
			} else {
				$title_html = trim( $title );
				$alt_html = trim( $title );
			}

			/*-----------Get image-------------*/
			$img = '';
			if ( !empty( $link ) ){
				if ( is_numeric( $link ) ){
					$img_id = preg_replace( '/[^\d]/', '', $link );

					$img = wp_get_attachment_image( $img_id, $size );
				}
			}

			/*-----------Tooltip-------------*/
			if ( $tooltip == 'true' ) { $class_tip = ' class="hastip tooltip-top"'; wp_enqueue_script( 'k2t-tipsy' ); } else { $class_tip = '';}

			$html .= '<div class="brand-cell ' . $anm . '" ' . $data_name . ' ' . $data_delay . ' ' . $style . '>' . 
						'<div class="wrap-item"><div class="mask-item"></div>'. $img .'</div>' . 
					 '</div>';
		endfor;

		$html .= '</div>'; // end brand row
		$html .= '</div>'; // end brand table
		$html .= do_action( 'k2t_brands_close' );
		$html .= '</div>'; // end brand open

		//Apply filters return
		$html = apply_filters( 'k2t_brands_return', $html );

		return $html;

		endif;
	}
}
