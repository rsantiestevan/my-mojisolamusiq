<?php
/**
 * Shortcode brands.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_brand_shortcode' ) ) {
	$html = $title = $tooltip = $link = $size = $anm = $anm_name = $anm_delay = $data_name = $id = $class = '';
	function k2t_brand_shortcode( $args, $content ) {
		extract( shortcode_atts( array(
			'title'   		 => '',
			'tooltip' 		 => 'false',
			'link'  		 => '',
			'size'  		 => '',
			'anm'       	 => '',
			'anm_name'  	 => '',
			'anm_delay' 	 => '2000',
			'id'        	 => '',
			'class'          => '',
		), $args ) );

		$html  = '<div class="k2t-brand">';
		$html .= $tooltip ? '<div class="tooltip"><p>'. $title .'</p></div>' : '';
		$html .= $tooltip ? '<div class="mask-item"></div>' : '';
		$html .= wp_get_attachment_image( $link, $size );
		$html .= do_action( 'k2t_brand_close' );
		$html .= '</div>';

		//Apply filters return
		$html = apply_filters( 'k2t_brand_return', $html );

		return $html;
	}
}
