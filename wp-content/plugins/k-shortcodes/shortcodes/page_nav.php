<?php
/**
 * Shortcode page navigation.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_page_nav_shortcode' ) ) {
	function k2t_page_nav_shortcode( $atts, $content ) {
		$html = $style = $anm = $anm_name = $anm_delay = $data_name = $data_delay = $id = $class = '';
		extract( shortcode_atts( array(
			'style'		=> '',
			'right'     => '',
			'anm'       => '',
			'anm_name'  => '',
			'anm_delay' => '',
			'id'        => '',
			'class'     => '',
		), $atts ) );
		if ( $anm ) {
			$anm        = ' animated';
			$data_name  = ' data-animation="' . $anm_name . '"';
			$data_delay = ' data-animation-delay="' . $anm_delay . '"';
		}
		if ( !empty( $right )) $style = ' style="right: ' . $right . ';"';
		$class .= $anm;
		if ( !empty( $id ) ) $id = 'id="' . $id . '"';
		$html  = '<div ' . $id . ' class="page-nav-shortcode ' .  $class . '"' . $data_name . $data_delay . $style . ' "><ul>';
		$html .= do_shortcode( $content );
		$html .= '</ul></div>';

		$html = k2t_furion_mini_output( $html );
		//Apply filters return
		$html = apply_filters( 'k2t_page_nav_shortcode', $html );

		return $html;
	}
}
