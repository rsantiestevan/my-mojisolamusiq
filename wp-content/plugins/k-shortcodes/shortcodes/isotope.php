<?php
/**
 * Shortcode k2t slider.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_isotope_shortcode' ) ) {
	function k2t_isotope_shortcode( $atts, $content ) {
		$html = '';
		extract( shortcode_atts( array(
			'column' 	 => 'column-2',
			'column_md'  => 'column-md-2',
			'column_sm'  => 'column-sm-2',
			'column_xs'  => 'column-xs-2',
			'class'		 => '',
		), $atts));
		$classes = array();
		$classes[] = $class;
		$classes[] = $column;
		$classes[] = $column_md;
		$classes[] = $column_sm;
		$classes[] = $column_xs;

		wp_enqueue_script( 'k2t-furion-isotope' );
		wp_enqueue_script( 'k2t-furion-imagesloaded' );
		wp_enqueue_script( 'k2t-shortcodes' );

		// Generate random id
		$length = 10;
		$id     = substr( str_shuffle( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ), 0, $length );

		$html .= '<div class="k2t-shortcode-isotope ' . implode( ' ', $classes ) . '">' ;
		$html .= do_shortcode( $content );
		$html .= '</div>';
		
		//Apply filters return
		$html = apply_filters( 'k2t_isotope_return', $html );

		return $html;
	}
}