<?php
/**
 * Shortcode embed.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_k2t_divider_shortcode' ) ) {
	function k2t_k2t_divider_shortcode ( $atts, $content ) {
		$html = $width = $color = $align = $margin_top = $margin_bottom = $anm = $anm_name = $anm_delay = $data_name = $data_delay = $id = $class = '';
		extract( shortcode_atts( array(
			'direction' 	=> 'd-vertical',
			'style'			=> 'solid',
			'width'			=> '',
			'height'    	=> '',
			'color'			=> '',
			'align'			=> 'align-left',
			'margin_top'	=> '',
			'margin_bottom' => '',
			'anm'       	=> '',
			'anm_name'  	=> '',
			'anm_delay' 	=> '',
			'id'        	=> '',
			'class'     	=> '',
		), $atts ) );

		$classes = array();

		// k2t-divider css inline margin
		$css_margin = '';

		if ( $margin_top != '' || $margin_bottom != '' ) :
			$css_margin = 'style="';
			if ( $margin_top != '' ) $css_margin .= 'margin-top:' . $margin_top . ';';
			if ( $margin_bottom != '' ) $css_margin .= 'margin-bottom:' . $margin_bottom . ';';
			$css_margin .= '"'; // end css margin;
		endif;

		if ( $anm ) {
			$anm  = 'animated';
			$data_name  = !empty( $anm_name ) ? 'data-animation="' . $anm_name . '"' : '';
			$data_delay = !empty( $anm_delay ) ?  'data-animation-delay="' . $anm_delay . '"' : '';
		}
		$id    = ( $id != '' ) ? ' id="' . $id . '"' : '';

		// Set up class

		$classes[] = 'k2t-divider';

		$classes[] = $class;

		$classes[] = $align;

		$classes[] = $direction;

		// Set up dynamic style 

		$style_inline = 'style="'; //open
		$style_inline .= 'border-style: ' . $style . ';' ;

		switch ( $direction ) {
			case 'd-vertical':
				$style_inline .= !empty( $color ) ? 'border-color :' . $color . ';' : '';
				$style_inline .= !empty( $width ) ? 'border-left-width :' . $width . ';' : '';
				$style_inline .= !empty( $height ) ? 'padding-top :' . $height . ';' : '';   // height of divider
				break;
			case 'd-horizontal':
				$style_inline .= !empty( $color ) ? 'border-color :' . $color . ';' : '';
				$style_inline .= !empty( $height ) ? 'border-top-width :' . $height . ';' : '';
				$style_inline .= !empty( $width ) ? 'width :' . $width . ';' : '';			// width of divider
				break;
		}

		$style_inline .= '"'; // close style 

		// output
		$html  = '<div ' .  $id .  ' class="' . implode( ' ', $classes ) . '" ' . $css_margin . ' >';
		$html .= 	'<div class="item-divider ' . $anm . '" ' . $style_inline . $data_name . $data_delay . '>divider</div>';
		$html .= '</div>';

		//Apply filters return
		$html = apply_filters( 'k2t_divider_return', $html );

		return $html;
	}
}
