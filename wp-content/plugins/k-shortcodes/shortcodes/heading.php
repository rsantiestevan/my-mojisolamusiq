<?php
/**
 * Shortcode heading.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_heading_shortcode' ) ) {
	function k2t_heading_shortcode( $atts, $content ) {
		$html = $h = $align = $font = $subtitle = $border = $border_color = $font_size = $anm = $anm_name = $anm_delay = $data_name = $data_delay = $id = $class = '';
		extract( shortcode_atts( array(
			'h'         	=> 'h2',
			'align'     	=> 'left',
			'font'      	=> '',
			'font_size' 	=> '',
			'color'			=> '',
			'excerpt'		=> '',
			'ex_font_size'	=> '',
			'ex_color'		=> '',
			'border'    	=> 'false',
			'border_style'	=> '',
			'cb_width'		=> '',
			'cb_height'		=> '',
			'cb_position'	=> 'top',
			'cb_lr_top'		=> '',
			'cb_lr_padding'	=> '26',
			'cb_tb_top'		=> '',
			'cb_tb_bottom'	=> '',
			'border_color'	=> '',
			'icon'			=> '',
			'anm'       	=> '',
			'anm_name'  	=> '',
			'anm_delay' 	=> '',
			'id'        	=> '',
			'class'     	=> '',
		), $atts ) );

		// ENQUEUE 

		wp_enqueue_script('k2t-shortcodes');
		wp_enqueue_script('k2t-inview');

		//Global $cl and $style for css
		$cl = array( 'k2t-heading' );
		$style = array();
		$style_ex = array();

		if ( $anm ) {
			$anm        = ' animated';
			$data_name  = ' data-animation="' . $anm_name . '"';
			$data_delay = ' data-animation-delay="' . $anm_delay . '"';
		}

		$length        	= 10;
		$random_id 		= substr( str_shuffle( "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ), 0, $length );
		$id 			= !empty( $id ) ? $id : $random_id ;
		$class 			= ( $class != '' ) ? ' ' . $class . '' : '';

		/*-----------Align-------------*/
		if ( trim( $align ) == '' ) {$cl[] = 'align-left'; } else {$cl[] = 'align-' . trim( $align ); }

		/*-----------Font-------------*/
		if ( $font ) {
			$font_class = ' has_font';
			$protocol = is_ssl() ? 'https' : 'http';
			wp_enqueue_style( 'k2t-google-font-' . str_replace( ' ', '-', $font ), "$protocol://fonts.googleapis.com/css?family=" . str_replace( ' ', '+', $font ) . ":100,200,300,400,500,600,700,800,900&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			$style[] = 'font-family: "' . $font . '"';
			$style_ex[] = 'font-family: "' . $font . '";';
		}

		/*-----------Color-------------*/
		if ( ! empty( $color ) ) {
			$style[] = 'color: ' . $color . '';
		}

		/*-----------Font size-------------*/
		if ( $h == 'custom' ) { $style[] = 'font-size: ' . trim( $font_size ); }

			/*-----------H tag and subtitle-------------*/
		if ( !in_array( trim( $h ), array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ) ) ) { $h = 'h2'; } else {$h = trim( $h ); }


		/*-----------Border-------------*/
		if ( trim( $border ) == 'true' || $border == '1' ) { $cl[] = 'has-border';} else {$cl[] = 'no-border'; }


		/*-----------Subtitle-------------*/

		if ( trim( $excerpt ) == '' ) {
			$subtitle_render = ''; $cl[] = 'no-subtitle'; } 
		else {
			if ( !empty( $ex_color )  ) $style_ex[] = 'color: ' . $ex_color . ';';
			if ( !empty( $ex_font_size ) ) $style_ex[] = 'font-size: ' . $ex_color . ';';
			$subtitle_render = '<p class="subtitle" style="' . implode( '', $style_ex) . '">' . $excerpt . '</p>'; $cl[] = 'has-subtitle';
		}

		/*-----------Border style-------------*/
		$border_style_html = ( trim( $border ) == 'true' ) ? '<div class="has-boder-style"><span></span></div>' : '';
		$border_style_html = ( trim( $border ) == 'true' && ( $border_style == 'bottom_icon' || $border_style == 'boxed_heading' ) ) ? '<div class="has-boder-style"><span><i></i></span><span><i class="'. ( !empty( $icon ) ? $icon : 'fa fa-star' ) .'"></i></span><span><i></i></span></div>' : $border_style_html;


		/*------------ Custom Border -------------- */
		
		$top_cb = $right_cb = $bottom_cb = $left_cb = '';

		if ( $border_style == 'custom_line' ) :
			$cl[] = 'cb-' . $cb_position;

			// set up style 
			$style_cb = array();
			if ( !empty( $cb_width ) ) $style_cb[] = 'width:' . $cb_width;  
			if ( !empty( $cb_height ) ) $style_cb[] = 'height:' . $cb_height;  
			if ( !empty( $border_color ) ) $style_cb[] = 'background-color:' . $border_color;  			
			switch ( $cb_position ) :
				case 'top':
					if ( !empty( $cb_tb_top ) ) $style_cb[] = 'margin-top:' . $cb_tb_top;  
					if ( !empty( $cb_tb_bottom ) ) $style_cb[] = 'margin-bottom:' . $cb_tb_bottom;  
					$top_cb = '<div class="wrap-custom-line"><p class="custom-line" style="' . implode(';', $style_cb ) . '">divider</p></div>';//output
					break;
				case 'right':
					if ( !empty( $cb_lr_padding ) ) $tmp = intval( str_replace( 'px', '', $cb_lr_padding) ); // spacing beetwen border-text for lert,right position
					$left_cb = '<span class="custom-line" style="' . implode(';', $style_cb ) . '">divider</span>'; //output
					if ( ! empty( $cb_width ) ) $style[] = 'padding-right:' . ( intval(str_replace('px', '', $cb_width)) + $tmp ) . 'px';
					break;
				case 'bottom':
					if ( !empty( $cb_tb_top ) ) $style_cb[] = 'margin-top:' . $cb_tb_top;  
					if ( !empty( $cb_tb_bottom ) ) $style_cb[] = 'margin-bottom:' . $cb_tb_bottom; 
					$bottom_cb = '<div class="wrap-custom-line"><p class="custom-line" style="' . implode(';', $style_cb ) . '">divider</p></div>';//output
					break;
				case 'left':
					if ( !empty( $cb_lr_padding ) ) $tmp = intval( str_replace( 'px', '', $cb_lr_padding) ); // spacing beetwen border-text for lert,right position
					$left_cb = '<span class="custom-line" style="' . implode(';', $style_cb ) . '">divider</span>'; //output
					if ( ! empty( $cb_width ) ) $style[] = 'padding-left:' . ( intval(str_replace('px', '', $cb_width)) + $tmp ) . 'px';
					break;
			endswitch;
		endif;
		//Add class $h to $cl
		$cl[] = $h;
		$cl[] = $border_style;

		//Apply filters to cl
		$cl = apply_filters( 'k2t_heading_classes', $cl );

		//Join cl class and style
		$cl = join( ' ', $cl );
		$style = join( '; ', $style );
		if ( !empty( $style ) ) { $style_html = ' style="' . esc_attr( trim( $style ) ) . '"'; } else { $style_html = ''; }

		$html = '<div '. $data_name . $data_delay .' id="'. $id .'" class="' . trim( $cl ). $anm . $class . '">';
		$html .= do_action( 'k2t_heading_open' );
		$html .= '<div class="k2t-heading-inner">' . $top_cb ;
		$html .= '<div class="text"><' . $h . ' class="heading"' . $style_html . '>' . $left_cb . '<span>' . do_shortcode( $content ) . '</span></' . $h . '>' . $right_cb . $bottom_cb . $border_style_html . $subtitle_render . '</div>';
		$html .= '</div>';
		$html .= do_action( 'k2t_heading_close' );
		$html .= '</div>';

		// Border color
		if ( ! empty( $border_color ) ) {
			$html .= '<style>';
			$html .= '#'. $id .'.k2t-heading.has-border .h:before, #'. $id .'.k2t-heading.has-border .h:after { color: '. $border_color .' !important;}';
			$html .= '</style>';
		}
		//Apply filters return
		$html = apply_filters( 'k2t_heading_return', $html );

		return $html;
	}
}
