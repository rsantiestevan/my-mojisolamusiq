<?php
/**
 * Shortcode testimonial.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */


if ( ! function_exists( 'k2t_testimonial_shortcode' ) ) {
	function k2t_testimonial_shortcode( $atts, $content ) {
		$html = $image_html = '';
		extract( shortcode_atts( array(
			'style'			=> 'style-1',
			'align'			=> 'align-center',
			'color'			=> '',
			'anm'      	 	=> '',
			'anm_name'  	=> '',
			'anm_delay' 	=> '',
			'class'         => '',
		), $atts));


		if ( $style == 'style-3') :
			wp_enqueue_script( 'isotope-js');
			wp_enqueue_script( 'jquery-imagesloaded' );
		else: 
			wp_enqueue_script( 'k2t-owlcarousel' );
		endif;

		if ( $anm ) {
			$anm        = ' animated ';
			$data_name  = ' data-animation="' . $anm_name . '"';
			$data_delay = ' data-animation-delay="' . $anm_delay . '"';
		}

		$class .= ' ' . $align;

		// owl

		//var_dump( $style );

		$items = ( $style == 'style-2' ) ? 2 : 1 ;

		$owl_magin = ( $style == 'style-2' ) ? 30 : 0 ;

		// Generate random id
		$length = 10;
		$id     = substr( str_shuffle( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ), 0, $length );

		$html .= '<div class="k2t-testimonial ' . $anm . $style .'"' . $data_delay . $data_name . '>';

		if ( $style == 'style-3' ) :

			$html .= '<div class="k2t-testi-isotope">';

		else : 

			$html .= '<div id="' . $id . '" class="owl-carousel ' . $class . '"' .
						' data-items="' . $items . '"' .
						' data-autoPlay="false"' .
						' data-margin="' . $owl_magin . '"' . 
						' data-loop="false"' . 
						' data-nav="false"' .
						' data-dots="true"' .
						' data-mobile="1"' . 
						' data-tablet="' . $items . '"' .
						' data-desktop="1"' .
						' data-URLhashListener="true">';
	
		endif;

		if (!preg_match_all("/(.?)\[(testi)\b(.*?)(?:(\/))?\](?:(.+?)\[\/testi\])/s", $content, $matches)) :
			return do_shortcode($content);
		else :
			for($i = 0; $i < count($matches[0]); $i++):

				$testi_obj 				= shortcode_parse_atts($matches[3][$i]);
				$testi_content 			= $matches[5][$i];
				$image_id				= $testi_obj['image'];
				$img_html 				= '';
				if ( !empty( $image_id ) ) $img_html = wp_get_attachment_image( $image_id, 'full' );

				if ( $style == '1') :
					$html .=	'<div data-hash="testi-'. esc_attr( $i ) .'">' .
									'<div class="testi-avatar">' . $img_html . '</div>' .
									'<div class="testimonial-content"><div class="speech">' . do_shortcode( $testi_content ) . '</div>' . 
									'</div>' . // end content
									'<div class="testimonial-info">' .
										'<div class="testimonial-name">'. esc_html( $testi_obj['name'] ) .'</div>' .
										'<div class="testimonial-job">'. esc_html( $testi_obj['job'] ) .'</div>' .
									'</div>' . // end info
								'</div>'; // end data
				else :
					if ( $i < 4 && $style == 'style-3' ) $filter = 'testi-show'; else $filter = '';
					$html .=	'<div class="testi-item ' . $filter . '" data-hash="testi-'. esc_attr( $i ) .'"><div>' .
									'<div class="testi-avatar">' . $img_html . '</div>' .
									'<div class="testimonial-info">' .
										'<div class="testimonial-name">'. esc_html( $testi_obj['name'] ) .'</div>' .
										'<div class="testimonial-job">'. esc_html( $testi_obj['job'] ) .'</div>' .
									'</div>' . // end info
									'<div class="testimonial-content"><div class="speech">' . do_shortcode( $testi_content ) . '</div>' . 
									'</div>' . // end content
								'</div></div>'; // end data
				endif;

			endfor;
			$html .= '</div>';

			// 
			if ( ! empty( $image_html ) ) {
				$html .= '<div class="testimonial-image">'. $image_html .'</div>';
			}

		endif;

		if ( $style == 'style-3' ) $html .= '<div class="filter-all"><a class="k2t-btn" href="#">' . esc_html__('View all comment','k-shortcodes') . '</a></div>';

		$html .= '</div>';
		
		//Apply filters return
		$html = apply_filters( 'k2t_k2t_slider_return', $html );

		return $html;
	}
}
