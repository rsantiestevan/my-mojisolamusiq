<?php
/**
 * Shortcode page navigation item.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_page_nav_item_shortcode' ) ) {
	function k2t_page_nav_item_shortcode( $atts, $content ) {
		$html = $style = $anm = $anm_name = $anm_delay = $data_name = $data_delay = $id = $class = '';
		extract( shortcode_atts( array(
			'id_link'	=> '',
			'id'        => '',
			'class'     => '',
		), $atts ) );
		$id_all = 'id="pni-' . $id_link;
		if ( !empty( $id ) ) $id_all .= ' ' . $id . '"'; else $id_all .= '"';
		$html = '<li><a ' . $id_all . ' class="nav-item-shortcode" href="#' . $id_link . '"><span>item</span></a></li>';

		//Apply filters return
		$html = apply_filters( 'k2t_page_nav_item_shortcode', $html );

		return $html;
	}
}
