<?php
/**
 * Shortcode embed.
 *
 * @since  1.0
 * @author SunriseTheme
 * @link   http://www.sunrisetheme.com
 */

if ( ! function_exists( 'k2t_k2t_social_shortcode' ) ) {
	function k2t_k2t_social_shortcode( $atts, $content ) {
		$html = $align = $anm = $anm_name = $anm_delay = $data_name = $data_delay = $id = $class = '';
		extract( shortcode_atts( array(
			'title' 	=> '',
			'align'     => '',
			'anm'       => '',
			'anm_name'  => '',
			'anm_delay' => '',
			'id'        => '',
			'class'     => '',
		), $atts ) );

		$cl = array( 'media-container' );

		if ( $anm ) {
			$anm        = ' animated';
			$data_name  = ' data-animation="' . $anm_name . '"';
			$data_delay = ' data-animation-delay="' . $anm_delay . '"';
		}
		$id    = ( $id != '' ) ? ' id="' . $id . '"' : '';
		$class = ( $class != '' ) ? ' ' . $class . '' : '';


		$cl = join( ' ', $cl );
		ob_start();
		echo '<div class="wrap-social" style="text-align : ' . $align . '">';
		if ( ! empty( $title ) )
			echo '<div class="title"><h4>' . $title .'</h4></div>';
		if ( function_exists( 'stevecadey_social_list' ) ) stevecadey_social_list();
		echo '</div>';
		$html = ob_get_clean();
		//Apply filters return
		$html = apply_filters( 'k2t_k2t_social_return', $html );

		return $html;
	}
}
