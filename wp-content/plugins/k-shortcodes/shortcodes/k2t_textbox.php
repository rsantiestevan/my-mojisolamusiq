<?php
/**
 * Shortcode embed.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_k2t_textbox_shortcode' ) ) {
	function k2t_k2t_textbox_shortcode ( $atts, $content ) {
		$html = $width = $anm = $anm_name = $anm_delay = $data_name = $data_delay = $id = $class = '';
		extract( shortcode_atts( array(
			'anm'       => '',
			'anm_name'  => '',
			'anm_delay' => '',
			'id'        => '',
			'class'     => '',
		), $atts ) );

		$cl = array( 'k2t-text-box' );

		if ( $anm ) {
			$anm        = ' animated';
			$data_name  = ' data-animation="' . $anm_name . '"';
			$data_delay = ' data-animation-delay="' . $anm_delay . '"';
		}
		$id    = ( $id != '' ) ? ' id="' . $id . '"' : '';
		$class = ( $class != '' ) ? ' ' . $class . '' : '';

		/*-------------Style--------------*/

		//Apply filters to cl
		$cl = apply_filters( 'k2t_k2t_textbox_classes', $cl );
		//Join $cl
		$cl = join( ' ', $cl );

		//Check content is url or iframe
		$html = '<div class="' . trim( $cl ) . $anm . $class . '"' . $data_name . $data_delay . $id . '>';
		$html .= do_shortcode(( $content ));
		$html .= '</div>';

		//Apply filters return
		$html = apply_filters( 'k2t_k2t_textbox_return', $html );

		return $html;
	}
}
