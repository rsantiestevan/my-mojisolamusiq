<?php
/**
 * Shortcode pricing.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_pricing_column_shortcode' ) ) {
	function k2t_pricing_column_shortcode( $atts, $content ) {
		$html = $title = $sub_title = $image = $price = $unit = $link = $featured_list = $id = $class = $anm = $anm_name = $anm_delay = '';
		extract( shortcode_atts( array(
			'title' 			=> '',
			'sub_title'         => '',
			'image'  			=> '',
			'old_price' 		=> '',
			'price'				=> '',
			'unit'        		=> '',
			'price_per'			=> '',
			'link'     			=> '',
			'link_text'     	=> '',
			'target'     		=> '_blank',
			'featured_list' 	=> '',
		), $atts ) );

		wp_enqueue_script( 'k2t-countTo' );
		wp_enqueue_script( 'k2t-inview' );

		//Global $cl
		$cl = array( 'k2t-pricing' );
		
		if ( $anm ) {
			$anm        = ' animated';
			$data_name  = ' data-animation="' . $anm_name . '"';
			$data_delay = ' data-animation-delay="' . $anm_delay . '"';
		}
		$id    = ( $id != '' ) ? ' id="' . $id . '"' : '';
		$class = ( $class != '' ) ? ' ' . $class . '' : '';

		//Apply filters to cl
		$cl = apply_filters( 'k2t_pricing_classes', $cl );

		//Join cl class
		$cl = join( ' ', $cl );
		$image_html = $price_html = $old_price_html = $unit_html = $button_html  = '';
		$html = '';
		$html .= do_action( 'k2t_pricing_item_open' );
			if ( !empty($image) )
				$image_html = '<div class="pricing-image">' . wp_get_attachment_image( $image ) . '</div>';
			$price_html = !empty( $price ) ? '<span>' . $price . '</span>' : '';
			$unit_html = !empty( $unit ) ? '<sup>'. $unit . '</sup>' : '';
			$price_per_html = !empty( $price_per ) ? '<span class="pricer-per">' . $price_per : '</span>';
			$old_price_html = !empty( $old_price ) ? '<div class="old-price">'. $unit_html . '<del>' . $old_price . '</del>' . $price_per_html . '</div>' : '' ;
			$button_html =  '<button>' . 
								'<a href="' . $link . '" target="' . $target . '">' . $link_text . '</a>' . 
						    '</button>';

		// OUT PUT HTML 

        $html .='<div class="pricing-column k2t-counter" data-delay="0">';
			$html .='<div class="wrap-pricing-item counter-inner">';
				$html .='<div class="pricing-header">';
					$html .= $image_html;
					$html .='<h3 class="pricing-title">';
						$html .=$title;
					$html .='</h3>';
					$html .=$old_price_html;
					$html .='<div class="pricing-sub_title">';
						$html .=$sub_title;
					$html .='</div>';
					$html .='<span class="price">';
						$html .=$unit_html;
					$html .='</span>';
					$html .='<span class="price number" data-from="0" data-to="'.$price.'" data-speed="2000" data-delayrefresh-interval="10">';
						$html .=$price_html;
					$html .='</span>';
					/*$html .='<span class="price">';
						$html .=$price_per_html;
					$html .='</span>';*/
				$html .='</div>';
				$html .='<div class="description">';
					$html .=do_shortcode( $content );
				$html .='</div>';
				$html .='<div class="pricing-footer">';
					$html .=$button_html;
				$html .='</div>';
			$html .='</div>';
		$html .='</div>';
		
        $html .= do_action( 'k2t_pricing_item_close' );
        //Apply filters return
		$html = apply_filters( 'k2t_pricing_item_return', $html );
		return $html;
	}
}
