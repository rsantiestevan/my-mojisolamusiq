<?php
/* ------------------------------------------------------- */
/* Carousel
/* ------------------------------------------------------- */
if (!function_exists('k2t_carousel_shortcode')){
	function k2t_carousel_shortcode($atts,$content){
		extract(shortcode_atts(array(
			'column'			=> '4',
			'desktop'			=> '3',
			'tablet'			=> '2',
			'mobile'			=> '1',
			'pager'				=> 'true',
			'navi'				=> 'true',
			'loop'				=> 'true',
		), $atts));
		
		//Global $cl and $data
		$cl = array('k2t-swiper-slider');
		$data = array();
		
		$cl[] = 'owl-carousel';

		var_dump( $column );
		
		/*--------------Column--------------*/
		if( in_array( trim($column), array('1','2','3','4','5','6','7') ) ) $data[] = 'data-items="'.trim($column).'"';

		/*--------------desktop--------------*/

		if( in_array( trim( $desktop ), array('1','2','3','4','5','6','7') ) ) $data[] = 'data-desktop="'.trim($column).'"'; 

		/*--------------tablet--------------*/
		
		if( in_array( trim( $tablet ), array('1','2','3','4','5','6','7') ) ) $data[] = 'data-tablet="'.trim($column).'"';

		/*--------------mobile--------------*/
		
		if( in_array( trim( $mobile ), array('1','2','3','4','5','6','7') ) ) $data[] = 'data-mobile="'.trim($column).'"';
		
		/*--------------Auto--------------*/
		if(trim($auto) != 'true'){$data[] = 'data-auto="false"';} else { $data[] = 'data-auto="true"';}
		
		/*--------------Auto time--------------*/
		if(is_numeric(trim($auto_time))){$data[] = 'data-auto-time="'.trim($auto_time).'"';} else { $data[] = 'data-auto-time="5000"';}
		
		/*--------------Speed--------------*/
		if(is_numeric(trim($speed))){$data[] = 'data-speed="'.trim($speed).'"';} else { $data[] = 'data-speed="5000"';}
		
		/*--------------Pager--------------*/
		if(trim($pager) != 'false'){$data[] = 'data-pager="true"';} else { $data[] = 'data-pager="false"';}
		
		/*--------------Navi--------------*/
		if(trim($navi) != 'false'){$data[] = 'data-nav="true"';} else { $data[] = 'data-nav="false"';}
		
		/*--------------Loop--------------*/
		if(trim($loop) != 'false'){$data[] = 'data-loop="true"';} else { $data[] = 'data-loop="false"';}
		
		

		if (!preg_match_all("/(.?)\[(carousel_item)\b(.*?)(?:(\/))?\](?:(.+?)\[\/carousel_item\])?(.?)/s", $content, $matches)) :
			return do_shortcode($content);
		else :
			
			//Apply filters to cl
			$cl = apply_filters('k2t_carousel_classes',$cl);
			
			//Join cl class
			$cl = join(' ', $cl);
			
			//Join $data
			$data = join(' ', $data);
						
			$return  = '<div class="'.trim($cl).'" '.trim($data).'>' ;
			$return .= do_action('k2t_carousel_open');
			$return .= do_shortcode( $content );
			$return .= do_action('k2t_carousel_close');
			$return .= '</div>';
			
			//Apply filters return
			$return = apply_filters('k2t_carousel_return',$return);
			
			return $return;
			
		endif;
	}
}