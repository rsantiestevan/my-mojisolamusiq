<?php
/**
 * Shortcode google map.
 *
 * @since  1.0
 * @author LunarTheme
 * @link   http://www.lunartheme.com
 */

if ( ! function_exists( 'k2t_google_map_shortcode' ) ) {
	function k2t_google_map_shortcode( $args, $content=NULL ) {
		$html = $lat = $lon = $z = $w = $h = $maptype = $mapstype = $address = $marker = $markerimage = $traffic = $draggable = $infowindow = $infowindowdefault = $hidecontrols = $scrollwheel = $color = $id = $class = $anm = $anm_name = $anm_delay = '';
		extract( shortcode_atts( array(
			'lat'               => '0',
			'geo_title'			=> '',
			'geo'               => '',
			'lon'               => '0',
			'id'                => '',
			'z'                 => '1',
			'w'                 => '600',
			'h'                 => '400',
			'maptype'           => 'ROADMAP',
			'mapstype'			=> '',
			'address'           => '',
			'marker'            => 'no',
			'markerimage'       => '',
			'traffic'           => 'no',
			'draggable'         => 'false',
			'infowindow'        => '',
			'infowindowdefault' => 'yes',
			'hidecontrols'      => 'false',
			'scrollwheel'       => 'false',
			'color'             => '',
			'anm'      			=> '',
			'anm_name'  		=> '',
			'anm_delay' 		=> '',
			'class'             => '',
		), $args ) );

		$id    = ( !empty( $id ) ) ? $id : 'map_'.rand();

		// get multi geo title to array 

		if ( !empty( $geo_title ) ) :
			$arr_title = explode(';', $geo_title);
		endif;

		for  ( $i = 0; $i < count( $arr_title ); $i++ ) {
			$arr_title[ $i ] = '"' . $arr_title[ $i ] . '"';
		}

		if ( !empty( $geo ) ) :

			// extract String geo to array;

			$arr_geo = explode(	';', $geo );

			foreach ($arr_geo as $key => $value) {
				$arr_geo[$key] = explode( ',', $value );
			}

			// move geo title , geo to one array

			for( $i = 0; $i < count($arr_geo); $i++) {
				if ( isset( $arr_title[$i] ) ) :
					array_unshift( $arr_geo[ $i ], $arr_title[$i] );
				else:
					array_unshift( $arr_geo[ $i ], '\'' . 'Marker' . '\'' );
				endif;
			}

			// creat string array pass to js
			// creat list of location

			$list = '<ul class="map-locations">';
			$map_select = '<div class="wrap-map-select"><select class="map-select">';
			$str_arr_markers = '[';
			$i = 0;	
			foreach ($arr_geo as $k => $geo_v) {
				$list 		 	 .= '<li><a href="' . '#' . '"' . ' id="loc_' . $id . '_' . $i . '" data-link="' . $i .'">';
				$map_select  	 .= '<option value="' . $i .'">';
				$str_arr_markers .= '[';
				$j = 0;
				foreach ($geo_v as $key => $value) {
					if ( $j == 0 ) $list 		.= trim(rtrim($value, '"'), '"');
					if ( $j == 0 ) $map_select  .= trim(rtrim($value, '"'), '"');
					
						$str_arr_markers .= $value . ',';
					$j++;
				}
				if ( $i == count( $arr_geo ) - 1 )
					$str_arr_markers .= ']';
				else
					$str_arr_markers .= '],';
				$list 		.= '</a></li>';
				$map_select .= '</option>';
				$i++;
			};
			$list .= '<li><a href="' . '#' . '"' . ' id="loc_' . $id . '_' . $i . '" data-link="' . $i .'">Zoom Out </a></li></ul>';
			$map_select .= '<option value="' . $i .'">Zoom Out</option></select><i class="zmdi zmdi-chevron-down"></i></div>';

			$str_arr_markers .= ']';

			$popup = '<div class="map-pop-up">' . do_shortcode( $content ) . $map_select . 
					'<a id="loc_' . $id . '" class="k2t-btn map-btn" data-link="0">' . esc_html__( 'Go', 'k-shortcodes') . '</a>' . 
					'<a class="open-btn"><i class="zmdi zmdi-search"></i></a>' .
				 '</div>';

			$popup = shortcode_unautop( $popup );

		endif;

		$data_name = $data_delay = '';

		if ( $anm ) {
			$class .= ' animated';
			$data_name  = !empty( $anm_name ) ? 'data-animation="' . $anm_name . '"' : '';
			$data_delay = !empty( $anm_delay ) ?  'data-animation-delay="' . $anm_delay . '"' : '';
		}

		$w = is_numeric( $w ) ? 'width:'. $w .'px;' : 'width:'. $w .';';
		$h = is_numeric( $h ) ? 'height:'. $h .'px;' : 'height:'. $h .';';
		$html = '<div class="k2t-wrap-map">' . $popup . '<div class="k2t-map ' . $class . '" id="k2t_' . $id . '" style="' . $w . $h . '"' . $data_name . $data_delay . ' data-id="' . $id .'"></div></div>';

		$gmap .= '<script>
		(function($) {
		$(document).ready(function() {';
		if ( !empty( $str_arr_markers ) ) :
			$gmap .= 'var markers = ' . $str_arr_markers . ';';
		endif;
		$gmap .= 'var myOptions = {
			zoom: ' . $z . ',
			center: new google.maps.LatLng(-33.92, 151.25),
			mapTypeId: google.maps.MapTypeId.' . $maptype . ',';

		if ( $scrollwheel == 'true' ) {
			$gmap .= 'scrollwheel: true,';
		}else{
			$gmap .= 'scrollwheel: false,';
		}
		if ( !empty( $hidecontrols ) ) {
			$gmap .= 'disableDefaultUI: "' . $hidecontrols . '",';
		}
		if ( $draggable == 'true' ) {
			$gmap .= 'draggable: true,';
		}else{
			$gmap .= 'draggable: false,';
		}
		if ( !empty( $color ) ) {
			$gmap .= 'backgroundColor: "' . $color . '",';
		}
		switch ( $mapstype ) {
			case 'grayscale':
				$gmap .= 'styles: [{"featureType": "landscape","stylers": [{"saturation": -100},{"lightness": 65},{"visibility": "on"}]},
					{"featureType": "poi","stylers": [{"saturation": -100},{"lightness": 51},{"visibility": "simplified"}]},
					{"featureType": "road.highway","stylers": [{"saturation": -100},{"visibility": "simplified"}]},
					{"featureType": "road.arterial","stylers": [{"saturation": -100},{"lightness": 30},{"visibility": "on"}]},
					{"featureType": "road.local","stylers": [{"saturation": -100},{"lightness": 40},{"visibility": "on"}]},
					{"featureType": "transit","stylers": [{"saturation": -100},{"visibility": "simplified"}]},
					{"featureType": "administrative.province","stylers": [{"visibility": "off"}]},
					{"featureType": "water","elementType": "labels","stylers": [{"visibility": "on"},{"lightness": -25},{"saturation": -100}]},
					{"featureType": "water","elementType": "geometry","stylers": [{"hue": "#ffff00"},{"lightness": -25},{"saturation": -97}]
				}]';
				break;
			case 'blue_water':
				$gmap .= 'styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{}]},
					{"featureType":"landscape","elementType":"all","stylers":[{}]},
					{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},
					{"featureType":"road","elementType":"all","stylers":[]},
					{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"on"}]},
					{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"on"}]},
					{"featureType":"transit","elementType":"all","stylers":[{"visibility":"on"}]},
					{"featureType":"water","elementType":"all","stylers":[{"color":"#afe0ff"},{"visibility":"on"}]
				}]';
				break;
			case 'pale_dawn':
				$gmap .= 'styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},
					{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},
					{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},
					{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},
					{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},
					{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},
					{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},
					{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},
					{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]
				}]';
				break;
			case 'shades_of_grey':
				$gmap .= 'styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},
					{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},
					{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
					{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},
					{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},
					{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},
					{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},
					{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},
					{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},
					{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},
					{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},
					{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},
					{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]
				}]';
				break;

			case 'stevecadey':
				$gmap .= 'styles: [{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},
				{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"off"}]},
				{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#f49935"}]},
				{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"}]},
				{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#fad959"}]},
				{"featureType":"road.arterial","elementType":"labels","stylers":[{"visibility":"off"}]},
				{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"}]},
				{"featureType":"road.local","elementType":"labels","stylers":[{"visibility":"simplified"}]},
				{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
				{"featureType":"water","elementType":"all","stylers":[{"hue":"#b1d8f4"}]}]';

			default:

				break;
		}
		$gmap .= '};
		var ' . $id . ' = new google.maps.Map(document.getElementById("k2t_' . $id . '"), myOptions);';
		//traffic
		if ( $traffic == 'true' ) {
			$gmap .= '
			var trafficLayer = new google.maps.TrafficLayer();
			trafficLayer.setMap(' . $id . ');
			';
		}
		//address
		if ( !empty( $address ) ) {
			$gmap .= 'var geocoder_' . $id . ' = new google.maps.Geocoder();
			var address = \'' . $address . '\';
			geocoder_' . $id . '.geocode( { \'address\': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					' . $id . '.setCenter(results[0].geometry.location);';

			if ( $marker == 'true' ) {
				//add custom image
				if ( !empty( $markerimage ) && is_numeric( $markerimage ) ){
					$gmap .= 'var image = "'. wp_get_attachment_url( $markerimage ) .'";';
				}elseif( !empty( $markerimage ) ){
					$gmap .= 'var image = "'. $markerimage .'";';
				}
				$gmap .= '
						var marker = new google.maps.Marker({
							map: ' . $id . ',
							';
				if ( !empty( $markerimage ) ) {
					$gmap .= 'icon: image,';
				}
				$gmap .= 'position: ' . $id . '.getCenter()});';
				//infowindow
				if ( !empty( $infowindow ) ) {
					//first convert and decode html chars
					$thiscontent = htmlspecialchars_decode( $infowindow );
					$gmap .= '
							var contentString = \'' . $thiscontent . '\';
							var infowindow = new google.maps.InfoWindow({
								content: contentString
							});
							google.maps.event.addListener(marker, \'click\', function() {
							  infowindow.open(' . $id . ',marker);});';
					//infowindow default
					if ( $infowindowdefault == 'true' ) {
						$gmap .= '
									infowindow.open(' . $id . ',marker);
								';
					}
				}
			}
			$gmap .= '} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
			});
			';
		}

		if ( !empty( $str_arr_markers ) ) {
			if ( !empty( $markerimage ) ){
				$gmap .= 'var image = "'. wp_get_attachment_url( $markerimage ) .'";';
			}

			$gmap .= 'var bounds = new google.maps.LatLngBounds();';
			$gmap .= 'var infowindow = new google.maps.InfoWindow();';
			$gmap .=
			'var locations = [];
			for(var i = 0; i < markers.length; i++ ) {
		        position = new google.maps.LatLng( markers[i][1], markers[i][2]);
		        marker = new google.maps.Marker({
		            position: position,
		            map: ' . $id . ',';
		     		if ( !empty( $markerimage ) ) $gmap .= 'icon: image,';
		    $gmap.= 'animation: google.maps.Animation.DROP,
		            title: markers[i][0],
		        });
		        locations.push(markers);
		        bounds.extend(marker.getPosition());
		        google.maps.event.addListener(marker, \'click\', (function(marker, i) {
			        return function() {
			          infowindow.setContent(markers[i][0]);
			          infowindow.open(' . $id . ', marker);
			        }
			    })(marker, i));4
		    }
		    var link_btn = "' . 'loc_' . $id . '";
		    	google.maps.event.addDomListener(document.getElementById( link_btn ), \'click\', function(e) {
		    		var loc = $(this).attr("data-link");
		    		var latlong = new google.maps.LatLng(markers[loc][1], markers[loc][2]);
			        ' . $id . '.setCenter( latlong );
			        var listener = google.maps.event.addListener('.$id.', "idle", function() { 
					  if ('.$id.'.getZoom() < '.$z.') '.$id.'.setZoom('.$z.'); 
					  google.maps.event.removeListener(listener); 
					});
					return fail;
			    });
		    ';
		    $gmap .= $id . '.fitBounds(bounds);
		    google.maps.event.addDomListener(document.getElementById( link_btn ), \'click\', function(e) {
	    		var loc = $(this).attr("data-link");
	    		if ( loc >= markers.length ) '.$id . '.fitBounds(bounds);
		    });
		    ';
		}
		//marker: show if address is not specified


		$gmap .= '});})(jQuery);</script>';

		//Apply filters return
		$gmap = apply_filters( 'k2t_google_map_return', $gmap );

		echo $gmap;

		return $html;
	}
}
