
function isotopeEvent(target){
    if(jQuery('.event-grid-style').length){
        var $container = jQuery('.event-grid-style');
        var $this = jQuery(this);
        $this.imagesLoaded( function() {
            $container.isotope({
                itemSelector: '.event-classic-item',
                layoutMode: 'fitRows',
                filter: target ,
            });
        });
    }
}

(function($) {

	/*  [ Init Function ]
	- - - - - - - - - - - - - - - - - - - */
	$(function() {
        if($( '#cd-dropdown' ).length){
           $( '#cd-dropdown' ).dropdown( {
                gutter : 5,
                stack : false,
                delay : 100,
                slidingIn : 100
            } );
        }

        // get image in event listing classic

        var $img_first = $('.event-classic-item').first().find('.event-thumb').find('img').attr('src');
        $('.event-classic-thumb').html('<div class="e-mask"></div><img src='+$img_first+'>');

        $('.event-classic-item').each(function(){
            var $this = $(this);
            $this.find('.event-meta-inner').find('.title').find('a').mouseover(function(){
                var $imgth = $this.first().find('.event-thumb').find('img').attr('src');
                $('.event-classic-thumb').addClass('has-effect');
                $('.event-classic-thumb').html('<div class="e-mask"></div><img src='+$imgth+'>');
            });
        });

        if ($().magnificPopup) {

            // zoom gallery in event single
            $('.gallery-item .gallery-mark a').magnificPopup({
                type:'image',
                gallery:{
                    enabled:true
                }
            });
        }

        // active gallery tab and comment tab in event single
        $('.event-gallery-comment').find('.event-tab').first().addClass('is-active');
        $('.event-gallery-comment').find('.content-tab').first().addClass('active');
        $('.event-comment-title').on('click',function(){
            $('.event-comment-title').addClass('is-active');
            $('.event-gallery-title').removeClass('is-active');
            $('.event-comment').addClass('active');
            $('.event-gallery').removeClass('active');
        });
        $('.event-gallery-title').on('click',function(){
            $('.event-comment-title').removeClass('is-active');
            $('.event-gallery-title').addClass('is-active');
            $('.event-gallery').addClass('active');
            $('.event-comment').removeClass('active');
        });

        // get price in event single
        $('.event_currency').html( $('.woocommerce-Price-amount.amount').find('.woocommerce-Price-currencySymbol').text() );
        $('.total_price').html( $('.hidden_total').val() );

		if(jQuery('.ticket-input').length){
			$(".ticket-input .button").on("click", function() {

			  var $button = $(this);
			  var oldValue = $button.parent().find("input").val();
			  if ($button.hasClass("inc")) {
				    var newVal = parseFloat(oldValue) + 1;
				} else {
			   // Don't allow decrementing below zero
				if (oldValue > 0) {
				  var newVal = parseFloat(oldValue) - 1;
				} else {
				  newVal = 0;
				}
			  }

              $('.total_price').html( newVal*$('.hidden_total').val() );

			  $button.parent().find("input").val(newVal);

			});
		}
	}); // ready

	if(jQuery('.k2t-event .countdown-container').length){

		jQuery(window).on('load', function() {

            $('.k2t-event .countdown-container').each(function(index){
                var labels = ['days', 'hours', 'minutes', 'seconds'],
                    template = _.template($(this).parent().next().find('script.countdown-template').html()),
                    currDate = '00:00:00:00:00',
                    nextDate = '00:00:00:00:00',
                    parser = /([0-9]{2})/gi,
                    $example = $(this);
                // Parse countdown string to an object
                function strfobj(str) {
                    var parsed = str.match(parser),
                        obj = {};
                    labels.forEach(function(label, i) {
                        obj[label] = parsed[i]
                    });
                    return obj;
                }
                // Return the time components that diffs
                function diff(obj1, obj2) {
                    var diff = [];
                    labels.forEach(function(key) {
                        if (obj1[key] !== obj2[key]) {
                            diff.push(key);
                        }
                    });
                    return diff;
                }
                // Build the layout
                var initData = strfobj(currDate);
                labels.forEach(function(label, i) {
                    $example.append(template({
                        curr: initData[label],
                        next: initData[label],
                        label: label
                    }));
                });
                // Starts the countdown
                $example.countdown($example.parent().next().find('script.countdown-template').data('startdate'), function(event) {
                    var newDate = event.strftime('%D:%H:%M:%S'),
                        data;
                    if (newDate !== nextDate) {
                        currDate = nextDate;
                        nextDate = newDate;
                        // Setup the data
                        data = {
                            'curr': strfobj(currDate),
                            'next': strfobj(nextDate)
                        };
                        // Apply the new values to each node that changed
                        diff(data.curr, data.next).forEach(function(label) {
                            var selector = '.%s'.replace(/%s/, label),
                                $node = $example.find(selector);
                            // Update the node
                            $node.removeClass('flip');
                            $node.find('.curr').text(data.curr[label]);
                            $node.find('.next').text(data.next[label]);
                            // Wait for a repaint to then flip
                            _.delay(function($node) {
                                $node.addClass('flip');
                            }, 50, $node);
                        });
                    }
                });
            });

		});
	}


})(jQuery);

