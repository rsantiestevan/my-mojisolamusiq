<?php

if( function_exists( 'register_field_group' ) ):

	register_field_group( array(
		'key' => 'group_52427bedfegdasd',
		'title' => 'k_event Setting',

		// k_event SETTIG

		'fields' => array(

			// k_event TAXONOMY

			array(
				'key'             => 'field_52ke27be6ddfwa',
				'label'           => 'k_event taxonomy',
				'type'            => 'tab',
				'instructions'    => '',
				'placement' 	  => 'top',
			),
			array(
				'key'     => 'field_52ke27bekmj56',
				'label'   => 'k_event Single slug',
				'name'    => 'tax_event_slug',
				'type'    => 'text',
			),
			array(
				'key'     => 'field_52ke27b2d4563',
				'label'   => 'k_event Category slug',
				'name'    => 'tax_event_cat_slug',
				'type'    => 'text',
			),
			array(
				'key'              => 'field_52ke39f23jdm00',
				'label'            => 'k_event layout',
				'name'             => 'tax_event_layout',
				'type' 			   => 'select',
				'choices' 		   => array(
					'no_sidebar'   => 'No Sidebar',
					'left_sidebar' => 'Left Sidebar',
					'right_sidebar'=> 'Right Sidebar',
				),
			),
			array(
				'key'   => 'field_52ke2892f9bc5f',
				'label' => 'Custom sidebar',
				'name'  => 'tax_event_custom_sidebar',
				'type'  => 'text',
			),
			array(
				'key'   => 'field_52ke2892kdmjnd',
				'label' => 'Number event display',
				'name'  => 'tax_event_cat_limit',
				'type'  => 'text',
			),
			array(
				'key'     => 'field_52ke27bed23536',
				'label'   => 'Style',
				'name' 	  => 'tax_event_style',
				'type'    => 'select',
				'choices' => array(
					'event-grid-style' 	  => 'Grid',
					'event-listing-style'   => 'Listing',
				),
			),

			// k_event TITLEBAR

			array(
				'key' => 'field_52ke39f231D456',
				'label' => 'k_event taxonomy title bar',
				'type' => 'tab',
			),
			array(
				'key'               => 'field_52ke39f23jdm01',
				'label'             => 'Show/Hide Title Bar',
				'name'              => 'tax_event_display_titlebar',
				'type'              => 'select',
				'instructions'      => 'Only support k_event layout "Full Width" and "No Sidebar"',
				'choices' => array(
					'show'  => 'Show',
					'hided' => 'Hide',
				),
			),
			array(
				'key'               => 'field_52ke39f23jdm02',
				'label'             => 'k_event titlebar font size',
				'name'              => 'tax_event_titlebar_font_size',
				'type'              => 'text',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_52ke39f23jdm03',
				'label'             => 'k_event title bar color',
				'name'              => 'tax_event_titlebar_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_52ke39f23jdm04',
				'label'             => 'Padding top',
				'name'              => 'tax_event_pading_top',
				'type'              => 'text',
				'instructions'      => 'Unit: px (Ex: 10px)',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					)
				),
			),
			array(
				'key'               => 'field_52ke39f23jdm05',
				'label'             => 'Padding bottom',
				'name'              => 'tax_event_pading_bottom',
				'type'              => 'text',
				'instructions'      => 'Unit: px (Ex: 10px)',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					)
				),
			),
			array(
				'key'               => 'field_52ke39f23jdm06',
				'label'             => 'Background color',
				'name'              => 'tax_event_background_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_52ke39f23jdm07',
				'label'             => 'Background image',
				'name'              => 'tax_event_background_image',
				'type'              => 'image',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'return_format' 	=> 'url',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_52ke39f23jdm08',
				'label'             => 'Background position',
				'name'              => 'tax_event_background_position',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					''      => 'None',
					'left top'      => 'Left Top',
					'left center'   => 'Left Center',
					'left bottom'   => 'Left Bottom',
					'right top'     => 'Right Top',
					'right center'  => 'Right Center',
					'right bottom'  => 'Right Bottom',
					'center top'    => 'Center top',
					'center center' => 'Center Center',
					'center bottom' => 'Center Bottom',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_52ke39f23jdm09',
				'label'             => 'Background size',
				'name'              => 'tax_event_background_size',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					''		  => 'None',
					'inherit' => 'Inherit',
					'cover'   => 'Cover',
					'contain' => 'Contain',
					'full'    => '100%',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_52ke39f23jdm10',
				'label'             => 'Background repeat',
				'name'              => 'tax_event_background_repeat',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					''		  	=> 'None',
					'no-repeat' => 'No Repeat',
					'repeat'    => 'Repeat',
					'repeat-x'  => 'Repeat X',
					'repeat-y'  => 'Repeat Y',
				),
				'default_value' => 'no-repeat',
			),
			array(
				'key'               => 'field_52ke39f23jdm11',
				'label'             => 'Background parallax',
				'name'              => 'tax_event_background_parallax',
				'type'              => 'select',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					'1' => 'True',
					'0' => 'False',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_52ke39f23jdm12',
				'label'             => 'Titlebar overlay opacity',
				'name'              => 'tax_event_titlebar_overlay_opacity',
				'type'              => 'select',
				'instructions'      => 'Set your overlay opacity in titlebar',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					0   => 0,
					1 	=> 1,
					2 	=> 2,
					3 	=> 3,
					4 	=> 4,
					5 	=> 5,
					6 	=> 6,
					7 	=> 7,
					8 	=> 8,
					9 	=> 9,
					10  => 10,
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_52ke39f23jdm012',
				'label'             => 'title bar overlay color',
				'name'              => 'tax_event_overlay_background_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
			array(
				'key'               => 'field_52ke39f23jdm014',
				'label'             => 'Titlebar clipmask opacity',
				'name'              => 'tax_event_titlebar_clipmask_opacity',
				'type'              => 'select',
				'instructions'      => 'Set your clipmask opacity in titlebar',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'choices' => array(
					0   => 0,
					1 	=> 1,
					2 	=> 2,
					3 	=> 3,
					4 	=> 4,
					5 	=> 5,
					6 	=> 6,
					7 	=> 7,
					8 	=> 8,
					9 	=> 9,
					10  => 10,
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_52ke39f23jdm015',
				'label'             => 'Titlebar mask pattern',
				'name'              => 'tax_event_clipmask_background_img',
				'type'              => 'image',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'return_format' 	=> 'url',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_52ke39f23jdm016',
				'label'             => 'Custom titlebar content',
				'name'              => 'tax_event_titlebar_custom_content',
				'type'              => 'wysiwyg',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_52ke39f23jdm01',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'tabs'              => 'all',
				'toolbar'           => 'full',
				'media_upload'      => 1,
			),

		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'K-event-setting',
				),
			),
		),

		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

endif;