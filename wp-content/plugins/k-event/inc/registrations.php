<?php
// Include register factory
include_once dirname( __FILE__) . '/class.register_factory.php';

if( empty($k2t_register) || !$k2t_register instanceof K2T_Register ){
	$k2t_register = new K2T_Register();
}
// Register event post type

$slug = ( get_option( 'tax_event_slug' ) != '' ) ?  get_option( 'tax_event_slug' ) : esc_html__( 'k-event', 'k-event' );

$slug_cat = ( get_option( 'k2t_event_category_slug' ) != '' ) ?  get_option( 'k2t_event_category_slug' ) : esc_html__( 'k-event-category', 'k-event' );
$slug_tag = ( get_option( 'k2t_event_tag_slug' ) != '' ) ?  get_option( 'k2t_event_tag_slug' ) : esc_html__( 'k-event-tag', 'k-event' );

$k2t_register->register_post_type(array(
	array(
		'post-type' => 'post-k-event',
		'args' => array(
			'labels' => array(
				'name' => esc_html__('K-Event', 'k-event'),  
				'singular_name' => esc_html__('K-Event', 'k-event'),  
				'add_new' => esc_html__('Add New Event', 'k-event'),  
				'add_new_item' => esc_html__('Add New Event', 'k-event'),  
				'edit_item' => esc_html__('Edit Event', 'k-event'),  
				'new_item' => esc_html__('New Event', 'k-event'),  
				'view_item' => esc_html__('View Event', 'k-event'),  
				'all_items' => esc_html__('All Events', 'k-event'),
				'search_items' => esc_html__('Search Event', 'k-event'),  
				'not_found' =>  esc_html__('No Event found', 'k-event'),  
				'not_found_in_trash' => esc_html__('No Event found in Trash', 'k-event'),  
				'parent_item_colon' => ''
			),
			'rewrite' => array( 'slug' => $slug, 'with_front' => FALSE )
		)
	)
));

// Register event category taxonomy
$k2t_register->register_taxonomy(array(
	array(
		'taxonomy' => 'k-event-category',
		'args' => array(
			'labels' => array(
				'name'                => _x( 'Event Categories', 'taxonomy general name','k-event'),
				'singular_name'       => _x( 'K-Event Category', 'taxonomy singular name','k-event'),
				'search_items'        => esc_html__( 'Search K-Event Categories','k-event'),
				'all_items'           => esc_html__( 'All K-Event Categories','k-event'),
				'parent_item'         => esc_html__( 'Parent K-Event Category','k-event'),
				'parent_item_colon'   => esc_html__( 'Parent K-Event Category:','k-event'),
				'edit_item'           => esc_html__( 'Edit K-Event Category','k-event'), 
				'update_item'         => esc_html__( 'Update K-Event Category','k-event'),
				'add_new_item'        => esc_html__( 'Add New K-Event Category','k-event'),
				'new_item_name'       => esc_html__( 'New K-Event Category Name','k-event'),
				'menu_name'           => esc_html__( 'K-Event Categories','k-event')
			),
			'rewrite' => array('slug' => $slug_cat ),
			'post-type' => 'post-k-event'
		)
	),
	array(
		'taxonomy' => 'k-event-tag',
		'args' => array(
			'labels' => array(
				'name'                => _x( 'K-Event Tags', 'taxonomy general name','k-event'),
				'singular_name'       => _x( 'K-Event Tag', 'taxonomy singular name','k-event'),
				'search_items'        => esc_html__( 'Search K-Event Tags','k-event'),
				'all_items'           => esc_html__( 'All K-Event Tags','k-event'),
				'parent_item'         => esc_html__( 'Parent K-Event Tag','k-event'),
				'parent_item_colon'   => esc_html__( 'Parent K-Event Tag:','k-event'),
				'edit_item'           => esc_html__( 'Edit K-Event Tag','k-event'), 
				'update_item'         => esc_html__( 'Update K-Event Tag','k-event'),
				'add_new_item'        => esc_html__( 'Add New K-Event Tag','k-event'),
				'new_item_name'       => esc_html__( 'New K-Event Tag Name','k-event'),
				'menu_name'           => esc_html__( 'K-Event Tags','k-event')
			),
			'rewrite' => array('slug' => $slug_tag ),
			'post-type' => 'post-k-event'
		)
	),
	// array(
	// 	'taxonomy' => 'k-event-setting',
	// 	'args' => array(
	// 		'labels' => array(
	// 			'name'                => _x( 'K-Event settings', 'taxonomy general name','k-event'),
	// 			'singular_name'       => _x( 'K-Event setting', 'taxonomy singular name','k-event'),
	// 			'search_items'        => esc_html__( 'Search K-Event settings','k-event'),
	// 			'all_items'           => esc_html__( 'All K-Event settings','k-event'),
	// 			'parent_item'         => esc_html__( 'Parent K-Event setting','k-event'),
	// 			'parent_item_colon'   => esc_html__( 'Parent K-Event setting:','k-event'),
	// 			'edit_item'           => esc_html__( 'Edit K-Event setting','k-event'), 
	// 			'update_item'         => esc_html__( 'Update K-Event setting','k-event'),
	// 			'add_new_item'        => esc_html__( 'Add New K-Event setting','k-event'),
	// 			'new_item_name'       => esc_html__( 'New K-Event setting Name','k-event'),
	// 			'menu_name'           => esc_html__( 'K-Event settings','k-event')
	// 		),
	// 		'post-type' => 'post-k-event'
	// 	)
	// )
));
