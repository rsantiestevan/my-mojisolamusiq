<?php

if( !function_exists('k_event_listing_shortcode') ){
	function k_event_listing_shortcode($atts, $content = null){

		extract( shortcode_atts( array(
			'style' 				=> 'event-grid-style',
			'column' 				=> '',
			'post_per_page'         => '10',
			'cate_event_id'			=> '',
			'pagination'			=> 'show',
			'filter'  				=> 'show',
			'switch_layout'			=> 'show',
			'show_hide_viewmore'	=> 'show',
		), $atts));
		
		$html = '';

		if ( $style == 'event-classic-style' ) :
			$html .= K_Event::K_Render_event_classic_listing( $style, $post_per_page, $cate_event_id, $filter, $pagination, $show_hide_viewmore );
		else : 
			$html .= K_Event::K_Render_event_listing( $style, $post_per_page, $cate_event_id, $filter, $pagination, $show_hide_viewmore );
		endif;

		wp_reset_postdata();
		return $html;
	}
	
	add_shortcode('k_event_listing', 'k_event_listing_shortcode');
}