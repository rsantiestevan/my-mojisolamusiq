<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
Class K_Event {
	
	static function K_Render_event_listing ( $style, $post_per_page, $cat = '', $filter, $pagination, $show_hide_viewmore, $taxonomy = 'k-event-category' ) {

		wp_enqueue_script('k-event');

		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );	
		$arr = array(
			'post_type' 		=> 'post-k-event',
			'posts_per_page' 	=> (int)$post_per_page,
			'order'				=> 'DESC',
			'post_status'		=> 'publish',
			'paged'				=> $paged,
			'orderby'			=> 'date',
		);
		if ( count( $cat ) > 0 && !empty( $cat ) ){
			$arr['tax_query'] = array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'id',
					'terms'    => explode( ',', $cat ),
				)
			);
		}
		
		ob_start();
		$query = new WP_Query( $arr );

		include ( K_EVENT_BASE_PATH . '/templates/event-filter-bar.php' );

		?>
		<div class="k2t-event-listing">
		<?php if( $style == 'event-listing-style'): ?>
			<div class="col-7 event-listing-classic">
		<?php endif; ?>

		<div class="k-event-listing k-event-page <?php echo esc_attr( $style );?>">
			<?php
			if( count( $query->posts ) > 0 ):
				while( $query->have_posts() ) : $query->the_post();

					$item_setting = array(
							'start_date'					=> '',
							'end_date'						=> '',
							'address'						=> '',
							'product'						=> '',
						);

					if ( function_exists('stevecadey_get_acf_field') ) $item_setting = stevecadey_get_acf_field( $item_setting, get_the_ID(), false, 'event_' );

					extract( $item_setting, EXTR_OVERWRITE );
				?>
					<!-- Start item --> 

					<article <?php post_class('event-classic-item');?>>
						<div class="event-wrap-inner">

							<!-- Event thumb --> 
							<?php if( has_post_thumbnail( get_the_ID() ) ): ?>
								<div class="event-thumb">
									<?php 
										$post_link = get_permalink(get_the_ID());
										$post_thumb_size = '';

										if( $style == 'event-listing-style' ){
											$post_thumb_size = 'thumb_500x585';
										}
										else
											$post_thumb_size = 'thumb_1200x600';
										$post_thumb = '<a href="'. esc_url( $post_link ) .'" title="'. get_the_title().'">' . get_the_post_thumbnail(get_the_ID(), $post_thumb_size, array('alt' => trim(get_the_title()))) . '</a>';
										echo $post_thumb;
									?>
								</div>
							<?php endif; ?>

							<!-- Event Meta --> 
							<div class="event-meta">

								<!-- Event Date --> 

								<?php if ( !empty( $start_date ) ) :?>
									<div class="date">
									<?php
										$new_start_date = date_i18n('M ,d', strtotime($start_date) );
										$time_up  = date_i18n( 'g:i A', strtotime( $start_date ) );
										$date = explode(',', $new_start_date);
										echo '<span>' . $date[1] . '</span>';
										echo '<span>' . $date[0] . '</span>';
									?>
									</div>
								<?php endif;?>

								<!-- Event title --> 
								<div class="event-meta-inner">
									<h3 class="title">
										<a href="<?php echo esc_url( get_permalink() );?>" title="<?php echo esc_attr( $title );?>">
											<?php the_title();?>
										</a>
									</h3>

									<!-- Time -->
									<div class="event-meta-sub">
										<?php if ( !empty( $start_date ) ) :?>
											<div class="event-time">
												<span><i class="zmdi zmdi-time"></i></span>
												<?php
													echo '<span>' . $time_up . '</span>';
												?>
											</div>
										<?php endif;?>

										<!-- Address -->

										<?php if ( ! empty( $address ) ) : ?>
											<div class="event-address">
												<span><i class="zmdi zmdi-pin"></i></span>
												<span><?php echo esc_html( $address ); ?></span>
											</div>
										<?php endif;?>

										<!-- Excerpt -->
									</div>
									<div class="event-excerpt">
										<?php
											echo get_the_excerpt();
										?>
									</div>
								</div>

								<!-- Join Event -->
								<?php if( $show_hide_viewmore == 'show'): ?>

								<div class="join-event">
									<?php    
										$btn_html = '<a class="event-view-more" href="' . get_permalink() . '">'. esc_html__('View More','k-event').'</a>';
										echo $btn_html;
									?>
								</div>
								
								<?php endif; ?>
							</div><!-- Event Meta --> 
						</div>	<!-- event-wrap-inner --> 
					</article><!-- event-classic-item --> 
				<?php
					endwhile;
			endif; ?>
		</div>
		<?php if( $style == 'event-listing-style'): ?>
			</div>
			<div class="col-5 event-classic-thumb"></div>
		<?php endif; ?>
		</div>
		<!-- Event pagination --> 		
		<?php  if ( $pagination == 'show' ) include( K_EVENT_BASE_PATH . '/templates/navigation.php' );?>

		<?php 
		$event_listing_html = ob_get_clean();
		wp_reset_postdata();
		return $event_listing_html;
	}


	static function K_Render_event_classic_listing ( $style, $post_per_page, $cat = '', $filter, $pagination, $show_hide_viewmore, $taxonomy = 'k-event-category' ) {

		wp_enqueue_script('k-event');

		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );	
		$arr = array(
			'post_type' 		=> 'post-k-event',
			'posts_per_page' 	=> (int)$post_per_page,
			'order'				=> 'DESC',
			'post_status'		=> 'publish',
			'paged'				=> $paged,
			'orderby'			=> 'date',
		);
		if ( count( $cat ) > 0 && !empty( $cat ) ){
			$arr['tax_query'] = array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'id',
					'terms'    => explode( ',', $cat ),
				)
			);
		}
		
		ob_start();
		$query = new WP_Query( $arr );

		include ( K_EVENT_BASE_PATH . '/templates/event-filter-bar.php' );

		?>
		<div class="k2t-event-listing k2t-event-classic">

		<div class="k-event-listing k-event-page <?php echo esc_attr( $style );?>">
			<?php
			if( count( $query->posts ) > 0 ):
				while( $query->have_posts() ) : $query->the_post();

					$item_setting = array(
							'start_date'					=> '',
							'end_date'						=> '',
							'address'						=> '',
							'product'						=> '',
						);

					if ( function_exists('stevecadey_get_acf_field') ) $item_setting = stevecadey_get_acf_field( $item_setting, get_the_ID(), false, 'event_' );

					extract( $item_setting, EXTR_OVERWRITE );
				?>
					<!-- Start item --> 

					<article <?php post_class('event-classic-item');?>>
						<div class="event-wrap-inner">

							<!-- Event Meta --> 
							<div class="event-meta">

								<!-- Event Date --> 

								<?php if ( !empty( $start_date ) ) :?>
									<div class="date">
									<?php
										$new_start_date = date_i18n('M, d Y', strtotime($start_date) );
										$time_up  = date_i18n( 'g:i A', strtotime( $start_date ) );
										$time_down = date_i18n('g:i A', strtotime($end_date) );
										$date = explode(',', $new_start_date);
										echo '<div class="e-time">' . $time_up . ' - ' . $time_down . '</div>';
									?>
									</div>
								<?php endif;?>

								<!-- Event title --> 
								<div class="event-meta-inner">
									<h3 class="title">
										<a href="<?php echo esc_url( get_permalink() );?>" title="<?php echo esc_attr( $title );?>">
											<?php the_title();?>
										</a>
									</h3>

									<!-- Time -->
									<div class="event-meta-sub">
										<?php if ( !empty( $start_date ) ) :?>
											<div class="event-time">
												<span><i class="zmdi zmdi-time"></i></span>
												<?php
													echo '<span>' . $new_start_date . '</span>';
												?>
											</div>
										<?php endif;?>

										<!-- Address -->

										<?php if ( ! empty( $address ) ) : ?>
											<div class="event-address">
												<span><i class="zmdi zmdi-pin"></i></span>
												<span><?php echo esc_html( $address ); ?></span>
											</div>
										<?php endif;?>

										<!-- Excerpt -->
									</div>
									<div class="event-excerpt">
										<?php
											echo get_the_excerpt();
										?>
									</div>
								</div>

								<!-- Join Event -->
								<?php if( $show_hide_viewmore == 'show'): ?>

								<div class="join-event">
									<?php    
										$btn_html = '<a class="event-view-more" href="' . get_permalink() . '">'. esc_html__('View More','k-event').'</a>';
										echo $btn_html;
									?>
								</div>
								
								<?php endif; ?>
							</div><!-- Event Meta --> 
						</div>	<!-- event-wrap-inner --> 
					</article><!-- event-classic-item --> 
				<?php
					endwhile;
			endif; ?>
		</div>
		</div>
		<!-- Event pagination --> 		
		<?php  if ( $pagination == 'show' ) include( K_EVENT_BASE_PATH . '/templates/navigation.php' );?>

		<?php 
		$event_listing_html = ob_get_clean();
		wp_reset_postdata();
		return $event_listing_html;
	}

	static function K_Render_event_grid ( $masonry_column, $post_per_page, $event_pagination, $event_masonry_filter, $arr_term_id = '', $taxonomy = 'k-event-category' ) {
		wp_enqueue_script( 'k-event' );
		wp_enqueue_script( 'jquery-isotope' );
		wp_enqueue_script( 'jquery-imagesloaded' );
		wp_enqueue_script( 'cd-dropdown' );
		wp_enqueue_script( 'modernizr' );
		var_dump($test);
		
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );	
		$arr = array(
			'post_type' 		=> 'post-k-event',
			'posts_per_page' 	=> (int)$post_per_page,
			'order'				=> 'DESC',
			'post_status'		=> 'publish',
			'paged'				=> $paged,
			'orderby'			=> 'date',
		);
		if ( count( $arr_term_id ) > 0 && !empty( $arr_term_id ) ){
			$arr['tax_query'] = array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'id',
					'terms'    => explode( ',', $arr_term_id ),
				)
			);
		}
		
		ob_start();
		$query = new WP_Query( $arr );
		?>
		<div class="event-listing-masonry-wrapper">
			<?php if ( $event_masonry_filter == 'show' ): ?>
				<?php 
					if(!function_exists("isMobile")){
						function isMobile() {
						    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
						}
					}
					$categories = get_categories(array('taxonomy' => 'k-event-category'));
					if( count( $categories ) > 0 ):
				?>
					<?php if(isMobile()) : ?>
						<select id="cd-dropdown" class="cd-select k2t-isotope-filter">
							<option value="-1" selected><?php esc_html_e( 'Sort Event', 'k-event' );?></option>
							<option class="*"><?php esc_html_e( 'All', 'k-event' );?></option>
							<?php foreach($categories as $category):?>
							<option class=".event-<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
							<?php endforeach;?>
						</select>
					<?php else: ?>
						<ul class="event-isotope-filter filter-list">
							<li class="*"><?php esc_html_e( 'All', 'k-event' );?></li>
							<?php foreach($categories as $category):?>
							<li class=".event-<?php echo $category->term_id; ?>">
								<?php echo $category->name; ?></li>
							<?php endforeach;?>
						</ul>
					<?php endif;?>
				<?php endif; ?>
			<?php endif; ?>

			<div class="event-listing-masonry <?php echo esc_html($masonry_column); ?>">
				<?php
				
				if( count( $query->posts ) > 0 ):
					while( $query->have_posts() ) : $query->the_post();
						self::grid_render_content();
					endwhile;
				endif; ?>
			</div><!--event-listing-masonry-->

			<!-- Event pagination -->

			<?php if ( $event_pagination == 'show' ) k_event_include_template( 'navigation.php' );?>

		</div><!--/event-listing-masonry-wrapper-->
		<?php 
		$event_listing_html = ob_get_clean();
		wp_reset_postdata();
		return $event_listing_html;
	}

	static function K_Render_event_listing_carousel ( $masonry_column, $number_post_show, $event_pagination, $event_navigation, $arr_term_id = '', $taxonomy = 'k-event-category' ) {
		$arr = array(
			'post_type' 		=> 'post-k-event',
			'posts_per_page' 	=> (int)$number_post_show,
			'order'				=> 'DESC',
			'post_status'		=> 'publish',
			'orderby'			=> 'date',
		);
		if ( count( $arr_term_id ) > 0 && !empty( $arr_term_id ) ){
			$arr['tax_query'] = array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'id',
					'terms'    => explode( ',', $arr_term_id ),
				)
			);
		}
		
		ob_start();
		$query = new WP_Query( $arr );
		?>
		<div class="event-listing-masonry-wrapper">

			<div class="event-listing-masonry event-listing-carousel owl-carousel owl-stretch"
				data-items="<?php echo esc_attr( str_replace( 'columns-', '', $masonry_column ) ); ?>" data-autoPlay="false" data-margin="30" data-nav="<?php echo ( $event_navigation == 'show' ? 'true' : 'false' );?>"
				data-dots="<?php echo ( $event_pagination == 'show' ? 'true' : 'false' );?>" data-mobile="1" data-tablet="2" data-desktop="<?php echo esc_attr( str_replace( 'columns-', '', $masonry_column ) ); ?>">
				<?php
				
				if( count( $query->posts ) > 0 ):
					while( $query->have_posts() ) : $query->the_post();
						self::grid_render_content();
					endwhile;
				endif; ?>
			</div><!--event-listing-masonry-->
		</div><!--/event-listing-masonry-wrapper-->
		<?php 
		$event_listing_html = ob_get_clean();
		wp_reset_postdata();
		return $event_listing_html;
	}

	static function grid_render_content () {
		$event_address = (function_exists('get_field')) ? get_field('event_address', get_the_ID()) : ''; $event_address = empty($event_address) ? '' : $event_address;
		$thumbnail = (has_post_thumbnail(get_the_ID())) ? true : false;
		$categories = get_the_terms(get_the_ID(), 'k-event-category');
		$title = get_the_title();
		$content = get_the_content();
		$post_link = get_permalink(get_the_ID());
		$post_thumb_size = 'thumb_1200x600';
		$post_thumb = '<a href="'. esc_url( $post_link ) .'" title="'. esc_attr( $title ) .'">' . get_the_post_thumbnail(get_the_ID(), $post_thumb_size, array('alt' => trim(get_the_title()))) . '</a>';
		$post_thumb_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
		$start_date = ( function_exists('get_field')) ? get_field( 'event_start_date', get_the_ID() ) : '';
		if ( ! empty( $start_date ) ) $start_date = strtotime($start_date);
		$post_classes = array();	
		if(count($categories) > 0 && is_array($categories)){
			foreach ($categories as $key => $category) {
				$post_classes[] = 'event-'.$category->term_id;
			}
		}
		$post_classes = implode(' ',$post_classes);
	?>
		<article class="masonry-item masonry-it <?php echo ($post_classes) ;?>">
			<div class="inner k2t-element-hover">
				<?php if (!empty($post_thumb)) {
					echo ($post_thumb);
				} ?>
				<div class="info">
					<a class="read-more" href="<?php echo esc_url($post_link);?>" title="<?php echo esc_attr( $title );?>"><i class="zmdi zmdi-plus"></i></a>
					<?php if(!empty($title)) : ?>
						<h3 class="title">
							<a href="<?php echo esc_url( $post_link );?>" title="<?php echo esc_attr( $title );?>">
								<?php echo esc_html($title);?>
							</a>
						</h3>
					<?php endif; ?>
					<div class="event-meta">
						<span class="date">
							<i class="zmdi zmdi-calendar-note"></i>
							<?php if ( !empty( $start_date ) ) :?>
							<time data-time="<?php echo esc_attr( date_i18n( 'Y-m-d\TH:i:s+00:00', $start_date ) ); ?>" class="entry-date"><?php echo esc_html( date_i18n( 'F d, Y - H:i', $start_date ) ); ?></time>
							<?php endif;?>
						</span>
						<?php if(!empty($event_address)) : ?>
							<span class="location">
								<i class="zmdi zmdi-pin"></i>
								<?php echo esc_html($event_address); ?>
							</span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</article>
	<?php
		wp_reset_postdata();
	}
}