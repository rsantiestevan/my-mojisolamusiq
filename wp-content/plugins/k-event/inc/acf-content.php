<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_55acb8d5c8b5f',
	'title' => 'Event settings',
	'fields' => array (
		array(
			'key'               => 'field_53df40c5593d3',
			'label'             => esc_html__('Event Layout', 'k-event'),
			'name'              => '',
			'prefix'            => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'placement' 		=> 'left',
		),
		array(
			'key'               => 'field_53df40dd593c9',
			'label'             => esc_html__('Event layout', 'k-event'),
			'name'              => 'event_layout',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'choices' => array(
				'default'       => esc_html__('Default', 'k-event'),
				'right_sidebar' => esc_html__('Right Sidebar', 'k-event'),
				'left_sidebar'  => esc_html__('Left Sidebar', 'k-event'),
				'no_sidebar'    => esc_html__('No Sidebar', 'k-event'),
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_53df4176c938d',
			'label'             => esc_html__('Custom sidebar name', 'k-event'),
			'name'              => 'event_custom_sidebar',
			'prefix'            => '',
			'type'              => 'text',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'prepend'           => '',
			'maxlength'         => '',
			'readonly'          => 0,
			'disabled'          => 0,
		),
		array (
			'key' 				=> 'field_55adb06131de9',
			'label' 			=> esc_html__('Event Information', 'k-event'),
			'name' 				=> '',
			'type' 				=> 'tab',
			'instructions' 		=> '',
			'required' 			=> 0,
			'conditional_logic' => 0,
			'wrapper' 			=> array (
				'width' 			=> '',
				'class' 			=> '',
				'id' 				=> '',
			),
			'placement' 		=> 'top',
			'endpoint' 			=> 0,
			'placement' 		=> 'left',
		),
		array (
			'key' => 'field_55adb09979437',
			'label' => esc_html__('Start Date', 'k-event'),
			'name' => 'event_start_date',
			'type' => 'date_time_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'show_date' => 'true',
			'date_format' => 'm/d/y',
			'time_format' => 'H:mm',
			'show_week_number' => 'false',
			'picker' => 'slider',
			'save_as_timestamp' => 'true',
			'get_as_timestamp' => 'false',
		),
		array (
			'key' => 'field_55adb0b979438',
			'label' => esc_html__('End Date', 'k-event'),
			'name' => 'event_end_date',
			'type' => 'date_time_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'show_date' => 'true',
			'date_format' => 'm/d/y',
			'time_format' => 'H:mm',
			'show_week_number' => 'false',
			'picker' => 'slider',
			'save_as_timestamp' => 'true',
			'get_as_timestamp' => 'false',
		),
		array (
			'key' => 'field_55c0240f4f6e81',
			'label' => esc_html__('Show/Hide Number Of Tickets?', 'k-event'),
			'name' => 'event_show_hide_number_tickets',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'show' => esc_html__('show', 'k-event'),
				'hide' => esc_html__('hide', 'k-event'),
			),
			'default_value' => array (
				'' => '',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_55c0240f4f6e831',
			'label' => esc_html__('Show/Hide Button Join', 'k-event'),
			'name' => 'event_show_hide_button_join',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'show' => esc_html__('show', 'k-event'),
				'hide' => esc_html__('hide', 'k-event'),
			),
			'default_value' => array (
				'' => '',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_55adb3f81090a',
			'label' => esc_html__('Event ID', 'k-event'),
			'name' => 'event_event_id',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55b8a51de1691',
			'label' => esc_html__('Color', 'k-event'),
			'name' => 'event_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#3f51b5',
		),
		array (
			'key' => 'field_55adb4101090b',
			'label' => esc_html__('Product', 'k-event'),
			'name' => 'event_product',
			'type' => 'post_object',
			'instructions' => esc_html__('Select a WooCommerce product to sell this event ', 'k-event'),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'product',
			),
			'taxonomy' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'return_format' => 'object',
			'ui' => 1,
		),
		array (
			'key' => 'field_55c0245a4f6e9',
			'label' => esc_html__('Singer', 'k-event'),
			'name' => 'event_teacher',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'post-k-teacher',
			),
			'taxonomy' => array (
			),
			'allow_null' => 0,
			'multiple' => 1,
			'return_format' => 'object',
			'ui' => 1,
		),
		// array (
		// 	'key' => 'field_55adb52sjmensj',
		// 	'label' => esc_html__('Other Speakers', 'k-event'),
		// 	'name' => 'event_speakers',
		// 	'type' => 'repeater',
		// 	'instructions' => '',
		// 	'required' => 0,
		// 	'wrapper' => array (
		// 		'width' => '',
		// 		'class' => '',
		// 		'id' => '',
		// 	),
		// 	'min' => '',
		// 	'max' => '',
		// 	'layout' => 'table',
		// 	'button_label' => esc_html__('Add Row', 'k-event'),
		// 	'sub_fields' => array (
		// 		array (
		// 			'key' => 'field_55adb5asjemsne',
		// 			'label' => esc_html__('Avatar', 'k-event'),
		// 			'name' => 'event_speaker_avatar',
		// 			'type' => 'image',
		// 			'instructions' => '',
		// 			'required' => 0,
		// 			'conditional_logic' => 0,
		// 			'wrapper' => array (
		// 				'width' => '',
		// 				'class' => '',
		// 				'id' => '',
		// 			),
		// 			'return_format' => 'url',
		// 			'preview_size' => 'thumbnail',
		// 			'library' => 'all',
		// 			'min_width' => '',
		// 			'min_height' => '',
		// 			'min_size' => '',
		// 			'max_width' => '',
		// 			'max_height' => '',
		// 			'max_size' => '',
		// 			'mime_types' => '',
		// 		),
		// 		array (
		// 			'key' => 'field_55adb5fjemnsje',
		// 			'label' => esc_html__('Name', 'k-event'),
		// 			'name' => 'event_speaker_name',
		// 			'type' => 'text',
		// 			'instructions' => '',
		// 			'required' => 0,
		// 			'conditional_logic' => 0,
		// 			'wrapper' => array (
		// 				'width' => '',
		// 				'class' => '',
		// 				'id' => '',
		// 			),
		// 			'default_value' => '',
		// 			'placeholder' => '',
		// 			'prepend' => '',
		// 			'append' => '',
		// 			'maxlength' => '',
		// 			'readonly' => 0,
		// 			'disabled' => 0,
		// 		),
		// 		array (
		// 			'key' => 'field_55adb5fejmsnej',
		// 			'label' => esc_html__('Link', 'k-event'),
		// 			'name' => 'event_speaker_url',
		// 			'type' => 'text',
		// 			'instructions' => '',
		// 			'required' => 0,
		// 			'conditional_logic' => 0,
		// 			'wrapper' => array (
		// 				'width' => '',
		// 				'class' => '',
		// 				'id' => '',
		// 			),
		// 			'default_value' => '',
		// 			'placeholder' => '',
		// 			'prepend' => '',
		// 			'append' => '',
		// 			'maxlength' => '',
		// 			'readonly' => 0,
		// 			'disabled' => 0,
		// 		),
		// 	),
		// ),
		array (
			'key' => 'field_55adb776ef5d2',
			'label' => esc_html__('Subscribe URL', 'k-event'),
			'name' => 'event_subscribe_url',
			'type' => 'text',
			'instructions' => esc_html__('Link to a subscribe form. Only work if no product is set. ', 'k-event'),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55adb785ef5d3',
			'label' => esc_html__('Subscribe Button Text', 'k-event'),
			'name' => 'event_subscribe_button_text',
			'type' => 'text',
			'instructions' => esc_html__('Text that appears on the subscribe button.', 'k-event'),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' 				=> 'field_55adbcffef5d4',
			'label' 			=> esc_html__('Event Location', 'k-event'),
			'name' 				=> '',
			'type' 				=> 'tab',
			'instructions' 		=> '',
			'required' 			=> 0,
			'conditional_logic' => 0,
			'wrapper' 			=> array (
				'width' 			=> '',
				'class' 			=> '',
				'id' 				=> '',
			),
			'placement' 		=> 'top',
			'endpoint' 			=> 0,
			'placement' 		=> 'left',
		),
		array (
			'key' => 'field_55adbd36ef5d5',
			'label' => esc_html__('Address', 'k-event'),
			'name' => 'event_address',
			'type' => 'text',
			'instructions' => esc_html__('Location Address of event ', 'k-event'),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55adbd54ef5d6',
			'label' => esc_html__('Phone', 'k-event'),
			'name' => 'event_phone',
			'type' => 'text',
			'instructions' => esc_html__('Contact Number of event ', 'k-event'),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55adbd6aef5d7',
			'label' => esc_html__('Website', 'k-event'),
			'name' => 'event_website',
			'type' => 'text',
			'instructions' => esc_html__('Website URL of event', 'k-event'),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55adbe86ef5d8',
			'label' => esc_html__('Email', 'k-event'),
			'name' => 'event_email',
			'type' => 'text',
			'instructions' => esc_html__('Email Contact of event', 'k-event'),
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array(
			'key'               => 'field_53fee96493014',
			'label'             => esc_html__('Event Titlebar', 'k-event'),
			'name'              => '',
			'prefix'            => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'placement' 		=> 'left',
		),
		array(
			'key'               => 'field_53fd4b60938t9',
			'label'             => esc_html__('Show/Hide Title Bar', 'k-event'),
			'name'              => 'event_display_titlebar',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'choices' => array(
				'default'=>'Default',
				'show'  => 'Show',
				'hided' => 'Hide',
			),
			'default_value' => 'show',
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_542e57d793109',
			'label'             => esc_html__('Event titlebar font size', 'k-event'),
			'name'              => 'event_titlebar_font_size',
			'prefix'            => '',
			'type'              => 'text',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'prepend'           => '',
			'maxlength'         => '',
			'readonly'          => 0,
			'disabled'          => 0,
		),
		array(
			'key'               => 'field_53fftww6930a9',
			'label'             => esc_html__('Event title bar color', 'k-event'),
			'name'              => 'event_titlebar_color',
			'prefix'            => '',
			'type'              => 'color_picker',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
		),
		array(
			'key'               => 'field_53feec3f930a6',
			'label'             => esc_html__('Padding top', 'k-event'),
			'name'              => 'event_pading_top',
			'prefix'            => '',
			'type'              => 'text',
			'instructions'      => esc_html__('Unit: px (Ex: 10px)', 'k-event'),
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				)
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'prepend'           => '',
			'maxlength'         => '',
			'readonly'          => 0,
			'disabled'          => 0,
		),
		array(
			'key'               => 'field_53feec3f931a7',
			'label'             => esc_html__('Padding bottom', 'k-event'),
			'name'              => 'event_pading_bottom',
			'prefix'            => '',
			'type'              => 'text',
			'instructions'      => esc_html__('Unit: px (Ex: 10px)', 'k-event'),
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				)
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'prepend'           => '',
			'maxlength'         => '',
			'readonly'          => 0,
			'disabled'          => 0,
		),
		array(
			'key'               => 'field_53feeceaa3019',
			'label'             => esc_html__('Background color', 'k-event'),
			'name'              => 'event_background_color',
			'prefix'            => '',
			'type'              => 'color_picker',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
		),
		array(
			'key'               => 'field_53feecb293a18',
			'label'             => esc_html__('Background image', 'k-event'),
			'name'              => 'event_background_image',
			'prefix'            => '',
			'type'              => 'image',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'return_format' 	=> 'url',
			'preview_size'      => 'thumbnail',
			'library'           => 'all',
		),
		array(
			'key'               => 'field_53feeda093a1b',
			'label'             => esc_html__('Background position', 'k-event'),
			'name'              => 'event_background_position',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				''      		=> esc_html__('None', 'k-event'),
				'left top'      => esc_html__('Left Top', 'k-event'),
				'left center'   => esc_html__('Left Center', 'k-event'),
				'left bottom'   => esc_html__('Left Bottom', 'k-event'),
				'right top'     => esc_html__('Right Top', 'k-event'),
				'right center'  => esc_html__('Right Center', 'k-event'),
				'right bottom'  => esc_html__('Right Bottom', 'k-event'),
				'center top'    => esc_html__('Center top', 'k-event'),
				'center center' => esc_html__('Center Center', 'k-event'),
				'center bottom' => esc_html__('Center Bottom', 'k-event'),
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_53fuida0936ab',
			'label'             => esc_html__('Background size', 'k-event'),
			'name'              => 'event_background_size',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				''		  => 'None',
				'inherit' => 'Inherit',
				'cover'   => 'Cover',
				'contain' => 'Contain',
				'full'    => '100%',
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_52rguda0a301b',
			'label'             => esc_html__('Background repeat', 'k-event'),
			'name'              => 'event_background_repeat',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				''		  	=> esc_html__('None', 'k-event'),
				'no-repeat' => esc_html__('No Repeat', 'k-event'),
				'repeat'    => esc_html__('Repeat', 'k-event'),
				'repeat-x'  => esc_html__('Repeat X', 'k-event'),
				'repeat-y'  => esc_html__('Repeat Y', 'k-event'),
			),
			'default_value' => 'repeat',
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_54336341939a7',
			'label'             => esc_html__('Background parallax', 'k-event'),
			'name'              => 'event_background_parallax',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				'1' => 'True',
				'0' => 'False',
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
			'key'               => 'field_53fef06093a20',
			'label'             => esc_html__('Titlebar overlay opacity', 'k-event'),
			'name'              => 'event_titlebar_overlay_opacity',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => esc_html__('Set your overlay opacity in titlebar', 'k-event'),
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				0   => 0,
				1 	=> 1,
				2 	=> 2,
				3 	=> 3,
				4 	=> 4,
				5 	=> 5,
				6 	=> 6,
				7 	=> 7,
				8 	=> 8,
				9 	=> 9,
				10  => 10,
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
				'key'               => 'field_53fef060jdmene',
				'label'             => 'Title bar overlay color',
				'name'              => 'event_overlay_background_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b60938t9',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
			),
		array(
			'key'               => 'field_53fef07c93029',
			'label'             => esc_html__('Titlebar clipmask opacity', 'k-event'),
			'name'              => 'event_titlebar_clipmask_opacity',
			'prefix'            => '',
			'type'              => 'select',
			'instructions'      => esc_html__('Set your clipmask opacity in titlebar', 'k-event'),
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'choices' => array(
				0   => 0,
				1 	=> 1,
				2 	=> 2,
				3 	=> 3,
				4 	=> 4,
				5 	=> 5,
				6 	=> 6,
				7 	=> 7,
				8 	=> 8,
				9 	=> 9,
				10  => 10,
			),
			'default_value' => array(),
			'allow_null'    => 0,
			'multiple'      => 0,
			'ui'            => 0,
			'ajax'          => 0,
			'placeholder'   => '',
			'disabled'      => 0,
			'readonly'      => 0,
		),
		array(
				'key'               => 'field_53fef07ejsmnes',
				'label'             => 'Titlebar mask pattern',
				'name'              => 'event_clipmask_background_img',
				'type'              => 'image',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b60938t9',
							'operator' => '==',
							'value'    => 'show',
						),
					),
				),
				'return_format' 	=> 'url',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
		array(
			'key'               => 'field_53fef0eb93a23',
			'label'             => esc_html__('Custom titlebar content', 'k-event'),
			'name'              => 'event_titlebar_custom_content',
			'prefix'            => '',
			'type'              => 'wysiwyg',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					'rule_rule_rule_rule_0' => array(
						'field'    => 'field_53fd4b60938t9',
						'operator' => '==',
						'value'    => 'show',
					),
				),
			),
			'default_value'     => '',
			'tabs'              => 'all',
			'toolbar'           => 'full',
			'media_upload'      => 1,
		),
		array(
			'key' => 'field_54339f080e790',
			'label' => 'Event Gallery',
			'name' => '',
			'prefix' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'placement' 		=> 'left',
		),
		array(
			'key' => 'field_54339fedje326',
			'label' => esc_html__('Gallery', 'k-event'),
			'name' => 'multi_gallery',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Row',
			'sub_fields' => array(
				array(
					'key' => 'field_54339fe32d63e',
					'label' => esc_html__( 'Gallery format', 'k-event' ),
					'name' => 'm_gallery',
					'prefix' => '',
					'type' => 'gallery',
					'instructions' => 'Option for format "gallery" only',
					'required' => 0,
					'conditional_logic' => 0,
					'min' => '',
					'max' => '',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post-k-event',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page( array(
		'page_title' => esc_html__( 'K Event Setting', 'k-event' ),
		'menu_slug'  => 'K-event-setting',
	));
}

function k_event_edit_admin_menus() {
    global $menu;
    global $submenu;

    $submenu['edit.php?post_type=post-k-event'][17][2] = 'admin.php?page=K-event-setting';

    remove_menu_page('K-event-setting'); // Remove the Tools Menu
}
add_action( 'admin_menu', 'k_event_edit_admin_menus', 99 );