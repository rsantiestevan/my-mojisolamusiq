<?php

/**
 * Teacher filter navigation.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get filter list

$filter_list = array();

if ( !empty( $cat ) ) :	// Have select categories

	$cat = explode( ',', $cat );

	foreach( $cat as $key => $value ) :
		$cat_name = get_cat_name( $value );
		$cat_name = strtolower( $cat_name );
		$filter_list[ str_replace(' ', '-', $cat_name) ] = get_cat_name( $value ) ;
	endforeach;

else:	// Get all categories

	$categories = get_categories( array( 'taxonomy' => 'k-event-category' ) );
	if ( count( $categories ) > 0 ) :
		foreach ($categories as $key => $value) {
			$filter_list[ $value->slug ] =  $value->cat_name;
		}
	endif;

endif;	

?>

<!-- Output -->
<?php

	if ( $filter && $filter == 'show' ) : ?>
		<div class="k2t-isotop-filter k-event-filter">

			<!-- Filter bar -->

			<?php if ( $filter && count($filter_list) > 0 ) : ?>
				<ul class="menu-filter">
					<li>
						<a class="active" href='#'><?php echo esc_html__('All','k-event');?></a>
					</li>
					<?php
						foreach ($filter_list as $key => $value) :
							echo
								'<li>' .
									'<a href="#" data-filter=".k-event-category-' . $key . '">' .
										$value .
									'</a>' .
								'</li>';
						endforeach;
					?>
				</ul>
			<?php endif;?>

			<!-- Switch layout bar  -->

			<?php if ( $switch_layout ) :

				$col_grid = '2';

				$grid_active = $list_active = '';

				$grid_active = 'sl-active';

			?>
			<?php endif;?>
		</div>
	<?php
	endif;
