<?php
/**
* Description for this even-related.php
*
* @package k-event
* @author Lunartheme
* @link http://www.lunartheme.com
*/

$related = get_related_tag_posts_ids( get_the_ID(), '10', 'k-event-category', 'post-k-event');

?>
<div class="k2t-related-event">
	<?php
		$related_post_title = 'Related Events';

		if(!empty($related_post_title)) :
	?>
		<h4 class="related-title">
			<?php esc_html_e( 'Related Events','k-event' ); ?>
		</h4>
	<?php endif;?>

	<div class="related-event-inner clearfix">

		<?php
			$args = array(
				'post__in'      => $related,
				'post__not_in'  => array($post->ID),
				'orderby'       => 'post__in',
				'no_found_rows' => true,
				'posts_per_page'=> '3',
				'post_type'		=> 'post-k-event'
			);
			$related_posts = get_posts( $args );

			if(count($related_posts) > 0) :
				foreach ( $related_posts as $post ): setup_postdata($post);

				$meta = array(
					'start_date'	=> '',
					'address'		=> '',
					'product'		=> '',
					'end_date'		=> '',
				);

				if ( function_exists('stevecadey_get_acf_field') ) :
					$meta = stevecadey_get_acf_field( $meta, $post->ID, false, 'event_' );
				endif;

				$new_start_date = date_i18n('M d', strtotime( $meta['start_date'] ) );

				$thumb_html = '';
				if( has_post_thumbnail( get_the_ID() ) ){
					$thumb_html = get_the_post_thumbnail(get_the_ID(), 'thumb_event_related', array('alt' => trim(get_the_title())));
				}else{
					$thumb_html = '<img src="' . plugin_dir_url( __FILE__ ) . 'images/thumb-400x256.png" alt="'.trim(get_the_title()).'" />';
				}
				$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'post' );
				?>
				<article class="related-event">
					<div class="related-inner">
						<div class="related-thumb">
							<!-- Date -->

							<div class="event-date">
								<?php
									echo '<span class="event-m">' . date_i18n('M d', strtotime( $meta['start_date'] ) ) . '</span>';
								?>
							</div>

							<a title="<?php the_title(); ?>" class="image-link" href="<?php the_permalink(get_the_ID())?>">
								<i class="recent-e-mask"></i>
								<?php echo ( $thumb_html );?>
							</a>
						</div>

						<div class="related-meta">
							<h5 class="event-item-title">
								<a href="<?php the_permalink(get_the_ID())?>" title="<?php the_title()?>"><?php the_title();?></a>
							</h5>

							<!-- Date & Time -->

							<?php
								if ( ! empty( $meta['start_date'] ) ) :
									$time_up  = date_i18n( 'g:i A', strtotime( $meta['start_date'] ) );
									?>

									<!-- Time -->

									<div class="event-time">
										<span><i class="zmdi zmdi-time"></i></span>
										<?php
											echo '<span>' . $time_up . '</span>';
										?>
									</div>

							<?php endif;?>
							<!-- END Time -->
							<!-- Address -->

							<?php if ( ! empty( $meta['address'] ) ) : ?>
								<div class="event-address">
									<span><i class="zmdi zmdi-pin"></i></span>
									<span><?php echo esc_html( $meta['address'] ); ?></span>
								</div>
							<?php endif;?>

						</div><!-- .related-meta -->
					</div><!-- .related-inner -->
				</article><!-- .related-post -->
				<?php
				endforeach;
			endif;
			wp_reset_postdata();
		?>

	</div><!-- .related-event-inner -->

</div><!-- .k2t-related-posts -->



