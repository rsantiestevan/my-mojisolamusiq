<?php
get_header();

// Register variables
$classes 						= array();

$single_pre 					= 'event_';

$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';

// Get metadata of event in single
$arr_event_meta_val  	= array();
$arr_event_meta 		= array(
	// Layout
	'custom_sidebar' 				=> '',
	'layout'						=> '',

	// Infomation
	'start_date'					=> '',
	'end_date'						=> '',
	'event_id'						=> '',
	'product'						=> '',
	'who_is_speakers'				=> '',
	'teacher'						=> '',
	'speakers'						=> '',
	'subscribe_url'					=> '',
	'subscribe_button_text'			=> '',
	'show_hide_button_join'			=> '',
	'show_hide_number_tickets'		=> '',

	// Location

	'address'						=> '',
	'phone'							=> '',
	'website'						=> '',
	'email'							=> '',
);

foreach ( $arr_event_meta as $meta => $val ) {
	if ( function_exists( 'get_field' ) ) {
		if ( get_field( $single_pre . $meta, get_the_ID() ) ) {
			$arr_event_meta_val[$meta] = get_field( $single_pre . $meta, get_the_ID() );
		}
	}
}
extract( shortcode_atts( $arr_event_meta, $arr_event_meta_val ) );

$event_info = array();
$new_date = strtotime($start_date);
$new_date = date_i18n('Y-m-d H:i', $new_date);
$event_info['start_date'] = $new_date;

wp_enqueue_script( 'k-event' );
wp_localize_script( 'k-event', 'event_info', $event_info );
wp_enqueue_script( 'k-countdown' );
wp_enqueue_script( 'k-lodash' );
wp_enqueue_script('magnific-popup');

// Layout of single event
// if ( ( empty( $layout ) || $layout == 'default' ) && ! empty( $smof_data['event_layout'] ) ) {
// 	$layout = $smof_data['event_layout'];
// } else if ( empty( $smof_data['event_layout'] ) ) {
// 	$layout = 'right_sidebar';
// }
if ( 'right_sidebar' == $layout || 'default' == $layout ){	
	$classes[] = 'right-sidebar';
} elseif ( 'left_sidebar' == $layout ) {
	$classes[] = 'left-sidebar';
} elseif ( 'no_sidebar' == $layout ) {
	$classes[] = 'no-sidebar';
}
	

// Get categories

$categories = get_the_terms( get_the_ID(), 'k-event-category' );
$tags = get_the_terms( get_the_ID(),'k-event-tag');


//var_dump( $layout );
?>

<div class="k2t-content <?php echo implode( ' ', $classes ) ?>">
	<div class="k2t-wrap">
		<main class="k2t-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div id="main-col" <?php post_class(); ?>>

					<div class="k2t-event-main-content k-event-page">

						<!-- The title -->

						<div class="event-meta">

								<!-- Event Date --> 

								<?php if ( !empty( $start_date ) ) :?>
									<div class="date">
									<?php
										$new_start_date = date_i18n('M ,d', strtotime($start_date) );
										$time_up  = date_i18n( 'g:i A', strtotime( $start_date ) );
										$date = explode(',', $new_start_date);
										echo '<span>' . $date[1] . '</span>';
										echo '<span>' . $date[0] . '</span>';
									?>
									</div>
								<?php endif;?>

								<!-- Event title --> 
								<div class="event-meta-inner">
									<h3 class="title">
										<?php the_title();?>
									</h3>

									<!-- Time -->
									<div class="event-meta-sub">
										<?php if ( !empty( $start_date ) ) :?>
											<div class="event-time">
												<span><i class="zmdi zmdi-time"></i></span>
												<?php
													echo '<span>' . $time_up . '</span>';
												?>
											</div>
										<?php endif;?>

										<!-- Address -->

										<?php if ( ! empty( $address ) ) : ?>
											<div class="event-address">
												<span><i class="zmdi zmdi-pin"></i></span>
												<span><?php echo esc_html( $address ); ?></span>
											</div>
										<?php endif;?>

										<!-- Excerpt -->
									</div>
									<div class="event-excerpt">
										<?php
											$excerpt = get_the_excerpt();
											echo $excerpt;
										?>
									</div>
								</div>

							</div><!-- Event Meta --> 

						<!-- The thumbnail -->

						<?php if( has_post_thumbnail( get_the_ID() ) ):?>
							<div class="event-thumbnail">
								<?php
									echo get_the_post_thumbnail(get_the_ID(), 'thumb_1200x600', array('alt'   => get_the_title() ) );
								?>
							</div>
						<?php endif;?>

						<!-- The content -->
						<div class="event-content">
							<?php the_content();?>
						</div>

						<div class="event-content-footer">

							<!-- Event Social -->

							<?php if( function_exists( 'stevecadey_social_share' ) ) { stevecadey_social_share(); } ?>

						</div>

					</div><!-- k2t-event main content-->

				</div><!-- #main-col -->

			<?php endwhile; ?>

			<div class="clear"></div>

			<?php
				$gallery_image = function_exists( 'get_field' ) ? get_field( 'multi_gallery', get_the_ID() ) : array();
				$gallery = $gallery_image[0]['m_gallery'];

				$count_gallery = count( $gallery );
				if( $count_gallery > 0 || comments_open() ):
			?>

			<div class="event-gallery-comment"> <!-- Event gallery -->
				<?php if( $count_gallery > 0 ) : ?>
					<div class="event-gallery-title event-tab">
						<?php esc_html_e('Gallery','k-event'); ?>
					</div>
				<?php endif; ?>
				<?php if( comments_open() ) :?>
					<div class="event-comment-title event-tab">
						<?php esc_html_e('Comment','k-event'); ?>
					</div>
				<?php endif; ?>
				<?php if( $count_gallery > 0 ): ?>
					<div class="event-gallery content-tab">
						<?php 
							$img_size = 'thumb_380x380';
							foreach ( $gallery as $slide ):
								if ( ! empty( $slide['ID'] ) ) : 
									$image = wp_get_attachment_image( $slide['ID'], $img_size, array( 'alt' => trim( get_the_title() ) ) );
									echo '<div class="gallery-item">' . $image . '<div class="gallery-mark"><a href="' . $slide['url'] . '"><i class="zmdi zmdi-search"></i></a></div></div>';
								endif;

							endforeach;
						?>
					</div> <!-- event gallery inner -->
				<?php endif; ?>
				
				<?php if ( comments_open() ) : ?>
					<div class="event-comment content-tab">
						<?php comments_template( '', false ); ?>
					</div> <!-- Event comment -->
				<?php endif; ?>
			</div> <!-- End Event Gallery -->

		<?php endif; ?>

		</main><!-- .k2t-blog -->

		<div class="event-sidebar">

			<!-- event-meta -->
			<?php
				$product = (function_exists('get_field')) ? get_field('event_product', get_the_ID()) : ''; 
				$product = empty($product) ? '' : $product;
				$product_price = new WC_Product( $product->ID );
				$price = $product_price->get_price_html();

				if ( !empty( $product_price ) ):
			?>
				<div class="event-meta">
					<h3 class="event-meta-title event-buy"><?php esc_html_e( "Buy ticket", 'k-event'); ?></h3>
					<div class="product-price">
						
						<span class="price">
							<?php
								echo ( $price );
							?>
							<input type="hidden" class="hidden_total" value="<?php echo $product_price->price; ?>" />
						</span>
						<?php if( !empty( $product_price ) ): ?>
							<?php if( isset( $show_hide_number_tickets) && $show_hide_number_tickets == 'show' ): ?>
								<div class="event-ticket">
									<div class="ticket-input">
	    								<input type="text" name="ticket" value="1">
	    								<span class="dec button"><i class="zmdi zmdi-minus"></i></span>
	    								<span class="inc button"><i class="zmdi zmdi-plus"></i></span>
	    							</div>
								</div>

								<div class="event-total-ticket">
									<span class="total-text"> <?php esc_html_e('Total','k-event'); ?> </span>
									<span class="total total_price"></span>
									<span class="total event_currency"></span>
								</div>
							<?php endif; ?>

							<?php if( isset( $show_hide_button_join ) && $show_hide_button_join == 'show' ): ?>
								
									<div class="k2t-join-event">
										<?php    
											date_default_timezone_set( 'Asia/Ho_Chi_Minh' );

											$diff = strtotime( $end_date ) -  time();

											if( $diff < 0 ): 
												$btn_html = '<div class="btn-out closed"><i class="zmdi zmdi-calendar-close"></i><span>' . esc_html__( 'Closed', 'k-event' ) . '</span></div>';
											else :
												if ( ! empty( $product ) ) :

													if ( $product_price->price > 0 ) :
															$btn_html = '<a class="k2t-btn" href="' .get_post_permalink($product->ID) . '">'. __('Buy Ticket','k-event').'</a>';
													else :
														$btn_html = '<a class="k2t-btn" href="' . get_post_permalink($product->ID) . '">'. __('Join Event','k-event').'</a>';
													endif;
												endif;
											endif;
											echo $btn_html;
										?>
									</div>

							<?php endif; ?>

                    	<?php endif; ?>
						
					</div><!-- product price -->

				</div><!-- event-meta -->
			<?php endif;?>
			<div class="event-meta">
				<h3 class="event-meta-title"><?php esc_html_e( "Details", 'k-event'); ?></h3>

				<!-- Date & Time -->
					<?php
						if ( ! empty( $start_date ) && ! empty( $end_date ) ) :

							$new_start_date = date_i18n('F d', strtotime($start_date) );
							$new_end_date = date_i18n('F d', strtotime($end_date) );
							$diff = strtotime( $new_end_date ) - strtotime( $new_start_date );

							$time_up  = date_i18n( 'g:i a', strtotime( $start_date ) );
							$time_end = date_i18n( 'g:i a', strtotime( $end_date ) );

							?>

							<!-- Date -->

							<div class="event-date event-detail">
								<div class="event-text"><?php esc_html_e('Start:','k-event') ?></div>
								<?php
									if ( $diff == 0 ) :
										echo '<div>' . $new_start_date . '</div>';
									else:
										echo '<div>' . $new_start_date . ' - ' . $time_up . '</div>';
									endif;
								?>
							</div>

							<!-- Time -->

							<div class="event-end event-detail">
								<div class="event-text"><?php esc_html_e('End:','k-event') ?></div>
								<?php
									if ( $diff == 0 ) :
										echo '<div><span>' . $time_up . ' - </span><span>' . $time_end . '</span></div>';
									else:
										echo '<span>' . $new_end_date . ' - ' . $time_end . '</span>';
									endif;
								?>
							</div>
					<?php endif;?>
				<!-- END Time -->

				<!-- Event Categories -->
				<?php
					if ( is_array( $categories ) ):
						echo '<div class="event-cat event-detail"><div class="event-text">' . esc_html__('Event Category:','k-event') . '</div>';
						foreach ( $categories as $key => $category ) {
							if ( $key == count( $categories ) - 1 ) {
								echo '<a href="'. esc_url( get_term_link( $category->term_id, 'k-event-category' ) ) .'" title="'. esc_attr( $category->name ) .'">'. esc_html( $category->name ) . '<i class="event-dot">' . esc_html(',','k-event') . '</i>' . '</a>';
							} else {
								echo '<a href="'. esc_url( get_term_link( $category->term_id, 'k-event-category' ) ) .'" title="'. esc_attr( $category->name ) .'">'. esc_html( $category->name ) . '<i class="event-dot">' . esc_html(',','k-event') . '</i>' . '</a>';
							}
						}
						echo "</div>";
					endif;
				?>
				<!-- END Event Categories -->

				<!-- Event Tags -->
				<?php
					if( is_array( $tags ) ) :
						echo '<div class="event-tags event-detail"><div class="event-text">' . esc_html__('Event Tags:','k-event') . '</div>';
						foreach ( $tags as $key => $tag ) {
							echo '<a href="'. esc_url( get_tag_link( $tag->term_id ) ) .'" title="'. esc_attr( $tag->name ) .'">'. esc_attr( $tag->name ) . '<i class="event-dot">' . esc_html(',','k-event') . '</i>' . '</a>';
						}
						echo "</div>";
					endif;
				?>
				<!-- END Event Tags -->

			</div><!-- event-meta -->

			<div class="event-meta">
				<h3 class="event-meta-title"><?php esc_html_e( "Organizer", 'k-event'); ?></h3>

				<!-- Speakers -->

				<?php if ( is_array( $teacher ) && count( $teacher ) > 0 ) : ?>
					<div class="entry-speakers">
						<div class="k2t-speakers">

							<!-- Teacher Speakers -->

							<?php foreach ( $teacher as $key => $teach ) : ?>

								<!-- Avatar of teacher -->

								<?php if ( function_exists('get_field') ) :

									$teacher_avatar = get_field('teacher_avatar', $teach->ID );
									?>
									<div class="speaker-avatar">

										<!-- teacher name -->

										<div class="name">
											<?php echo '<a href="'. esc_url( get_permalink( $teach->ID ) ) .'" target="_blank" title="'. esc_attr( get_the_title( $teach->ID ) ) .'">' . esc_html( $teach->post_title ) . '<i class="event-dot">' . esc_html(',','k-event') . '</i>' . '</a>';?>
										</div>

									</div>
								<?php endif;?>


							<?php endforeach;?>

						</div><!-- K2t Speakers -->
					</div><!-- Entry Speakers -->
				<?php endif;?>

				<!-- END Speakers -->

				<!-- Phone -->

				<?php if ( ! empty( $phone ) ) : ?>
					<div class="event-phone">
						<div class="event-text"><?php esc_html_e('Phone:','k-event'); ?></div>
						<div><span><?php echo esc_html( $phone ); ?></span></div>
					</div>
				<?php endif;?>

			</div><!-- event-meta -->

			<?php if ( true ) include( 'event-related.php' ); ?>
			<!-- sidebar -->

			<?php
				if ( true ) {
					//get_sidebar();
				}
			?>
		</div><!-- event-sidebar -->

	</div><!-- .k2t-wrap -->
</div><!-- .k2t-content -->

<?php get_footer(); ?>