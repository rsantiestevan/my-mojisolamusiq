<?php
/**
 * The template for displaying Category pages
 */
 
global $wp_query;

get_header();

// Register variables

$classes 						= array();

$tax_type = is_tax('k-event-category') ? 'k-event-category' : 'k-event-tag';

$pre					= 'tax_event_';

// Get metadata of event in single

$tax_stt 		= array( 
	// Layout
	'layout'						=> 'no_sidebar', 
	'custom_sidebar' 				=> '',
	'cat_limit' 					=> '',
	'style'							=> '',
);

if ( 'stevecadey_get_acf_field' ) $tax_stt = stevecadey_get_acf_field( $tax_stt, 'option', false, $pre );

$classes[] =  str_replace( '_', '-', $tax_stt['layout'] );

// Get current term
$term = get_queried_object()->term_taxonomy_id;

?>
<div class="k2t-content <?php echo esc_attr( implode( ' ', $classes ) ) ?>">

	<div class="container k2t-wrap">

		<!-- Main -->
		<main class="k2t-main page" role="main">
			<?php
				echo K_Event::K_Render_event_listing( $tax_stt['style'], $tax_stt['cat_limit'], 'show', $term, '', 'show', '', $tax_type );
			?>
		</main>

		<!-- Sidebar -->
		<?php
			if ( $tax_stt['layout'] != 'no_sidebar' ) {
				get_sidebar( $tax_stt['custom_sidebar'] );
			}
		?>
	</div>
</div><!-- .k2t-content -->

<?php get_footer(); ?>