<?php
/**
 * Theme functions for stevecadey
 *
 * Do not edit the core files.
 * Add any modifications necessary under a child theme.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

/*--------------------------------------------------------------
	Define Constants
--------------------------------------------------------------*/
define( 'STEVECADEY_THEME_PATH', get_template_directory() . '/' );
define( 'STEVECADEY_THEME_URL', get_template_directory_uri() . '/' );
define( 'STEVECADEY_FRAMEWORK_PATH', STEVECADEY_THEME_PATH . 'framework/' );
define( 'STEVECADEY_FRAMEWORK_URL', STEVECADEY_THEME_URL . 'framework/' );
define( 'STEVECADEY_TEMPLATE_PATH', STEVECADEY_THEME_PATH . 'templates/' );

/*--------------------------------------------------------------
	Admin - Framework
--------------------------------------------------------------*/
require_once STEVECADEY_FRAMEWORK_PATH . 'index.php';
require_once STEVECADEY_FRAMEWORK_PATH . 'inc/k2timporter/import.php'; // Advance Importer
require_once STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/k2t-icon/hooks.php'; // add Icon Feature
require_once STEVECADEY_FRAMEWORK_PATH . 'extensions/widgets/widget-register.php'; // Adding widgets

/*--------------------------------------------------------------
	3rd-plugins
--------------------------------------------------------------*/
require_once STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/class-tgm-plugin-activation.php'; // Load TGM Plugin Activation library if not already loaded
require_once STEVECADEY_FRAMEWORK_PATH . 'inc/register_custom_fields.php'; // Adding advanced custom fields
require_once STEVECADEY_FRAMEWORK_PATH . 'inc/mega-menu/mega-menu-framework.php'; // Adding k2t mega menu
require_once STEVECADEY_FRAMEWORK_PATH . 'inc/mega-menu/mega-menus.php';

if ( class_exists('woocommerce') ) {
	require_once STEVECADEY_FRAMEWORK_PATH . 'inc/class.k2t_template_woo.php';		// intergrate with woocommerce
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}