<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */
if ( empty( $smof_data ) ) $smof_data = of_get_options();

$footer_class = stevecadey_footer_bg();

$bottom_class = stevecadey_footer_bottom(); ?>

	</div><!-- .k2t-body -->

	<?php if ( ! is_404() ) : ?>
	<footer class="k2t-footer dark-style">
		<?php
			if ( stevecadey_check_footer() ) :?>
				<div class="k2t-bottom" style="<?php echo esc_attr( implode( ' ', $footer_class ) ); ?>">
					<div class="k2t-wrap">
						<div class="k2t-row">
							<?php
								stevecadey_get_footer_layout();
							?>
						</div><!-- .k2t-row -->
					</div><!-- .k2t-wrap -->
				</div><!-- .k2t-bottom -->
		<?php endif;?>

		<?php if( isset( $smof_data['footer_bottom_menu'] ) && $smof_data['footer_bottom_menu'] == '1' ): ?>
			<div class="k2t-info" style="<?php echo esc_attr( implode( ' ', $bottom_class ) ); ?>">
				<div class="k2t-wrap">
					<div class="k2t-row">
						<div class="col-4 col-left">
							<?php if( function_exists( 'stevecadey_footer_logo' ) ):
								stevecadey_footer_logo();
								endif;
							?>
						</div>
						<div class="col-4">
							<?php if( $smof_data['footer_copyright_text'] != '' ): ?>
								<?php echo do_shortcode( wp_kses_post( $smof_data['footer_copyright_text'] ) ); ?>
							<?php endif;?>
						</div>
						<div class="col-4 col-right">
							<?php if( function_exists( 'stevecadey_bottom_social' ) ):
								stevecadey_bottom_social();
								endif;
							?>
						</div>
					</div>
				</div>
			</div><!-- .k2t-info -->
		<?php endif;?>

	</footer><!-- .k2t-footer -->
	<?php endif;?>
</div><!-- .k2t-container -->

<!-- Show Offcanvas sidebar -->

<?php if ( $smof_data['offcanvas_turnon'] && ! is_404() ) :

	$offcanvas_style = stevecadey_off_canvas(); ?>

	<div class="offcanvas-sidebar <?php echo esc_attr( $smof_data['offcanvas_sidebar_position'] . '-pos');?>" style="<?php echo esc_attr( $offcanvas_style );?>">
		<span class="btn-close"></span>
		<div class="k2t-sidebar">
			<?php dynamic_sidebar( $smof_data['offcanvas_sidebar'] ); ?>
		</div>
	</div>
<?php endif; ?>

<!-- End Show Offcanvas sidebar -->

<?php if ( ! is_404() ) : ?>

		<div class="k2t-searchbox">
			<div class="k2t-searchbox-close hamburger hamburger--squeeze js-hamburger is-active">
	        	<div class="hamburger-box">
	          		<div class="hamburger-inner"></div>
	        	</div>
	      	</div>
			<div class="mark"></div>
			<form class="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
				<input type="text" value="<?php echo esc_url( get_search_query() ); ?>" name="s" id="s" placeholder="<?php esc_html_e('Type Your Keyword','stevecadey'); ?>" />
			</form>
		</div>

		<?php
		if ( $smof_data['footer_gototop'] ) :
			echo '<a href="#" class="k2t-btt k2t-element-hover"><span aria-hidden="true" class="zmdi zmdi-chevron-up"></span></a>';
		endif;
	endif;

wp_footer(); ?>

</body>
</html>
