<?php
/**
 * The template for displaying woocommerce.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
if ( empty( $smof_data ) )
	$smof_data = of_get_options();

$stevecadey_classes = array();

$pre = 'shop_';
if ( is_product() ) {
	$pre = 'product_';
}


$arr_page_meta_val  	= array();
$arr_page_meta 		= array( 
	// Layout
	'layout'						=> 'no_sidebar', 
	'column'						=> '',
);

foreach ( $arr_page_meta as $meta => $val ) {
	if ( function_exists( 'get_field' ) ) {
		if ( ! empty( $smof_data[$pre . $meta] ) ) {
			$arr_page_meta_val[$meta] = $smof_data[$pre . $meta];
		}
	}
}
extract( shortcode_atts( $arr_page_meta, $arr_page_meta_val ) );

if ( is_tax('product_cat') && function_exists('get_field') ) :
	$queried_object = get_queried_object();
	$layout = get_field( 'product_cat_layout', 'product_cat_' . $queried_object->term_id )  ;
endif;

if ( is_tax('product_color') && function_exists('get_field') ) :
	$queried_object = get_queried_object();
	$layout = get_field( 'product_color_layout', 'product_color_' . $queried_object->term_id )  ;
endif;

$stevecadey_classes[] = str_replace( '_', '-', $layout );

get_header();?>
	<div class="k2t-content <?php echo esc_attr( implode( ' ', $stevecadey_classes ) ); ?>">
		<div class="shop-titlebar"></div>
		<section class="k2t-wrap">
			<main id="main" class="k2t-shop <?php if ( ! is_product() ) : echo 'columns-' . esc_attr( $column ) . ''; endif; ?>">
			<?php if ( ! is_product() ) : ?>

			<?php endif;?>

				<?php woocommerce_content(); ?>
				
			</main><!-- #main -->
			
			<?php
			if ( 'no_sidebar' != $layout ) { ?>
				<div class="k2t-shop-sidebar" role="complementary">
					<?php
						dynamic_sidebar( 'shop_sidebar' );	
					?>
				</div><!-- .k2t-sidebar -->
			<?php } ?>

		</section><!-- .container -->

		<?php 
		$product_custom_footer = isset ( $smof_data['product_custom_footer'] ) ? $smof_data['product_custom_footer'] : '';
		if ( ! empty( $product_custom_footer ) ) :
			echo '<div>';
			echo apply_filters( 'the_content', $product_custom_footer );
			echo '</div>';
		endif;?>

	</div><!-- .k2t-content -->
<?php get_footer(); ?>