<?php
/**
 * The template for displaying single post content.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options

if ( empty( $smof_data ) ) $smof_data = of_get_options();

// Get single setting

$single_stt = array(
					'link'					=> '',
					'single_layout'			=> '',
					'link_format_url'		=> '',
					'display_categories'	=> '',
					'display_posted_author'	=> '',
					'display_post_date'		=> '',
					'display_tags'			=> '',
					'display_authorbox'		=> '',
					'display_related_post'	=> '',
			  );

$single_stt	= stevecadey_get_acf_field( $single_stt, get_the_ID() );

?>

<div id="main-col" <?php post_class(); ?>>

	<section class="entry-box">

		<!-- Thumnail -->

		<?php include get_template_directory() . '/templates/blog/post-format.php'; ?>

		<div class="entry-content-single">

			<?php
				// title

				if ( get_post_format() == 'link' ) :
					the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $single_stt['link_format_url'] ) ), '</a></h2>' );
				else :
					if ( $smof_data['blog_post_link'] ) {
						the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );
					}
				endif;
			?>

			<div class="top-meta">

				<?php if ( $single_stt['display_posted_author'] ) : ?>
					<span class="author-meta">
						<?php
							echo '<i class="zmdi zmdi-account"></i><a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"> ' . get_the_author() . '</a>';
						?>
					</span>
				<?php endif;?>
				<span class="entry-favo">
					<i class="zmdi zmdi-eye"></i>
						<?php echo esc_html( stevecadey_wpb_get_post_views( get_the_ID() ) ) ?>
				</span>

				<?php if ( $smof_data['blog_number_comment'] ) :?>
					<span class="entry-comment"><i class="zmdi zmdi-comment-text"></i><a href="<?php comments_link(); ?>"><?php comments_number( '0', '1', '%' ); ?></a></span>
				<?php endif;?>

				<span class="entry-favo"><?php if(function_exists('wp_ulike')) wp_ulike('get'); ?></span>

			</div><!-- End top meta -->

			<!-- Content -->

			<?php if ( get_post_format() != 'quote' ) :?>
				<div class="post-entry clearfix">
					<?php the_content(); ?>
				</div><!-- .post-entry -->
			<?php endif;?>

			<div class='bottom-meta'>

				<!-- Social list -->

				<div class="share-meta">
					<?php
						if (function_exists( 'stevecadey_social_share' ) ) :
							stevecadey_social_share();
						endif;
					?>
				</div><!-- End share-meta -->

				<!-- Tag list -->

				<div class="tag-meta">
					<?php
						if ( $single_stt['display_tags'] ) :
							$tags = get_the_tags();
							if( $tags ) :
								echo '<div class="widget_tag_cloud"><div class="tagcloud"><span>' . esc_html__('Tags:', 'stevecadey') . '</span>';
								foreach ( $tags as $key => $tag) {
									echo '<a href="'. esc_url( get_tag_link( $tag->term_id ) ) .'" title="'. esc_attr( $tag->name ) .'">'. esc_attr( $tag->name ) . '<i class="event-dot">' . esc_html(',','k-event') . '</i>' . '</a>';
								}
								echo '</div></div>';
							endif;
						endif;// End display tags
					?>
				</div> <!-- End tag-meta -->

			</div><!-- End bottom-meta -->
		</div>

	</section><!--end:entry-box-->

	<!-- Author box -->

	<?php
		if ( $single_stt['display_authorbox'] ) : ?>
			<article class="about-author clearfix">
				<div class="author-avatar">
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), '70' , false );
				?>
				</div>
				<div class="author-infor">
					<div class="author-info">
						<h4><?php echo get_the_author_link();?></h4>
					</div>
					<span class="description"><?php echo get_the_author_meta( 'description' );?></span>
				</div>
			</article><!-- End about-author-->
	<?php endif;?>

	<!-- Comment box -->

	<?php
	if ( $smof_data['single_commnet_form'] ) :
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	endif;	?>

</div>

<!-- Related -->
<?php 

if( isset( $smof_data['display_related_post'] ) && $smof_data['display_related_post'] ):
include get_template_directory() . '/templates/blog/post-related.php'; endif;
