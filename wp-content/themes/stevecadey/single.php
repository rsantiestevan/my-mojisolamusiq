<?php
/**
 * The template for displaying all single posts.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

$blog_single_stt = array(
					'single_layout' => 'right_sidebar',
				);

$blog_single_stt = stevecadey_get_acf_field( $blog_single_stt, get_the_ID() );

// Setup layout

$stevecadey_classes = array();

$stevecadey_classes[] = str_replace( '_', '-', $blog_single_stt['single_layout'] );

get_header(); ?>

	<div  class="k2t-content <?php echo implode( ' ', $stevecadey_classes ) ?>">
		<div class="k2t-wrap">
			<main class="k2t-blog" role="main">
				<div class="k2t-blog-inner">
					<?php

						while ( have_posts() ) : the_post();
							get_template_part( 'content', 'single' );
						endwhile;
					?>
				</div>
			</main><!-- .k2t-blog -->

			<?php 	// SIDEBAR
				if ( $blog_single_stt['single_layout'] != 'no_sidebar' ) get_sidebar();
			?>
		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
