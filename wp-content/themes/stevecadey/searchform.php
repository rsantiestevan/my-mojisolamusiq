<?php
/**
 * Search form.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */
?>
<form method="get" id="searchform" class="searchform search_default" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="form-group">
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" placeholder="<?php esc_html_e( 'Type your keyword...', 'stevecadey' ); ?>" />
		<button type="submit" ><?php esc_html_e( 'Search', 'stevecadey' ); ?></button>
	</div>
</form>