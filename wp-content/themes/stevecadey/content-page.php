<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="page-entry">
		<div class="content-page">
		<?php
			the_content();
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'stevecadey' ),
				'after'  => '</div>',
			) );
		?>
		</div><!-- .content-page -->
	</div><!-- .page-entry -->

</article><!-- #post-## -->
