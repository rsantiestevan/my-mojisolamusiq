<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme option
if ( empty( $smof_data ) )
	$smof_data = of_get_options();
get_header(); ?>

	<section class="k2t-not-found">
		<main class="k2t-wrap">

			<div class="error-404-text">
				<h1></h1>
			</div><!--end:error-404-right-->

			<div class="error-404-image">
				<img src="<?php echo esc_url( $smof_data['404_image'] ); ?>" alt="404 page" />
			</div><!--end:error-404-left-->

		</main><!-- .k2t-wrap -->
	</section><!-- .k2t-not-found -->

<?php get_footer();

