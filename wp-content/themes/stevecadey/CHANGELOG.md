# SteveCadey 1.0 change log
![mou icon](http://i.imgur.com/tXh7m8x.jpg)

## Version 1.0.7
### Released date: 03-02-2017
*Fix: Theme Option 
*Update: Visual Composer, Revolution slider and Advance Custom Fields plugins

## Version 1.0.6
### Released date: 02-13-2017
*Fix: Title bar
*Update: Visual Composer, Revolution slider and Advance Custom Fields plugins

## Version 1.0.5
### Released date: 12-22-2016
*Fix: Social icon on Header 
*Fix: Widget bugs
*Fix: Load text domain
*Fix: K-Arist plugins
*Fix: K-Event plugins
*Update: Visual Composer, Revolution slider and Advance Custom Fields plugins

## Version 1.0.4
### Released date: 12-06-2016
*Add: Envato market recommend plugin for Auto update theme
*Fix: Animated
*Fix: Blog post, Event listing
*Fix: Event classic listing
*Fix: K-Gallery image hover
*Update: Visual Composer, Revolution slider and Advance Custom Fields plugins

## Version 1.0.3
### Released date: 11-21-2016
*Fix: Responsive for Search form
*Fix: Audio post format
*Fix: K-Artist carousel
*Fix: Responsive for Playlist
*Fix: Countdown shortcode
*Fix: Titlebar
*Fix: Recent post
*Update: Visual Composer, Revolution slider and Advance Custom Fields plugins

## Version 1.0.2
### Released date: 11-08-2016
*Fix: Responsive for mobile device
*Fix: Typography style css
*Fix: K-Event plugin
*Fix: K-Artist title bar
*Fix: Blog style css
*Fix: Recent Event for K-Event
*Fix: Related Event for K-Event
*Update: Visual Composer, Revolution slider and Advance Custom Fields plugins

## Version 1.0.1
### Released date: 11-03-2016
*Fix: K-Gallery plugin
*Fix: K-Event plugin
*Fix: K-Artist plugin
*Update: Visual Composer, Revolution slider and Advance Custom Fields plugins

## Version 1.0
### Released date: 11-02-2016
#### Initial release