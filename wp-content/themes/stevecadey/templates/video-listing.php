<?php
/**
 * The blog template file.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 * Template Name: Video Listing
 */

// Set up archive setting

$archive_stt = array(
					'blog_layout'			=> 'right_sidebar',
					'blog_style'			=> 'large',
				);

// Get page template setting

$page_stt = array(
					'blog_cat_filter'		=> '',
					'blog_filter'			=> '',
				);

$page_stt = stevecadey_get_acf_field( $page_stt, get_the_ID() );

// Set up class blog layout
$stevecadey_class 			= array();
$stevecadey_class[] 		= str_replace('_','-', $archive_stt['blog_layout'] );
$stevecadey_class[] 		= 'b-' . $archive_stt['blog_style'];

// Start out put

get_header(); ?>

	<div class="k2t-content <?php echo esc_attr( implode( ' ', $stevecadey_class ) );?>">

		<div class="k2t-wrap">

			<main class="k2t-blog">

				<?php
					$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );
					$args = array(
						'post_type'      => 'post',
						'posts_per_page' => get_option('posts_per_page '),
						'paged'			 => $paged,
						'cat'			 => $page_stt['blog_cat_filter'],
					);
					$wp_query = new WP_query( $args );
					if ( $wp_query->have_posts() ) :
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							include STEVECADEY_TEMPLATE_PATH . 'blog/content-video.php';
						endwhile;
						include_once get_template_directory() . '/templates/navigation.php';
						wp_reset_postdata();
					endif;
				?>

			</main><!-- .k2t-main -->

			<?php get_sidebar(); ?>

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer(); ?>
