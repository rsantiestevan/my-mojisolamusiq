<?php
/**
 * The template for displaying title and breadcrumb of event.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

if ( empty( $smof_data ) )
	$smof_data = of_get_options();

// Get post or page id
if ( is_home() ) {
	$id = get_option( 'page_for_posts' );
} elseif ( function_exists( 'is_shop' ) && is_shop() ) {
	$id = get_option( 'woocommerce_shop_page_id' );
} else {
	$id = get_the_ID();
}


$stevecadey_classes = $css = $html = array();

// Check pre
$pre 		= 'page-';
$single_pre = 'page_';

if ( is_single() ) {
	if ( is_singular( 'product' ) ) {
		$pre = 'product_';
	} else {
		$pre = 'single_';
	}
} 
elseif ( function_exists( 'is_shop' ) && is_shop() ) {
	$pre = 'shop_';
} 
elseif ( is_page() || is_archive() || is_author() || is_category() || is_home() || is_tag() ) {
	$pre = 'blog_';
}


$acf_titlebar = array(
	'layout'									=> '',
	'display_titlebar' 							=> '',
	'display_meta' 								=> 'show',
	'display_excerpt'							=> '',
	'display_breadcum'							=> 'show',
	'titlebar_font_size' 						=> '',
	'titlebar_color' 							=> '',
	'pading_top' 								=> '',
	'pading_bottom'								=> '',
	'background_color' 							=> '',
	'background_image' 							=> '',
	'background_position' 						=> '',
	'background_size' 							=> '',
	'background_repeat' 						=> '',
	'background_parallax' 						=> '',
	'titlebar_overlay_opacity' 					=> '',
	'overlay_background_color'					=> '',
	'titlebar_clipmask_opacity' 				=> '',
	'clipmask_background_img' 					=> '',
	'titlebar_custom_content'  					=> '',
	'breadcrumb'								=> '',
);

$acf_titlebar = stevecadey_get_acf_field( $acf_titlebar, $id, false );

// get smof_data if setting of single is default

if ( $acf_titlebar['display_titlebar'] == 'default' ) 
	$acf_titlebar = stevecadey_get_smof_field( $acf_titlebar, $pre );

if ( is_singular('product') || ( function_exists( 'is_shop' ) && is_shop() ) ) 
	$acf_titlebar['display_titlebar'] = 'show';

extract( $acf_titlebar );

if ( is_singular( 'product' ) ) $stevecadey_classes[] = str_replace('-', '_', $layout);

// All meta data

if ( ! $display_meta ) 		$stevecadey_classes[] = 'hidden-all';
if ( ! $display_excerpt ) 	$stevecadey_classes[] = 'hidden-excerpt';
if ( ! $display_breadcum ) 	$stevecadey_classes[] = 'hidden-bc';

// Title bar font size

if ( $titlebar_font_size ) {
	if ( is_numeric( $titlebar_font_size ) ) {
		$titlebar_font_size = ! empty( $titlebar_font_size ) ? 'font-size:' . $titlebar_font_size . 'px;' : '';
	} else {
		$titlebar_font_size = ! empty( $titlebar_font_size ) ? 'font-size:' . $titlebar_font_size . ';' : '';
	}
}

// Title bar color
if ( $titlebar_color ) {
	$titlebar_color = ! empty( $titlebar_color ) ? 'color:' . $titlebar_color . ';' : '';
}

// Padding for title bar
if ( $pading_top ) {
	if ( is_numeric( $pading_top ) ) {
		$css[] = ! empty( $pading_top ) ? 'padding-top:' . $pading_top . 'px;' : '';
	} else {
		$css[] = ! empty( $pading_top ) ? 'padding-top:' . $pading_top . ';' : '';
	}
}
if ( $pading_bottom ) {
	if ( is_numeric( $pading_bottom ) ) {
		$css[] = ! empty( $pading_bottom ) ? 'padding-bottom:' . $pading_bottom . 'px;' : '';
	} else {
		$css[] = ! empty( $pading_bottom ) ? 'padding-bottom:' . $pading_bottom . ';' : '';
	}
}

// Background color
if ( $background_color ) {
	$css[] = ! empty( $background_color ) ? 'background-color: ' . $background_color . ';' : '';
}

// Background image
if ( $background_image ) {
	if ( is_numeric( $background_image ) ) {
		$background_image = wp_get_attachment_image_src( $background_image, 'full' );
		$background_image = $background_image[0];
	}
	$css[] = ! empty( $background_image ) ? 'background-image: url(' . $background_image . ');' : '';
	$css[] = ! empty( $background_position ) ? 'background-position: ' . $background_position . ';' : '';
	$css[] = ! empty( $background_position ) ? 'background-position: left center;' : '';
	$css[] = ! empty( $background_repeat ) ? 'background-repeat: ' . $background_repeat . ';' : '';

	if ( 'full' == $background_size ) {
		$css[] = ! empty( $background_size ) ? 'background-size: 100%;' : '';
	} else {
		$css[] = ! empty( $background_size ) ? 'background-size: ' . $background_size . ';' : '';
	}
}

// Background parallax
$inline_attr = '';
if ( $background_parallax ) {
	$stevecadey_classes[] 	= empty( $background_parallax ) ? '' : 'parallax';
	$css[] 		= 'background-size: 100%; background-attachment: fixed;';
	if( function_exists( 'stevecadey_parallax_titlebar' ) ){
		stevecadey_parallax_titlebar();
	}
	$inline_attr = 'data-stellar-background-ratio=0.3';
}

// Title bar mask color & background
if ( $titlebar_overlay_opacity || $overlay_background_color ) :

	$overlay_opacity = empty( $titlebar_overlay_opacity ) ? '' : 'opacity: 0.' . $titlebar_overlay_opacity . ';';
	$overlay_bg_img = empty( $overlay_background_color) ? '' : 'background: ' . $overlay_background_color . ';';

	$html[] = empty( $titlebar_overlay_opacity ) ? '' : '<div class="mask colors" style="' .  esc_attr( $overlay_opacity ) . $overlay_background_color . '"></div>';

endif;

if ( $titlebar_clipmask_opacity || $clipmask_background_img ) :

	$clipmask_opacity = empty( $titlebar_clipmask_opacity ) ? '' : 'opacity: 0.' . $titlebar_clipmask_opacity . ';';

	$clipmask_bg_img = empty( $clipmask_background_img ) ? '' : 'background: url("' . $clipmask_background_img . '");';

	$html[] = empty( $titlebar_clipmask_opacity ) ? '' : '<div class="mask pattern" style="' . esc_attr( $clipmask_opacity ) . esc_attr( $clipmask_bg_img ) . '"></div>';

endif;

if ( $acf_titlebar['display_titlebar'] ) :
?>

	<div class="k2t-title-bar <?php echo esc_attr( implode( ' ', $stevecadey_classes ) ); ?>" style="<?php echo esc_attr( implode( '', $css ) ); ?>" <?php echo esc_attr($inline_attr); ?>>
		<?php echo implode( ' ', $html ); ?>
		<div>
			<div class="container k2t-wrap">
				<h1 class="main-title" style="<?php echo esc_attr($titlebar_font_size . $titlebar_color); ?>">
					<?php
						if ( is_tag() ) {

							printf( single_tag_title() );

						} elseif ( is_day() ) {

							printf( the_time( 'F j, Y' ) );

						} elseif ( is_month() ) {

							printf( the_time( 'F, Y' ) );

						} elseif ( is_year() ) {

							printf( the_time( 'Y' ) );

						} elseif ( is_search() ) {

							printf( esc_html__( 'Search for ', 'stevecadey' ) . get_search_query() );

						} elseif ( is_front_page() ) {

							printf( bloginfo( 'name' ) );

						} elseif ( is_single() ) {

							printf( single_post_title() );

						} elseif ( is_tax( 'portfolio-category' ) ) {

						    $term = get_queried_object();
						    printf(  $term->name );

						} elseif ( is_category() ) {

							printf( single_cat_title() );

						} elseif ( is_author() ) {

							$curauth = get_queried_object();

							printf( $curauth->nickname );

						} elseif ( is_page() ) {
							the_title();

						} elseif ( is_home() ) {

							printf( esc_html__( 'Blog', 'stevecadey' ) );

						} elseif ( is_404() ) {

							printf( esc_html__( 'Error 404', 'stevecadey' ) );

						} elseif (  function_exists( 'is_product_category' ) && is_product_category() ) {

							$id          = get_the_ID();
							$product_cat = wp_get_post_terms( $id, 'product_cat' );
							$title = $slug = array();
							if ( $product_cat ) {
								foreach ( $product_cat as $category ) {
									$title[] = "{$category->name}";
								}
							}

							printf( $title[0] );

						} elseif ( is_post_type_archive( 'product' ) ) {

							printf( esc_html__( 'Shop', 'stevecadey' ) );

						} elseif ( is_post_type_archive() ) {

							printf( post_type_archive_title() );

						} elseif (
							( function_exists( 'is_woocommerce' ) && is_woocommerce() ) ||
							( function_exists( 'is_cart' ) && is_cart() ) ||
							( function_exists( 'is_checkout' ) && is_checkout() )
						) {
							$product_cat = wp_get_post_terms( $id, 'product_cat' );
							$title = array();
							if ( $product_cat ) {
								foreach ( $product_cat as $category ) {
									$title[] = "{$category->name}";
								}
							}
							echo ( $title[0] );
						}
					?>
				</h1>
				<div class="main-excerpt">
					<?php
						if ( is_single() ) {

							if ( is_singular( 'product' ) ) {

								$content = get_extended( $post->ID );
								if (empty($titlebar_custom_content)) {
									echo esc_html__( 'ID: ', 'stevecadey' ) . esc_html( $content['main'] );
								} else {
									;
								}

							} else if ( is_singular( 'post-project' ) ){
								// $categories = get_the_terms(get_the_ID(), 'project-category');
								// $cat_name = $categories[0]->name;
        						//  echo '<span class="entry-category">' .  esc_html($cat_name) . '</span>';
							} else {
								// $author_id=$post->post_author;
								// echo '<span class="entry-date">' . get_the_date() . '</span>';
								// echo '<span class="entry-author"></i>' .  get_the_author_meta( 'user_nicename', $author_id ) . '</span>';
							}

						} elseif ( is_tax( 'portfolio-category' ) ) {
						    $term = get_queried_object();
						    printf(  esc_html( $term->description ) );
						} elseif ( is_category() ) {
							// Show an optional term description.
							$term_description = term_description();
							if ( ! empty( $term_description ) ) {
								printf( '<div class="taxonomy-description">%s</div>', $term_description );
							} elseif ( $titlebar_custom_content ) {
								echo do_shortcode( wp_kses_post( $titlebar_custom_content ) );
							}
						}
						if ( !empty( $titlebar_custom_content) )
							echo '<div class="custom_content">' . do_shortcode( wp_kses_post( $titlebar_custom_content ) ) . '</div>';
					?>
				</div><!-- .main-excerpt -->
			</div>
		</div>

		<?php
			if ( isset( $smof_data['breadcrumb'] ) &&  $smof_data['breadcrumb'] ) $bc_hidden = ''; else $bc_hidden = 'hidden';
			if ( function_exists( 'is_shop' )  ) {
				if ( true ) {
					echo '<div><div class="container k2t-wrap breadcrumb ' . esc_attr($bc_hidden) . '">';
					stevecadey_breadcrumbs();
					echo '</div><!-- k2t-wrap --></div>';
				}
			} else {
				if ( is_author() ) {

					$curauth = get_queried_object();
					echo '<div><div class="container k2t-wrap breadcrumb ' . esc_attr($bc_hidden) . '"><p class="author-email">';
						printf( $curauth->user_email );
					echo '</p></div><!-- k2t-wrap --></div>';
				}
				else
					if ( isset( $smof_data['breadcrumb'] ) && $smof_data['breadcrumb'] ) {
					echo '<div><div class="container k2t-wrap breadcrumb ' . esc_attr($bc_hidden) . '">';
					stevecadey_breadcrumbs();
					echo '</div><!-- k2t-wrap --></div>';
				}
			}
		?>

	</div><!-- .k2t-title-bar -->

<?php endif;