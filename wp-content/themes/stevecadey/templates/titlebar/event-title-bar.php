<?php
/**
 * The template for displaying title and breadcrumb of Event.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
if ( empty( $smof_data ) )
	$smof_data = of_get_options();

// Get post or page id
$id 		= is_archive() ? 'option' : get_the_ID();
$stevecadey_classes 	= $css = $html = array();

// Check pre
$pre 		= is_archive() ? 'tax_event_' :'event_';


// Get metadata of Event in single
$setting 		= array(
	'layout'									=> '',
	'display_titlebar' 							=> 'show',
	'titlebar_font_size' 						=> '',
	'titlebar_color' 							=> '',
	'pading_top' 								=> '',
	'pading_bottom'								=> '',
	'background_color' 							=> '',
	'background_image' 							=> '',
	'background_position' 						=> '',
	'background_size' 							=> '',
	'background_repeat' 						=> '',
	'background_parallax' 						=> '',
	'titlebar_overlay_opacity' 					=> '',
	'overlay_background_color' 					=> '',
	'titlebar_clipmask_opacity' 				=> '',
	'clipmask_background_img' 					=> '',
	'titlebar_custom_content'  					=> ''
);

if ( function_exists( 'stevecadey_get_acf_field' ) ) {
	$setting = stevecadey_get_acf_field( $setting, $id, false, $pre );
}

extract( $setting, EXTR_OVERWRITE );

// class layout

$stevecadey_classes[] = str_replace( '_', '-', $layout );

// Title bar font size
if ( $titlebar_font_size ) {
	if ( is_numeric( $titlebar_font_size ) ) {
		$titlebar_font_size = ! empty( $titlebar_font_size ) ? 'font-size:' . $titlebar_font_size . 'px;' : '';
	} else {
		$titlebar_font_size = ! empty( $titlebar_font_size ) ? 'font-size:' . $titlebar_font_size . ';' : '';
	}
}

// Title bar color
if ( $titlebar_color ) {
	$titlebar_color = ! empty( $titlebar_color ) ? 'color:' . $titlebar_color . ';' : '';
}
// Padding for title bar
if ( $pading_top ) {
	if ( is_numeric( $pading_top ) ) {
		$css[] = ! empty( $pading_top ) ? 'padding-top:' . $pading_top . 'px;' : '';
	} else {
		$css[] = ! empty( $pading_top ) ? 'padding-top:' . $pading_top . ';' : '';
	}
}
if ( $pading_bottom ) {
	if ( is_numeric( $pading_bottom ) ) {
		$css[] = ! empty( $pading_bottom ) ? 'padding-bottom:' . $pading_bottom . 'px;' : '';
	} else {
		$css[] = ! empty( $pading_bottom ) ? 'padding-bottom:' . $pading_bottom . ';' : '';
	}
}

// Background color
if ( $background_color ) {
	$css[] = ! empty( $background_color ) ? 'background-color: ' . $background_color . ';' : '';
}

// Background image
if ( $background_image ) {
	if ( is_numeric( $background_image ) ) {
		$background_image = wp_get_attachment_image_src( $background_image );
		$background_image = $background_image[0];
	}
	$css[] = ! empty( $background_image ) ? 'background-image: url(' . $background_image . ');' : '';
	$css[] = ! empty( $background_position ) ? 'background-position: ' . $background_position . ';' : '';
	$css[] = ! empty( $background_repeat ) ? 'background-repeat: ' . $background_repeat . ';' : '';
	if ( 'full' == $background_size ) {
		$css[] = ! empty( $background_size ) ? 'background-size: 100%;' : '';
	} else {
		$css[] = ! empty( $background_size ) ? 'background-size: ' . $background_size . ';' : '';
	}
}

// Background parallax
$inline_attr = '';
if ( $background_parallax ) {
	$stevecadey_classes[] 	= empty( $background_parallax ) ? '' : 'parallax';
	$css[] 		= 'background-size: 100%; background-attachment: fixed;';
	if( function_exists( 'stevecadey_parallax_titlebar' ) ){
		stevecadey_parallax_titlebar();
	}
	$inline_attr = 'data-stellar-background-ratio=0.3';
}

// Title bar mask color & background

if ( $titlebar_overlay_opacity || $overlay_background_color ) :

	$overlay_opacity = empty( $titlebar_overlay_opacity ) ? '' : 'opacity: 0.' . $titlebar_overlay_opacity . ';';
	$overlay_bg_color = empty( $overlay_background_color) ? '' : 'background: ' . $overlay_background_color . ';';

	$html[] = empty( $titlebar_overlay_opacity ) ? '' : '<div class="mask colors" style="' .  esc_attr( $overlay_opacity ) . $overlay_bg_color . '"></div>';

endif;

if ( $titlebar_clipmask_opacity || $clipmask_background_img ) :

	$clipmask_opacity = empty( $titlebar_clipmask_opacity ) ? '' : 'opacity: 0.' . $titlebar_clipmask_opacity . ';';

	$clipmask_bg_img = empty( $clipmask_background_img ) ? '' : 'background-image: url("' . $clipmask_background_img . '");';

	$html[] = empty( $titlebar_clipmask_opacity ) ? '' : '<div class="mask pattern" style="' . esc_attr( $clipmask_opacity ) . esc_attr( $clipmask_bg_img ) . '"></div>';

endif;

if ( 'show' == $display_titlebar ) :
?>

	<div class="k2t-title-bar event-title-bar <?php echo esc_attr( implode( ' ', $stevecadey_classes ) ); ?>" style="<?php echo esc_attr( implode( ' ', $css ) ); ?>" <?php echo esc_attr($inline_attr); ?>>
		<?php echo implode( ' ', $html ); ?>
		<div>
			<div class="container k2t-wrap">
				<h1 class="main-title" style="<?php echo esc_attr($titlebar_color); ?>">
					<?php
						if ( is_single() ) {
							printf( single_post_title() );
						} elseif ( is_archive() ) {

							global $wp_query;
						    $term = $wp_query->get_queried_object();
						    printf(  $term->name );

						}
					?>
				</h1>
				<div class="main-excerpt" style="<?php echo esc_attr($titlebar_color); ?>">
					<?php
						if ( ! empty( $titlebar_custom_content ) ) :

							echo do_shortcode( $titlebar_custom_content );

						else :
							
							if ( is_single() ) :
								$event_position = (function_exists('get_field')) ? get_field('event_position', get_the_ID()) : ''; $event_position = empty($event_position) ? '' : $event_position;
								echo esc_html($event_position);
							else :
								global $wp_query;
							    $term = $wp_query->get_queried_object();
							    printf(  esc_html( $term->description ) );
							endif;

						endif;
					?>
				</div><!-- .main-excerpt -->
				<?php
					if ( isset( $smof_data['breadcrumb'] ) &&  $smof_data['breadcrumb'] ) $bc_hidden = ''; else $bc_hidden = 'hidden';
					if ( function_exists( 'is_shop' )  ) {
						if ( true ) {
							echo '<div><div class="container k2t-wrap breadcrumb ' . esc_attr($bc_hidden) . '">';
							stevecadey_breadcrumbs();
							echo '</div><!-- k2t-wrap --></div>';
						}
					} else {
						if ( is_author() ) {

							$curauth = get_queried_object();
							echo '<div><div class="container k2t-wrap breadcrumb ' . esc_attr($bc_hidden) . '"><p class="author-email">';
								printf( $curauth->user_email );
							echo '</p></div><!-- k2t-wrap --></div>';
						}
						else
							if ( $smof_data['breadcrumb'] ) {
							echo '<div><div class="container k2t-wrap breadcrumb ' . esc_attr($bc_hidden) . '">';
							stevecadey_breadcrumbs();
							echo '</div><!-- k2t-wrap --></div>';
						}
					}
				?>

			</div>
		</div>

	</div><!-- .k2t-title-bar -->

<?php endif;