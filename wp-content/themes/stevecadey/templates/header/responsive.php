<?php
/**
 * The template for displaying menu responsive.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

if ( empty( $smof_data ) )
	$smof_data = of_get_options();

$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
if ( empty( $header_style ) )
	$header_style = $smof_data['header_style'];

// Get logo type
$logo = isset ( $smof_data[ $header_style . 'logo'] ) ? trim( $smof_data[ $header_style . 'logo' ] ) : '';
?>
<div class="k2t-header-m">
	<div class="k2t-menu-m">
		<a class="m-trigger mobile-menu-toggle">
			<div class="hamburger hamburger--slider js-hamburger">
        		<div class="hamburger-box">
          		<div class="hamburger-inner"></div>
        		</div>
      		</div>
      	</a>
		<div class="mobile-menu-wrap dark-div">
			<a href="#" class="mobile-menu-toggle"><i class="zmdi zmdi-close"></i></a>
			<ul class="mobile-menu">
				<?php
					wp_nav_menu(array(
						'theme_location'  => 'mobile',
						'container' => false,
						'items_wrap' => '%3$s',
						'before'	 => '<div class="wrap-link-item">',
						'after'		 => '<span class="open-sub-menu"></span></div>',
					));
				?>
			</ul>
		</div>
	</div>

	<div class="k2t-logo-m">
		<?php if ( $logo == '' || ( isset( $smof_data[ $header_style . 'text-logo'] ) && $smof_data[ $header_style . 'use-text-logo'] ) ) : ?>
			<h1 class="logo-text">
				<a class="k2t-logo" rel="home" href="<?php echo esc_url( home_url( "/" ) ); ?>">
					<?php
						if ( ! isset( $smof_data[ $header_style . 'text-logo'] ) || empty( $smof_data[ $header_style . 'text-logo'] ) ) {
							echo esc_html( bloginfo( 'name' ) );
						} else {
							echo esc_html( $smof_data[ $header_style . 'text-logo'] );
						}
					?>
				</a><!-- .k2t-logo -->
			</h1><!-- .logo-text -->
		<?php else : ?>
			<a class="k2t-logo" rel="home" href="<?php echo esc_url( home_url( "/" ) ); ?>">
				<img src="<?php echo esc_url( $logo );?>" alt="<?php esc_attr( bloginfo( 'name' ) );?>" />
			</a><!-- .k2t-logo -->
		<?php endif; ?>
	</div><!-- .k2t-logo-m -->

	<div class="k2t-right-m">
		<div class="search-box">
			<span> <i class="fa fa-search"></i> </span>
		</div><!-- .search-box -->
	</div><!-- .k2t-right-m -->
</div><!-- .k2t-header-m -->