<?php
/**
 * The top header for theme.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */
$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
if ( empty($header_style) )
	$header_style = $smof_data['header_style'];
if ( $smof_data[ $header_style . 'header_section_1'] != '' ) :
	// Get all data of top header
	$data = json_decode ( $smof_data[ $header_style . 'header_section_1' ], true );
	// Get number of column display
	$col = $data['columns_num'];
	$align = array();
	// Get section properties
	$hex      		     = isset( $smof_data['bg_color_setting_' . $header_style . 'header_section_1'   ] ) ? $smof_data['bg_color_setting_' . $header_style . 'header_section_1'  ] : '';
	$bg_image 			 = isset( $smof_data['bg_image_setting_' . $header_style . 'header_section_1_upload' ] ) ? $smof_data['bg_image_setting_' . $header_style . 'header_section_1_upload' ] : '' ;
	$opacity  		 	 = isset( $smof_data['opacity_setting_' . $header_style . 'header_section_1' ] ) ?  $smof_data['opacity_setting_' . $header_style . 'header_section_1'  ] : '' ;
	$opacity_only_sticky = isset( $smof_data['opacity_only_sticky_' . $header_style . 'header_section_1' ] ) ? $smof_data['opacity_only_sticky_' . $header_style . 'header_section_1'] : '' ;
	$align['col-0'] 	 = isset( $smof_data['align_column_1_setting_' . $header_style . 'header_section_1' ] ) ? $smof_data['align_column_1_setting_' . $header_style . 'header_section_1'  ] : '' ;
	$align['col-1'] 	 = isset( $smof_data['align_column_2_setting_' . $header_style . 'header_section_1' ] ) ? $smof_data['align_column_2_setting_' . $header_style . 'header_section_1'  ] : '' ;
	$align['col-2'] 	 = isset( $smof_data['align_column_3_setting_' . $header_style . 'header_section_1' ] ) ? $smof_data['align_column_3_setting_' . $header_style . 'header_section_1'  ] : '' ;
	$rgb      			 = stevecadey_hex2rgb( $hex );
	$class = array();
	$style = array();
	if ( $opacity < 100 && $opacity_only_sticky == 'no' ) {
		$a = ', 0.' . $opacity;
	} else {
		$a = ', 1';
	}
	if ( $hex ) {
		$style[] = 'background-color: rgba(' . $rgb['0'] . ',' . $rgb['1'] . ',' . $rgb['2'] . $a .');';
	}
	if ( $opacity_only_sticky == 'yes' ) {
		$class[] = 'opacity-only-sticky';
	}
	if ( $bg_image ) {
		$style[] = 'background-image: url( ' . $bg_image . ' );';
		if ( $hex == '' &&  $opacity != '' ) {
			$style[] = 'opacity: ' . $opacity . ';';
		}
	}
	if ( $header_height ) {
		if ( is_numeric( $header_height ) ) {
			$style[] = 'height: ' . $header_height . 'px;';
		} else {
			$style[] = 'height: ' . $header_height . ';';
		}
	}

	/**
	 * Top header output.
	 *
	 * @since  1.0
	 */
	function stevecadey_top_header_value( $data, $id, $section, $header_style ) {
		$values = $data['columns'][$id]['value'];
		$i = 0;
		foreach ( $values as $val ) {
			if ( function_exists( 'stevecadey_data' ) ) {
				stevecadey_data( $id, $i, $section, $header_style );
			}
			$i++;
		}
	}
	?>
	<div id="k2t-header-top" class="k2t-header-top <?php echo implode( ' ', $class ); ?>">
		<div class="k2t-wrap" style="<?php echo esc_attr( implode( ' ', $style ) ); ?>">
			<div class="k2t-row">
				<?php
					$section = $header_style . 'header_section_1';
					for ( $i = 0; $i < $col; $i++ ) {
						echo '<div class="col-' . esc_attr( $data['columns'][$i]['percent'] ) . ' align-' . $align['col-' . $i ] . '">';
							stevecadey_top_header_value( $data, $i, $section , $header_style );
						echo '</div>';
					}
				?>
			</div><!-- .row -->
		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-header-top -->
<?php endif; ?>
