<?php
/**
 * Blog filter navigation.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get filter list

$filter_list = array();

if ( isset( $page_stt['blog_cat_filter'] ) && is_array( $page_stt['blog_cat_filter'] ) && count( $page_stt['blog_cat_filter'] ) > 0 ) :	// Have select categories

	foreach( $page_stt['blog_cat_filter'] as $key => $value ) :
		$cat_name = get_cat_name( $value );
		$cat_name = strtolower( $cat_name );
		$filter_list[ str_replace(' ', '-', $cat_name) ] = get_cat_name( $value ) ;
	endforeach;

else:	// Get all categories

	$categories = get_categories();
	if ( count( $categories ) > 0 ) :
		foreach ($categories as $key => $value) {
			$filter_list[ $value->slug ] =  $value->cat_name;
		}
	endif;

endif;	?>

<!-- Output -->
<?php
	if ( count($filter_list) > 0 ) : ?>
		<div class="k2t-isotop-filter">
			<ul class="menu-filter">
				<li>
					<a class="active" href='#'><?php echo esc_html__('All post','stevecadey');?></a>
				</li>
				<?php
					foreach ($filter_list as $key => $value) :
						echo
							'<li>' .
								'<a href="#" data-filter=".category-' . $key . '">' .
									$value .
								'</a>' .
							'</li>';
					endforeach;
				?>
			</ul>
		</div>
	<?php
	endif;
