<?php
/**
 * The template for displaying content quote.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

if ( !empty( $quote['quote_content'] ) ) :
?>
	<div class="quote-wrapper">
    	<div class="quote-inner">

            	<div class="quote-content">
            		<blockquote class="quote">
            			<?php echo wp_strip_all_tags( $quote['quote_content'] );
                        ?>
            		</blockquote>
                    <div class="author">
                    	<a href="<?php echo esc_url( $quote['author_quote_url'] );?>">
                    		<?php echo esc_html__('By ','stevecadey') . get_the_author();?>
                    	</a>
                    </div>
            	</div><!--end:quote-content-->

        </div><!--end:quote-inner-->
    </div><!--end:quote-wrapper-->

<?php
else :
	the_content();
endif;
