<?php

/**
 * The template for displaying related post.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */


if( $single_stt['display_related_post'] ) :

	// Set number of related post


	if ( empty( $single_stt['single_layout'] ) || $single_stt['single_layout'] == 'default' ) {
		$single_stt['single_layout'] = $smof_data['single_layout'];
	}

	$num_related = ( $single_stt['single_layout'] ==  'no_sidebar' ) ? '4' : '3';

	wp_localize_script( 'k2t-main-script', 'related_num_slider', $num_related );	?>

	<div class="related-posts">

		<h2 class="related-title"><?php echo esc_html( $smof_data['single_related_post_title'] );?></h2>

		<div class="related-post-wrap">
			<?php
			$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => $smof_data['single_related_post_number'], 'post__not_in' => array($post->ID) ) );

			if( $related ) :
				foreach( $related as $post ) :
					setup_postdata($post); ?>

		            <div class="related-item">

		            	<!-- Thumbnail -->

		            	<a rel="external" href="<?php echo the_permalink()?>">
		            		<div class="related-thum">
			            		<?php
			            		if ( has_post_thumbnail() ) :
									echo get_the_post_thumbnail( get_the_ID(), 'stevecadey_blog_related' );
								else :
									stevecadey_place_holder( 'stevecadey_blog_related' );
								endif;
			            		?>
			            	</div>
		            	</a>

		            	<!-- Top meta -->

		        		<div class="top-meta">

		        			<!-- Categories -->

							<?php if ( $single_stt['display_categories'] != 'hide' ) : ?>
								<div class="categories-list">
									<i class="zmdi zmdi-tag"></i>
									<?php $categories_list = get_the_category_list('<i class="zmdi zmdi-tag"></i>');
										if ( $categories_list ) :
											printf( esc_html__( '%1$s', 'stevecadey' ), $categories_list ) ;
										endif;
									?>
								</div>
							<?php endif;?>

			        		<!-- Title -->

			        		<h5><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>

			        		<!-- Date -->

							<?php if ( $single_stt['display_post_date'] ) : ?>
								<div class="date-meta">
									<?php echo '<i class="zmdi zmdi-time"></i><span>' . get_the_date('M d, Y') . '</span>';?>
								</div>
							<?php endif;?>

		        			<!-- Author -->

							<?php if ( $single_stt['display_posted_author'] ) : ?>
								<div class="author-meta">
								<?php
									echo '<i class="zmdi zmdi-account"></i><a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"> ' . get_the_author() . '</a>';
								?>
								</div>
							<?php endif;?>

						</div><!-- End Top meta -->

		 			</div><!-- End related-psot -->

				<?php endforeach;	// related

				wp_reset_postdata();	// reset

			endif;	?> <!-- End $related -->

		</div>	<!-- End related-post-wrap -->

	</div><!-- End related-post -->

<?php endif; // End $smof_data['single-related-post'];
