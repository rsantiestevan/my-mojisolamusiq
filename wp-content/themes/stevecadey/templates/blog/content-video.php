<?php
/**
 * The template for displaying content video image thumbnail.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// get theme option

if ( empty( $smof_data ) )
	$smof_data = of_get_options();

// Get single setting

$single_stt = array(
					'single_layout'			=> '',
					'link_format_url'		=> '',
					'display_categories'	=> '',
					'display_posted_author'	=> '',
					'display_post_date'		=> '',
					'display_tags'			=> '',
					'display_authorbox'		=> '',
					'display_related_post'	=> '',
			  );

$single_stt	= stevecadey_get_acf_field( $single_stt, get_the_ID() );

// Get post format
$post_format = get_post_format();

// animation

$anm = $data_name = $data_delay = '';

if ( isset( $smof_data['blog_anm'] ) && $smof_data['blog_anm'] == '1' ) :
	$anm        = ' animated';
	$data_name  = ' data-animation=' . $smof_data['blog_type_anm'];
	$data_delay = ' data-animation-delay=' . $smof_data['blog_anm_delay'];
endif;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'video-listing element hentry post-item ' . $post_format . '-post' . $anm );?> <?php echo esc_attr( $data_name ) .  esc_attr( $data_delay );?> >
	<div class="post-inner post-video">

	<?php

		if( 'video' == $post_format ):

			// title

			if ( get_post_format() == 'link' ) :
				the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( $single_stt['link_format_url'] ) ), '</a></h4>' );
			else :
				if ( $smof_data['blog_post_link'] ) {
					the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );
				}
			endif;

			include get_template_directory() . '/templates/blog/post-format.php';
		endif; 
	?>
	</div><!--End:post-inner-->
</article><!--End:post-item-->


