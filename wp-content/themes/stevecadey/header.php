<?php
/**
 * The header for theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>           
            
<body <?php body_class(); ?>>

<?php

if ( empty( $smof_data ) ) $smof_data = of_get_options();
if ( ! empty( $smof_data['pageloader'] ) ) : ?>
	<div id="loader-wrapper">
		<span class="loader"><span class="loader-inner"></span></span>
	</div>
<?php endif; ?>

<div class="k2t-container">  

	<?php if ( ! is_404() ) : ?>
	<?php
		$stevecadey_classes = stevecadey_header_class();
	?>
	<header class="k2t-header <?php echo esc_attr( implode( ' ', $stevecadey_classes ) ); ?>">
		<?php
			$header_style = stevecadey_get_header_style();
			// Include top header layout
			if ( ! empty( $smof_data[ $header_style . 'use_top_header'] ) ) {
				include_once STEVECADEY_TEMPLATE_PATH . 'header/top.php';
			}
			// Include middle header layout
			if ( ! empty( $smof_data[ $header_style . 'use_mid_header'] ) ) {
				include_once STEVECADEY_TEMPLATE_PATH . 'header/mid.php';
			}

			// Include bottom header layout
			if ( ! empty( $smof_data[ $header_style . 'use_bot_header'] ) ) {
				include_once STEVECADEY_TEMPLATE_PATH . 'header/bot.php';
			}

			include_once STEVECADEY_TEMPLATE_PATH . 'header/responsive.php';
		?>

	</header><!-- .k2t-header -->
	<?php endif;?>

	<div class="k2t-body">

	<?php
		if ( ! is_404() ) :

			if ( is_singular( 'post-k-event' ) || is_tax( 'k-event-category' ) || is_tax( 'k-event-tag' ) ) {
				get_template_part( 'templates/titlebar/event-title', 'bar' );
			} else if ( is_singular( 'post-k-teacher' ) || is_tax( 'k-teacher-category' ) || is_tax( 'k-teacher-tag' ) ) {
				get_template_part( 'templates/titlebar/teacher-title', 'bar' );
			}else {
				get_template_part( 'templates/titlebar/title', 'bar' );
			}
		endif;