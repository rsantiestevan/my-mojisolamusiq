<?php
/**
 * The template for displaying search results pages.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

get_header(); ?>

	<section class="k2t-content right-sidebar">
		<div class="k2t-wrap">
			<main class="k2t-main">

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h2 class="page-title"><?php echo  esc_html__( 'Search Results for: ', 'stevecadey' ) . '<span>' . get_search_query() . '</span>'; ?></h2>
				</header><!-- .page-header -->

			<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'content', 'search' );
				endwhile;
				else :
					get_template_part( 'content', 'none' );
				endif;

				include_once STEVECADEY_TEMPLATE_PATH . 'navigation.php';
			?>

			</main><!-- #main -->

			<?php get_sidebar(); ?>
		</div><!-- .k2t-wrap -->
	</section><!-- .k2t-content -->

<?php get_footer(); ?>
