<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price"><?php echo ( $product->get_price_html() ); ?></p>

	<meta itemprop="price" content="<?php echo ( $product->get_price() ); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo ( $product->is_in_stock() ? 'InStock' : 'OutOfStock' ); ?>" />

</div>
<?php
	if ( $product->is_in_stock() ) :
		echo '<div class="product-stock">';
			if ( $product->get_total_stock() > 0 ) 
				echo '<span class="total-stock"><i class="fa fa-check-circle"></i>' . esc_html__( 'Only ', 'stevecadey' ) . $product->get_total_stock() . esc_html__( ' left', 'stevecadey' ) . '</span>';
		echo '<span class="available"><i class="zmdi zmdi-check-circle"></i></span><span>' . esc_html__('In Stock', 'stevecadey' ) . '</span>';
		echo '</div>'; // end product stock;
	endif;