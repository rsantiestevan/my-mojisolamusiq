<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product, $_wp_additional_image_sizes;
wp_enqueue_script( 'k2t-owlcarousel' );

$shop_single    = wc_get_image_size( 'shop_single' );
$shop_thumbnail = wc_get_image_size( 'shop_thumbnail' );

$light_box 		= ( get_option('woocommerce_enable_lightbox') != 'no' ) ? true : false ;

$nav_ctr = 'false';

?>
<div id="single-product-image-wrap" class="images">
	<div class="product-img-cont">
		<div class="product-img">
			<?php
				//gallery images
				$attachment_ids = $product->get_gallery_attachment_ids();
				if ( $attachment_ids ) {
					//  IMAGE PRODUCT FOR variation
					if ( has_post_thumbnail() ) {

						$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
						$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
						$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
						$image       	= get_the_post_thumbnail( $post->ID, 'shop_single' );
						$image_full     = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
						$attachment_count = count( $product->get_gallery_attachment_ids() );

						if ( $attachment_count > 0 ) {
							$gallery = '[product-gallery]';
						} else {
							$gallery = '';
						}

						echo '<div class="item product-thumnail">';
						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div href="%s" itemprop="image" class="produc-main-image" title="%s" data-rel="' . $gallery . '">%s</div>', $image_link, $image_caption, $image ), $post->ID );
						if ( $light_box ) :
							echo '<a class="click-fullscreen reload-button-link" rel="prettyPhoto"  href="' . esc_url( $image_full[0] ) . '"><span class="icon-zoom"></span></a>';
						endif;
						echo '</div>';

					} else {

						echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_html__( 'Placeholder', 'stevecadey' ) ), $post->ID );

					}
					foreach ( $attachment_ids as $attachment_id ) { 
						// Get image link by attachment ID
						$image 		 	= wp_get_attachment_url( $attachment_id );
						$image_full 	= wp_get_attachment_image_src( $attachment_id, 'full' );
						$image_alt   	= get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
						$image_url      =  wp_get_attachment_url( $attachment_id );
						if ( $image != '' ) { 
							echo '<div class="item product-gallery">' . wp_get_attachment_image( $attachment_id, 'shop_single' );
							if ( $light_box ) :
								echo '<a class="click-fullscreen" rel="prettyPhoto" href="' . esc_url( $image_full[0] ) . '"><span class="icon-zoom"></span></a>';
							endif;
							echo '</div>';
						}
					}

				} else {
					if ( has_post_thumbnail() ) {
						$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
						$thumb 			   = wp_get_attachment_url( $post_thumbnail_id );
						$image_full        = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
						$thumb_url         = wp_get_attachment_url( $post_thumbnail_id ) ;
						echo '<div class="item">
								<div class="item-cont">
									' . wp_get_attachment_image( $post_thumbnail_id, 'shop_single' ) . 
								'</div>';
								if ( $light_box ):
									echo '<a class="click-fullscreen reload-button-link" rel="prettyPhoto" href="' . esc_url( $image_full[0] ) . '"><span class="icon-zoom"></span></a>';
								endif;
						echo '</div>';
					} else {
							echo '<div class="item">
									<img src="' . get_template_directory_uri() . '/assets/img/placeholder/570x444.png" alt="" />
								  </div>';
					}
				}
			?>



		</div>
	</div>
<?php if ( $attachment_ids ) : ?>
		<div id="nav">
			<?php 
			//featured image                   ------------------- NAV CAROUSEL -------------------
					//gallery images
			if ( $attachment_ids ) :

				if ( has_post_thumbnail() ) :  // PRODUCT THUMBNAIL
					$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
					$thumb =  wp_get_attachment_url( $post_thumbnail_id );
					$thumb_url = get_post_thumbnail_id( $post->ID, 'shop_thumbnail' );
					echo '<a class="secondary url clicked">' . wp_get_attachment_image( $post_thumbnail_id, 'shop_thumbnail' ) . '</a>';
				endif;

				foreach ( $attachment_ids as $attachment_id ) :  // PRODUCT GALLERY ITEM
					$image  	 = wp_get_attachment_url( $attachment_id );
					$image_alt   = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
					$image_url   = wp_get_attachment_url( $attachment_id,'shop_thumbnail' );
					if ($image) { 
						echo '<a class="secondary url">' . wp_get_attachment_image( $attachment_id , 'shop_thumbnail' ) . '</a>';
					}
				endforeach;
			
			endif; // END NAV-CAROUSEL IF HAVE GALLERY 

			$nav_ctr = ( count( $attachment_ids ) >= 4 ) ? 'true' : 'false';

			?>
		</div>
<?php endif;?>
		<script type="text/javascript">	
			(function($) {
				"use strict";

				$(document).ready(function(){
					var sync1 = $(".product-img");
					var sync2 = $("#nav");
					var flag = false;
					<?php 
			    		if ( $attachment_ids ) :
			    			echo 'var main_src = "' . $image_url .'";';
			    		endif;
			    	?>
					var slides = sync1.owlCarousel({
						items:1,
						loop:false,
						margin:10,
						autoplay:false,
						autoplayTimeout:6000,
						autoplayHoverPause:false,
						nav: false,
						dots: false
					});
					var thumbs = sync2.owlCarousel({
				        items:4,
						loop:false,
						margin:20,
						autoplay:false,
						nav: <?php echo esc_js($nav_ctr);?>,
						dots: false,
						navText: [
							'<i class="fa fa-chevron-left"></i>',
							'<i class="fa fa-chevron-right"></i>'
						],
					}).on('click', '.owl-item', function(e) {
				        e.preventDefault();	
				        sync1.trigger('to.owl.carousel', [$(e.target).parents('.owl-item').index(), 300, true]);
					}).on('change.owl.carousel', function(e) {
		                if (e.namespace && e.property.name === 'position' && !flag) {
		                console.log('...');
				    }
					}).data('owl.carousel');
					
					$('#nav a').each(function(index){
						$(this).on('click', function(e){
							e.preventDefault(); 
						    $(this).addClass('clicked');
							$(this).parent().siblings().children('a').removeClass('clicked');
						});
					});
					$('body').on('change', '.variations select', function(){
						<?php 
							$src_w = $_wp_additional_image_sizes['shop_single']['width'];
							$src_h = $_wp_additional_image_sizes['shop_single']['height'];
							$nav_w = $_wp_additional_image_sizes['shop_thumbnail']['width'];
							$nav_h = $_wp_additional_image_sizes['shop_thumbnail']['height'];
							echo 'var shop_single =' . '"' . $src_w . 'x' . $src_h . '";';
							echo 'var nav_thumb =' . '"' . $nav_w . 'x' . $nav_h . '";';
						?>

						var src = $('div.images img:eq(0)').attr('src');
						if ( src.indexOf( shop_single ) >= 0 )
							var nav_src = src.replace( shop_single, nav_thumb);
						else {
							var index   = src.lastIndexOf( '.' );
							var nav_src = src.slice(0, index ) + nav_thumb + src.slice(index );
						} ;	

						$('.reload-button-link').attr('href',src);
						if ( main_src != src) {
							$('#nav img:eq(0)').trigger('click');
						};
						$.ajax({
						    url: nav_src,
						    type:'HEAD',
						    error: function()
						    {
						        //file not exists
						        $('#nav img:eq(0)').attr('src',src);
						    },
						    success: function()
						    {
						        //file exists
						        $('#nav img:eq(0)').attr('src',nav_src);
						    }
						});
					});
				});
			})(jQuery);
		</script>
</div><!--end:single-product-image-wrap-->
<?php //do_action( 'woocommerce_product_thumbnails' ); ?>