<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
//wc_get_template_part('loop/switch-product', 'view');

$shop_style = 'shop-grid';

// cat page


if ( function_exists('get_field') && is_tax('product_cat') ) :
	$queried_object = get_queried_object();
	$shop_style = get_field( 'product_cat_style', 'product_cat_' . $queried_object->term_id )  ;
	$shop_style = str_replace( '_', '-', $shop_style );
endif;

if ( function_exists('get_field') && is_tax('product_color') ) :
	$queried_object = get_queried_object();
	$shop_style = get_field( 'product_color_style', 'product_color_' . $queried_object->term_id )  ;
	$shop_style = str_replace( '_', '-', $shop_style );
endif;

if ( empty( $smof_data ) ) $smof_data = of_get_options();

// related

if ( is_singular( 'product' ) ) $shop_style = 'shop-grid';

// shop page

if ( is_shop() ) $shop_style = str_replace( '_', '-', $smof_data['shop_style'] );

?>
<ul class="products <?php echo esc_attr( $shop_style );?> " >