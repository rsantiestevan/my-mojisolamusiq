<?php
/**
 * Product loop sale flash
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<?php if ( $product->is_on_sale() ) : ?>

	<?php 
		$price = $product->regular_price;
		$sale = $product->sale_price;
		if ( !empty( $price )) $per = round( ( 1 - $sale / $price ) * 100 ); else $per = '';
		
		echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale k2t-element-hover">' . esc_html__('-', 'stevecadey') . $per . esc_html__( '%', 'stevecadey' ) . '</span>', $post, $product ); 
	?>

<?php endif; ?>
