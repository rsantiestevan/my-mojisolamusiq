<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>

<?php if ( $price_html = $product->get_price_html() ) : ?>
	<span class="price"><?php echo ( $price_html ); ?></span>
<?php endif; ?>

<?php
	if ( $product->is_in_stock() ) :
		echo '<div class="product-stock">';
			if ( $product->get_total_stock() > 0 ) 
				echo '<span class="total-stock"><i class="fa fa-check-circle"></i>' . esc_html__( 'Only ', 'stevecadey' ) . $product->get_total_stock() . esc_html__( ' left', 'stevecadey' ) . '</span>';
		echo '<span class="available"><i class="zmdi zmdi-check-circle"></i></span><span>' . esc_html__('In Stock', 'stevecadey' ) . '</span>';
		echo '</div>'; // end product stock;
	endif;
