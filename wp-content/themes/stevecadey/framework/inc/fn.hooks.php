<?php

/**
 * Required action filters
 *
 * @uses add_action()
 *
 * @since 1.0.0
 */

/**
 * AJAX Saving Options
 *
 * @since 1.0.0
 */
add_action( 'wp_ajax_of_ajax_post_action', 'of_ajax_callback' );

/*
 * Filter body class
*/
if ( ! function_exists( 'stevecadey_body_class_filter' ) ) {
	function stevecadey_body_class_filter($classes) {
		global $smof_data;
		if ( isset( $smof_data['shadow_style'] ) ) $classes[] = 'stevecadey-shadow';
	    return $classes;
	}

	add_filter( 'body_class', 'stevecadey_body_class_filter' );
}

// func change logo for default login page
function stevecadey_login_logo() {
	global $smof_data;
	if( isset( $smof_data['login_logo'] ) && $smof_data['login_logo'] != ''):
	?>
    <style type="text/css">
        .login h1 a {
            background: url(<?php echo esc_url( $smof_data['login_logo'] ); ?>) no-repeat center center !important;
            width: 100% !important;
            height: 95px !important;
        }
    </style>
<?php endif; }
add_action( 'login_enqueue_scripts', 'stevecadey_login_logo' );

/*
	Remove script version
*/
function stevecadey_remove_version( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'stevecadey_remove_version', 9999 );
add_filter( 'script_loader_src', 'stevecadey_remove_version', 9999 );

// Hooks on Back-end
if( is_admin() ){

	//Enqueue Script and Css in Backend
	if ( ! function_exists ( 'stevecadey_backend_scripts' ) ){
		function stevecadey_backend_scripts() {
			wp_enqueue_style( 'k-backend-style', STEVECADEY_THEME_URL . 'assets/css/k2t-backend.css' );
		}
		add_action( 'admin_enqueue_scripts', 'stevecadey_backend_scripts' );
	}

	/*--------------------------------------------------------------
	Var for Script Backup
	--------------------------------------------------------------*/
	if ( ! function_exists( 'stevecadey_sample_import_add_admin_head' ) ) {
		function stevecadey_sample_import_add_admin_head() {
			echo '<scr' . 'ipt>';
			echo 'var home_url = "' . esc_url( site_url() ) . '";';
			echo 'var installing_proccess  = 0;';
			echo 'var cache_installing_url = "' . STEVECADEY_FRAMEWORK_URL . 'inc/k2timporter/tmp_backup/cache_proccess";';
			echo '</scr' . 'ipt>';
		}
		add_action( 'admin_head', 'stevecadey_sample_import_add_admin_head');
	}
}

// Enqueue css login admin
function stevecadey_themeslug_enqueue_style() {
	wp_enqueue_style( 'admin-style', STEVECADEY_FRAMEWORK_URL . 'assets/css/admin-style.css' );
}
add_action( 'login_enqueue_scripts', 'stevecadey_themeslug_enqueue_style', 10 );

// Move comment textarea to bottom

function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );


/* View count single post */
function stevecadey_wpb_set_post_views( $postID ) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta( $postID , $count_key, true );
    if ( $count == '' ){
        $count = 0;
        delete_post_meta( $postID , $count_key );
        add_post_meta( $postID , $count_key, '0' );
    } else {
        $count++;
        update_post_meta( $postID, $count_key, $count );
    }
}

function stevecadey_wpb_get_post_views( $postID ) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta( $postID , $count_key, true);
    if( $count == '' ) {
        delete_post_meta( $postID , $count_key );
        add_post_meta( $postID , $count_key, '0' );
        return "0";
    }
    return $count;
}

// Hooks on Front-end
if( ! is_admin() ){

	/*--------------------------------------------------------------
	Enqueue front-end script
	--------------------------------------------------------------*/
	if ( ! function_exists( 'stevecadey_front_end_enqueue_script' ) ) :
		function stevecadey_front_end_enqueue_script() {
			global $smof_data;

			//livereload
			if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
				wp_register_script('livereload', 'http://localhost:35729/livereload.js?snipver=1', null, false, true);
				wp_enqueue_script('livereload');
			}
			
			// Load zoom effect for title bar.
			if ( function_exists( 'get_field' ) && get_field( 'background_zoom', get_the_ID() ) ) {
				wp_enqueue_script( 'zoomeffects-script', STEVECADEY_THEME_URL . 'assets/js/vendor/zoom-effect.js', array(), '', true );
			}
			// Load jquery easing.
			wp_enqueue_script( 'jquery-stellar', STEVECADEY_THEME_URL . 'assets/js/vendor/jquery.stellar.min.js', array(), '', true );
			// Load jquery easing.
			wp_enqueue_script( 'jquery-easing-script', STEVECADEY_THEME_URL . 'assets/js/vendor/jquery-easing.js', array(), '', true );
			// Load infinite scroll library.
			wp_enqueue_script( 'infinitescroll-script', STEVECADEY_THEME_URL . 'assets/js/vendor/jquery.infinitescroll.min.js', array(), '', true );
			// Enqueue jquery isotope
			wp_enqueue_script( 'jquery-isotope', STEVECADEY_THEME_URL . 'assets/js/vendor/isotope.pkgd.min.js', array(), '', true );
			// Jquery Library: Imagesloaded
			wp_enqueue_script( 'jquery-imagesloaded', STEVECADEY_THEME_URL . 'assets/js/vendor/imagesloaded.pkgd.min.js', array( 'jquery' ), '3.1.6', true );
			// Enqueue jquery carousel
			wp_enqueue_script( 'owlcarousel', STEVECADEY_THEME_URL . 'assets/js/vendor/owl.carousel.min.js', array(), '', true);

			// Load our custom javascript.
			$mainParams = array();
			wp_enqueue_script( 'jquery-mousewheel', STEVECADEY_THEME_URL . 'assets/js/vendor/jquery.mousewheel.min.js', array( 'jquery' ), '', true );
			wp_enqueue_script( 'k2t-main-script', STEVECADEY_THEME_URL . '/assets/js/main.js', array( 'jquery' ), '', true );

			// enqueue script

			if ( is_singular() && get_option( 'thread_comments' ) )
				wp_enqueue_script( 'comment-reply' );

			// set variable for javascript

			$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
			if ( !isset($header_style) || $header_style == '' )
				$header_style = $smof_data['header_style'];
			if ( isset( $smof_data['offcanvas_swipe'] ) && $smof_data['offcanvas_swipe'] ) {
				$mainParams['offcanvas_turnon'] = $smof_data['offcanvas_turnon'];
			}
			if ( isset( $smof_data[ $header_style . 'fixed_header'] ) && $smof_data[ $header_style . 'fixed_header'] ) {
				$mainParams['fixed_header'] = $smof_data[ $header_style . 'fixed_header'];
			}
			if ( isset( $smof_data[ $header_style . 'sticky_menu' ] ) ) {
				$mainParams['sticky_menu'] = $smof_data[ $header_style . 'sticky_menu'];
			}
			if ( isset( $smof_data[ $header_style . 'smart_sticky'] ) ) {
				$mainParams['smart_sticky'] = $smof_data[ $header_style . 'smart_sticky'];
			}
			if ( isset( $smof_data['blog_style'] ) && 'masonry' == $smof_data['blog_style'] ) {
				$mainParams['blog_style'] = $smof_data['blog_style'];
			}

			wp_localize_script( 'k2t-main-script', 'mainParams', $mainParams );
		}
		add_action( 'wp_enqueue_scripts', 'stevecadey_front_end_enqueue_script' );
	endif;

	/*--------------------------------------------------------------
		Enqueue front-end style
	--------------------------------------------------------------*/
	if ( ! function_exists( 'stevecadey_front_end_enqueue_style' ) ) :
		function stevecadey_front_end_enqueue_style() {
			global $smof_data;

			wp_enqueue_style( 'owlcarousel', STEVECADEY_THEME_URL . 'assets/css/vendor/owl.carousel.css' );

			// Load material icon
			wp_enqueue_style( 'material-icon', STEVECADEY_FRAMEWORK_URL . '/extensions/plugins/k2t-icon/assets/css/material-design-iconic-font.min.css' );

			// Load font awesome for first active theme
			wp_enqueue_style( 'k2t-font-awesome-style', STEVECADEY_THEME_URL . 'assets/css/vendor/font-awesome.min.css' );

			// Load humbuger css
			wp_enqueue_style( 'k2t-humberger-css', STEVECADEY_THEME_URL . 'assets/css/vendor/hamburgers.css' );

			wp_enqueue_style( 'k2t-style-less', STEVECADEY_THEME_URL . 'assets/css/style.css' );

			// Load responsive stylesheet.
			wp_enqueue_style( 'k2t-reponsive-style', STEVECADEY_THEME_URL . 'assets/css/responsive.css' );

		}
		add_action( 'wp_enqueue_scripts', 'stevecadey_front_end_enqueue_style' );
	endif;


	/**
	 * Enqueue stylesheet.
	 *
	 * @package stevecadey
	 * @author  LunarTheme
	 * @link    http://www.lunartheme.com
	 */


	if ( ! function_exists( 'stevecadey_front_end_enqueue_inline_css' ) ) {
		function stevecadey_front_end_enqueue_inline_css() {
			global $smof_data;
			ob_start();
			?>
			<style>

				<?php
				/* Content width
				------------------------------------------------- */
				if ( ! empty( $smof_data['boxed_layout'] ) ) {
					echo '
						.boxed .k2t-container { max-width: ' . $smof_data['use_content_width'] . 'px; }
					';
				} else {
					if ( isset ( $smof_data['use_content_width'] ) ) {
						/*echo '
							.k2t-wrap, .container { max-width: ' . $smof_data['use_content_width'] . 'px; }
						';*/
					}
				}

				/* Sidebar width
				------------------------------------------------- */
				$sidebar_width = $smof_data['sidebar_width'];
				if ( ! empty( $page_sidebar_width ) ) {
					echo '
						.k2t-sidebar, .k2t-sidebar-sub { width:' . $page_sidebar_width . '%; }
						.k2t-blog, .k2t-main { width:' . ( 100 - $page_sidebar_width ) . '%; }
					';
				}

				// CANVAS CUSTOM STYLE

				if ( $smof_data['offcanvas_turnon'] && ! is_404() ) :
					$offcanvas_sidebar_custom_css = $smof_data['offcanvas_sidebar_custom_css'];
					if ( ! empty( $offcanvas_sidebar_custom_css ) ) {
						echo esc_html( $offcanvas_sidebar_custom_css );
					}
				endif;

				// PRIMARY COLOR
				if ( $smof_data['theme_primary_color'] == 'custom' ) :?>

					a:hover,
					a:focus,
					.k2t-header-mid .k2t-menu > li.current-menu-item > a,
					.k2t-header-mid .k2t-menu > li.current-menu-parent > a,
					.k2t-header-mid .k2t-menu > li.current-menu-ancestor > a,
					.k2t-heading.has-border.two_dots .h:before, .k2t-heading.has-border.two_dots .h:after,
					.k2t-iconbox.layout-1:hover .iconbox-icon,
					.k2t-btt,
					.widget-title:after,
					.k2t-info .widget_nav_menu ul li a:hover,
					.k2t-footer .k2t-wrap .k2t-row > div a:hover,
					.k2t-gallery-heading .filter-list li.active, .k2t-gallery-heading .filter-list li:hover,
					.k2t-iconbox.layout-4 .iconbox-icon i,
					.event-isotope-filter li.active,
					.widget_categories > ul > .cat-item .children li a:hover,
					.contact-info a:hover,
					.vc_toggle.vc_toggle_default .vc_toggle_title h4:hover,
					.k2t-project-heading h2:before,
					.k2t-project-heading h2:after,
					.k2t-page-topnav ul.menu > li.active > a,
					.k2t-gallery-heading .gallery-title:after,
					.widget ul li.current-cat a,
					.entry-box .entry-comment a:hover,
					.k2t-header-mid .search-box:hover,
					.entry-box .post-entry a,
					.course-isotope-filter li.active,
					.k2t-blog .post-item.sticky .entry-content::before,
					.k2t-blog .post-item header .entry-author a:hover,
					.k2t-blog .post-item header .entry-comment a:hover {
						color: <?php echo  esc_html( $smof_data['primary_color'] ) ;?>;
					}

					button:hover,
					input[type="button"]:hover,
					input[type="reset"]:hover,
					input[type="submit"]:hover ,
					.h-element .shop-cart .cart-control span,
					#close-canvas,
					.k2t-header-top,
					.tp-caption.primarybutton a,
					.tp-caption.primarybutton a:hover,
					.k2t-btt:hover,
					article[class*="course-"] .more-link,
					article[class*="course-"] .more-link:hover,
					.event-listing-masonry .masonry-item .read-more,
					.k2t-blog .post-item .more-link,
					.k2t-blog .post-item .more-link:hover,
					.owl-controls .owl-dots > div.active,
					.mc4wp-form input[type=submit],
					.mc4wp-form input[type=submit]:hover,
					.k2t-footer .widget.social-widget ul li a:hover,
					.owl-controls .owl-nav > div > i:hover,
					.k2t-member .team-socials-link li:hover,
					.k2t-gallery-heading .filter-list li:after,
					.wpcf7 #commentform input[type="submit"],
					.event-classic-item .more-link,
					.event-classic-item .more-link:hover,
					.k2t-pagination-lite,
					.k2t-pagination-lite a:hover,
					.k2t_widget_recent_event .join-event,
					.k2t_widget_recent_event .join-event:hover,
					.event-isotope-filter li:after,
					.single-post-k-event .event-link,
					.single-post-k-event .event-link:hover,
					.widget_categories > ul > .cat-item > a:before,
					.vc_tta.vc_tta-style-outline.vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab.vc_active a,
					.error404, .error404 .k2t-body,
					.k2t-page-topnav ul.menu > li > a:before,
					.project-fields .project-link,
					.project-fields .project-link:hover,
					.teacher-listing article .social a:hover,
					.teacher-connect table th,
					.widget #wp-calendar caption,
					.widget #wp-calendar td#today,
					.k2t-blog .cat-icon,
					.k2t-navigation ul li span.current,
					.widget .tagcloud a:hover,
					.about-author,
					.form-submit #submit,
					.form-submit #submit:hover,
					.k2t-button a,
					.single-post-k-course .course-link,
					.single-post-k-course .course-link:hover,
					.k2t-searchbox .mark,
					body.search .k2t-main .searchform .form-group button,
					.loader-inner,
					.course-isotope-filter li:after,
					.cd-dropdown > span,
					.cd-dropdown ul li span,
					.k2t-related-course .related-thumb a i,
					blockquote::before,
					q::before,
					.entry-box .widget_tag_cloud .tagcloud a:hover,
					.single-footer-nav,
					.woocommerce #respond input#submit,
					.woocommerce a.button,
					.woocommerce button.button,
					.woocommerce input.button,
					.woocommerce-page #payment #place_order,
					.shop-cart .shop-item .buttons .button,
					*::-moz-selection {
						background-color: <?php echo esc_html( $smof_data['primary_color'] );?>;
						border-color: <?php echo esc_html( $smof_data['primary_color'] );?>;
					}
					.k2t-header-mid .k2t-menu > li:hover a,
					.k2t-header-mid .k2t-menu > li > a:hover {
						border-bottom-color: <?php echo esc_html( $smof_data['primary_color'] );?>;
					}
					#commentform > p.focus input[type="text"],
					#commentform > p.focus input[type="email"],
					#commentform > p.focus textarea {
						border-bottom: 3px solid <?php echo esc_html( $smof_data['primary_color'] );?>;
					}
					.woocommerce #respond input#submit:hover, .woocommerce a.button:hover,
					.woocommerce button.button:hover,
					.woocommerce input.button:hover,
					.woocommerce-page #payment #place_order:hover,
					.shop-cart .shop-item .buttons .button:hover,
					.k2t-pagination-lite a:hover {
						
					}
					*::-moz-selection,
					::selection,
					.k2t-header-mid .k2t-menu > li.current-menu-item > a,
					.k2t-header-mid .k2t-menu > li.current-menu-parent > a,
					.k2t-header-mid .k2t-menu > li.current-menu-ancestor > a,
					.tp-caption.primarybutton a,
					.tp-caption.primarybutton a:hover,
					.k2t-footer .widget.social-widget ul li a:hover,
					.owl-controls .owl-nav > div > i:hover,
					#commentform > p.focus input[type="text"],
					#commentform > p.focus input[type="email"],
					#commentform > p.focus textarea,
					.widget .tagcloud a:hover,
					.loader,
					.comment-list .comment-body footer .action-link > a:hover {
						border-color: <?php echo esc_html( $smof_data['primary_color'] );?>;
					}

					.k2t-gallery-shortcode .view .mask {
						background: rgba(33, 150, 243, 0.95);
					}
					.contact-info .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner:hover .vc_icon_element-icon:before,
					.woocommerce .b-action a.button.added:before,
					.b-action .yith-wcwl-wishlistexistsbrowse a:before,
					.b-action .yith-wcwl-wishlistaddedbrowse a:before,
					.widget_product_categories > ul > .cat-item .children li a:hover,
					.woocommerce ul.products li.product h3 a:hover,
					.woocommerce .cart-collaterals .cart_totals table tr.order-total td,
					.woocommerce-page .cart-collaterals .cart_totals table tr.order-total td{
						color: <?php echo esc_html( $smof_data['primary_color'] );?>!important;
					}

					.widget #wp-calendar tr th:last-child,
					.widget #wp-calendar tr th:nth-last-child(2),
					.widget #wp-calendar tr td:last-child,
					.widget #wp-calendar tr td:nth-last-child(2),
					.widget #wp-calendar tr td:last-child a,
					.widget #wp-calendar tr td:nth-last-child(2) a{
						color: <?php echo esc_html( $smof_data['primary_color'] );?>;
					}

					@media only screen and (max-width: 1199px) {
						.widget #wp-calendar td#today {
							color: <?php echo esc_html( $smof_data['primary_color'] );?>!important;
						}
					}
					@media only screen and (min-width: 768px) {
						.event-isotope-filter li:hover {
							color: <?php echo esc_html( $smof_data['primary_color'] );?>;
						}
						.course-isotope-filter li:hover,
						.k2t-page-topnav ul.menu > li:hover > a{
							color: <?php echo esc_html( $smof_data['primary_color'] );?>;
						}
					}
					@media only screen and (max-width: 767px) {
						.vc_tta.vc_general .vc_tta-panel.vc_active div.vc_tta-panel-heading {
							background: <?php echo esc_html( $smof_data['primary_color'] );?>!important;
						}
					}

				<?php endif;

				/* Logo margin
				------------------------------------------------- */
				$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
				if ( empty( $header_style ) )
					$header_style = $smof_data['header_style'];
				if ( isset ( $smof_data[ $header_style . 'logo_margin_top'] ) || isset ( $smof_data[ $header_style . 'logo_margin_left'] ) || isset ( $smof_data[ $header_style . 'logo_margin_right'] ) || isset ( $smof_data[ $header_style . 'logo_margin_bottom'] ) ) :
					echo '
						.k2t-logo { display:block; margin-top: ' . esc_html( $smof_data[ $header_style . 'logo_margin_top'] ) . 'px;margin-left: ' . esc_html( $smof_data[ $header_style . 'logo_margin_left'] ) . 'px;margin-right: ' . esc_html( $smof_data[ $header_style . 'logo_margin_right'] ) . 'px;margin-bottom: ' . esc_html( $smof_data[ $header_style . 'logo_margin_bottom'] ) . 'px; }
					';
				endif;

				/* Global color scheme
				------------------------------------------------- */
				if ( $smof_data['heading_color'] || $smof_data['heading_font'] ) :
					echo '
						h1, h2, h3, h4, h1 *, h2 *, h3 *, h4 * { color: ' . esc_html( $smof_data['heading_color'] ) . '; font-family: ' . esc_html( $smof_data['heading_font'] ) . '; }
					';
				endif;

				if ( $smof_data['text_color'] ) :
					echo '
						body, button, input, select, textarea { color: ' . esc_html( $smof_data['text_color'] ) . '; }
					';
				endif;

				if ( $smof_data['footer_link_color'] ) :
					echo '
						.k2t-footer a { color: ' . esc_html( $smof_data['footer_link_color'] ) . '; }
					';
				endif;

				if ( $smof_data['link_color'] ) :
					echo '
						a { color: ' . esc_html( $smof_data['link_color'] ) . '; }
					';
				endif;

				if ( $smof_data['link_hover_color'] ) :
					echo '
						a:hover, a:focus { color: ' . esc_html( $smof_data['link_hover_color'] ) . '!important; }
					';
				endif;

				if ( $smof_data['main_menu_color'] ) :
					echo '
						.k2t-header-mid .k2t-menu li a,
						.k2t-header-mid .k2t-menu > li:hover a, .k2t-header-mid .k2t-menu > li > a:hover, .k2t-header-mid .k2t-menu > li.current-menu-item > a, .k2t-header-mid .k2t-menu > li.current-menu-parent > a, .k2t-header-mid .k2t-menu > li.current-menu-ancestor > a { color: ' . esc_html( $smof_data['main_menu_color'] ) . '!important; }
					';
				endif;

				if ( $smof_data['sub_menu_color'] ) :
					echo '
						.k2t-header-mid .k2t-menu li ul li a span::before,
						.k2t-header-mid .k2t-menu > li:hover a{ color: ' . esc_html( $smof_data['sub_menu_color'] ) . ' !important; }
					';
				endif;

				/* Typography
				------------------------------------------------- */
				if ( $smof_data['body_font'] || $smof_data['body_size'] ) :
					echo '
						body { font-family: ' . esc_html( $smof_data['body_font'] ) . '; font-size: ' . esc_html( $smof_data['body_size'] ) . 'px; }
					';
				endif;

				if ( $smof_data['mainnav_font'] || $smof_data['mainnav_size'] ) :
					echo '
						.k2t-header-mid .k2t-menu, .k2t-header .k2t-menu .mega-container ul, .vertical-menu .k2t-header-mid .k2t-menu { font-family: ' . esc_html( $smof_data['mainnav_font'] ) . '; font-size: ' . esc_html( $smof_data['mainnav_size'] ) . 'px; }
					';
				endif;

				if ( $smof_data['mainnav_text_transform'] ) :
					echo '
						.k2t-header-mid .k2t-menu > li > a { text-transform: ' . esc_html( $smof_data['mainnav_text_transform'] ) . '; }
					';
				endif;

				if ( $smof_data['mainnav_font_weight'] ) :
					echo '
						.k2t-header-mid .k2t-menu > li > a { font-weight: ' . esc_html( $smof_data['mainnav_font_weight'] ) . '; }
					';
				endif;

				if ( $smof_data['h1_size'] || $smof_data['h2_size'] || $smof_data['h3_size'] || $smof_data['h4_size'] || $smof_data['h5_size'] || $smof_data['h6_size'] ) :
					echo '
						h1 { font-size: ' . esc_html( $smof_data['h1_size'] ) . 'px; }
						h2 { font-size: ' . esc_html( $smof_data['h2_size'] ) . 'px; }
						h3 { font-size: ' . esc_html( $smof_data['h3_size'] ) . 'px; }
						h4 { font-size: ' . esc_html( $smof_data['h4_size'] ) . 'px; }
						h5 { font-size: ' . esc_html( $smof_data['h5_size'] ) . 'px; }
						h6 { font-size: ' . esc_html( $smof_data['h6_size'] ) . 'px; }
					';
				endif;

				if ( $smof_data['submenu_mainnav_size'] ) :
					echo '
						.k2t-header-mid .k2t-menu .sub-menu { font-size: ' . esc_html( $smof_data['submenu_mainnav_size'] ) . 'px; }
					';
				endif;

				/* Custom CSS
				------------------------------------------------- */
				if ( isset ( $smof_data['custom_css'] ) ) :
					$custom_css = $smof_data['custom_css'];
					echo ( $custom_css );
				endif;

				/* Header custom css */
				if ( !empty( $smof_data['max_width_header'] ) ) : ?>
					.k2t-header.full-width .k2t-wrap,
					.k2t-header-top.full-width .k2t-wrap,
					.k2t-header-mid.full-width .k2t-wrap,
					.k2t-header-bot.full-width .k2t-wrap {
						max-width: <?php echo esc_html( $smof_data['max_width_header'] ); ?>;
					}
				<?php endif;
				for ( $h = 1; $h < 4; $h++ ) :
					$opacity_only_sticky = isset( $smof_data['opacity_only_sticky_header_section_' . $h ] ) ? $smof_data['opacity_only_sticky_header_section_' . $h ] : '' ;
					$css    			 = isset( $smof_data['custom_css_setting_header_section_' . $h ] ) ? $smof_data['custom_css_setting_header_section_' . $h ] : '' ;
					$opacity		 	 = isset( $smof_data['opacity_setting_header_section_' . $h ] ) ?  $smof_data['opacity_setting_header_section_' . $h ] : '' ;
					$hex 		         = isset( $smof_data['bg_color_setting_header_section_' . $h ] ) ? $smof_data['bg_color_setting_header_section_' . $h  ] : '';
					$rgb      		     = stevecadey_hex2rgb( $hex );
					switch ( $h ) {
						case '1':
							$header_pos = 'top';
							break;
						case '2':
							$header_pos = 'mid';
							break;
						case '3':
							$header_pos = 'bot';
							break;
						default:
							# code...
							break;
					}
					if ( $opacity_only_sticky == 'yes' ) {
						if ( $opacity < 100 ) {
							$a = ', 0.' . $opacity;
						} else {
							$a = ', 1';
						}
						$css .= '  .k2t-header-' . $header_pos . '.opacity-only-sticky.sticky .k2t-wrap { ' . 'background-color: rgba(' . $rgb['0'] . ',' . $rgb['1'] . ',' . $rgb['2'] . $a .') !important;'  .  ' } ';
					}
					if ( ! empty( $css )  ) {
						echo esc_html( $css );
					}
				endfor;

				?>

				/* Primary color
				------------------------------------------------- */


			</style>
		<?php
			$output = ob_get_clean();
			$output = stevecadey_mini_output( $output );

			echo ( $output );
		} //end function
		add_action( 'wp_head','stevecadey_front_end_enqueue_inline_css' );
	}


	/*--------------------------------------------------------------
		Enqueue google fonts
	--------------------------------------------------------------*/
	if ( ! function_exists( 'stevecadey_enqueue_google_fonts' ) ) {
		function stevecadey_enqueue_google_fonts() {
			global $smof_data;

			$protocol = is_ssl() ? 'https' : 'http';
			if ( isset ( $smof_data['body_font'] ) && in_array ( $smof_data['body_font'], stevecadey_google_fonts() ) ) {
				$body_font = $smof_data['body_font'];
				wp_enqueue_style( 'k2t-google-font-' . str_replace( ' ','-',$body_font ), "$protocol://fonts.googleapis.com/css?family=" . str_replace(' ','+', $body_font ) . ":100,200,300,400,500,600,700,800,900&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			}

			if ( isset ( $smof_data['heading_font'] ) && in_array ( $smof_data['heading_font'], stevecadey_google_fonts() ) ) {
				$heading_font = $smof_data['heading_font'];
				wp_enqueue_style( 'k2t-google-font-' . str_replace( ' ','-',$heading_font ), "$protocol://fonts.googleapis.com/css?family=" . str_replace(' ','+', $heading_font ) . ":100,200,300,400,500,600,700,800,900&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			}

			if ( isset ( $smof_data['mainnav_font'] ) && in_array ( $smof_data['mainnav_font'], stevecadey_google_fonts() ) ) {
				$mainnav_font = $smof_data['mainnav_font'];
				wp_enqueue_style( 'k2t-google-font-' . str_replace( ' ','-',$mainnav_font ), "$protocol://fonts.googleapis.com/css?family=" . str_replace(' ','+', $mainnav_font ) . ":100,200,300,400,500,600,700,800,900&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			}
		}
		add_action( 'wp_enqueue_scripts', 'stevecadey_enqueue_google_fonts' );
	}

}

// Hooks on Back-end
else{

	/**
	 * Removes tabs such as the "Design Options" from the Visual Composer Settings
	 *
	 * @package stevecadey
	 */
	if ( class_exists( 'Vc_Manager' ) ) :
		vc_set_as_theme( true );
	endif;

	/*-------------------------------------------------------------------
		Map for Visual Composer Shortcode.
	--------------------------------------------------------------------*/
	if ( class_exists( 'Vc_Manager' ) ) :
		if ( ! function_exists( 'stevecadey_vc_map_shortcodes' ) ) :

			function stevecadey_vc_map_shortcodes() {

				// Include plugin.php
				include_once ( ABSPATH . 'wp-admin/includes/plugin.php' );

				$k2t_icon = array( '', 'fa fa-glass', 'fa fa-music', 'fa fa-search', 'fa fa-envelope-o', 'fa fa-heart', 'fa fa-star', 'fa fa-star-o', 'fa fa-user', 'fa fa-film', 'fa fa-th-large', 'fa fa-th', 'fa fa-th-list', 'fa fa-check', 'fa fa-remove', 'fa fa-close', 'fa fa-times', 'fa fa-search-plus', 'fa fa-search-minus', 'fa fa-power-off', 'fa fa-signal', 'fa fa-gear', 'fa fa-cog', 'fa fa-trash-o', 'fa fa-home', 'fa fa-file-o', 'fa fa-clock-o', 'fa fa-road', 'fa fa-download', 'fa fa-arrow-circle-o-down', 'fa fa-arrow-circle-o-up', 'fa fa-inbox', 'fa fa-play-circle-o', 'fa fa-rotate-right', 'fa fa-repeat', 'fa fa-refresh', 'fa fa-list-alt', 'fa fa-lock', 'fa fa-flag', 'fa fa-headphones', 'fa fa-volume-off', 'fa fa-volume-down', 'fa fa-volume-up', 'fa fa-qrcode', 'fa fa-barcode', 'fa fa-tag', 'fa fa-tags', 'fa fa-book', 'fa fa-bookmark', 'fa fa-print', 'fa fa-camera', 'fa fa-font', 'fa fa-bold', 'fa fa-italic', 'fa fa-text-height', 'fa fa-text-width', 'fa fa-align-left', 'fa fa-align-center', 'fa fa-align-right', 'fa fa-align-justify', 'fa fa-list', 'fa fa-dedent', 'fa fa-outdent', 'fa fa-indent', 'fa fa-video-camera', 'fa fa-photo', 'fa fa-image', 'fa fa-picture-o', 'fa fa-pencil', 'fa fa-map-marker', 'fa fa-adjust', 'fa fa-tint', 'fa fa-edit', 'fa fa-pencil-square-o', 'fa fa-share-square-o', 'fa fa-check-square-o', 'fa fa-arrows', 'fa fa-step-backward', 'fa fa-fast-backward', 'fa fa-backward', 'fa fa-play', 'fa fa-pause', 'fa fa-stop', 'fa fa-forward', 'fa fa-fast-forward', 'fa fa-step-forward', 'fa fa-eject', 'fa fa-chevron-left', 'fa fa-chevron-right', 'fa fa-plus-circle', 'fa fa-minus-circle', 'fa fa-times-circle', 'fa fa-check-circle', 'fa fa-question-circle', 'fa fa-info-circle', 'fa fa-crosshairs', 'fa fa-times-circle-o', 'fa fa-check-circle-o', 'fa fa-ban', 'fa fa-arrow-left', 'fa fa-arrow-right', 'fa fa-arrow-up', 'fa fa-arrow-down', 'fa fa-mail-forward', 'fa fa-share', 'fa fa-expand', 'fa fa-compress', 'fa fa-plus', 'fa fa-minus', 'fa fa-asterisk', 'fa fa-exclamation-circle', 'fa fa-gift', 'fa fa-leaf', 'fa fa-fire', 'fa fa-eye', 'fa fa-eye-slash', 'fa fa-warning', 'fa fa-exclamation-triangle', 'fa fa-plane', 'fa fa-calendar', 'fa fa-random', 'fa fa-comment', 'fa fa-magnet', 'fa fa-chevron-up', 'fa fa-chevron-down', 'fa fa-retweet', 'fa fa-shopping-cart', 'fa fa-folder', 'fa fa-folder-open', 'fa fa-arrows-v', 'fa fa-arrows-h', 'fa fa-bar-chart-o', 'fa fa-bar-chart', 'fa fa-twitter-square', 'fa fa-facebook-square', 'fa fa-camera-retro', 'fa fa-key', 'fa fa-gears', 'fa fa-cogs', 'fa fa-comments', 'fa fa-thumbs-o-up', 'fa fa-thumbs-o-down', 'fa fa-star-half', 'fa fa-heart-o', 'fa fa-sign-out', 'fa fa-linkedin-square', 'fa fa-thumb-tack', 'fa fa-external-link', 'fa fa-sign-in', 'fa fa-trophy', 'fa fa-github-square', 'fa fa-upload', 'fa fa-lemon-o', 'fa fa-phone', 'fa fa-square-o', 'fa fa-bookmark-o', 'fa fa-phone-square', 'fa fa-twitter', 'fa fa-facebook', 'fa fa-github', 'fa fa-unlock', 'fa fa-credit-card', 'fa fa-rss', 'fa fa-hdd-o', 'fa fa-bullhorn', 'fa fa-bell', 'fa fa-certificate', 'fa fa-hand-o-right', 'fa fa-hand-o-left', 'fa fa-hand-o-up', 'fa fa-hand-o-down', 'fa fa-arrow-circle-left', 'fa fa-arrow-circle-right', 'fa fa-arrow-circle-up', 'fa fa-arrow-circle-down', 'fa fa-globe', 'fa fa-wrench', 'fa fa-tasks', 'fa fa-filter', 'fa fa-briefcase', 'fa fa-arrows-alt', 'fa fa-group', 'fa fa-users', 'fa fa-chain', 'fa fa-link', 'fa fa-cloud', 'fa fa-flask', 'fa fa-cut', 'fa fa-scissors', 'fa fa-copy', 'fa fa-files-o', 'fa fa-paperclip', 'fa fa-save', 'fa fa-floppy-o', 'fa fa-square', 'fa fa-navicon', 'fa fa-reorder', 'fa fa-bars', 'fa fa-list-ul', 'fa fa-list-ol', 'fa fa-strikethrough', 'fa fa-underline', 'fa fa-table', 'fa fa-magic', 'fa fa-truck', 'fa fa-pinterest', 'fa fa-pinterest-square', 'fa fa-google-plus-square', 'fa fa-google-plus', 'fa fa-money', 'fa fa-caret-down', 'fa fa-caret-up', 'fa fa-caret-left', 'fa fa-caret-right', 'fa fa-columns', 'fa fa-unsorted', 'fa fa-sort', 'fa fa-sort-down', 'fa fa-sort-desc', 'fa fa-sort-up', 'fa fa-sort-asc', 'fa fa-envelope', 'fa fa-linkedin', 'fa fa-rotate-left', 'fa fa-undo', 'fa fa-legal', 'fa fa-gavel', 'fa fa-dashboard', 'fa fa-tachometer', 'fa fa-comment-o', 'fa fa-comments-o', 'fa fa-flash', 'fa fa-bolt', 'fa fa-sitemap', 'fa fa-umbrella', 'fa fa-paste', 'fa fa-clipboard', 'fa fa-lightbulb-o', 'fa fa-exchange', 'fa fa-cloud-download', 'fa fa-cloud-upload', 'fa fa-user-md', 'fa fa-stethoscope', 'fa fa-suitcase', 'fa fa-bell-o', 'fa fa-coffee', 'fa fa-cutlery', 'fa fa-file-text-o', 'fa fa-building-o', 'fa fa-hospital-o', 'fa fa-ambulance', 'fa fa-medkit', 'fa fa-fighter-jet', 'fa fa-beer', 'fa fa-h-square', 'fa fa-plus-square', 'fa fa-angle-double-left', 'fa fa-angle-double-right', 'fa fa-angle-double-up', 'fa fa-angle-double-down', 'fa fa-angle-left', 'fa fa-angle-right', 'fa fa-angle-up', 'fa fa-angle-down', 'fa fa-desktop', 'fa fa-laptop', 'fa fa-tablet', 'fa fa-mobile-phone', 'fa fa-mobile', 'fa fa-circle-o', 'fa fa-quote-left', 'fa fa-quote-right', 'fa fa-spinner', 'fa fa-circle', 'fa fa-mail-reply', 'fa fa-reply', 'fa fa-github-alt', 'fa fa-folder-o', 'fa fa-folder-open-o', 'fa fa-smile-o', 'fa fa-frown-o', 'fa fa-meh-o', 'fa fa-gamepad', 'fa fa-keyboard-o', 'fa fa-flag-o', 'fa fa-flag-checkered', 'fa fa-terminal', 'fa fa-code', 'fa fa-mail-reply-all', 'fa fa-reply-all', 'fa fa-star-half-empty', 'fa fa-star-half-full', 'fa fa-star-half-o', 'fa fa-location-arrow', 'fa fa-crop', 'fa fa-code-fork', 'fa fa-unlink', 'fa fa-chain-broken', 'fa fa-question', 'fa fa-info', 'fa fa-exclamation', 'fa fa-superscript', 'fa fa-subscript', 'fa fa-eraser', 'fa fa-puzzle-piece', 'fa fa-microphone', 'fa fa-microphone-slash', 'fa fa-shield', 'fa fa-calendar-o', 'fa fa-fire-extinguisher', 'fa fa-rocket', 'fa fa-maxcdn', 'fa fa-chevron-circle-left', 'fa fa-chevron-circle-right', 'fa fa-chevron-circle-up', 'fa fa-chevron-circle-down', 'fa fa-html5', 'fa fa-css3', 'fa fa-anchor', 'fa fa-unlock-alt', 'fa fa-bullseye', 'fa fa-ellipsis-h', 'fa fa-ellipsis-v', 'fa fa-rss-square', 'fa fa-play-circle', 'fa fa-ticket', 'fa fa-minus-square', 'fa fa-minus-square-o', 'fa fa-level-up', 'fa fa-level-down', 'fa fa-check-square', 'fa fa-pencil-square', 'fa fa-external-link-square', 'fa fa-share-square', 'fa fa-compass', 'fa fa-toggle-down', 'fa fa-caret-square-o-down', 'fa fa-toggle-up', 'fa fa-caret-square-o-up', 'fa fa-toggle-right', 'fa fa-caret-square-o-right', 'fa fa-euro', 'fa fa-eur', 'fa fa-gbp', 'fa fa-dollar', 'fa fa-usd', 'fa fa-rupee', 'fa fa-inr', 'fa fa-cny', 'fa fa-rmb', 'fa fa-yen', 'fa fa-jpy', 'fa fa-ruble', 'fa fa-rouble', 'fa fa-rub', 'fa fa-won', 'fa fa-krw', 'fa fa-bitcoin', 'fa fa-btc', 'fa fa-file', 'fa fa-file-text', 'fa fa-sort-alpha-asc', 'fa fa-sort-alpha-desc', 'fa fa-sort-amount-asc', 'fa fa-sort-amount-desc', 'fa fa-sort-numeric-asc', 'fa fa-sort-numeric-desc', 'fa fa-thumbs-up', 'fa fa-thumbs-down', 'fa fa-youtube-square', 'fa fa-youtube', 'fa fa-xing', 'fa fa-xing-square', 'fa fa-youtube-play', 'fa fa-dropbox', 'fa fa-stack-overflow', 'fa fa-instagram', 'fa fa-flickr', 'fa fa-adn', 'fa fa-bitbucket', 'fa fa-bitbucket-square', 'fa fa-tumblr', 'fa fa-tumblr-square', 'fa fa-long-arrow-down', 'fa fa-long-arrow-up', 'fa fa-long-arrow-left', 'fa fa-long-arrow-right', 'fa fa-apple', 'fa fa-windows', 'fa fa-android', 'fa fa-linux', 'fa fa-dribbble', 'fa fa-skype', 'fa fa-foursquare', 'fa fa-trello', 'fa fa-female', 'fa fa-male', 'fa fa-gittip', 'fa fa-sun-o', 'fa fa-moon-o', 'fa fa-archive', 'fa fa-bug', 'fa fa-vk', 'fa fa-weibo', 'fa fa-renren', 'fa fa-pagelines', 'fa fa-stack-exchange', 'fa fa-arrow-circle-o-right', 'fa fa-arrow-circle-o-left', 'fa fa-toggle-left', 'fa fa-caret-square-o-left', 'fa fa-dot-circle-o', 'fa fa-wheelchair', 'fa fa-vimeo-square', 'fa fa-turkish-lira', 'fa fa-try', 'fa fa-plus-square-o', 'fa fa-space-shuttle', 'fa fa-slack', 'fa fa-envelope-square', 'fa fa-wordpress', 'fa fa-openid', 'fa fa-institution', 'fa fa-bank', 'fa fa-university', 'fa fa-mortar-board', 'fa fa-graduation-cap', 'fa fa-yahoo', 'fa fa-google', 'fa fa-reddit', 'fa fa-reddit-square', 'fa fa-stumbleupon-circle', 'fa fa-stumbleupon', 'fa fa-delicious', 'fa fa-digg', 'fa fa-pied-piper', 'fa fa-pied-piper-alt', 'fa fa-drupal', 'fa fa-joomla', 'fa fa-language', 'fa fa-fax', 'fa fa-building', 'fa fa-child', 'fa fa-paw', 'fa fa-spoon', 'fa fa-cube', 'fa fa-cubes', 'fa fa-behance', 'fa fa-behance-square', 'fa fa-steam', 'fa fa-steam-square', 'fa fa-recycle', 'fa fa-automobile', 'fa fa-car', 'fa fa-cab', 'fa fa-taxi', 'fa fa-tree', 'fa fa-spotify', 'fa fa-deviantart', 'fa fa-soundcloud', 'fa fa-database', 'fa fa-file-pdf-o', 'fa fa-file-word-o', 'fa fa-file-excel-o', 'fa fa-file-powerpoint-o', 'fa fa-file-photo-o', 'fa fa-file-picture-o', 'fa fa-file-image-o', 'fa fa-file-zip-o', 'fa fa-file-archive-o', 'fa fa-file-sound-o', 'fa fa-file-audio-o', 'fa fa-file-movie-o', 'fa fa-file-video-o', 'fa fa-file-code-o', 'fa fa-vine', 'fa fa-codepen', 'fa fa-jsfiddle', 'fa fa-life-bouy', 'fa fa-life-buoy', 'fa fa-life-saver', 'fa fa-support', 'fa fa-life-ring', 'fa fa-circle-o-notch', 'fa fa-ra', 'fa fa-rebel', 'fa fa-ge', 'fa fa-empire', 'fa fa-git-square', 'fa fa-git', 'fa fa-hacker-news', 'fa fa-tencent-weibo', 'fa fa-qq', 'fa fa-wechat', 'fa fa-weixin', 'fa fa-send', 'fa fa-paper-plane', 'fa fa-send-o', 'fa fa-paper-plane-o', 'fa fa-history', 'fa fa-circle-thin', 'fa fa-header', 'fa fa-paragraph', 'fa fa-sliders', 'fa fa-share-alt', 'fa fa-share-alt-square', 'fa fa-bomb', 'fa fa-soccer-ball-o', 'fa fa-futbol-o', 'fa fa-tty', 'fa fa-binoculars', 'fa fa-plug', 'fa fa-slideshare', 'fa fa-twitch', 'fa fa-yelp', 'fa fa-newspaper-o', 'fa fa-wifi', 'fa fa-calculator', 'fa fa-paypal', 'fa fa-google-wallet', 'fa fa-cc-visa', 'fa fa-cc-mastercard', 'fa fa-cc-discover', 'fa fa-cc-amex', 'fa fa-cc-paypal', 'fa fa-cc-stripe', 'fa fa-bell-slash', 'fa fa-bell-slash-o', 'fa fa-trash', 'fa fa-copyright', 'fa fa-at', 'fa fa-eyedropper', 'fa fa-paint-brush', 'fa fa-birthday-cake', 'fa fa-area-chart', 'fa fa-pie-chart', 'fa fa-line-chart', 'fa fa-lastfm', 'fa fa-lastfm-square', 'fa fa-toggle-off', 'fa fa-toggle-on', 'fa fa-bicycle', 'fa fa-bus', 'fa fa-ioxhost', 'fa fa-angellist', 'fa fa-cc', 'fa fa-shekel', 'fa fa-sheqel', 'fa fa-ils', 'fa fa-meanpath' );
				sort( $k2t_icon );
				trim( join( 'fa ', $k2t_icon ) );

				$k2t_margin_top = array(
					'param_name'  => 'mgt',
					'heading'     => esc_html__( 'Margin Top', 'stevecadey' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
					'type'        => 'textfield',
				);
				$k2t_margin_right = array(
					'param_name'  => 'mgr',
					'heading'     => esc_html__( 'Margin Right', 'stevecadey' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
					'type'        => 'textfield',
				);
				$k2t_margin_bottom = array(
					'param_name'  => 'mgb',
					'heading'     => esc_html__( 'Margin Bottom', 'stevecadey' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
					'type'        => 'textfield',
				);
				$k2t_margin_left = array(
					'param_name'  => 'mgl',
					'heading'     => esc_html__( 'Margin Left', 'stevecadey' ),
					'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
					'type'        => 'textfield',
				);
				$k2t_id = array(
					'param_name'  => 'id',
					'heading'     => esc_html__( 'ID', 'stevecadey' ),
					'description' => esc_html__( '(Optional) Enter a unique ID.', 'stevecadey' ),
					'type'        => 'textfield',
				);
				$k2t_class = array(
					'param_name'  => 'class',
					'heading'     => esc_html__( 'Class', 'stevecadey' ),
					'description' => esc_html__( '(Optional) Enter a unique class name.', 'stevecadey' ),
					'type'        => 'textfield',
				);
				$k2t_animation = array(
					'param_name' => 'anm',
					'heading' 	 => esc_html__( 'Enable Animation', 'stevecadey' ),
					'type' 		 => 'checkbox',
					'value'      => array(
						'' => true
					)
				);
				$k2t_animation_name = array(
					'param_name' => 'anm_name',
					'heading' 	 => esc_html__( 'Animation', 'stevecadey' ),
					'type' 		 => 'dropdown',
					'dependency' => array(
						'element' => 'anm',
						'value'   => array( '1' ),
						'not_empty' => false,
					),
					'value'      => array( 'bounce', 'flash', 'pulse', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInUp', 'fade', 'fadeIn', 'fadeInDown', 'fadeInDownBig', 'fadeInLeft', 'fadeInLeftBig', 'fadeInRight', 'fadeInRightBig', 'fadeInUp', 'fadeInUpBig', 'flip', 'flipInX', 'flipInY', 'lightSpeedIn', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'rollIn', 'zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp', 'inview-scroll' ),
				);
				$k2t_animation_delay = array(
					'param_name'  => 'anm_delay',
					'heading'     => esc_html__( 'Animation Delay', 'stevecadey' ),
					'description' => esc_html__( 'Numeric value only, 1000 = 1second.', 'stevecadey' ),
					'type'        => 'textfield',
					'std'		  => '2000',
					'dependency' => array(
						'element' => 'anm',
						'value'   => array( '1' ),
						'not_empty' => false,
					),
				);
				/*  [ Pie Chart ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_pie_chart = array(
					'base'            => 'piechart',
					'name'            => esc_html__( 'K2T Pie Chart', 'stevecadey' ),
					'icon'            => 'fa fa-pie-chart',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'percent',
							'heading'     => esc_html__( 'Percent', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, between 1-100.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Outer Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'trackcolor',
							'heading'     => esc_html__( 'Track Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'textcolor',
							'heading'     => esc_html__( 'Text Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'textbackground',
							'heading'     => esc_html__( 'Text Background', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						array(
							'param_name'  => 'text',
							'heading'     => esc_html__( 'Text', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'thickness',
							'heading'     => esc_html__( 'Thickness', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'speed',
							'heading'     => esc_html__( 'Speed', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, 1000 = 1second.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'delay',
							'heading'     => esc_html__( 'Delay', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'size',
							'heading'     => esc_html__( 'Size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, size = width = height, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'linecap',
							'heading' 	 => esc_html__( 'Linecap', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Butt', 'stevecadey' )   => 'butt',
								esc_html__( 'Square', 'stevecadey' ) => 'square',
								esc_html__( 'Round', 'stevecadey' )  => 'round'
							),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_pie_chart );

					/*  [ Counter ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_counter = array(
					'base'            => 'counter',
					'name'            => esc_html__( 'K2T Counter', 'stevecadey' ),
					'icon'            => 'fa fa-list-ol',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_child'		  => array( 'isotope' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'style_type',
							'heading' 	 => esc_html__( 'Style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Icon Center', 'stevecadey' )  => '1',
								esc_html__( 'Icon Left', 'stevecadey' ) => '2',
							),
						),
						array(
							'param_name'  => 'border_width',
							'heading'     => esc_html__( 'Border Width', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name' => 'border_style',
							'heading'    => esc_html__( 'Border Style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Solid', 'stevecadey' )  => 'solid',
								esc_html__( 'Dashed', 'stevecadey' ) => 'dashed'
							),
						),
						array(
							'param_name'  => 'border_color',
							'heading'     => esc_html__( 'Border', 'stevecadey' ),
							'type'        => 'colorpicker'
						),
						array(
							'param_name' => 'icon_type',
							'heading'    => esc_html__( 'Icon Type', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Icon font', 'stevecadey' )    => 'icon_font',
								esc_html__( 'Icon Graphic', 'stevecadey' ) => 'icon_graphic'
							),
						),
						array(
							'param_name'  => 'icon_font',
							'heading'     => esc_html__( 'Choose Icon', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_size',
							'heading'     => esc_html__( 'Icon size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_color',
							'heading'     => esc_html__( 'Icon Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_background',
							'heading'     => esc_html__( 'Icon Background', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_border_color',
							'heading'     => esc_html__( 'Icon Border', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name' => 'icon_border_style',
							'heading'    => esc_html__( 'Icon Border Style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Solid', 'stevecadey' )  => 'solid',
								esc_html__( 'Dashed', 'stevecadey' ) => 'dashed'
							),
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							)
						),
						array(
							'param_name'  => 'icon_border_width',
							'heading'     => esc_html__( 'Icon Border Width', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_border_radius',
							'heading'     => esc_html__( 'Icon Border Radius', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_font' ),
							),
						),
						array(
							'param_name'  => 'icon_graphic',
							'heading'     => esc_html__( 'Upload icon graphic', 'stevecadey' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_graphic' ),
							),
						),
						array(
							'param_name'  => 'number',
							'heading'     => esc_html__( 'Counter to number', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'number_font_size',
							'heading'     => esc_html__( 'Number font size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'number_color',
							'heading'     => esc_html__( 'Number Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Counter Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'title_font_size',
							'heading'     => esc_html__( 'Title font size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'title_color',
							'heading'     => esc_html__( 'Title Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'speed',
							'heading'     => esc_html__( 'Animation Speed', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'delay',
							'heading'     => esc_html__( 'Animation Delay', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_counter );

				/*  [ Brands ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_brands = array(
					'base'            => 'brands',
					'name'            => esc_html__( 'K2T Brands', 'stevecadey' ),
					'icon'            => 'fa fa-photo',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_parent'       => array( 'only' => 'brand' ),
					'content_element' => true,
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'param_name'  => 'column',
							'heading' 	  => esc_html__( 'Column', 'stevecadey' ),
							'description' => esc_html__( 'Select column display brand', 'stevecadey'),
							'type' 		  => 'dropdown',
							'value'       => array( '0', '1', '2', '3', '4', '5', '6', '7', '8' ),
						),
						array(
							'param_name'  => 'align',
							'heading'     => esc_html__( 'align', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'value' 	  => array(
								esc_html__( 'Left', 'stevecadey' ) => 'align-left',
								esc_html__( 'Right', 'stevecadey' ) => 'align-right',
								esc_html__( 'Center', 'stevecadey' ) => 'align-center',
							),
							'holder'      => 'div',
							'dependency'  => array(
								'element' => 'column',
								'value'  => '0',
							),
						),
						array(
							'param_name'  => 'padding',
							'heading'     => esc_html__( 'Padding', 'stevecadey' ),
							'description' => esc_html__( 'space beetwen items like : 20px ', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency'  => array(
								'element' => 'align',
								'value'	  => array( 'align-left', 'align-right', 'align-center' )
							),
						),
						array(
							'param_name'  => 'opacity_hover',
							'heading'     => esc_html__( 'Opacity hover', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'opacity-hover'
							)
						),
						array(
							'param_name'  => 'grayscale',
							'heading'     => esc_html__( 'Grayscale', 'stevecadey' ),
							'description' => esc_html__( 'Display grayscale.', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_id, $k2t_class
					)
				);
				vc_map( $k2t_brands );

				/*  [ Brand Items ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_brands_item = array(
					'base'            => 'brand',
					'name'            => esc_html__( 'K2T Brands Item', 'stevecadey' ),
					'icon'            => 'fa fa-photo',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_child'        => array( 'only' => 'brands' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Brand Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'tooltip',
							'heading'     => esc_html__( 'Tooltip', 'stevecadey' ),
							'description' => esc_html__( 'Enable tooltip.', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Upload Brand', 'stevecadey' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'size',
							'heading'     => esc_html__( 'Size', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'		  => get_intermediate_image_sizes(),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
					)
				);
				vc_map( $k2t_brands_item );

				/*  [ Button ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_button = array(
					'base'            => 'button',
					'name'            => esc_html__( 'K2T Button', 'stevecadey' ),
					'icon'            => 'fa fa-dot-circle-o',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Button Text', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'target',
							'heading' 	 => esc_html__( 'Link Target', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Open in a new window', 'stevecadey' )                      => '_blank',
								esc_html__( 'Open in the same frame as it was clicked', 'stevecadey' )  => '_self'
							),
							'dependency' => array(
								'element' 		=> 'link',
								'not_empty'   	=> true,
							),
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						array(
							'param_name' => 'icon_position',
							'heading' 	 => esc_html__( 'Icon Position', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Right', 'stevecadey' ) 				=> 'right',
								esc_html__( 'Left', 'stevecadey' )  				=> 'left'
							),
							'dependency' => array(
								'element' 		=> 'icon',
								'not_empty'   	=> true,
							),
						),
						array(
							'param_name' => 'button_style',
							'heading' 	 => esc_html__( 'Button style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Default button', 'stevecadey' ) 		=> 'default',
								esc_html__( 'Outline button', 'stevecadey' ) 		=> 'outline',
								esc_html__( 'Around button', 'stevecadey' ) 		=> 'around',
								esc_html__( 'Shadow button', 'stevecadey' ) 		=> 'shadow',
							),
						),
						array(
							'param_name' => 'button_color',
							'heading' 	 => esc_html__( 'Button color', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Default', 'stevecadey' )   			=> 'default',
								esc_html__( 'Red', 'stevecadey' )   				=> 'red',
								esc_html__( 'Deep Purple', 'stevecadey' ) 		=> 'deep_purple',
								esc_html__( 'Blue', 'stevecadey' )  				=> 'blue',
								esc_html__( 'Green', 'stevecadey' )  			=> 'green',
								esc_html__( 'Amber', 'stevecadey' )  			=> 'amber',
								esc_html__( 'Deep Orange', 'stevecadey' )  		=> 'deep_orange',
								esc_html__( 'Black', 'stevecadey' )  			=> 'black',
								esc_html__( 'White', 'stevecadey' )  			=> 'white',
								esc_html__( 'Custom', 'stevecadey' )  			=> 'custom',
							),
							'dependency' => array(
								'element' => 'button_style',
								'value'   => array( 'default', 'around', 'shadow' )
							),
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Button Background Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'text_color',
							'heading'     => esc_html__( 'Button Text Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'hover_bg_color',
							'heading'     => esc_html__( 'Background Hover Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'hover_text_color',
							'heading'     => esc_html__( 'Text Hover Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'border_color',
							'heading'     => esc_html__( 'Button border Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'border_width',
							'heading'     => esc_html__( 'Button border width', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name'  => 'hover_border_color',
							'heading'     => esc_html__( 'Border Hover Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency'  => array(
								'element' => 'button_color',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name' => 'size',
							'heading' 	 => esc_html__( 'Size', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Small', 'stevecadey' )	=> 'small',
								esc_html__( 'Medium', 'stevecadey' ) => 'medium',
								esc_html__( 'Large', 'stevecadey' )  => 'large'
							),
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'stevecadey' ) 	=> 'left',
								esc_html__( 'Center', 'stevecadey' ) => 'center',
								esc_html__( 'Right', 'stevecadey' )  => 'right'
							),
						),
						array(
							'param_name'  => 'fullwidth',
							'heading'     => esc_html__( 'Button Full Width', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'pill',
							'heading'     => esc_html__( 'Pill', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'radius',
							'heading'     => esc_html__( 'Button radius', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'pill',
								'value'   => 'true'
							),
						),
						array(
							'param_name'  => 'd3',
							'heading'     => esc_html__( '3D', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_margin_top,
						$k2t_margin_right,
						$k2t_margin_bottom,
						$k2t_margin_left,
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_button );

				/*  [ Circle button ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_circle_button = array(
					'base'            => 'circle_button',
					'name'            => esc_html__( 'K2T Circle Button', 'stevecadey' ),
					'icon'            => 'fa fa-circle',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'name',
							'heading'     => esc_html__( 'Button Name', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link To', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'icon_hover',
							'heading' 	 => esc_html__( 'Icon Hover', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						array(
							'param_name'  => 'background_color',
							'heading'     => esc_html__( 'Button Background Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_circle_button );

				/*  [ Google Map ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_google_map = array(
					'base'            => 'google_map',
					'name'            => esc_html__( 'K2T Google Maps', 'stevecadey' ),
					'icon'            => 'fa fa-map-marker',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'z',
							'heading'     => esc_html__( 'Zoom Level', 'stevecadey' ),
							'description' => esc_html__( 'Between 0-20', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'location_type',
							'heading' 	 => esc_html__( 'Map style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'LatLong - multiple Marker', 'stevecadey' )   => 'long_lat',
								esc_html__( 'Address', 'stevecadey' )   => 'address',
							),
						),
						array(
							'param_name'  => 'geo_title',
							'heading'     => esc_html__( 'Places title', 'stevecadey' ),
							'type'        => 'textfield',
							'description' => esc_html__( 'Multi title seperate by \',\' . Don\'t use "', 'stevecadey' ),
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'location_type',
								'value'   => array( 'long_lat' ),
							),
						),
						array(
							'param_name'  => 'geo',
							'heading'     => esc_html__( 'geographical coordinates', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'description' => esc_html__( 'Lat and Long seperate by \',\' Multi geo seperate by \';\' Ex: 3.33, 4.44; 2.22, 1.11', 'stevecadey' ),
							'dependency' => array(
								'element' => 'location_type',
								'value'   => array( 'long_lat' ),
							),
						),
						array(
							'param_name'  => 'address',
							'heading'     => esc_html__( 'Address', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'location_type',
								'value'   => array( 'address' ),
							),
						),
						array(
							'param_name'  => 'infowindow',
							'heading'     => esc_html__( 'Content Info Map', 'stevecadey' ),
							'description' => esc_html__( 'Strong, br are accepted.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'location_type',
								'value'   => array( 'address' ),
							),
						),
						array(
							'param_name'  => 'w',
							'heading'     => esc_html__( 'Width', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'h',
							'heading'     => esc_html__( 'Height', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'marker',
							'heading' 	 => esc_html__( 'Marker', 'stevecadey' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							),
						),
						array(
							'param_name'  => 'markerimage',
							'heading'     => esc_html__( 'Marker Image', 'stevecadey' ),
							'description' => esc_html__( 'Change default Marker.', 'stevecadey' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'marker',
								'value'   => array( 'true' ),
							),
						),
						array(
							'param_name' => 'traffic',
							'heading' 	 => esc_html__( 'Show Traffic', 'stevecadey' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'draggable',
							'heading' 	 => esc_html__( 'Draggable', 'stevecadey' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'infowindowdefault',
							'heading' 	 => esc_html__( 'Show Info Map', 'stevecadey' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'hidecontrols',
							'heading' 	 => esc_html__( 'Hide Control', 'stevecadey' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'scrollwheel',
							'heading' 	 => esc_html__( 'Scroll wheel zooming', 'stevecadey' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'maptype',
							'heading' 	 => esc_html__( 'Map Type', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'ROADMAP', 'stevecadey' )   => 'ROADMAP',
								esc_html__( 'SATELLITE', 'stevecadey' ) => 'SATELLITE',
								esc_html__( 'HYBRID', 'stevecadey' )    => 'HYBRID',
								esc_html__( 'TERRAIN', 'stevecadey' )   => 'TERRAIN'
							),
						),
						array(
							'param_name' => 'mapstype',
							'heading' 	 => esc_html__( 'Map style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'None', 'stevecadey' )   => '',
								esc_html__( 'stevecadey default', 'stevecadey' )   => 'stevecadey',
								esc_html__( 'Subtle Grayscale', 'stevecadey' )   => 'grayscale',
								esc_html__( 'Blue water', 'stevecadey' ) => 'blue_water',
								esc_html__( 'Pale Dawn', 'stevecadey' ) => 'pale_dawn',
								esc_html__( 'Shades of Grey', 'stevecadey' ) => 'shades_of_grey',
							),
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Background Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'content',
							'heading'	  => 'Content',
							'type'		  => 'textarea_html',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_google_map );

				/*  [ K2t Artist ]
				- - - - - - - - - - - - - - - - - - - */

				if ( is_plugin_active( 'k-artist/hooks.php' ) ) :
					$k2t_teacher_listing = array(
						'base'            => 'k_teacher_listing',
						'name'            => esc_html__( 'K2T Artist Listing', 'stevecadey' ),
						'icon'            => 'fa fa-group',
						'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
						'content_element' => true,
						'params'          => array(
							array(
								'param_name'  => 'style',
								'heading'     => esc_html__( 'Artist Listing Style', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'       => array(
									esc_html__('Grid', 'stevecadey')	   => 'teacher-grid-style',
									esc_html__('Carousel','stevecadey') => 'teacher-carousel',
									// esc_html__('Listing', 'stevecadey') => 'teacher-listing-style',
								),
							),

							array(
								'param_name'  => 'items_desktop',
								'heading'     => esc_html__( 'Slides per view on desktop', 'stevecadey' ),
								'description' => esc_html__( 'Item to display for desktop small (device width <= 1200px).', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'style',
									'value'	  => 'teacher-carousel',
								),
							),
							array(
								'param_name'  => 'items_tablet',
								'heading'     => esc_html__( 'Slides per view on tablet', 'stevecadey' ),
								'description' => esc_html__( 'Item to display for tablet (device width <= 768px).', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'style',
									'value'	  => 'teacher-carousel',
								),
							),
							array(
								'param_name'  => 'items_mobile',
								'heading'     => esc_html__( 'Slides per view on mobile', 'stevecadey' ),
								'description' => esc_html__( 'Item to display for mobile (device width <= 480px).', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'style',
									'value'	  => 'teacher-carousel',
								),
							),

							array(
								'param_name'  => 'column',
								'heading'     => esc_html__( 'Artist Columns', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('2 Columns', 'stevecadey') => '2',
									esc_html__('3 Columns', 'stevecadey') => '3',
									esc_html__('4 Columns', 'stevecadey') => '4',
								),
								'dependency'  => array(
									'element' => 'style',
									'value'   => array( 'teacher-grid-style' ),
								),
							),
							array(
								'param_name'  => 'cat',
								'heading'     => esc_html__( 'Artist Categories ID', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'std'		  => '',
							),
							array(
								'param_name'  => 'teacher_per_page',
								'heading'     => esc_html__( 'Teacher per page', 'stevecadey' ),
								'description' => esc_html__( 'Fill out -1 if you want to display ALL teachers.', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'std'         => '12',
								'dependency'  => array(
									'element' => 'style',
									'value'	  => 'teacher-grid-style',
								),
							),
							array(
								'param_name'  => 'filter',
								'heading'     => esc_html__( 'Display Filter', 'stevecadey' ),
								'type'        => 'dropdown',
								'holder'      => 'div',
								'value'		  => array(
									esc_html__('Show', 'stevecadey') => 'show',
									esc_html__('Hide', 'stevecadey') => 'hide',
								),
								'dependency'  => array(
									'element' => 'style',
									'value'	  => 'teacher-grid-style',
								),
							),
							array(
								'param_name'  => 'show_hide_pagination',
								'heading'     => esc_html__( 'Show/Hide Pagination', 'stevecadey' ),
								'type'        => 'dropdown',
								'holder'      => 'div',
								'value'		  => array(
									esc_html__('Show', 'stevecadey') => 'show',
									esc_html__('Hide', 'stevecadey') => 'hide',
								),
								'dependency'  => array(
									'element' => 'style',
									'value'   => 'teacher-grid-style',
								),
							),
							$k2t_animation,
							$k2t_animation_name,
							$k2t_animation_delay,
							$k2t_id,
							$k2t_class
						)
					);
					vc_map( $k2t_teacher_listing );

				endif; // K teacher


				/*  [ Heading ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_heading = array(
					'base'            => 'heading',
					'name'            => esc_html__( 'K2T Heading', 'stevecadey' ),
					'icon'            => 'fa fa-header',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'value'       => ''
						),
						array(
							'param_name' => 'h',
							'heading' 	 => esc_html__( 'Heading Tag', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'H1', 'stevecadey' ) => 'h1',
								esc_html__( 'H2', 'stevecadey' ) => 'h2',
								esc_html__( 'H3', 'stevecadey' ) => 'h3',
								esc_html__( 'H4', 'stevecadey' ) => 'h4',
								esc_html__( 'H5', 'stevecadey' ) => 'h5',
								esc_html__( 'H6', 'stevecadey' ) => 'h6',
								esc_html__( 'Custom', 'stevecadey' ) => 'custom',
							),
						),
						array(
							'param_name' => 'border',
							'heading' 	 => esc_html__( 'Has border', 'stevecadey' ),
							'type' 		 => 'checkbox',
							'value'      => array(
								'' => 'true'
							)
						),
						array(
							'param_name' => 'border_style',
							'heading' 	 => esc_html__( 'Border Style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Bottom Icon', 'stevecadey' ) 		=> 'bottom_icon',
								esc_html__( 'Double Line', 'stevecadey' )  		=> 'double_line',
								esc_html__( 'Dotted Line', 'stevecadey' )  		=> 'three_dotted',
								esc_html__( 'Special', 'stevecadey' )  		    => 'special_heading',
								esc_html__( 'Custom Line', 'stevecadey' )  		=> 'custom_line',
							),
							'dependency' => array(
								'element' => 'border',
								'value'   => array( 'true' )
							),
						),
						array(
							'param_name'  => 'font_size',
							'heading'     => esc_html__( 'Custom Font Size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'h',
								'value'   => array( 'custom' )
							),
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'stevecadey' )   => 'left',
								esc_html__( 'Center', 'stevecadey' ) => 'center',
								esc_html__( 'Right', 'stevecadey' )  => 'right'
							),
						),
						array(
							'param_name'  => 'font',
							'heading'     => esc_html__( 'Title Font', 'stevecadey' ),
							'description' => esc_html__( 'Use Google Font', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Title Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'excerpt',
							'heading'     => esc_html__( 'Excertp', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'value'       => ''
						),
						array(
							'param_name'  => 'ex_font_size',
							'heading'     => esc_html__( 'Custom Font Size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'ex_color',
							'heading'     => esc_html__( 'Excerpt Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'cb_width',
							'heading'     => esc_html__( 'Width', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'custom_line' )
							),
						),
						array(
							'param_name'  => 'cb_height',
							'heading'     => esc_html__( 'Height', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'custom_line' )
							),
						),
						array(
							'param_name'  => 'cb_position',
							'heading'     => esc_html__( 'position', 'stevecadey' ),
							'description' => esc_html__( 'postion of border', 'stevecadey' ),
							'type'        => 'dropdown',
							'value' 	  => array(
								esc_html__( 'Top', 'stevecadey' )		  => 'top',
								esc_html__( 'Right', 'stevecadey' )	  => 'right',
								esc_html__( 'Bottom', 'stevecadey' )     => 'bottom',
								esc_html__( 'Left', 'stevecadey' )	  	  => 'left',
							),
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'custom_line' )
							),
						),
						array(
							'param_name'  => 'cb_lr_top',
							'heading'     => esc_html__( 'Padding top', 'stevecadey' ),
							'description' => esc_html__( 'spacing from top . Ex: 25px', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'right', 'left' ),
							),
						),
						array(
							'param_name'  => 'cb_lr_padding',
							'heading'     => esc_html__( 'Padding beetwen', 'stevecadey' ),
							'description' => esc_html__( 'spacing beetwen border and heading text . Ex: 26px', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'right', 'left' ),
							),
						),
						array(
							'param_name'  => 'cb_tb_top',
							'heading'     => esc_html__( 'Padding top', 'stevecadey' ),
							'description' => esc_html__( 'spacing top . Ex: 25px', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'top', 'bottom' ),
							),
						),
						array(
							'param_name'  => 'cb_tb_bottom',
							'heading'     => esc_html__( 'Padding bottm', 'stevecadey' ),
							'description' => esc_html__( 'spacing bottom . Ex: 25px', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'cb_position',
								'value'   => array( 'top', 'bottom' ),
							),
						),
						array(
							'param_name'  => 'border_color',
							'heading'     => esc_html__( 'Border Color', 'stevecadey' ),
							'type'        => 'colorpicker',
							'dependency' => array(
								'element' => 'border',
								'value'   => array( 'true' )
							),
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
							'dependency' => array(
								'element' => 'border_style',
								'value'   => array( 'bottom_icon', 'boxed_heading' )
							),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_heading );

				/*  [ Icon Box ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_icon_box = array(
					'base'            => 'iconbox',
					'name'            => esc_html__( 'K2T Icon Box', 'stevecadey' ),
					'icon'            => 'fa fa-th',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'layout',
							'heading' 	 => esc_html__( 'Layout', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array( '1', '2', '3'),
						),
						array(
							'param_name'  => 'bgcolor',
							'heading'     => esc_html__( 'Background Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'title_link',
							'heading'     => esc_html__( 'Title link to', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'fontsize',
							'heading'     => esc_html__( 'Title Font Size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'text_transform',
							'heading' 	 => esc_html__( 'Text Transform', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Inherit', 'stevecadey' )    => 'inherit',
								esc_html__( 'Uppercase', 'stevecadey' )  => 'uppercase',
								esc_html__( 'Lowercase', 'stevecadey' )  => 'lowercase',
								esc_html__( 'Initial', 'stevecadey' )    => 'initial',
								esc_html__( 'Capitalize', 'stevecadey' ) => 'capitalize',
							),
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Title Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title_margin_bottom',
							'heading'     => esc_html__( 'Title margin bottom', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
						),
						array(
							'param_name' => 'icon_type',
							'heading' 	 => esc_html__( 'Icon Type', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								'Icon Fonts' => 'icon_fonts',
								'Graphics'   => 'graphics',
							)
						),
						array(
							'param_name' => 'graphic',
							'heading' 	 => esc_html__( 'Choose Images', 'stevecadey' ),
							'type' 		 => 'attach_image',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'graphics' )
							),
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_fonts' )
							),
							'value'      => '',
						),
						array(
							'param_name'  => 'icon_font_size',
							'heading'     => esc_html__( 'Icon size', 'stevecadey' ),
							'type'        => 'textfield',
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_fonts' )
							),
						),
						array(
							'param_name' => 'icon_color',
							'heading' 	 => esc_html__( 'Icon Color', 'stevecadey' ),
							'type' 		 => 'colorpicker',
							'dependency' => array(
								'element' => 'icon_type',
								'value'   => array( 'icon_fonts' )
							),
						),
						array(
							'param_name'  => 'icon_margin_bottom',
							'heading'     => esc_html__( 'icon margin bottom', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link to', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link_text',
							'heading'     => esc_html__( 'Link text', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Content', 'stevecadey' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_margin_top,
						$k2t_margin_right,
						$k2t_margin_bottom,
						$k2t_margin_left,
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_icon_box );

				/*  [ Icon List ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_icon_list = array(
					'base'            => 'iconlist',
					'name'            => esc_html__( 'K2T Icon List', 'stevecadey' ),
					'icon'            => 'fa fa-list',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_parent'       => array( 'only' => 'li' ),
					'content_element' => true,
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Icon Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				$k2t_icon_list_item = array(
					'base'            => 'li',
					'name'            => esc_html__( 'K2T Icon List', 'stevecadey' ),
					'icon'            => 'fa fa-ellipsis-v',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_child'        => array( 'only' => 'iconlist' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'icon',
							'heading' 	 => esc_html__( 'Choose Icon', 'stevecadey' ),
							'type' 		 => 'k2t_icon',
							'value'      => '',
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
					)
				);
				vc_map( $k2t_icon_list );
				vc_map( $k2t_icon_list_item );

				/*  wrap member */

				$k2t_isotope = array(
					'base'            => 'isotope',
					'name'            => esc_html__( 'K2T Grid Member', 'stevecadey' ),
					'icon'            => 'fa fa-exchange',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_parent'       => array( 'only' => 'member,counter' ),
					'js_view'         => 'VcColumnView',
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'column',
							'heading'     => esc_html__( 'Number of columns', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','stevecadey') => 'column-2',
								esc_html__('3 column','stevecadey') => 'column-3',
								esc_html__('4 column','stevecadey') => 'column-4',
								esc_html__('5 column','stevecadey') => 'column-5',
							),
						),
						array(
							'param_name'  => 'column_md',
							'heading'     => esc_html__( 'Number of columns on Medium device >= 992px', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','stevecadey') => 'column-md-2',
								esc_html__('3 column','stevecadey') => 'column-md-3',
								esc_html__('4 column','stevecadey') => 'column-md-4',
								esc_html__('5 column','stevecadey') => 'column-md-5',
							),
						),
						array(
							'param_name'  => 'column_sm',
							'heading'     => esc_html__( 'Number of columns on Small device >= 768px', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','stevecadey') => 'column-sm-2',
								esc_html__('3 column','stevecadey') => 'column-sm-3',
								esc_html__('4 column','stevecadey') => 'column-sm-4',
								esc_html__('5 column','stevecadey') => 'column-sm-5',
							),
						),
						array(
							'param_name'  => 'column_xs',
							'heading'     => esc_html__( 'Number of columns on Small device < 768px', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__('2 column','stevecadey') => 'column-xs-2',
								esc_html__('3 column','stevecadey') => 'column-xs-3',
								esc_html__('4 column','stevecadey') => 'column-xs-4',
								esc_html__('5 column','stevecadey') => 'column-xs-5',
							),
						),
					),
				);

				vc_map( $k2t_isotope );

				/*  [ Member ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_member = array(
					'base'            => 'member',
					'name'            => esc_html__( 'K2T Member Item', 'stevecadey' ),
					'icon'            => 'fa fa-user',
					'as_child' 		  => 'isotope',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'style',
							'heading' 	 => esc_html__( 'Style Member', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Default', 'stevecadey' ) => 'default',
								esc_html__( 'Boxed', 'stevecadey' ) => 'boxed',
								esc_html__( 'Zoom', 'stevecadey' )  => 'zoom',
								esc_html__( 'slice', 'stevecadey' )  => 'slice',
							)
						),
						array(
							'param_name'  => 'image',
							'heading'     => esc_html__( 'Member Avatar', 'stevecadey' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
						),
						array(
							'param_name' => 'size',
							'heading' 	 => esc_html__( 'Image size', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => get_intermediate_image_sizes(),
						),
						array(
							'param_name'  => 'name',
							'heading'     => esc_html__( 'Member Name', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'role',
							'heading'     => esc_html__( 'Role', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'facebook',
							'heading'     => esc_html__( 'Facebook URL', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'twitter',
							'heading'     => esc_html__( 'Twitter URL', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'skype',
							'heading'     => esc_html__( 'Skype', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'pinterest',
							'heading'     => esc_html__( 'Pinterest URL', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'instagram',
							'heading'     => esc_html__( 'Instagram', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'dribbble',
							'heading'     => esc_html__( 'Dribbble URL', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'google_plus',
							'heading'     => esc_html__( 'Google Plus URL', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Member Info', 'stevecadey' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_member );

				/*  [ Pricing Table ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_pricing = array(
					'base'            => 'pricing',
					'name'            => esc_html__( 'K2T Pricing Table', 'stevecadey' ),
					'icon'            => 'fa fa-table',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_parent'       => array( 'only' => 'pricing_column' ),
					'content_element' => true,
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'param_name' => 'separated',
							'heading' 	 => esc_html__( 'Separated', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'True', 'stevecadey' )  => 'true',
								esc_html__( 'False', 'stevecadey' ) => 'false',
							)
						),
						$k2t_id,
						$k2t_class
					)
				);
				$k2t_pricing_item = array(
					'base'            => 'pricing_column',
					'name'            => esc_html__( 'K2T Pricing Columns', 'stevecadey' ),
					'icon'            => 'fa fa-table',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_child'        => array( 'only' => 'pricing' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'sub_title',
							'heading'     => esc_html__( 'Sub Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'image',
							'heading'     => esc_html__( 'Pricing Image', 'stevecadey' ),
							'type'        => 'attach_image',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'old_price',
							'heading'     => esc_html__( 'Old price', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'price',
							'heading'     => esc_html__( 'Price', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'price_per',
							'heading'     => esc_html__( 'Price Per', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'unit',
							'heading'     => esc_html__( 'Unit', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link',
							'heading'     => esc_html__( 'Link to', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link_text',
							'heading'     => esc_html__( 'Link Text', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'target',
							'heading' 	 => esc_html__( 'Link Target', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Open in a new window', 'stevecadey' )                      => '_blank',
								esc_html__( 'Open in the same frame as it was clicked', 'stevecadey' )  => '_self'
							),
						),
						array(
							'param_name' => 'featured',
							'heading' 	 => esc_html__( 'Featured', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'False', 'stevecadey' ) => 'false',
								esc_html__( 'True', 'stevecadey' )  => 'true',
							)
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'List Item', 'stevecadey' ),
							'description' => esc_html__( 'Using ul li tag.', 'stevecadey' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_pricing );
				vc_map( $k2t_pricing_item );

				/*  [ Progress ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_progress = array(
					'base'            => 'progress',
					'name'            => esc_html__( 'K2T Progress', 'stevecadey' ),
					'icon'            => 'fa fa-sliders',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'percent',
							'heading'     => esc_html__( 'Percent', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, between 1-100.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'background_color',
							'heading'     => esc_html__( 'Background Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'text_color',
							'heading'     => esc_html__( 'Text Color', 'stevecadey' ),
							'type'        => 'colorpicker',
						),
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'height',
							'heading'     => esc_html__( 'Height', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'striped',
							'heading'     => esc_html__( 'Striped', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_progress );

				/*  [ Responsive Text ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_responsive_text = array(
					'base'            => 'responsive_text',
					'name'            => esc_html__( 'K2T Responsive text', 'stevecadey' ),
					'icon'            => 'fa fa-arrows-alt',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'compression',
							'heading'     => esc_html__( 'Compression', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'min_size',
							'heading'     => esc_html__( 'Min Font Size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'max_size',
							'heading'     => esc_html__( 'Max Font Size', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, unit is pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_responsive_text );

				/*  [ Testimonial ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_textimonial = array(
					'base'            => 'testimonial',
					'name'            => esc_html__( 'K2T Testimonials', 'stevecadey' ),
					'icon'            => 'fa fa-exchange',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_parent'       => array( 'only' => 'testi' ),
					'js_view'         => 'VcColumnView',
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'style',
							'heading'     => esc_html__( 'Name', 'stevecadey' ),
							'type'        => 'dropdown',
							'value'		  => array(
								esc_html__( 'Style 1', 'stevecadey' )	=>	'style-1',
								esc_html__( 'Style 2', 'stevecadey' )	=>	'style-2',
								esc_html__( 'Style 3', 'stevecadey' )	=>	'style-3',
							),
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'align',
							'heading'     => esc_html__( 'Name', 'stevecadey' ),
							'type'        => 'dropdown',
							'value'		  => array(
								esc_html__( 'Center', 'stevecadey' )	=>	'align-center',
								esc_html__( 'Left', 'stevecadey' )		=>	'align-left',
								esc_html__( 'Right', 'stevecadey' )		=>	'align-right',
							),
							'dependency'  => array(
								'element'	=> 'style',
								'value'		=> 'style-1',
								),
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'color',
							'heading'     => esc_html__( 'Text Color', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'colorpicker',
							'holder'      => 'div'
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_class,
					)
				);
				vc_map( $k2t_textimonial );
				$k2t_testi = array(
					'base'            => 'testi',
					'name'            => esc_html__( 'K2T Testimonial', 'stevecadey' ),
					'icon'            => 'fa fa-comments-o',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_child'        => array( 'only' => 'testimonial' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'image',
							'heading'     => esc_html__( 'Avatar', 'stevecadey' ),
							'description' => esc_html__( 'Choose avatar for testimonial author.', 'stevecadey' ),
							'type'        => 'attach_image',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'name',
							'heading'     => esc_html__( 'Name', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						)
						,array(
							'param_name'  => 'job',
							'heading'     => esc_html__( 'Job', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Text', 'stevecadey' ),
							'description' => esc_html__( 'Enter your testimonial.', 'stevecadey' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_testi );

				/*  [ Blockquote ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_blockquote = array(
					'base'            => 'blockquote',
					'name'            => esc_html__( 'K2T Blockquote', 'stevecadey' ),
					'icon'            => 'fa fa-quote-left',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name' => 'style',
							'heading' 	 => esc_html__( 'Style', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Style 1', 'stevecadey' )   => '1',
								esc_html__( 'Style 2', 'stevecadey' )   => '2',
							),
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'stevecadey' )   => 'left',
								esc_html__( 'Center', 'stevecadey' ) => 'center',
								esc_html__( 'Right', 'stevecadey' )  => 'right'
							),
						),
						array(
							'param_name'  => 'author',
							'heading'     => esc_html__( 'Author', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'link_author',
							'heading'     => esc_html__( 'Link to', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Content', 'stevecadey' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_blockquote );

				/*  [ Countdown ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_countdown = array(
					'base'            => 'countdown',
					'name'            => esc_html__( 'K2T Countdown', 'stevecadey' ),
					'icon'            => 'fa fa-sort-numeric-desc',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'time',
							'heading'     => esc_html__( 'Time', 'stevecadey' ),
							'description' => esc_html__( 'The time in this format: m/d/y h:mm tt', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name' => 'align',
							'heading' 	 => esc_html__( 'Align', 'stevecadey' ),
							'type' 		 => 'dropdown',
							'value'      => array(
								esc_html__( 'Left', 'stevecadey' )   => 'left',
								esc_html__( 'Center', 'stevecadey' ) => 'center',
								esc_html__( 'Right', 'stevecadey' )  => 'right'
							),
						),
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_countdown );

				/*  [ Embed ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_embed = array(
					'base'            => 'k2t_embed',
					'name'            => esc_html__( 'K2T Embed', 'stevecadey' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'width',
							'heading'     => esc_html__( 'Width', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only, Unit is Pixel.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'URL or embed code', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_embed );


				/*  [ k2t text box ]
				- - - - - - - - - - - - - - - - - - - */


				$k2t_textbox = array(
					'base'            => 'k2t_textbox',
					'name'            => esc_html__( 'K2T Textbox', 'stevecadey' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'content',
							'heading'     => esc_html__( 'Content', 'stevecadey' ),
							'description' => esc_html__( 'Enter your text.', 'stevecadey' ),
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'value'       => ''
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_textbox );

				/*  [ K2T Slider ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_slider = array(
					'base'            => 'k2t_slider',
					'name'            => esc_html__( 'K2T Carousel', 'stevecadey' ),
					'icon'            => 'fa fa-exchange',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'as_parent'       => array( 'only' => 'vc_single_image, vc_raw_html, event, member' ),
					'js_view'         => 'VcColumnView',
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'items',
							'heading'     => esc_html__( 'Slides per view', 'stevecadey' ),
							'description' => esc_html__( 'Numeric value only.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'items_desktop',
							'heading'     => esc_html__( 'Slides per view on desktop', 'stevecadey' ),
							'description' => esc_html__( 'Item to display for desktop small (device width <= 1200px).', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'items_tablet',
							'heading'     => esc_html__( 'Slides per view on tablet', 'stevecadey' ),
							'description' => esc_html__( 'Item to display for tablet (device width <= 768px).', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'items_mobile',
							'heading'     => esc_html__( 'Slides per view on mobile', 'stevecadey' ),
							'description' => esc_html__( 'Item to display for mobile (device width <= 480px).', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'item_margin',
							'heading'     => esc_html__( 'Margin between items', 'stevecadey' ),
							'description' => esc_html__( 'Ex: 30', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div',
						),
						array(
							'param_name'  => 'auto_play',
							'heading'     => esc_html__( 'Auto Play', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => true
							),
						),
						array(
							'param_name'  => 'navigation',
							'heading'     => esc_html__( 'Navigation', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => true
							),
						),
						array(
							'param_name'  => 'pagination',
							'heading'     => esc_html__( 'Pagination', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'value'       => array(
								'' => true
							),
						),
						$k2t_class
					)
				);
				vc_map( $k2t_slider );

				/*  [ Social ]
				- - - - - - - - - - - - - - - - - - - */

				$k2t_social = array(
					'base'            => 'k2t_social',
					'name'            => esc_html__( 'K2T Social', 'stevecadey' ),
					'icon'            => 'fa fa-terminal',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'align',
							'heading'     => esc_html__( 'align', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value'      => array(
								esc_html__( 'Left', 'stevecadey' ) 	=> 'left',
								esc_html__( 'Right', 'stevecadey' )    => 'right',
								esc_html__( 'Center', 'stevecadey' ) 	=> 'center',
							),
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_social );

				/*  [ Blog Post ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_blog_post = array(
					'base'            => 'blog_post',
					'name'            => esc_html__( 'K2T Blog Post', 'stevecadey' ),
					'icon'            => 'fa fa-file-text',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'style',
							'heading'     => esc_html__( 'Style', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__( 'Masonry', 'stevecadey' ) => 'blog-masonry',
								esc_html__( 'Grid', 'stevecadey' ) => 'blog-grid',
								esc_html__( 'Large', 'stevecadey' ) => 'blog-large',
							),
						),
						array(
							'param_name'  => 'column',
							'heading'     => esc_html__( 'Column', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__( '2 column', 'stevecadey' ) => '2',
								esc_html__( '3 column', 'stevecadey' ) => '3',
								esc_html__( '4 column', 'stevecadey' ) => '4',
								esc_html__( '5 column', 'stevecadey' ) => '5',
							),
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-grid', 'blog-masonry' ),
							),
						),
						array(
							'param_name'  => 'limit',
							'heading'     => esc_html__( 'Number of posts to show', 'stevecadey' ),
							'description' => esc_html__( 'Empty is show all posts.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'cat',
							'heading'     => esc_html__( 'Show posts associated with certain categories', 'stevecadey' ),
							'description' => esc_html__( 'Using category id, separate multiple categories with commas.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'navigation',
							'heading'     => esc_html__( 'Navigation', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'pagination',
							'heading'     => esc_html__( 'Pagination', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'auto_play',
							'heading'     => esc_html__( 'Auto Play', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'blog-slider' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_blog_post );


				/*  [ Video Post ]
				- - - - - - - - - - - - - - - - - - - */
				$k2t_video_post = array(
					'base'            => 'video_listing',
					'name'            => esc_html__( 'K2T Video Listing', 'stevecadey' ),
					'icon'            => 'fa fa-file-text',
					'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
					'content_element' => true,
					'params'          => array(
						array(
							'param_name'  => 'style',
							'heading'     => esc_html__( 'Style', 'stevecadey' ),
							'description' => esc_html__( '', 'stevecadey' ),
							'type'        => 'dropdown',
							'holder'      => 'div',
							'value' 	  => array(
								esc_html__( 'Listing', 'stevecadey' ) => 'listing',
								esc_html__( 'Carousel', 'stevecadey' ) => 'carousel',
							),
						),

						array(
							'param_name'  => 'limit',
							'heading'     => esc_html__( 'Number of posts to show', 'stevecadey' ),
							'description' => esc_html__( 'Empty is show all posts.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'cat',
							'heading'     => esc_html__( 'Show posts associated with certain categories', 'stevecadey' ),
							'description' => esc_html__( 'Using category id, separate multiple categories with commas.', 'stevecadey' ),
							'type'        => 'textfield',
							'holder'      => 'div'
						),
						array(
							'param_name'  => 'navigation',
							'heading'     => esc_html__( 'Navigation', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'carousel' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'pagination',
							'heading'     => esc_html__( 'Pagination', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'carousel', 'listing' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						array(
							'param_name'  => 'auto_play',
							'heading'     => esc_html__( 'Auto Play', 'stevecadey' ),
							'type'        => 'checkbox',
							'holder'      => 'div',
							'dependency' => array(
								'element' => 'style',
								'value'   => array( 'carousel' ),
							),
							'value'       => array(
								'' => 'true'
							)
						),
						$k2t_animation,
						$k2t_animation_name,
						$k2t_animation_delay,
						$k2t_id,
						$k2t_class
					)
				);
				vc_map( $k2t_video_post );

				/*  [ K2t Event ]
				- - - - - - - - - - - - - - - - - - - */
				if ( is_plugin_active( 'k-event/hooks.php' ) ) {
					$k2t_event_listing = array(
						'base'            => 'k_event_listing',
						'name'            => esc_html__( 'K2T Event Listing', 'stevecadey' ),
						'icon'            => 'fa fa-calendar',
						'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
						'content_element' => true,
						'params'          => array(
							array(
								'param_name'  => 'style',
								'heading'     => esc_html__( 'Event Listing Style', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'		  => array(
									esc_html__('Event Grid', 'stevecadey') 		=> 'event-grid-style',
									esc_html__('Event Listing', 'stevecadey') 	=> 'event-listing-style',
									esc_html__('Event Classic', 'stevecadey') 	=> 'event-classic-style',
								),
								'holder'      => 'div'
							),
							array(
								'param_name'  => 'cate_event_id',
								'heading'     => esc_html__( 'Show Event associated with certain categories', 'stevecadey' ),
								'description' => esc_html__( 'Using category id, separate multiple categories with commas.', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'dependency' => array(
									'element' => 'style',
									'value'   => array( 'event-grid-style', 'event-listing-style', 'event-classic-style' ),
								),
							),
							array(
								'param_name'  => 'post_per_page',
								'heading'     => esc_html__( 'Number of Event to show', 'stevecadey' ),
								'description' => esc_html__( 'Empty is show all posts.', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
								'dependency' => array(
									'element' => 'style',
									'value'   => array( 'event-grid-style', 'event-listing-style', 'event-classic-style' ),
								),
							),
							array(
								'param_name'  => 'filter',
								'heading'     => esc_html__( 'Show/Hide Filter', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Show', 'stevecadey') => 'show',
									esc_html__('Hide', 'stevecadey') => 'hide',
								),
								'holder'      => 'div',
								'dependency' => array(
									'element' => 'style',
									'value'   => array( 'event-grid-style','event-listing-style', 'event-classic-style' ),
								),
							),

							array(
								'param_name'  => 'show_hide_viewmore',
								'heading'     => esc_html__( 'Show/Hide Button View More', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Show', 'stevecadey') => 'show',
									esc_html__('Hide', 'stevecadey') => 'hide',
								),
								'holder'      => 'div',
								'dependency' => array(
									'element' => 'style',
									'value'   => array( 'event-grid-style', 'event-listing-style', 'event-classic-style' ),
								),
							),

							array(
								'param_name'  => 'pagination',
								'heading'     => esc_html__( 'Show/Hide Pagination', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Show', 'stevecadey') => 'show',
									esc_html__('Hide', 'stevecadey') => '',
								),
								'holder'      => 'div',
								'dependency' => array(
									'element' => 'style',
									'value'   => array( 'event-grid-style', 'event-listing-style', 'event-classic-style' ),
								),
							),
							$k2t_id,
							$k2t_class
						)
					);
					vc_map( $k2t_event_listing );
				}

				/*  [ K2t Gallery ]
				- - - - - - - - - - - - - - - - - - - */
				if ( is_plugin_active( 'k-gallery/init.php' ) ) {
					$k2t_gallery_listing = array(
						'base'            => 'k2t-gallery',
						'name'            => esc_html__( 'K2T Gallery', 'stevecadey' ),
						'icon'            => 'fa fa-image',
						'category'        => esc_html__( 'Stevecadey Shortcodes', 'stevecadey' ),
						'content_element' => true,
						'params'          => array(
							array(
								'param_name'  => 'title',
								'heading'     => esc_html__( 'Title', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
							),
							array(
								'param_name'  => 'gallery_style',
								'heading'     => esc_html__( 'Gallery Style', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Free Style', 'stevecadey') 	=> 'free_style',
									esc_html__('Grid', 'stevecadey') 		=> 'grid_style',
								),
								'holder'      => 'div',
							),
							array(
								'param_name'  => 'number',
								'heading'     => esc_html__( 'Number image to show', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
							),
							array(
								'param_name'  => 'categories',
								'heading'     => esc_html__( 'Categories', 'stevecadey' ),
								'type'        => 'textfield',
								'holder'      => 'div',
							),
							array(
								'param_name'  => 'filter',
								'heading'     => esc_html__( 'Show/Hide Filter', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'       => array(
									esc_html__('Show', 'stevecadey') 		=> 'show',
									esc_html__('Hide', 'stevecadey') 		=> 'hide',
								),
								'std'		  => 'hide',
								'holder'      => 'div',
							),
							array(
								'param_name'  => 'gallery_pagination',
								'heading'     => esc_html__( 'Show/Hide Pagination', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'       => array(
									esc_html__('Show', 'stevecadey') 		=> 'show',
									esc_html__('Hide', 'stevecadey') 		=> 'hide',
								),
								'std'		  => 'hide',
								'holder'      => 'div',
							),
							array(
								'param_name'  => 'text_align',
								'heading'     => esc_html__( 'Filter Align', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('Center', 'stevecadey') 	=> 'center',
									esc_html__('Left', 'stevecadey') 	=> 'left',
									esc_html__('Right', 'stevecadey') 	=> 'right',
								),
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'filter',
									'value'   => array( 'show' ),
								),
							),
							array(
								'param_name'  => 'column',
								'heading'     => esc_html__( 'Columns', 'stevecadey' ),
								'type'        => 'dropdown',
								'value'      => array(
									esc_html__('3 Columns', 'stevecadey') 	=> '3',
									esc_html__('4 Columns', 'stevecadey') 	=> '4',
									esc_html__('5 Columns', 'stevecadey') 	=> '5',
									esc_html__('6 Columns', 'stevecadey') 	=> '6',
								),
								'holder'      => 'div',
								'dependency'  => array(
									'element' => 'gallery_style',
									'value'   => array( 'grid_style' ),
								),
							),

							$k2t_id,
							$k2t_class
						)
					);
					vc_map( $k2t_gallery_listing );
				}

			vc_add_param( 'vc_single_image', $k2t_animation );
			vc_add_param( 'vc_single_image', $k2t_animation_name );
			vc_add_param( 'vc_single_image', $k2t_animation_delay );

			vc_add_param( 'vc_column_text', $k2t_animation );
			vc_add_param( 'vc_column_text', $k2t_animation_name );
			vc_add_param( 'vc_column_text', $k2t_animation_delay );

			}

			add_action( 'admin_init', 'stevecadey_vc_map_shortcodes' );

			/*  [ Extend container class (parents) ]
			- - - - - - - - - - - - - - - - - - - - - - - - - */
			class WPBakeryShortCode_Accordion extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Brands extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Iconlist extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Pricing extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Sticky_Tab extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_K2t_Slider extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_isotope extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_testimonial extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_stevecadey_Slider extends WPBakeryShortCodesContainer {}
			class WPBakeryShortCode_Page_Nav extends WPBakeryShortCodesContainer {}

			/*  [ Extend shortcode class (children) ]
			- - - - - - - - - - - - - - - - - - - - - - - - - */
			class WPBakeryShortCode_Toggle extends WPBakeryShortCode {}
			class WPBakeryShortCode_Brand extends WPBakeryShortCode {}
			class WPBakeryShortCode_Li extends WPBakeryShortCode {}
			class WPBakeryShortCode_Pricing_Column extends WPBakeryShortCode {}
			class WPBakeryShortCode_Step extends WPBakeryShortCode {}
			class WPBakeryShortCode_Tab extends WPBakeryShortCode {}
			class WPBakeryShortCode_stevecadey_Slide extends WPBakeryShortCode {}
			class WPBakeryShortCode_Page_Nav_Item extends WPBakeryShortCode {}

		endif;
	endif;

}