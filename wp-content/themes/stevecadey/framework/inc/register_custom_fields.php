<?php
/**
 * Advanced custom field for theme.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link    http://www.lunartheme.com
 */

if ( function_exists( 'register_field_group' ) ):

	// Page acf

	register_field_group(
		array(
			'key'    => 'group_53d8edf4beaff',
			'title'  => 'Page Options',
			'fields' => array(
				array(
					'key'               => 'field_53e77e2f35e12',
					'label'             => 'Page layout',
					'type'              => 'tab',
					'placement' 		=> 'left',
				),

				// Page Tab layout
					array(
						'key'               => 'field_53d8ee35180f4',
						'label'             => 'Page layout',
						'name'              => 'page_layout',
						'type'              => 'select',
						'choices' => array(
							'right_sidebar' => 'Right Sidebar',
							'left_sidebar'  => 'Left Sidebar',
							'no_sidebar'    => 'No Sidebar',
						),
						'default_value'     => 'right_sidebar',
					),
					array(
						'key'               => 'field_53fd51a9335a3',
						'label'             => 'Custom sidebar',
						'name'              => 'page_custom_sidebar',
						'type'              => 'text',
					),
					array(
						'key'               => 'field_53fd51a548698',
						'label'             => 'Header style',
						'name'              => 'page_header_style',
						'type'              => 'select',
						'choices' => array(
							''          => 'Default',
							'h1_header_' => 'Header style 1',
							'h2_header_' => 'Header style 2',
						),
					),
					array(
						'key'               => 'field_542e60920bbc1',
						'label'             => 'Page sidebar width',
						'name'              => 'page_sidebar_width',
						'type'              => 'number',
						'prepend'           => '%',
						'min'               => 0,
						'max'               => 100,
						'step'              => '',
					),

					array(
						'key'               => 'field_542d131580b0c',
						'label'             => 'Top padding of content',
						'name'              => 'page_padding_top',
						'type'              => 'text',
						'default_value'     => '0',
					),
					array(
						'key'               => 'field_542d132d80b0d',
						'label'             => 'Bottom padding of content',
						'name'              => 'page_padding_bottom',
						'type'              => 'text',
						'required'          => 0,
						'conditional_logic' => 0,
						'default_value'     => '0',
					),

				array(
					'key'               => 'field_53fee96451014',
					'label'             => 'Titlebar',
					'type'              => 'tab',
					'placement' 		=> 'left',
				),

				// Page Tab Title bar
					array(
						'key'               => 'field_53fd4b6033834',
						'label'             => 'Show/Hide Title Bar',
						'name'              => 'display_titlebar',
						'type'              => 'select',
						'instructions'      => 'Only support page layout "Full Width" and "No Sidebar"',
						'choices' => array(
							'show'  => 'Show',
							'hided' => 'Hide',
						),
						'default_value' => 'show',
					),
					array(
						'key'               => 'field_53fd4b6454211',
						'label'             => 'Show/Hide Meta data',
						'name'              => 'display_meta',
						'type'              => 'select',
						'instructions'      => '',
						'choices' => array(
							'1' 		 => 'Show',
							'' 	 		=> 	'Hide',
						),
						'default_value' => 'show',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => '1',
								),
							),
						),
					),
					array(
						'key'               => 'field_53fd4b6ksmnje',
						'label'             => 'Show/Hide Main excerpt',
						'name'              => 'display_excerpt',
						'type'              => 'select',
						'instructions'      => '',
						'choices' => array(
							'1' 		 => 'Show',
							'' 	 		=> 	'Hide',
						),
						'default_value' => 'show',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => '1',
								),
							),
						),
					),
					array(
						'key'               => 'field_53fd4b6ejmmlk',
						'label'             => 'Show/Hide breadcrumbs',
						'name'              => 'display_breadcum',
						'type'              => 'select',
						'instructions'      => '',
						'choices' => array(
							'1' 		 => 	'Show',
							'' 	 		 => 	'Hide',
						),
						'default_value' => 'show',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => '1',
								),
							),
						),
					),
					array(
						'key'               => 'field_542e57d750106',
						'label'             => 'Page titlebar font size',
						'name'              => 'titlebar_font_size',
						'type'              => 'text',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
					),
					array(
						'key'               => 'field_53fftww651019',
						'label'             => 'Page title bar color',
						'name'              => 'titlebar_color',
						'type'              => 'color_picker',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
					),
					array(
						'key'               => 'field_53feec3f51016',
						'label'             => 'Padding top',
						'name'              => 'pading_top',
						'type'              => 'text',
						'instructions'      => 'Unit: px (Ex: 10px)',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							)
						),
					),
					array(
						'key'               => 'field_53feec4s51016',
						'label'             => 'Padding bottom',
						'name'              => 'pading_bottom',
						'type'              => 'text',
						'instructions'      => 'Unit: px (Ex: 10px)',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							)
						),
					),
					array(
						'key'               => 'field_53feecea51019',
						'label'             => 'Background color',
						'name'              => 'background_color',
						'type'              => 'color_picker',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
					),
					array(
						'key'               => 'field_53feecb251018',
						'label'             => 'Background image',
						'name'              => 'background_image',
						'type'              => 'image',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'return_format' 	=> 'url',
						'preview_size'      => 'thumbnail',
						'library'           => 'all',
					),
					array(
						'key'               => 'field_53feeda05101b',
						'label'             => 'Background position',
						'name'              => 'background_position',
						'type'              => 'select',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'choices' => array(
							''      => 'None',
							'left top'      => 'Left Top',
							'left center'   => 'Left Center',
							'left bottom'   => 'Left Bottom',
							'right top'     => 'Right Top',
							'right center'  => 'Right Center',
							'right bottom'  => 'Right Bottom',
							'center top'    => 'Center top',
							'center center' => 'Center Center',
							'center bottom' => 'Center Bottom',
						),
						'default_value' => array(),
					),
					array(
						'key'               => 'field_53fuida05167b',
						'label'             => 'Background size',
						'name'              => 'background_size',
						'type'              => 'select',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'choices' => array(
							''		  => 'None',
							'inherit' => 'Inherit',
							'cover'   => 'Cover',
							'contain' => 'Contain',
							'full'    => '100%',
						),
						'default_value' => array(),
					),
					array(
						'key'               => 'field_52rguda05101b',
						'label'             => 'Background repeat',
						'name'              => 'background_repeat',
						'type'              => 'select',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'choices' => array(
							''		  	=> 'no-repeat',
							'no-repeat' => 'No Repeat',
							'repeat'    => 'Repeat',
							'repeat-x'  => 'Repeat X',
							'repeat-y'  => 'Repeat Y',
						),
						'default_value' => 'no-repeat',
					),
					array(
						'key'               => 'field_54336341fb9f7',
						'label'             => 'Background parallax',
						'name'              => 'background_parallax',
						'type'              => 'select',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'choices' => array(
							'1' => 'True',
							'0' => 'False',
						),
						'default_value' => array(),
					),
					array(
						'key'               => 'field_53fef06051020',
						'label'             => 'Titlebar overlay opacity',
						'name'              => 'titlebar_overlay_opacity',
						'type'              => 'select',
						'instructions'      => 'Set your overlay opacity in titlebar',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'choices' => array(
							0   => 0,
							1 	=> 1,
							2 	=> 2,
							3 	=> 3,
							4 	=> 4,
							5 	=> 5,
							6 	=> 6,
							7 	=> 7,
							8 	=> 8,
							9 	=> 9,
							10  => 10,
						),
						'default_value' => array(),
					),
					array(
						'key'               => 'field_53fef07c51021',
						'label'             => 'Titlebar clipmask opacity',
						'name'              => 'titlebar_clipmask_opacity',
						'type'              => 'select',
						'instructions'      => 'Set your clipmask opacity in titlebar',
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'choices' => array(
							0   => 0,
							1 	=> 1,
							2 	=> 2,
							3 	=> 3,
							4 	=> 4,
							5 	=> 5,
							6 	=> 6,
							7 	=> 7,
							8 	=> 8,
							9 	=> 9,
							10  => 10,
						),
						'default_value' => array(),
					),
					array(
						'key'               => 'field_53fef0eb51023',
						'label'             => 'Custom titlebar content',
						'name'              => 'titlebar_custom_content',
						'type'              => 'wysiwyg',
						'required'          => 0,
						'conditional_logic' => array(
							array(
								'rule_rule_rule_rule_0' => array(
									'field'    => 'field_53fd4b6033834',
									'operator' => '==',
									'value'    => 'show',
								),
							),
						),
						'tabs'              => 'all',
						'toolbar'           => 'full',
						'media_upload'      => 1,
					),

				array(
					'key' => 'field_55af590378723',
					'label' => 'Blog',
					'type' => 'tab',
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
				),

				// Page tab blog setting for template

					array(
						'key' => 'field_55af591778724',
						'label' => 'Select categories',
						'name' => 'blog_cat_filter',
						'type' => 'taxonomy',
						'instructions' => 'Only support blog page template',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'taxonomy' => 'category',
						'field_type' => 'multi_select',
						'allow_null' => 0,
						'add_term' => 1,
						'save_terms' => 1,
						'load_terms' => 0,
						'return_format' => 'id',
						'multiple' => 0,
					),
					array(
						'key'               => 'field_55af5913jmnje',
						'label'             => 'Show/Hide Blog filter',
						'name'              => 'blog_filter',
						'type'              => 'select',
						'choices' => array(
							false => 'Hide',
							true => 'Show',
						),
						'default_value' => array(),
					),
			),
			'location' => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'page',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
		) );

	// Post single Acf

	register_field_group( array(
		'key'    => 'group_53d9acd573a57',
		'title'  => 'Post options',
		'fields' => array(
			array(
				'key'               => 'field_53df40c5588c3',
				'label'             => 'Post Layout',
				'type'              => 'tab',
				'placement' 		=> 'left',
			),

		// Post Tab layout

			array(
				'key'               => 'field_53df40dd588c5',
				'label'             => 'Post layout',
				'name'              => 'single_layout',
				'type'              => 'select',
				'choices' => array(
					'default'       => 'Default',
					'right_sidebar' => 'Right Sidebar',
					'left_sidebar'  => 'Left Sidebar',
					'no_sidebar'    => 'No Sidebar',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_53df4176c5f9b',
				'label'             => 'Custom sidebar name',
				'name'              => 'single_custom_sidebar',
				'type'              => 'text',
			),
			array(
				'key'               => 'field_543b350c0528f',
				'label'             => 'Post metadata',
				'type'              => 'tab',
				'placement' 		=> 'left',
			),
			array(
				'key'               => 'field_53e2ed3bf58b4',
				'label'             => 'Show/Hide Categories',
				'name'              => 'display_categories',
				'type'              => 'select',
				'choices' => array(
					'default' 	=> 'Default',
					'show' 		=> 'Show',
					''			=> 'Hide',
				),
				'default_value' => 'default',
			),
			array(
				'key'               => 'field_53e2ed8ke039t',
				'label'             => 'Show/Hide Posted author',
				'name'              => 'display_posted_author',
				'type'              => 'select',
				'choices' => array(
					'default' 	=> 'Default',
					'show' 		=> 'Show',
					''			=> 'Hide',
				),
				'default_value' => 'default',
			),
			array(
				'key'               => 'field_53e2ed80f58b5',
				'label'             => 'Show/Hide post date',
				'name'              => 'display_post_date',
				'type'              => 'select',
				'choices' => array(
					'default' 	=> 'Default',
					'show' 		=> 'Show',
					''			=> 'Hide',
				),
				'default_value' => 'default',
			),
			array(
				'key'               => 'field_53e2f31b8b379',
				'label'             => 'Show/Hide tags',
				'name'              => 'display_tags',
				'type'              => 'select',
				'choices' => array(
					'default' 	=> 'Default',
					'show' 		=> 'Show',
					''			=> 'Hide',
				),
				'default_value' => 'default',
			),
			array(
				'key'               => 'field_53e2eda4f58b7',
				'label'             => 'Show/Hide authorbox',
				'name'              => 'display_authorbox',
				'type'              => 'select',
				'choices' => array(
					'default' 	=> 'Default',
					'show' 		=> 'Show',
					''			=> 'Hide',
				),
				'default_value' => array( 'default' ),
			),
			array(
				'key'               => 'field_53e2edbff58b8',
				'label'             => 'Show/Hide related post',
				'name'              => 'display_related_post',
				'type'              => 'select',
				'choices' => array(
					'default' 	=> 'Default',
					'show' 		=> 'Show',
					''			=> 'Hide',
				),
				'default_value' => 'default',
			),

		array(
					'key'               => 'field_53fee96451014',
					'label'             => 'Titlebar',
					'type'              => 'tab',
					'placement' 		=> 'left',
				),

		// Post title bar tab

			array(
				'key'               => 'field_53fd4b6033834',
				'label'             => 'Show/Hide Title Bar',
				'name'              => 'display_titlebar',
				'type'              => 'select',
				'instructions'      => '',
				'choices' => array(
					'default' 	 => 'Default',
					'1' 		 => 'Custom',
					'' 	 		=> 	'Hide',
				),
				'default_value' => 'show',
			),
			array(
				'key'               => 'field_53fd4b6454211',
				'label'             => 'Show/Hide Meta data',
				'name'              => 'display_meta',
				'type'              => 'select',
				'instructions'      => '',
				'choices' => array(
					'1' 		 => 'Show',
					'' 	 		=> 	'Hide',
				),
				'default_value' => 'show',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
			),
			array(
				'key'               => 'field_53fd4b6ksmnje',
				'label'             => 'Show/Hide Main excerpt',
				'name'              => 'display_excerpt',
				'type'              => 'select',
				'instructions'      => '',
				'choices' => array(
					'1' 		 => 'Show',
					'' 	 		=> 	'Hide',
				),
				'default_value' => 'show',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
			),
			array(
				'key'               => 'field_53fd4b6ejmmlk',
				'label'             => 'Show/Hide breadcrumbs',
				'name'              => 'display_breadcum',
				'type'              => 'select',
				'instructions'      => '',
				'choices' => array(
					'1' 		 => 	'Show',
					'' 	 		 => 	'Hide',
				),
				'default_value' => 'show',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
			),
			array(
				'key'               => 'field_542e57d750106',
				'label'             => 'titlebar font size',
				'name'              => 'titlebar_font_size',
				'type'              => 'text',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
			),
			array(
				'key'               => 'field_53fftww651019',
				'label'             => 'title bar color',
				'name'              => 'titlebar_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
			),
			array(
				'key'               => 'field_53feec3f51016',
				'label'             => 'Padding top',
				'name'              => 'pading_top',
				'type'              => 'text',
				'instructions'      => 'Unit: px (Ex: 10px)',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					)
				),
			),
			array(
				'key'               => 'field_53feec4s51016',
				'label'             => 'Padding bottom',
				'name'              => 'pading_bottom',
				'type'              => 'text',
				'instructions'      => 'Unit: px (Ex: 10px)',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					)
				),
			),
			array(
				'key'               => 'field_53feecea51019',
				'label'             => 'Background color',
				'name'              => 'background_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
			),
			array(
				'key'               => 'field_53feecb251018',
				'label'             => 'Background image',
				'name'              => 'background_image',
				'type'              => 'image',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'return_format' 	=> 'url',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_53feeda05101b',
				'label'             => 'Background position',
				'name'              => 'background_position',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'choices' => array(
					''      => 'None',
					'left top'      => 'Left Top',
					'left center'   => 'Left Center',
					'left bottom'   => 'Left Bottom',
					'right top'     => 'Right Top',
					'right center'  => 'Right Center',
					'right bottom'  => 'Right Bottom',
					'center top'    => 'Center top',
					'center center' => 'Center Center',
					'center bottom' => 'Center Bottom',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_53fuida05167b',
				'label'             => 'Background size',
				'name'              => 'background_size',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'choices' => array(
					''		  => 'None',
					'inherit' => 'Inherit',
					'cover'   => 'Cover',
					'contain' => 'Contain',
					'full'    => '100%',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_52rguda05101b',
				'label'             => 'Background repeat',
				'name'              => 'background_repeat',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'choices' => array(
					''		  	=> 'None',
					'no-repeat' => 'No Repeat',
					'repeat'    => 'Repeat',
					'repeat-x'  => 'Repeat X',
					'repeat-y'  => 'Repeat Y',
				),
				'default_value' => 'repeat',
			),
			array(
				'key'               => 'field_54336341fb9f7',
				'label'             => 'Background parallax',
				'name'              => 'background_parallax',
				'type'              => 'select',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'choices' => array(
					'1' => 'True',
					'0' => 'False',
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_53fef06051020',
				'label'             => 'Titlebar overlay opacity',
				'name'              => 'titlebar_overlay_opacity',
				'type'              => 'select',
				'instructions'      => 'Set your overlay opacity in titlebar',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'choices' => array(
					0   => 0,
					1 	=> 1,
					2 	=> 2,
					3 	=> 3,
					4 	=> 4,
					5 	=> 5,
					6 	=> 6,
					7 	=> 7,
					8 	=> 8,
					9 	=> 9,
					10  => 10,
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_53feeceejmnke',
				'label'             => 'Background overlay color',
				'name'              => 'background_color',
				'type'              => 'color_picker',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
			),
			array(
				'key'               => 'field_53fef07c51021',
				'label'             => 'Titlebar clipmask opacity',
				'name'              => 'titlebar_clipmask_opacity',
				'type'              => 'select',
				'instructions'      => 'Set your clipmask opacity in titlebar',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'choices' => array(
					0   => 0,
					1 	=> 1,
					2 	=> 2,
					3 	=> 3,
					4 	=> 4,
					5 	=> 5,
					6 	=> 6,
					7 	=> 7,
					8 	=> 8,
					9 	=> 9,
					10  => 10,
				),
				'default_value' => array(),
			),
			array(
				'key'               => 'field_53feecslmnjw',
				'label'             => 'Clipmask background image',
				'name'              => 'clipmask_background_img',
				'type'              => 'image',
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'return_format' 	=> 'url',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_53fef0eb51023',
				'label'             => 'Custom titlebar content',
				'name'              => 'titlebar_custom_content',
				'type'              => 'wysiwyg',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						'rule_rule_rule_rule_0' => array(
							'field'    => 'field_53fd4b6033834',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'tabs'              => 'all',
				'toolbar'           => 'full',
				'media_upload'      => 1,
			),


		// Post single fomat tab

			array(
				'key'               => 'field_53df3ff5e761f',
				'label'             => 'Post format',
				'type'              => 'tab',
				'placement' 		=> 'left',
			),
			array(
				'key'               => 'field_53e2edeff58b8',
				'label'             => 'Choose video source',
				'name'              => 'video_format_source',
				'type'              => 'select',
				'choices' => array(
					'link'  => 'Video Link',
					'embed' => 'Video Embed code',
					'local' => 'Upload local file',
				),
				'default_value' => 'link',
			),
			array(
				'key'               => 'field_54338f305aaf9',
				'label'             => 'Video format url',
				'name'              => 'video_url',
				'type'              => 'text',
				'instructions'      => 'You can choose Youtube or Vimeo link (eg: https://www.youtube.com/watch?v=uxHXATYpt2w)',
				'conditional_logic' => array(
					array(
						'rule' => array(
							'field'    => 'field_53e2edeff58b8',
							'operator' => '==',
							'value'    => 'link',
						),
					),
				),
			),
			array(
				'key'               => 'field_53da05e21c492',
				'label'             => 'Video embed code',
				'name'              => 'video_code',
				'type'              => 'textarea',
				'conditional_logic' => array(
					array(
						'rule' => array(
							'field'    => 'field_53e2edeff58b8',
							'operator' => '==',
							'value'    => 'embed',
						),
					),
				),
				'new_lines'         => 'wpautop',
			),
			array(
				'key'               => 'field_53df78207e598',
				'label'             => 'Upload video file',
				'name'              => 'video_local',
				'type'              => 'file',
				'instructions'      => 'Support .mp4 file only',
				'conditional_logic' => array(
					array(
						'rule' => array(
							'field'    => 'field_53e2edeff58b8',
							'operator' => '==',
							'value'    => 'local',
						),
					),
				),
				'return_format'     => 'array',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_53e2etiff58b8',
				'label'             => 'Choose audio source',
				'name'              => 'audio_format_source',
				'prefix'            => '',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'choices' => array(
					'link'  => 'Soundcloud link',
					'local' => 'Upload local file',
				),
				'default_value' => 'link',
				'allow_null'    => 0,
				'multiple'      => 0,
				'ui'            => 0,
				'ajax'          => 0,
				'placeholder'   => '',
				'disabled'      => 0,
				'readonly'      => 0,
			),
			array(
				'key'               => 'field_53de3fbbcb5e4',
				'label'             => 'Soundcloud link',
				'name'              => 'audio_url',
				'prefix'            => '',
				'type'              => 'text',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						'rule' => array(
							'field'    => 'field_53e2etiff58b8',
							'operator' => '==',
							'value'    => 'link',
						),
					),
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'prepend'           => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			),
			array(
				'key'               => 'field_53df78207e597',
				'label'             => 'Upload audio file',
				'name'              => 'audio_local',
				'prefix'            => '',
				'type'              => 'file',
				'instructions'      => 'Support .mp3 file only',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						'rule' => array(
							'field'    => 'field_53e2etiff58b8',
							'operator' => '==',
							'value'    => 'local',
						),
					),
				),
				'return_format'     => 'array',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_543390af5aafa',
				'label'             => 'Link format url',
				'name'              => 'link_format_url',
				'prefix'            => '',
				'type'              => 'text',
				'instructions'      => 'Option for format "link" only',
				'required'          => 0,
				'conditional_logic' => 0,
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'prepend'           => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			),
			array(
				'key'               => 'field_53d9aedbf3c36',
				'label'             => 'Quote author',
				'name'              => 'quote_author',
				'prefix'            => '',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'prepend'           => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			),
			array(
				'key'               => 'field_543390cd5aafb',
				'label'             => 'Author quote url',
				'name'              => 'author_quote_url',
				'prefix'            => '',
				'type'              => 'text',
				'instructions'      => 'Option for format "quote" only',
				'required'          => 0,
				'conditional_logic' => 0,
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'prepend'           => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			),
			array(
				'key'               => 'field_543830cd5aafb',
				'label'             => 'Quote Content',
				'name'              => 'quote_content',
				'prefix'            => '',
				'type'              => 'textarea',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'default_value'     => '',
				'placeholder'       => '',
				'maxlength'         => '',
				'rows'              => '',
				'new_lines'         => 'wpautop',
				'readonly'          => 0,
				'disabled'          => 0,
			),
			array(
				'key'               => 'field_53de3e23b0d0f',
				'label'             => 'Gallery format',
				'name'              => 'post_gallery',
				'prefix'            => '',
				'type'              => 'gallery',
				'instructions'      => 'Option for format "gallery" only',
				'required'          => 0,
				'conditional_logic' => 0,
				'min'               => '',
				'max'               => '',
				'preview_size'      => 'thumbnail',
				'library'           => 'all',
			),
			array(
				'key'               => 'field_54005a6a90104',
				'label'             => 'Gallery auto play',
				'name'              => 'gallery_auto',
				'prefix'            => '',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'choices' => array(
					'true'  => 'True',
					'false' => 'False',
				),
				'default_value'     => array(
					'false' => 'false',
				),
				'allow_null'  => 0,
				'multiple'    => 0,
				'ui'          => 0,
				'ajax'        => 0,
				'placeholder' => '',
				'disabled'    => 0,
				'readonly'    => 0,
			),
			array(
				'key'               => 'field_54005ed990105',
				'label'             => 'Gallery duration time',
				'name'              => 'gallery_auto_time_wait',
				'prefix'            => '',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'default_value'     => 5000,
				'placeholder'       => '',
				'prepend'           => '',
				'prepend'           => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			),
			array(
				'key'               => 'field_54005f8e90106',
				'label'             => 'Gallery speed',
				'name'              => 'gallery_speed',
				'prefix'            => '',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'default_value'     => 300,
				'placeholder'       => '',
				'prepend'           => '',
				'prepend'           => '',
				'maxlength'         => '',
				'readonly'          => 0,
				'disabled'          => 0,
			),
			array(
				'key'               => 'field_54005fa690107',
				'label'             => 'Gallery pagination',
				'name'              => 'gallery_pagination',
				'prefix'            => '',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'choices' => array(
					'true'  => 'True',
					'false' => 'False',
				),
				'default_value' => 'false',
				'allow_null'    => 0,
				'multiple'      => 0,
				'ui'            => 0,
				'ajax'          => 0,
				'placeholder'   => '',
				'disabled'      => 0,
				'readonly'      => 0,
			),
			array(
				'key'               => 'field_5400601f90108',
				'label'             => 'Gallery navigation',
				'name'              => 'gallery_navigation',
				'prefix'            => '',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'choices' => array(
					'true'  => 'True',
					'false' => 'False',
				),
				'default_value' => 'false',
				'allow_null'    => 0,
				'multiple'      => 0,
				'ui'            => 0,
				'ajax'          => 0,
				'placeholder'   => '',
				'disabled'      => 0,
				'readonly'      => 0,
			),
			array(
				'key'               => 'field_5400607990109',
				'label'             => 'Gallery mousewheel using',
				'name'              => 'gallery_mousewheel',
				'prefix'            => '',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'choices' => array(
					'true'  => 'True',
					'false' => 'False',
				),
				'default_value' => array(),
				'allow_null'    => 0,
				'multiple'      => 0,
				'ui'            => 0,
				'ajax'          => 0,
				'placeholder'   => '',
				'disabled'      => 0,
				'readonly'      => 0,
			),

		//

		),
		'location' => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'post',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
	) );

	register_field_group( array(
		'key'    => 'group_68d9acd573a57',
		'title'  => 'Product extra option',
		'fields' => array(
			array(
				'key'               => 'field_68d9acd573a51',
				'label'             => 'Product Size Guide',
				'type'              => 'tab',
				'placement' 		=> 'top',
			),

		// Post Tab layout

			array(
				'key'               => 'field_68d9acd573a52',
				'label'             => '',
				'name'              => 'product_size_guide',
				'type'              => 'wysiwyg',
				'default_value' 	=> '',
			),
			array(
				'key'               => 'field_68d9acd573a53',
				'label'             => 'Product more detail info',
				'type'              => 'tab',
				'placement' 		=> 'top',
			),
			array(
				'key'               => 'field_68d9acd573a54',
				'label'             => '',
				'name'              => 'product_detail_info',
				'type'              => 'wysiwyg',
				'default_value' 	=> '',
			),

		),
		'location' => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'product',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
	) );

endif;
