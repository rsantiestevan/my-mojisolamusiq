<?php
/**
 * Themes functions config woocommerce.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link http://www.lunartheme.com
 */

// Don't duplicate me!
if ( ! class_exists( 'k2t_template_woo' ) ) {

	/**
	 * Class to apply woocommerce templates
	 *
	 * @since 4.0.0
	 */
	class k2t_template_woo {

		/**
		 * Constructor.
		 * @return  void
		 * @since   1.0
		 */
		function __construct() {
			global $smof_data;

			// Add action
			add_action( 'widgets_init', array( $this, 'k2t_woocommerce_widgets_init' ) );
			add_action( 'wp_enqueue_scripts',  array( $this, 'k2t_woocommerce_enqueue_style' ) );
			add_action( 'after_setup_theme', array( $this, 'k2t_woocommerce_image_dimensions' ), 1 );

			// Add filters
			add_filter( 'add_to_cart_fragments', array( $this, 'k2t_add_to_cart_fragment' ) );
			if ( isset($smof_data['shop-products-per-page']) ) {
				add_filter( 'loop_shop_per_page', create_function( '$cols', 'return ' . $smof_data['shop-products-per-page'] . ';' ), 20 );
			}

			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
			add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 25 );

			//woocommerce_template_loop_price

			// single 

			add_action( 'woocommerce_single_product_summary',  array( $this, 'k2t_woocommerce_before_price' ), 5 );
			add_action( 'woocommerce_single_product_summary',  array( $this, 'k2t_woocommerce_after_rate' ), 10 );
			add_action( 'woocommerce_single_product_summary',  'stevecadey_size_guide', 40 );
			add_action( 'woocommerce_single_product_summary',  'stevecadey_detail_info', 40 );
			remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
			add_action( 'woocommerce_after_single_product_summary',  array( $this, 'k2t_woocommerce_show_or_hide_related_product' ) );

			add_action('woocommerce_share','stevecadey_add_share');

			// single after add to cart button

			add_action( 'woocommerce_after_add_to_cart_button', 'single_show_add_to_wishlist' );

			add_action( 'woocommerce_single_variation', 'single_variable_show_add_to_wishlist', 30 );

			// woocommerce_after_shop_loop_item

			remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
			remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
			remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
			remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );

			add_action('woocommerce_after_shop_loop_item', array( $this, 'k2t_woocommerce_after_shop_loop_item_show_title'), 5 );
			add_action('woocommerce_after_shop_loop_item', array( $this, 'k2t_woocommerce_after_shop_loop_item_wrap_price'), 10 );
			add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 10 );
			add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_rating', 10 );
			add_action('woocommerce_after_shop_loop_item', array( $this,'k2t_woocommerce_after_shop_loop_item_end_wrap_price'), 10 );
			add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 15 );
			add_action('woocommerce_after_shop_loop_item', array( $this, 'k2t_woocommerce_after_shop_loop_item_wrap_button'), 20 );
			add_action('woocommerce_after_shop_loop_item', 'show_add_to_wishlist', 25 );
			add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 25 );
			add_action( 'woocommerce_after_shop_loop_item', array( 'YITH_WCQV_Frontend', 'yith_add_quick_view_button' ), 25 );
			add_action('woocommerce_after_shop_loop_item', array( $this, 'k2t_woocommerce_after_shop_loop_item_end_wrap_button'), 30 );

			add_filter( 'woocommerce_product_add_to_cart_text', array( $this, 'custom_woocommerce_product_add_to_cart_text' ) );

			// Empty cart

			// change placeholder of variation checkbox

			add_filter('woocommerce_dropdown_variation_attribute_options_args','change_variation_place_holder');

			function change_variation_place_holder( $args ) {
				$args['show_option_none'] = esc_html__('Select option','stevecadey');
				return $args;
			}

			// check for empty-cart get param to clear the cart
			add_action( 'init', 'woocommerce_clear_cart_url' );
			function woocommerce_clear_cart_url() {
			  global $woocommerce;

				if ( isset( $_GET['empty-cart'] ) ) {
					$woocommerce->cart->empty_cart();
				}
			}

			// change tab description

			add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
			function woo_rename_tabs( $tabs ) {

				$tabs['description']['title'] = esc_html__( 'product description', 'stevecadey' );		// Rename the description tab

				return $tabs;

			}

			// Change "Default Sorting" to "Our sorting" on shop page and in WC Product Settings
			function k2t_change_default_sorting_name( $catalog_orderby ) {
			    return  array(
							'menu_order' => esc_html__( 'Default sorting', 'stevecadey' ),
							'popularity' => esc_html__( 'popularity', 'stevecadey' ),
							'rating'     => esc_html__( 'average rating', 'stevecadey' ),
							'date'       => esc_html__( 'newness', 'stevecadey' ),
							'price'      => esc_html__( 'low to high', 'stevecadey' ),
							'price-desc' => esc_html__( 'high to low', 'stevecadey' )
						);
			}
			add_filter( 'woocommerce_catalog_orderby', 'k2t_change_default_sorting_name' );
			add_filter( 'woocommerce_default_catalog_orderby_options', 'k2t_change_default_sorting_name' );

			/**
			 * Add stevecadey share
			 *
			 * @since 1.0
			 */
			function stevecadey_add_share()
			{
				global $product;
				$sku = $product->get_sku();
				if ( isset( $sku ) && !empty( $sku ) && false ) :
					echo '<div class="prooduct-sku">' .
							'<span>' . esc_html__('SKU: ', 'stevecadey') . '</span>' .
							'<span>' . esc_html($sku) . '</span>' .
						'</div>';
				endif;
				// if ( function_exists('stevecadey_social_share') ){
				// 	echo '<div class="wrap-share">';
				//     echo '<span class="share-label">' . esc_html('Share:','stevecadey') . '</span>';
				//     stevecadey_social_share();
				//     echo '</div>';
				// }
			}

			/**
			 * Add stevecadey size guide
			 *
			 * @since 1.0
			 */
			function stevecadey_size_guide() {
				ob_start();

				$size_guide = function_exists('get_field') ? get_field('product_size_guide') : '';

				// echo '<div class="size-guide">';
				// 	echo '<i class="zmdi zmdi-ruler"></i>';
				// 	echo '<a href="#">' . esc_html__( 'Size Guide', 'stevecadey' ) . '</a>';
				// 	echo '<div class="size-guide-popup">';
				// 		echo $size_guide;
				// 	echo '</div>';
				// echo '</div>';

				echo ob_get_clean();
			}

			/**
			 * Add stevecadey 
			 *
			 * @since 1.0
			 */
			function stevecadey_detail_info() {
				$detail = function_exists('get_field') ? get_field('product_detail_info') : '';
				echo '<div class="product-detail-info">';
				echo $detail;
				echo '</div>';
			}

			/**
			 * Add to wishlist
			 *
			 * @since 1.0
			 */
			function show_add_to_wishlist() {
				if ( is_plugin_active('yith-woocommerce-wishlist/init.php')){
					echo do_shortcode('[yith_wcwl_add_to_wishlist]');
				}
			}

			/**
			 * single simple Add wishlist Shortcode
			 *
			 * @since 1.0
			 */
			function single_show_add_to_wishlist()
			{
				global $product;
				if ( is_plugin_active('yith-woocommerce-wishlist/init.php') && ! $product->is_type( 'variable' ) ) :
					echo do_shortcode('[yith_wcwl_add_to_wishlist]');
				endif;
			}

			/**
			 * single variable Add wishlist Shortcode
			 *
			 * @since 1.0
			 */
			function single_variable_show_add_to_wishlist()
			{
				global $product;
				if ( is_plugin_active('yith-woocommerce-wishlist/init.php') && $product->is_type( 'variable' ) ) :
					echo do_shortcode('[yith_wcwl_add_to_wishlist]');
				endif;
			}

			/**
			 * Change layout button before shop loop
			 *
			 * @since 1.0
			 */
			function k2t_before_shop_loop_switch_btn()
			{
				//echo '<div class="switch-layout"><span>' . esc_html__( 'View', 'stevecadey' ) . '</span><a class="multi-layout"></a><a class="one-layout"></a></div>';
			}
			add_action( 'woocommerce_before_shop_loop', 'k2t_before_shop_loop_switch_btn', 15 );
			/**
			 * Setting theme options for display results count
			 *
			 * @since 1.0
			 */
			if ( isset(  $smof_data['shop-display-result-count'] ) && ! $smof_data['shop-display-result-count'] ) {
				remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
			}

			/**
			 * Setting theme options for display ordering
			 *
			 * @since 1.0
			 */
			if ( isset(  $smof_data['shop-display-sorting'] ) && ! $smof_data['shop-display-sorting'] ) {
				remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
			}
		}

		/**
		 * change add text on add to card button
		 *
		 * custom_woocommerce_template_loop_add_to_cart
		*/
		public static function custom_woocommerce_product_add_to_cart_text() {
			global $product;

			$product_type = $product->product_type;

			switch ( $product_type ) {
				case 'external':
					return esc_html__( 'Buy product', 'stevecadey' );
				break;
				case 'grouped':
					return esc_html__( 'View items', 'stevecadey' );
				break;
				case 'simple':
					return esc_html__( 'Add to cart', 'stevecadey' );
				break;
				case 'variable':
					return esc_html__( 'Selection', 'stevecadey' );
				break;
				default:
					return esc_html__( 'Read more', 'stevecadey' );
			}
		}

		/**
		 * Register widget.
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_widgets_init() {
			register_sidebar( array(
				'name'          => esc_html__( 'Shop Sidebar', 'stevecadey' ),
				'id'            => 'shop_sidebar',
				'description'   => esc_html__( 'This sidebar is used for WooCommerce Plugin, on shop pages.', 'stevecadey' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h2 class="widget-title"><span>',
				'after_title'   => '</span></h2>',
			) );
		}
		/**
		 * Enqueue style.
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_enqueue_style() {
			// Load woocommerce style.
			wp_enqueue_style( 'wcm-style', STEVECADEY_THEME_URL . 'assets/css/woocommerce.css' );
			// Enqueue jquery sequence
			wp_register_script( 'sequence-jquery-min', STEVECADEY_THEME_URL . 'assets/js/vendor/sequence.jquery-min.js', array(), '', true );
		}

		public static function k2t_woocommercer_enqueue_script() {

		}

		/**
		 * Change html structure to before shop item
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_before_shop_loop_item() {
			global $smof_data;
			echo '<article class="product-item"><div class="product-thumb">';
		}

		/**
		 * Change html structure to after shop item
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_after_shop_loop_item() {
			global $product;
			echo '</div></article>';
		}

		/**
		 * Change html structure to before item action
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_before_shop_loop_item_title() {
			echo '</div><div class="product-name">';
		}

		/**
		 * Change html structure to after item action
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_after_shop_loop_item_title() {
			echo '</div><div class="product-meta">';
		}


		/**
		 * Change go to review tab
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_go_to_review_tab() {
			 echo '<a class="move-to-review" href="#tab-reviews">' . esc_html__( 'Add your review','stevecadey' ) .  '</a>';
		}


		/**
		 * Change html structure to price and rating on single product
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_before_price() {
			echo '<div class="p-rate-price">';
		}
		public static function k2t_woocommerce_after_rate() {
			echo '</div>';
		}

		/**
		 * Show or hide related product
		 *
		 * @since 1.0
		 */
		public static function k2t_woocommerce_show_or_hide_related_product() {
			global $smof_data;
			if ( $smof_data['product_single_display_related_products'] ) :
				woocommerce_output_related_products();
			endif;
		}

		/**
		 * Wishlist Button
		 *
		 * @return  array
		 * @since 	1.0
		 */
		public static function k2t_wishlist_button() {

			global $product, $yith_wcwl;

			if ( class_exists( 'YITH_WCWL_UI' ) )  {
				$url          = $yith_wcwl->get_wishlist_url();
				$product_type = $product->product_type;
				$exists       = $yith_wcwl->is_product_in_wishlist( $product->id );
				$classes      = 'class="add_to_wishlist"';

				$html  = '<div class="yith-wcwl-add-to-wishlist">';
				    $html .= '<div class="yith-wcwl-add-button';  // the class attribute is closed in the next row
				    $html .= $exists ? ' hide" style="display:none;"' : ' show"';
				    $html .= '><a href="' . htmlspecialchars( $yith_wcwl->get_addtowishlist_url() ) . '" data-product-id="' . $product->id . '" data-product-type="' . $product_type . '" ' . $classes . ' ><i class="fa fa-heart"></i></a>';
				    $html .= '</div>';

				$html .= '<div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;"><a href="' . $url . '"><i class="fa fa-heart"></i></a></div>';
				$html .= '<div class="yith-wcwl-wishlistexistsbrowse ' . ( $exists ? 'show' : 'hide' ) . '" style="display:' . ( $exists ? 'block' : 'none' ) . '"><a href="' . $url . '"><i class="fa fa-heart"></i></a></div>';
				$html .= '<div style="clear:both"></div><div class="yith-wcwl-wishlistaddresponse"></div>';
				$html .= '</div>';

			return $html;
			}
		}

		/**
		 * Add shopcart menu to header
		 *
		 * @return  array
		 */
		public static function k2t_add_to_cart_fragment( $fragments ) {
			global $woocommerce;
			ob_start();
		?>
			<a class="cart-control" href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'stevecadey' ); ?>">
				<i class="zmdi zmdi-shopping-cart-plus"></i>
				<span><?php echo esc_html( $woocommerce->cart->get_cart_contents_count() ); ?></span>
			</a>
		<?php
			$fragments['a.cart-control'] = ob_get_clean();
			return $fragments;
		}
		public static function k2t_shoping_cart() {
			global $woocommerce;

			$cart_total = apply_filters( 'add_to_cart_fragments' , array() );

			echo '<div class="shop-cart">';
			echo ( $cart_total['a.cart-control'] );
			echo '<div class="shop-item">';
			echo '<div class="widget_shopping_cart_content"></div>';
			echo '</div>';
			echo '</div>';
		}

		/**
		 * Set WooCommerce image dimensions upon theme activation
		 * @since 1.0
		 */
		public static function k2t_woocommerce_image_dimensions() {

			// check resize one time 4 shop

			add_option( 'wc_img_size_4_shop', true, '', 'no' );

			if ( get_option('wc_img_size_4_shop') ) :
				$catalog = array(
					'width'  => '570', // px
					'height' => '615', // px
					'crop'	 => 1
				);

				$single = array(
					'width'  => '570', // px
					'height' => '615', // px
					'crop'	 => 1
				);
				$thumbnail = array(
					'width' 	=> '78', // px
					'height'	=> '104', // px
					'crop'		=> 1
				);


				// Image sizes

				update_option( 'shop_catalog_image_size', $catalog ); // Product category thumbs
				update_option( 'shop_single_image_size', $single ); // Single product image
				update_option( 'shop_thumbnail_image_size', $thumbnail ); // Image gallery thumbs
				update_option( 'wc_img_size_4_shop', false);

			endif;
		}
		// woocommerce_after_shop_loop_item
		public static function k2t_woocommerce_after_shop_loop_item_show_title() {
			echo '<h3><a href="' . esc_url( get_the_permalink() ) . '">' . esc_html( get_the_title() ) . '</a></h3>';
		}

		public static function k2t_woocommerce_after_shop_loop_item_wrap_price() {

			echo '<div class="wrap-price">'; //wrap price

		}
		public static function k2t_woocommerce_after_shop_loop_item_end_wrap_price() {
			echo '</div>';
		}
		public static function k2t_woocommerce_after_shop_loop_item_wrap_button() {
			echo '<div class="b-action">';
		}
		public static function k2t_woocommerce_after_shop_loop_item_end_wrap_button() {
			echo '</div>';
		}

	}

}
new k2t_template_woo();