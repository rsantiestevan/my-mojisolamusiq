<?php
/**
 * Main functions for theme.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

if ( ! function_exists( 'stevecadey_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @since stevecadey 1.0
	 */
	function stevecadey_setup() {
		global $content_width;
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on grid, use a find and replace
		 * to change 'stevecadey' to the name of your theme in all the template files
		 */

		load_theme_textdomain( 'stevecadey', get_template_directory() . '/languages' );

		/**
		 * Add default posts and comments RSS feed links to head.
		 */
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Add custom header default
		 */
		add_theme_support( 'custom-header' );

		/**
		 * Add custom background default
		 */
		add_theme_support( "custom-background" );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array( 'video', 'audio', 'gallery', 'link', 'quote', 'image' ) );

		/**
		 * Add editor style default
		 */
		add_editor_style( 'css/style.css' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		// Add images size
		add_image_size( 'stevecadey_recent_post', 122, 100, true );	// widget recent post
		add_image_size( 'stevecadey_blog_masonry_2', 570, 999, false );	// masonry blog 2
		add_image_size( 'stevecadey_blog_masonry_3', 370, 999, false );	// masonry blog 3
		add_image_size( 'stevecadey_blog_masonry_4', 270, 999, false );	// masonry blog 3
		add_image_size( 'stevecadey_blog_grid_4', 270, 999, false );	// masonry blog 4
		add_image_size( 'stevecadey_blog_grid_2', 570, 321, true );	// grid blog 2
		add_image_size( 'stevecadey_blog_grid_3', 370, 208, true );	// grid blog 3
		add_image_size( 'stevecadey_blog_grid_4', 270, 152, true );	// grid blog 4
		add_image_size( 'stevecadey_blog_related', 370, 192, true );	// single blog related
		add_image_size( 'stevecadey_blog_single', 1200, 600, true );	// single blog
		add_image_size( 'stevecadey_blog_large', 1200, 240, true );	// blog large shortcode

		/**
		 * Add support title-tag
		 */
		add_theme_support( 'title-tag' );

		/**
		 * This theme uses wp_nav_menu() in one location.
		 *
		 * @link http://codex.wordpress.org/Post_Formats
		 */
		register_nav_menus(
			array(
				'mobile'  => esc_html__( 'Mobile Menu', 'stevecadey' ),
				'primary' => esc_html__( 'Main Menu', 'stevecadey' ),
			)
		);

		/**
		 * Set the content width based on the theme's design and stylesheet.
		 */
		if ( ! isset( $content_width ) ) {
			$content_width = isset( $smof_data['content-width'] ) ? $smof_data['content-width'] : 1200;
		}
	}
	add_action( 'after_setup_theme', 'stevecadey_setup' );
}

/* =========== Retina Logo ================ */
if ( !function_exists( 'stevecadey_replace_retina_logo' ) ) {
    function stevecadey_replace_retina_logo() {
        global $smof_data;
        $header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
        $logo = isset( $smof_data[$header_style . 'logo'] ) ? $smof_data[$header_style . 'logo'] : get_template_directory_uri() . '/assets/img/logo.png';
        $logo_s = ( isset( $logo ) && file_exists( $logo ) ) ? getimagesize( $logo ) : array();
        echo "<sc"."ript>";?>
        jQuery(document).ready(function(){
            var retina = window.devicePixelRatio > 1 ? true : false;
            <?php if ( isset( $smof_data[$header_style . 'retina_logo'] ) && $smof_data[$header_style . 'retina_logo'] ): ?>
            if(retina) {
                jQuery('.k2t-logo img').attr('src', '<?php echo $smof_data[$header_style . 'retina_logo']; ?>');
                <?php if ( isset( $logo_s[0] ) && !empty( $logo_s[0] ) ):?>
                jQuery('.k2t-logo img').attr('width', '<?php echo $logo_s[0]; ?>');
                <?php endif;?>
                <?php if ( isset( $logo_s[1] ) && !empty( $logo_s[1] ) ):?>
                jQuery('.k2t-logo img').attr('height', '<?php echo $logo_s[1]; ?>');
                <?php endif;?>
            }
            <?php endif; ?>
        });
        <?php echo "</sc"."ript>";
    }
    add_action( 'wp_head', 'stevecadey_replace_retina_logo' );
}

function stevecadey_welcome_admin_menu() {
	global $submenu, $pagenow;

	if ( current_user_can( 'edit_theme_options' ) ) {
		$menu = 'add_menu_' . 'page';
		// Add page menu
		$menu(
			esc_html__( 'Stevecadey Welcome', 'stevecadey' ),
			esc_html__( 'Stevecadey Welcome', 'stevecadey' ),
			'manage_options',
			'stevecadey-welcome',
			array( 'stevecadey_welcome', 'html' ),
			get_template_directory_uri() . '/framework/assets/images/back-end-logo.png',
			2
		);

		// Add submenu
		$sub_menu = 'add_submenu_' . 'page';
		$sub_menu(
			'Stevecadey WP',
			esc_html__( ' Dashboard', 'stevecadey' ),
			esc_html__( 'Dashboard', 'stevecadey' ),
			'manage_options',
			'stevecadey-welcome',
			array( 'stevecadey_welcome', 'html' )
		);

	}

	// redirect to welcome page
	if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) && $_GET['activated'] == 'true' ) {
		wp_redirect( admin_url ( 'admin.php?page=stevecadey-welcome' ) );
	}

	stevecadey_welcome::initialize();
}

add_action( 'admin_menu', 'stevecadey_welcome_admin_menu' );

/**
 * Minifield ouput
 *
 * @return  void
 */
if ( ! function_exists('stevecadey_mini_output') ) {
	function stevecadey_mini_output( $input ){
		 if(trim($input) === "") return $input;
    // Remove extra white-space(s) between HTML attribute(s)
    $input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
        return '<' . $matches[1] . preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]) . $matches[3] . '>';
    }, str_replace("\r", "", $input));
    // Minify inline CSS declaration(s)
    if(strpos($input, ' style=') !== false) {
        $input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
            return '<' . $matches[1] . ' style=' . $matches[2] . minify_css($matches[3]) . $matches[2];
        }, $input);
    }
    return preg_replace(
        array(
            // t = text
            // o = tag open
            // c = tag close
            // Keep important white-space(s) after self-closing HTML tag(s)
            '#<(img|input)(>| .*?>)#s',
            // Remove a line break and two or more white-space(s) between tag(s)
            '#(<!--.*?-->)|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
            '#(<!--.*?-->)|(?<!\>)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s', // t+c || o+t
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s', // o+o || c+c
            '#(<!--.*?-->)|(<\/.*?>)\s+(\s)(?!\<)|(?<!\>)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s', // c+t || t+o || o+t -- separated by long white-space(s)
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<\/.*?>)#s', // empty tag
            '#<(img|input)(>| .*?>)<\/\1>#s', // reset previous fix
            '#(&nbsp;)&nbsp;(?![<\s])#', // clean up ...
            '#(?<=\>)(&nbsp;)(?=\<)#', // --ibid
            // Remove HTML comment(s) except IE comment(s)
            '#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s'
        ),
        array(
            '<$1$2</$1>',
            '$1$2$3',
            '$1$2$3',
            '$1$2$3$4$5',
            '$1$2$3$4$5$6$7',
            '$1$2$3',
            '<$1$2',
            '$1 ',
            '$1',
            ""
        ),
    $input);
	}
}

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	function stevecadey_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}

		global $page, $paged;

		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}

		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( esc_html__( 'Page %s', 'stevecadey' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'stevecadey_wp_title', 10, 2 );

	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function stevecadey_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'stevecadey_render_title' );
endif;

/**
 * Register required plugins.
 *
 * @return  void
 */
if ( ! function_exists( 'stevecadey_register_theme_dependency' ) ) {
	function stevecadey_register_theme_dependency() {
		$theme = wp_get_theme();
		$version = $theme->version;
		$plugins = array(
			array(
				'name'     => esc_html__('Visual composer','stevecadey'),
				'slug'     => 'js_composer',
				'source'   => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/js_composer.zip',
				'required' => true,
			),
			array(
				'name'     => esc_html__('Advanced Custom Fields Pro','stevecadey'),
				'slug'     => 'advanced-custom-fields-pro',
				'source'   => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/advanced-custom-fields-pro.zip',
				'required' => true,
			),
			array(
				'name'     => esc_html__('Revolution Slider','stevecadey'),
				'slug'     => 'revslider',
				'source'   => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/revslider.zip',
				'required' => false,
			),
			array(
				'name'               => esc_html__( 'Envato Market', 'stevecadey' ), 
				'slug'               => 'envato-market',
				'source'             => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/envato-market.zip',
				'required'           => false,
			),
			array(
				'name'               => esc_html__('K Shortcodes','stevecadey'),
				'slug'               => 'k-shortcodes',
				'source'             => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/k-shortcodes.zip',
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'version'			 => $version,
				'message'			 => 'Important update',
			),

			array(
				'name'               => esc_html__('K Event','stevecadey'),
				'slug'               => 'k-event',
				'source'             => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/k-event.zip',
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'version'			 => $version,
				'message'			 => 'Important update',
			),

			array(
				'name'               => esc_html__('K Gallery','stevecadey'),
				'slug'               => 'k-gallery',
				'source'             => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/k-gallery.zip',
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'version'			 => $version,
				'message'			 => 'Important update',
			),
			array(
				'name'     => esc_html__('WonderPlugin Audio Player','stevecadey'),
				'slug'     => 'wonderplugin-audio',
				'source'   => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/wonderplugin-audio.zip',
				'required' => false,
			),
			
			array(
				'name'               => esc_html__('K Artist','stevecadey'),
				'slug'               => 'k-artist',
				'source'             => STEVECADEY_FRAMEWORK_PATH . 'extensions/plugins/k-artist.zip',
				'required'           => true,
				'force_activation'   => false,
				'force_deactivation' => false,
				'version'			 => $version,
				'message'			 => 'Important update',
			),

			array(
				'name'     => esc_html__( 'Instagram Feed','stevecadey' ),
				'slug'     => 'instagram-feed',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'Accesspress Twitter feed','stevecadey' ),
				'slug'     => 'accesspress-twitter-feed',
				'required' => false,
			),

			array(
				'name'     => esc_html__('YITH WooCommerce Wishlist','stevecadey'),
				'slug'     => 'yith-woocommerce-wishlist',
				'required' => false,
			),
			array(
				'name'     => esc_html__('WooCommerce','stevecadey'),
				'slug'     => 'woocommerce',
				'required' => false,
			),
		);

		tgmpa( $plugins );
	}
	add_action( 'tgmpa_register', 'stevecadey_register_theme_dependency' );
}

/**
 * Print custom code at the end of head section.
 *
 * @package stevecadey
 */
if ( ! function_exists( 'stevecadey_add_head_code' ) ) {
	function stevecadey_add_head_code() {
		global $smof_data;
		if ( isset ( $smof_data['header_code'] ) && $smof_data['header_code'] ) {
			echo ( $smof_data['header_code'] );
		}
	}
	add_action( 'wp_head', 'stevecadey_add_head_code' );
}

/**
 * Print custom code at the end of body section.
 *
 * @package stevecadey
 */
if ( ! function_exists( 'stevecadey_add_footer_code' ) ) {
	function stevecadey_add_footer_code() {
		global $smof_data;
		if ( isset ( $smof_data['footer_code'] ) && $smof_data['footer_code'] ) {
			echo ( $smof_data['footer_code'] );
		}
	}
	add_action( 'wp_footer', 'stevecadey_add_footer_code' );
}

/**
 * Change favicon option
 *
 * @package stevecadey
 */
if ( ! is_admin() && ( ! function_exists( 'wp_site_icon' ) || ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) ) {
	if ( ! function_exists( 'stevecadey_extra_icons' ) ) {
		function stevecadey_extra_icons() {
			global $smof_data;
			if ( isset ( $smof_data['favicon'] ) && $smof_data['favicon'] ) {
				echo '<link sizes="16x16" href="'. esc_url( $smof_data['favicon'] ) .'" rel="icon" />';
			}
			if ( isset ( $smof_data['apple_iphone_icon'] ) && $smof_data['apple_iphone_icon'] ) {
				echo '<link rel="icon" sizes="57x57" href="' . esc_url( $smof_data["apple_iphone_icon"] ) . '" />';
			}
			if ( isset ( $smof_data['apple_iphone_retina_icon'] ) && $smof_data['apple_iphone_retina_icon'] ) {
				echo '<link rel="icon" sizes="114x114" href="' . esc_url( $smof_data["apple_iphone_retina_icon"] ) . '" />';
			}
			if ( isset ( $smof_data['apple_ipad_icon'] ) && $smof_data['apple_ipad_icon'] ) {
				echo '<link rel="icon" sizes="72x72" href="' . esc_url( $smof_data["apple_ipad_icon"] ) . '" />';
			}
			if ( isset ( $smof_data['apple_ipad_retina_icon'] ) && $smof_data['apple_ipad_retina_icon'] ) {
				echo '<link rel="icon" sizes="144x144" href="' . esc_url( $smof_data["apple_ipad_retina_icon"] ) . '" />';
			}
		}
		add_action( 'wp_head', 'stevecadey_extra_icons', 1 );
	}
}

/**
 * Add a thumbnail column in edit.php
 * Source: http://wordpress.org/support/topic/adding-custum-post-type-thumbnail-to-the-edit-screen
 */
if ( ! function_exists( 'stevecadey_columns_filter' ) ) {
	function stevecadey_columns_filter( $columns ) {
		$column_thumbnail = array( 'thumbnail' => esc_html__( 'Thumbnail', 'stevecadey' ) );
		$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
		return $columns;
	}
	add_filter( 'manage_edit-post_columns', 'stevecadey_columns_filter', 10, 1 );
}
if ( ! function_exists( 'stevecadey_add_thumbnail_value_editscreen' ) ) {
	function stevecadey_add_thumbnail_value_editscreen( $column_name, $post_id ) {

		$width  = (int) 50;
		$height = (int) 50;

		if ( 'thumbnail' == $column_name ) {
			// thumbnail of WP 2.9
			$thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
			// image from gallery
			$attachments = get_children( array( 'post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image' ) );
			if ( $thumbnail_id )
				$thumb = wp_get_attachment_image( $thumbnail_id, array( $width, $height ), true );
			elseif ( $attachments ) {
				foreach ( $attachments as $attachment_id => $attachment ) {
					$thumb = wp_get_attachment_image( $attachment_id, array( $width, $height ), true );
				}
			}
			if ( isset( $thumb ) && $thumb ) {
				echo ( $thumb );
			} else {
				echo '<em>' . esc_html__( 'None', 'stevecadey' ) . '</em>';
			}
		}
	}
	add_action( 'manage_posts_custom_column', 'stevecadey_add_thumbnail_value_editscreen', 10, 2 );
}

/**
 * Custom function to use to open and display each comment.
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_comments' ) ) :
	function stevecadey_comments( $comment, $args, $depth ) {
	// Globalize comment object
		$GLOBALS['comment'] = $comment;

		switch ( $comment->comment_type ) :

			case 'pingback'  :
			case 'trackback' :
				?>
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					<p>
						<?php
						esc_html_e( 'Pingback:', 'stevecadey' );
						comment_author_link();
						edit_comment_link( esc_html__( 'Edit', 'stevecadey' ), '<span class="edit-link">', '</span>' );
						?>
					</p>
				<?php
			break;

			default :
				global $post;
				?>
				<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
					<article id="comment-<?php comment_ID(); ?>" class="comment-body">
						<?php
							echo '<div class="avatar">' . get_avatar( $comment, 70 ) . '</div>';
						if ( '0' == $comment->comment_approved ) : ?>
						<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'stevecadey' ); ?></p>
						<?php endif; ?>

						<div class="comment-top">

							<div class="comment-box">
								<div class="comment-meta">
									<div class="comment-info">
										<?php
										echo '<span>' . get_comment_date( 'F d, Y' ) . '</span>';

										printf(
											'<div class="comment-author">%1$s</div>',
											'<span>' . get_comment_author_link() . '</span>',
											( $comment->user_id == $post->post_author ) ? '<span class="author-post">' . esc_html__( 'Post author', 'stevecadey' ) . '</span>' : ''
										);
										?>
									</div>

								</div><!-- .comment-meta -->

								<div class="comment-content">
									<?php comment_text(); ?>
								</div>
							</div>

							<div class="action-link">
								<?php
									edit_comment_link( '<span class="zmdi zmdi-edit"></span>' );
									comment_reply_link(
										array_merge(
											$args,
											array(
												'reply_text' => '<span class="zmdi zmdi-mail-reply"></span>',
												'depth'      => $depth,
												'max_depth'  => $args['max_depth'],
											)
										)
									);
								?>
							</div><!-- .action-link -->

						</div><!-- .comment-top -->

					</article><!-- #comment- -->
				<?php
			break;

		endswitch;
	}
endif;

/**
 * Excerpt or content for blog blog
 *
 * @since 1.0
 */

if ( ! function_exists( 'stevecadey_blog_excerpt' ) ) {
	function stevecadey_blog_excerpt() {
		
		global $smof_data;

		if ( $smof_data['blog_excerpt'] ) :
			echo '<div class="blog-content">';
			if ( 'excerpts' == $smof_data['blog_display'] ) {
				$excerpt = get_the_excerpt();
				if( empty( $excerpt ) ) {
					$excerpt = '<p class="excerpt">' . wp_trim_words( get_the_content(),  $smof_data[ 'excerpt_length' ] ) . '</p>';
				}
				echo $excerpt;
			} else {
				the_content();
			}
			echo '</div>';
		endif;
	}
}

/**
 * Readmore button.
 *
 * @since 1.0
 */

if ( ! function_exists( 'stevecadey_readmore_btn' ) ) {
	function stevecadey_readmore_btn() {
		echo '<div class="wrap-readmore">' .
				'<a class="k2t-readmore k2t-btn" href="' . get_permalink() . '">'
					. esc_html__( 'Read more', 'stevecadey' ) .
				'</a>' .
			 '</div>';
	}
}

/**
 * Get place holder.
 *
 * @since 1.0
 */

if ( ! function_exists( 'stevecadey_place_holder' ) ) {
	function stevecadey_place_holder( $img_size, $enable = false ) {
		global $smof_data, $_wp_additional_image_sizes;	

		$w = $_wp_additional_image_sizes[$img_size]['width'];
		$h = $_wp_additional_image_sizes[$img_size]['height'];

		if ( $enable ) {
			echo '<a class="placeholder" href="'. esc_url( get_permalink() ) .'">' . 
					'<span class="mask-item"></span>' . 
					'<img src="' . get_template_directory_uri() . '/assets/img/placeholder/place-holder-' . $w . 'x' . $h .'.jpg" alt="' . get_the_title() . '" />' .
				 '</a>';
		}
	}
}

/**
 * Add social network.
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_social_array' ) ) {
	function stevecadey_social_array() {
		return array(
			'facebook'		=>	esc_html__( ' Facebook', 'stevecadey' ),
			'twitter'		=>	esc_html__( ' Twitter', 'stevecadey' ),
			'google-plus'	=>	esc_html__( ' Google+', 'stevecadey' ),
			'linkedin'	 	=>	esc_html__( ' LinkedIn', 'stevecadey' ),
			'tumblr'	 	=>	esc_html__( ' Tumblr', 'stevecadey' ),
			'pinterest'	 	=>	esc_html__( ' Pinterest', 'stevecadey' ),
			'youtube'	 	=>	esc_html__( ' YouTube', 'stevecadey' ),
			'skype'	 		=>	esc_html__( ' Skype', 'stevecadey' ),
			'instagram'	 	=>	esc_html__( ' Instagram', 'stevecadey' ),
			'delicious'	 	=>	esc_html__( ' Delicious', 'stevecadey' ),
			'reddit'		=>	esc_html__( ' Reddit', 'stevecadey' ),
			'stumbleupon'	=>	esc_html__( ' StumbleUpon', 'stevecadey' ),
			'wordpress'	 	=>	esc_html__( ' WordPress', 'stevecadey' ),
			'joomla'		=>	esc_html__( ' Joomla', 'stevecadey' ),
			'blogger'	 	=>	esc_html__( ' Blogger', 'stevecadey' ),
			'vimeo'	 		=>	esc_html__( ' Vimeo', 'stevecadey' ),
			'yahoo'	 		=>	esc_html__( ' Yahoo!', 'stevecadey' ),
			'flickr'	 	=>	esc_html__( ' Flickr', 'stevecadey' ),
			'picasa'	 	=>	esc_html__( ' Picasa', 'stevecadey' ),
			'deviantart'	=>	esc_html__( ' DeviantArt', 'stevecadey' ),
			'github'	 	=>	esc_html__( ' GitHub', 'stevecadey' ),
			'stackoverflow'	=>	esc_html__( ' StackOverFlow', 'stevecadey' ),
			'xing'	 		=>	esc_html__( ' Xing', 'stevecadey' ),
			'flattr'	 	=>	esc_html__( ' Flattr', 'stevecadey' ),
			'foursquare'	=>	esc_html__( ' Foursquare', 'stevecadey' ),
			'paypal'	 	=>	esc_html__( ' Paypal', 'stevecadey' ),
			'yelp'	 		=>	esc_html__( ' Yelp', 'stevecadey' ),
			'soundcloud'	=>	esc_html__( ' SoundCloud', 'stevecadey' ),
			'lastfm'	 	=>	esc_html__( ' Last.fm', 'stevecadey' ),
			'lanyrd'	 	=>	esc_html__( ' Lanyrd', 'stevecadey' ),
			'dribbble'	 	=>	esc_html__( ' Dribbble', 'stevecadey' ),
			'forrst'	 	=>	esc_html__( ' Forrst', 'stevecadey' ),
			'steam'	 		=>	esc_html__( ' Steam', 'stevecadey' ),
			'behance'		=>	esc_html__( ' Behance', 'stevecadey' ),
			'mixi'			=>	esc_html__( ' Mixi', 'stevecadey' ),
			'weibo'			=>	esc_html__( ' Weibo', 'stevecadey' ),
			'renren'		=>	esc_html__( ' Renren', 'stevecadey' ),
			'evernote'		=>	esc_html__( ' Evernote', 'stevecadey' ),
			'dropbox'		=>	esc_html__( ' Dropbox', 'stevecadey' ),
			'bitbucket'		=>	esc_html__( ' Bitbucket', 'stevecadey' ),
			'trello'		=>	esc_html__( ' Trello', 'stevecadey' ),
			'vk'			=>	esc_html__( ' VKontakte', 'stevecadey' ),
			'home'			=>	esc_html__( ' Homepage', 'stevecadey' ),
			'envelope-alt'	=>	esc_html__( ' Email', 'stevecadey' ),
			'rss'			=>	esc_html__( ' RSS', 'stevecadey' ),
		);
	}
}

/**
 * Get acf field array
 *
 * @since 1.0
 */

if ( ! function_exists( 'stevecadey_get_acf_field' ) ) {
	function stevecadey_get_acf_field( $setting = array(), $id, $get_smof = true, $pre = '' ) {
		global $smof_data;
		if ( function_exists( 'get_field' ) ) :
			foreach ($setting as $key => $value) :

				$setting[ $key ]  = get_field( $pre . $key, $id );	// get acf

				// Get smof_data if is default or ! isset ; flag = false -> only get acf

				if ( $get_smof && ( !isset( $setting[ $pre . $key ] ) || $setting[ $pre . $key ] == 'default' ) ) :
					if ( isset( $smof_data[$key]) ) $setting[ $key ] = $smof_data[ $pre . $key ];
				endif;

			endforeach;
		endif;

		return $setting;
	}
}

/**
 * Get smof data.
 *
 * @since 1.0
 */

if ( ! function_exists( 'stevecadey_get_smof_field' ) ) {
	function stevecadey_get_smof_field( $setting = array(), $pre = '' ) {
		global $smof_data;

		foreach ($setting as $key => $value) :

			// get smof_data

			$setting[ $key ]  = isset( $smof_data[ $pre . $key ] ) ? $smof_data[ $pre . $key ] : '';

		endforeach;

		return $setting;
	}
}


if ( ! function_exists( 'stevecadey_social_list' ) ) {
	function stevecadey_social_list() {
		global $smof_data;
		echo '<ul class="social">';
		foreach ( stevecadey_social_array() as $s => $v ) {
			if ( $smof_data[ 'social_' . $s ] ) :
				echo '<li>';
				echo '<a href="' . $smof_data[ 'social_' . $s ] . '" target="_blank">';
				echo 	'<i class="fa fa-' .  $s .'"></i>';
				echo '</a>';
				echo '</li>';
			endif;
		}
		echo '</ul>';
	}
}

// Footer Bottom Logo

if( !function_exists( 'stevecadey_footer_logo' ) ):
	function stevecadey_footer_logo(){
		global $smof_data; 
		if( isset( $smof_data['footer_logo'] ) && $smof_data['footer_logo'] ):
		?>
			<div class="k2t-footer-logo">
				<img src="<?php echo esc_url($smof_data['footer_logo']); ?>" alt="footer-logo" />
			</div>
		<?php endif;
	}
endif;

// Footer Bottom Social

if( ! function_exists( 'stevecadey_bottom_social' ) ):
	function stevecadey_bottom_social(){
		global $smof_data;
		if( isset( $smof_data['bottom_footer_social'] ) && $smof_data['bottom_footer_social'] == '1' ): ?>
			<div class="k2t-social-share social">
				<ul>
					<?php if ( isset ( $smof_data['bottom_footer_fb'] ) && $smof_data['bottom_footer_fb'] ):?>
						<li>
							<a class="facebook" title="facebook" href="<?php echo esc_url( $smof_data['bottom_footer_fb'] ) ?>" target="_blank">
								<i class="zmdi zmdi-facebook"></i>
							</a>
						</li>
					<?php endif;?>

					<?php if ( isset ( $smof_data['bottom_footer_tw'] ) && $smof_data['bottom_footer_tw'] ):?>
						<li>
							<a class="twitter" title="twitter" href="<?php echo esc_url( $smof_data['bottom_footer_tw']) ?>" target="_blank">
								<i class="zmdi zmdi-twitter"></i>
							</a>
						</li>
					<?php endif;?>

					<?php if ( isset ( $smof_data['bottom_footer_ins'] ) && $smof_data['bottom_footer_ins'] ):?>
						<li>
							<a class="instagram" title="instagram" href="<?php echo esc_url( $smof_data['bottom_footer_ins'] ) ?>" target="_blank">
								<i class="zmdi zmdi-instagram"></i>
							</a>
						</li>
					<?php endif;?>

					<?php if ( isset ( $smof_data['bottom_footer_googleplus'] ) && $smof_data['bottom_footer_googleplus'] ):?>
						<li>
							<a class="googleplus" title="googleplus" href="<?php echo esc_url($smof_data['bottom_footer_googleplus']); ?>" target="_blank">
								<i class="zmdi zmdi-google"></i>
							</a>
						</li>
					<?php endif;?>

					<?php if ( isset ( $smof_data['bottom_footer_youtube'] ) && $smof_data['bottom_footer_youtube'] ):?>
						<li>
							<a class="youtube" title="youtube" href="<?php echo esc_url( $smof_data['bottom_footer_youtube']) ?>" target="_blank">
								<i class="fa fa-youtube"></i>
							</a>
						</li>
					<?php endif;?>

					<?php if ( isset ( $smof_data['bottom_footer_tumblr'] ) && $smof_data['bottom_footer_tumblr'] ):?>
						<li>
							<a class="tumblr" title="tumblr" href="<?php echo esc_url( $smof_data['bottom_footer_tumblr'] ) ?>" target="_blank">
								<i class="fa fa-tumblr"></i>
							</a>
						</li>
					<?php endif;?>

					<?php if ( isset ( $smof_data['bottom_footer_pinterest'] ) && $smof_data['bottom_footer_pinterest'] ):?>
						<li>
							<a class="pinterest" title="pinterest" href="<?php echo esc_url( $smof_data['bottom_footer_pinterest'] ); ?>" target="_blank">
								<i class="fa fa-pinterest"></i>
							</a>
						</li>
					<?php endif;?>
				</ul><!-- .social -->
			</div><!-- .social-share -->
		<?php endif;
	}
endif;

/**
 * Social share.
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_social_share' ) ) {
	function stevecadey_social_share() {
		global $smof_data, $post;
		$twitter_username = isset ( $smof_data['twitter_username'] ) ? trim( $smof_data['twitter_username'] ) : '';

		// Get post thumbnail
		$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), false, '' ); ?>
		<span class="share-title">
			<?php esc_html_e( 'Share This: ', 'stevecadey' ); ?>
		</span>
		<div class="k2t-social-share social">
			<ul>
				<?php if ( isset ( $smof_data['social_share_facebook'] ) && $smof_data['social_share_facebook'] ):?>
					<li>
						<a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							<i class="zmdi zmdi-facebook"></i>
						</a>
					</li>
				<?php endif;?>

				<?php if ( isset ( $smof_data['social_share_twitter'] ) && $smof_data['social_share_twitter'] ):?>
					<li>
						<a class="twitter" href="https://twitter.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							<i class="zmdi zmdi-twitter"></i>
						</a>
					</li>
				<?php endif;?>

				<?php if ( isset ( $smof_data['social_share_instagram'] ) && $smof_data['social_share_instagram'] ):?>
					<li>
						<a class="instagram" href="https://instagram.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							<i class="zmdi zmdi-instagram"></i>
						</a>
					</li>
				<?php endif;?>

				<?php if ( isset ( $smof_data['social_share_google'] ) && $smof_data['social_share_google'] ):?>
					<li>
						<a class="googleplus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							<i class="zmdi zmdi-google"></i>
						</a>
					</li>
				<?php endif;?>

				<?php if ( isset ( $smof_data['social_share_linkedin'] ) && $smof_data['social_share_linkedin'] ):?>
					<li>
						<a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode( get_permalink() );?>&title=<?php echo urlencode( get_the_title() );?>" title="<?php esc_html_e( 'LinkedIn', 'stevecadey' );?>">
							<i class="zmdi zmdi-linkedin"></i>
						</a>
					</li>
				<?php endif;?>

				<?php if ( isset ( $smof_data['social_share_tumblr'] ) && $smof_data['social_share_tumblr'] ):?>
					<li>
						<a class="tumblr" href="https://www.tumblr.com/share/link?url=<?php echo urlencode( get_permalink() );?>&name=<?php echo urlencode( get_the_title() );?>" title="<?php esc_html_e( 'Tumblr', 'stevecadey' );?>">
							<i class="zmdi zmdi-tumblr"></i>
						</a>
					</li>
				<?php endif;?>

				<?php if ( isset ( $smof_data['social_share_email'] ) && $smof_data['social_share_email'] ):?>
					<li>
						<a class="em" href="mailto:?subject=<?php the_title(); ?>&body=<?php echo strip_tags( apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ); ?> <?php the_permalink(); ?>">
							<i class="zmdi zmdi-email"></i>
						</a>
					</li>
				<?php endif;?>
			</ul><!-- .social -->
		</div><!-- .social-share -->
	<?php
	}
}

/**
 * Get related post
 *
 * @link http://wordpress.org/support/topic/custom-query-related-posts-by-common-tag-amount
 * @link http://pastebin.com/NnDzdSLd
 */
if ( ! function_exists( 'stevecadey_get_related_tag_posts_ids' ) ) {
	function stevecadey_get_related_tag_posts_ids( $post_id, $number = 5, $taxonomy = 'post_tag', $post_type = 'post' ) {

		$related_ids = false;

		$post_ids = array();
		// get tag ids belonging to $post_id
		$tag_ids = wp_get_post_terms( $post_id, $taxonomy, array( 'fields' => 'ids' ) );
		if ( $tag_ids ) {
			// get all posts that have the same tags
			$tag_posts = get_posts(
				array(
					'post_type'   => $post_type,
					'posts_per_page' => -1, // return all posts
					'no_found_rows'  => true, // no need for pagination
					'fields'         => 'ids', // only return ids
					'post__not_in'   => array( $post_id ), // exclude $post_id from results
					'tax_query'      => array(
						array(
							'taxonomy' => $taxonomy,
							'field'    => 'id',
							'terms'    => $tag_ids,
							'operator' => 'IN'
						)
					)
				)
			);

			// loop through posts with the same tags
			if ( $tag_posts ) {
				$score = array();
				$i = 0;
				foreach ( $tag_posts as $tag_post ) {
					// get tags for related post
					$terms = wp_get_post_terms( $tag_post, $taxonomy, array( 'fields' => 'ids' ) );
					$total_score = 0;

					foreach ( $terms as $term ) {
						if ( in_array( $term, $tag_ids ) ) {
							++$total_score;
						}
					}

					if ( $total_score > 0 ) {
						$score[$i]['ID'] = $tag_post;
						// add number $i for sorting
						$score[$i]['score'] = array( $total_score, $i );
					}
					++$i;
				}

				// sort the related posts from high score to low score
				uasort( $score, 'stevecadey_sort_tag_score' );
				// get sorted related post ids
				$related_ids = wp_list_pluck( $score, 'ID' );
				// limit ids
				$related_ids = array_slice( $related_ids, 0, (int) $number );
			}
		}
		return $related_ids;
	}
}
if ( ! function_exists( 'stevecadey_sort_tag_score' ) ) {
	function stevecadey_sort_tag_score( $item1, $item2 ) {
		if ( $item1['score'][0] != $item2['score'][0] ) {
			return $item1['score'][0] < $item2['score'][0] ? 1 : -1;
		} else {
			return $item1['score'][1] < $item2['score'][1] ? -1 : 1; // ASC
		}
	}
}

/**
 * Add field to custom user profile
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_add_custom_user_profile' ) ) {
	function stevecadey_add_custom_user_profile( $user ) {
		?>
		<table class="form-table">
			<tr>
				<th><label for="user-location"><?php esc_html_e( 'User Location', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="user-location" id="user-location" value="<?php echo esc_attr( get_the_author_meta( 'user-location', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="facebook-text"><?php esc_html_e( 'Facebook Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="facebook-text" id="facebook-text" value="<?php echo esc_attr( get_the_author_meta( 'facebook-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="facebook"><?php esc_html_e( 'Facebook Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="twitter-text"><?php esc_html_e( 'twitter text ', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="twitter-text" id="twitter-text" value="<?php echo esc_attr( get_the_author_meta( 'twitter-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="twitter"><?php esc_html_e( 'Twitter Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="google-plus-text"><?php esc_html_e( 'Google+ text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="google-plus-text" id="google-plus-text" value="<?php echo esc_attr( get_the_author_meta( 'google-plus-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="google-plus"><?php esc_html_e( 'Google+ Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="google-plus" id="google-plus" value="<?php echo esc_attr( get_the_author_meta( 'google-plus', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="pinterest-text"><?php esc_html_e( 'pinterest-text text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="pinterest-text" id="pinterest-text" value="<?php echo esc_attr( get_the_author_meta( 'pinterest-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="pinterest"><?php esc_html_e( 'Pinterest Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="pinterest" id="pinterest" value="<?php echo esc_attr( get_the_author_meta( 'pinterest', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="k2t-youtube-text"><?php esc_html_e( 'Youtube Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="k2t-youtube-text" id="k2t-youtube-text" value="<?php echo esc_attr( get_the_author_meta( 'k2t-youtube-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="k2t-youtube"><?php esc_html_e( 'Youtube Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="k2t-youtube" id="k2t-youtube" value="<?php echo esc_attr( get_the_author_meta( 'k2t-youtube', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="vimeo-text"><?php esc_html_e( 'Vimeo Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="vimeo-text" id="vimeo-text" value="<?php echo esc_attr( get_the_author_meta( 'vimeo-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="vimeo"><?php esc_html_e( 'Vimeo Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="vimeo" id="vimeo" value="<?php echo esc_attr( get_the_author_meta( 'vimeo', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="linkedin-text"><?php esc_html_e( 'Linkedin Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="linkedin-text" id="linkedin-text" value="<?php echo esc_attr( get_the_author_meta( 'linkedin-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="linkedin"><?php esc_html_e( 'Linkedin Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="tumblr-text"><?php esc_html_e( 'Tumblr Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="tumblr-text" id="tumblr-text" value="<?php echo esc_attr( get_the_author_meta( 'tumblr-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="tumblr"><?php esc_html_e( 'Tumblr Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="tumblr" id="tumblr" value="<?php echo esc_attr( get_the_author_meta( 'tumblr', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="custom_email-text"><?php esc_html_e( 'Email Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="custom_email-text" id="custom_email-text" value="<?php echo esc_attr( get_the_author_meta( 'custom_email-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="custom_email"><?php esc_html_e( 'Email Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="custom_email" id="custom_email" value="<?php echo esc_attr( get_the_author_meta( 'custom_email', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="flickr-text"><?php esc_html_e( 'flickr text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="flickr-text" id="flickr-text" value="<?php echo esc_attr( get_the_author_meta( 'flickr-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="flickr"><?php esc_html_e( 'Flickr Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="flickr" id="flickr" value="<?php echo esc_attr( get_the_author_meta( 'flickr', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="behance-text"><?php esc_html_e( 'Behance Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="behance-text" id="behance-text" value="<?php echo esc_attr( get_the_author_meta( 'behance-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="behance"><?php esc_html_e( 'Behance Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="behance" id="behance" value="<?php echo esc_attr( get_the_author_meta( 'behance', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="dribbble-text"><?php esc_html_e( 'dribbble Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="dribbble-text" id="dribbble-text" value="<?php echo esc_attr( get_the_author_meta( 'dribbble-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="dribbble"><?php esc_html_e( 'Dribbble Link', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="dribbble" id="dribbble" value="<?php echo esc_attr( get_the_author_meta( 'dribbble', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="skype-text"><?php esc_html_e( 'Skype Text', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="skype-text" id="skype-text" value="<?php echo esc_attr( get_the_author_meta( 'skype-text', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
			<tr>
				<th><label for="skype"><?php esc_html_e( 'Skype ID', 'stevecadey' ); ?></label></th>
				<td>
					<input type="text" name="skype" id="skype" value="<?php echo esc_attr( get_the_author_meta( 'skype', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"></span>
				</td>
			</tr>
		</table>
	<?php
	}
}

/**
 * Save custom user profile.
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_save_custom_user_profile' ) ) {
	function stevecadey_save_custom_user_profile( $user_id ) {
		if ( ! current_user_can( 'edit_user', $user_id ) )
			return FALSE;
		update_user_meta( $user_id, 'user-location', $_POST['user-location'] );
		update_user_meta( $user_id, 'facebook-text', $_POST['facebook-text'] );
		update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
		update_user_meta( $user_id, 'twitter-text', $_POST['twitter-text'] );
		update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
		update_user_meta( $user_id, 'google-plus-text', $_POST['google-plus-text'] );
		update_user_meta( $user_id, 'google-plus', $_POST['google-plus'] );
		update_user_meta( $user_id, 'pinterest-text', $_POST['pinterest-text'] );
		update_user_meta( $user_id, 'pinterest', $_POST['pinterest'] );
		update_user_meta( $user_id, 'k2t-youtube-text', $_POST['k2t-youtube-text'] );
		update_user_meta( $user_id, 'k2t-youtube', $_POST['k2t-youtube'] );
		update_user_meta( $user_id, 'vimeo-text', $_POST['vimeo-text'] );
		update_user_meta( $user_id, 'vimeo', $_POST['vimeo'] );
		update_user_meta( $user_id, 'linkedin-text', $_POST['linkedin-text'] );
		update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
		update_user_meta( $user_id, 'tumblr-text', $_POST['tumblr-text'] );
		update_user_meta( $user_id, 'tumblr', $_POST['tumblr'] );
		update_user_meta( $user_id, 'custom_email-text', $_POST['custom_email-text'] );
		update_user_meta( $user_id, 'custom_email', $_POST['custom_email'] );
		update_user_meta( $user_id, 'flickr-text', $_POST['flickr-text'] );
		update_user_meta( $user_id, 'flickr', $_POST['flickr'] );
		update_user_meta( $user_id, 'behance-text', $_POST['behance-text'] );
		update_user_meta( $user_id, 'behance', $_POST['behance'] );
		update_user_meta( $user_id, 'dribbble-text', $_POST['dribbble-text'] );
		update_user_meta( $user_id, 'dribbble', $_POST['dribbble'] );
		update_user_meta( $user_id, 'skype-text', $_POST['skype-text'] );
		update_user_meta( $user_id, 'skype', $_POST['skype'] );
	}
}
add_action( 'show_user_profile', 'stevecadey_add_custom_user_profile' );
add_action( 'edit_user_profile', 'stevecadey_add_custom_user_profile' );
add_action( 'personal_options_update', 'stevecadey_save_custom_user_profile' );
add_action( 'edit_user_profile_update', 'stevecadey_save_custom_user_profile' );


/**
 * Get user profile social.
 *
 * @since 1.0
 */

if ( ! function_exists( 'stevecadey_get_user_social' ) ) {
	function stevecadey_get_user_social( $id = '' ){

		$user_id = empty( $id ) ? get_the_author_meta( 'ID' ) : $id ;

		$author_meta = get_user_meta( $user_id );

		echo '<div class="k2t-social-share social">';
			echo '<ul>';
			foreach ( stevecadey_social_array() as $s => $v ) {
				if ( !empty( $author_meta[$s][0] ) ) :
					echo '<li>';
					echo '<a href="' . $author_meta[$s][0] . '" target="_blank">';
					echo 	'<i class="fa fa-' .  $s .'"></i>';
					echo '</a>';
					echo '</li>';
				endif;
			}
			echo '</ul>';
		echo "</div>";
	}
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_widgets_init' ) ) {
	function stevecadey_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Primary Sidebar', 'stevecadey' ),
			'id'            => 'primary_sidebar',
			'description'   => esc_html__( 'The primary sidebar of your site, appears on the right or left of post/page content.', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Offcanvas Sidebar', 'stevecadey' ),
			'id'            => 'offcanvas_sidebar',
			'description'   => esc_html__( 'Offcanvas Sidebar', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Secondary Sidebar', 'stevecadey' ),
			'id'            => 'secondary_sidebar',
			'description'   => esc_html__( 'The secondary sidebar of your site, appears on the right or left of post/page content.', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer 1', 'stevecadey' ),
			'id'            => 'footer-1',
			'description'   => esc_html__( 'Footer sidebar number 1, used in the footer area.', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer 2', 'stevecadey' ),
			'id'            => 'footer-2',
			'description'   => esc_html__( 'Footer sidebar number 2, used in the footer area.', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer 3', 'stevecadey' ),
			'id'            => 'footer-3',
			'description'   => esc_html__( 'Footer sidebar number 3, used in the footer area.', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer 4', 'stevecadey' ),
			'id'            => 'footer-4',
			'description'   => esc_html__( 'Footer sidebar number 4, used in the footer area.', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer bottom', 'stevecadey' ),
			'id'            => 'footer-bottom',
			'description'   => esc_html__( 'Footer sidebar bottom, used in the bottom footer area.', 'stevecadey' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
		) );

	}
	add_action( 'widgets_init', 'stevecadey_widgets_init' );
}

/**
 * Change default wordpress menu class.
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_change_menu_class' ) ) {
	function stevecadey_change_menu_class( $classes, $item ) {
		if ( in_array( 'current-menu-item', $classes ) ) {
			$classes[] = 'active';
		}
		if ( in_array( 'menu-item-has-children', $classes ) ) {
			$classes[] = 'children';
		}
		return $classes;
	}
	add_filter( 'nav_menu_css_class' , 'stevecadey_change_menu_class' , 10 , 2);
}

/**
 * Add span tag to categories post count.
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_cat_postcount' ) ) {
	function stevecadey_cat_postcount( $html ) {
		$html = str_replace('</a> (', '</a> <span class="count">(', $html );
		$html = str_replace(')', ')</span>', $html );

		return $html;
	}
	add_filter( 'wp_list_categories', 'stevecadey_cat_postcount' );
}
if ( ! function_exists( 'stevecadey_blog_postcount' ) ) {
	function stevecadey_blog_postcount( $html ) {
		$html = str_replace( '</a>&nbsp;(', '</a><span class="count">(', $html );
		$html = str_replace( ')', ')</span>', $html );

		return $html;
	}
	add_filter( 'get_blogs_link', 'stevecadey_blog_postcount' );
}

// Common Parallax titlebar
if( ! function_exists( 'stevecadey_parallax_titlebar' ) ):
	function stevecadey_parallax_titlebar(){
		echo '
		<scr' . 'ipt>
			(function($) {
				"use strict";

				$(document).ready(function() {
					$.stellar({
						horizontalScrolling: false,
						verticalOffset: 200,
					});
				});

			})(jQuery);
		</scr' . 'ipt>';
	}
	add_action( 'wp_footer', 'stevecadey_parallax_titlebar' );
endif;

/**
 * Custom breadcrumbs.
 *
 * @since 1.0
 */
if ( ! function_exists( 'stevecadey_breadcrumbs' ) ) {
	function stevecadey_breadcrumbs(){
		$text['home']     = esc_html__( 'Home', 'stevecadey' ); // text for the 'Home' link
		$text['blog']     = esc_html__( 'Blog', 'stevecadey' ); // text for the 'Blog' link
		$text['category'] = esc_html__( 'blog by Category "%s"', 'stevecadey' ); // text for a category page
		$text['tax'] 	  = esc_html__( '%s', 'stevecadey' ); // text for a taxonomy page
		$text['search']   = esc_html__( 'Search Results for "%s"', 'stevecadey' ); // text for a search results page
		$text['tag']      = esc_html__( 'Posts Tagged "%s"', 'stevecadey' ); // text for a tag page
		$text['author']   = esc_html__( 'Articles Posted by %s', 'stevecadey' ); // text for an author page
		$text['404']      = esc_html__( 'Error 404', 'stevecadey' ); // text for the 404 page
		$text['shop']     = esc_html__( 'stevecadey Store', 'stevecadey' ); // text for the 404 page

		$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
		$delimiter   = ''; // delimiter between crumbs
		$before      = '<li class="current">'; // tag before the current crumb
		$after       = '</li>'; // tag after the current crumb

		global $post;
		$homeLink   = home_url();
		$linkBefore = '<li typeof="v:Breadcrumb">';
		$linkAfter  = '</li>';
		$linkAttr   = ' rel="v:url" property="v:title"';
		$link       = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

		if ( is_front_page() ) {
			echo '<ul class="k2t-breadcrumbs"><a href="' . esc_url( $homeLink ) . '">' . esc_html( $text['home'] ) . '</a></ul>';
		} elseif ( is_home() ) {
			echo '<ul class="k2t-breadcrumbs"><a href="' . esc_url( $homeLink ) . '">' . esc_html( $text['blog'] ) . '</a></ul>';
		} else {

			echo '<ul class="k2t-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf( $link, esc_url( $homeLink ), esc_html( $text['home'] ) ) . $delimiter;

			if ( is_category() ) {
				$thisCat = get_category( get_query_var( 'cat' ), false );
				if ( $thisCat->parent != 0 ) {
					$cats = get_category_parents( $thisCat->parent, TRUE, $delimiter );
					$cats = str_replace( '<a', $linkBefore . '<a' . $linkAttr, $cats );
					$cats = str_replace( '</a>', '</a>' . $linkAfter, $cats );
					echo ( $cats );
				}
				echo ( $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after );

			} elseif ( is_tax() ) {
				$thisCat = get_category( get_query_var( 'cat' ), false );
				if ( $thisCat ) {
					if ( ! empty( $thisCat->parent ) ) {
						$cats = get_category_parents( $thisCat->parent, TRUE, $delimiter );
						$cats = str_replace( '<a', $linkBefore . '<a' . $linkAttr, $cats );
						$cats = str_replace( '</a>', '</a>' . $linkAfter, $cats);
						echo ( $cats );
					}
					echo ( $before . sprintf( $text['tax'], single_cat_title( '', false ) ) . $after );
				}
			}elseif ( is_search() ) {
				echo ( $before . sprintf( $text['search'], get_search_query() ) . $after );
			} elseif ( is_day() ) {
				echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				echo sprintf( $link, get_month_link( get_the_time( 'Y' ),get_the_time( 'm' ) ), get_the_time( 'F' ) ) . $delimiter;
				echo ( $before . get_the_time( 'd' ) . $after );
			} elseif ( is_month() ) {
				echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				echo ( $before . get_the_time( 'F' ) . $after );
			} elseif ( is_year() ) {
				echo ( $before . get_the_time( 'Y' ) . $after );
			} elseif ( function_exists( 'is_product' ) && is_product() ) {
				$id = get_the_ID();
				$product_cat = wp_get_post_terms( $id, 'product_cat' );
				$title = $slug = array();
				if ( $product_cat ) {
					foreach ( $product_cat as $category ) {
						$title[] = "{$category->name}";
						$slug[]  = "{$category->slug}";
					}
					echo '<li class="current"><a href="' . get_term_link( $slug[0], 'product_cat' ) . '">' . esc_html( $title[0] ) . '</a></li>';
				}

			} elseif ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
				echo '<li class="current">' . $text['shop'] . '</li>';
			} elseif ( is_single() && !is_attachment() ) {
				if ( get_post_type() != 'post' ) {
					$post_type = get_post_type_object(get_post_type());
					$slug = $post_type->rewrite;
					printf( $link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name );
					if ( $showCurrent == 1 ) echo ( $delimiter . $before . get_the_title() . $after );
				} else {
					$cat = get_the_category(); $cat = $cat[0];
					$cats = get_category_parents( $cat, TRUE, $delimiter );
					if ( $showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats );
					$cats = str_replace( '<a', $linkBefore . '<a' . $linkAttr, $cats);
					$cats = str_replace( '</a>', '</a>' . $linkAfter, $cats);
					echo ( $cats );
					if ( $showCurrent == 1 ) echo ( $before . get_the_title() . $after );
				}

			} elseif ( ! is_single() && !is_page() && get_post_type() != 'post' && ! is_404() ) {
				$post_type = get_post_type_object(get_post_type() );
				echo ( $before . $post_type->labels->singular_name . $after );

			} elseif ( is_attachment() ) {
				$parent = get_post( $post->post_parent );
				$cat = get_the_category( $parent->ID );
				$cat = $cat[0];
				$cats = get_category_parents( $cat, TRUE, $delimiter );
				$cats = str_replace( '<a', $linkBefore . '<a' . $linkAttr, $cats );
				$cats = str_replace( '</a>', '</a>' . $linkAfter, $cats );
				echo ( $cats );
				printf( $link, get_permalink( $parent ), $parent->post_title );
				if ( $showCurrent == 1 ) echo ( $delimiter . $before . get_the_title() . $after );

			} elseif ( is_page() && !$post->post_parent ) {
				if ( $showCurrent == 1 ) echo ( $before . get_the_title() . $after );

			} elseif ( is_page() && $post->post_parent ) {
				$parent_id  = $post->post_parent;
				$breadcrumbs = array();
				while ( $parent_id) {
					$page = get_page( $parent_id );
					$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
					$parent_id  = $page->post_parent;
				}
				$breadcrumbs = array_reverse( $breadcrumbs );
				for ( $i = 0; $i < count( $breadcrumbs ); $i++ ) {
					echo ( $breadcrumbs[$i] );
					if ( $i != count( $breadcrumbs)-1) echo ( $delimiter );
				}
				if ( $showCurrent == 1 ) echo ( $delimiter . $before . get_the_title() . $after );

			} elseif ( is_tag() ) {
				echo ( $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after );

			} elseif ( is_author() ) {
				global $author;
				$userdata = get_userdata( $author );
				echo ( $before . sprintf( $text['author'], $userdata->display_name ) . $after );

			} elseif ( is_404() ) {
				echo ( $before . $text['404'] . $after );
			} elseif ( is_post_type_blog() ) {
				echo '' . $current_before;
					post_type_blog_title();
				echo '' . $current_after;
			}

			if ( get_query_var( 'paged' ) ) {
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo '(';
				echo esc_html__( 'Page', 'stevecadey' ) . ' ' . get_query_var( 'paged' );
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
			}

			echo '</ul>';

		}
	}
}

/**
 * Control excerpt length & more.
 *
 * @since 1.0
 */
function stevecadey_excerpt_length( $length ) {
	return 20;
}
function stevecadey_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_length', 'stevecadey_excerpt_length', 999 );
add_filter( 'excerpt_more', 'stevecadey_excerpt_more' );

/**
 * WP Editor.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_wp_editor' ) ) {
	function stevecadey_wp_editor( $id_col, $id_element, $section ) {
		global $smof_data;

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get content
		$content = stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['value'] );

		// Get custom class
		$custom_class = stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id = stevecadey_k2tde(  $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );

		$custom_id    = ( $custom_id != '' ) ? ' id="' . esc_attr( $custom_id ) . '"' : '';

		// Output to frontend
		echo '<div class="h-element element-editor ' . esc_attr( $custom_class ) . '" ' . $custom_id . '>';
			echo do_shortcode( wp_kses_post( $content ) );
		echo '</div>';
	}
}

/**
 * Search box.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_search_box' ) ) {
	function stevecadey_search_box( $id_col, $id_element, $section ) {
		global $smof_data;

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get custom class
		$custom_class =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		$custom_id    = ( $custom_id != '' ) ? ' id="' . esc_attr( $custom_id ) . '"' : '';

		// Output to frontend
		echo '
		<div ' . $custom_id . ' class="h-element search-box ' . esc_attr( $custom_class ) . '">
			<span aria-hidden="true" class="icon_search zmdi zmdi-search"></span>
		</div>
		';
	}
}

/**
 * Social network.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_social' ) ) {
	function stevecadey_social( $id_col, $id_element, $section ) {
		global $smof_data;

		$html = $list = $link = '';

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get custom class
		$custom_class =  stevecadey_k2tde(  $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		$custom_id = ( $custom_id != '' ) ? ' id="' . $custom_id . '"' : '';

		// Link target
		$target = isset ( $smof_data['social-target'] ) ? $smof_data['social-target'] : '_blank';

		// Get social list
		stevecadey_social_list();

		if ( $list ) {
			$html .= '<ul ' . $custom_id . ' class="h-element social ' . $custom_class . '">';
			$html .= $list;
			$html .= '</ul>';
		}
		echo ( $html );
	}
}

/**
 * Custom menu.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_custom_menu' ) ) {
	function stevecadey_custom_menu( $id_col, $id_element, $section ) {
		global $smof_data;

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get menu name
		$menu =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['menu_id'] ) ;

		// Get custom class
		$custom_class =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		if ( isset( $menu ) && ! empty( $menu ) && $menu != 'theme-default' ) {
			wp_nav_menu(
				array(
					'menu'        => $menu,
					'container'   => false,
					'menu_id'     => $custom_id,
					'menu_class'  => 'h-element k2t-menu ' . $custom_class,
					'fallback_cb' => '',
					'walker'      => new K2TCoreFrontendWalker()
				)
			);
		} else {
			wp_nav_menu(
				array(
					'theme_location' => 'primary',
					'container'   => false,
					'menu_id'     => $custom_id,
					'menu_class'  => 'h-element k2t-menu ' . $custom_class,
					'fallback_cb' => '',
					'walker'      => new K2TCoreFrontendWalker()
				)
			);
		}
	}
}

/**
 * Woocommerce cart.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_cart' ) ) {
	function stevecadey_cart( $id_col, $id_element, $section ) {
		global $smof_data;

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get custom class
		$custom_class =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		$custom_id    = ( $custom_id != '' ) ? ' id="' . $custom_id . '"' : '';

		// Output to frontend
		echo '<div ' . $custom_id . ' class="h-element ' . $custom_class . '">';
		if ( class_exists( 'k2t_template_woo' ) ) :
			k2t_template_woo::k2t_shoping_cart();
		endif;
		echo '</div>';
	}
}

/**
 * Widgets in header.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_widget' ) ) {
	function stevecadey_widget( $id_col, $id_element, $section ) {
		global $smof_data;

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get sidebar id
		$sidebar =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['widget_id'] );

		// Get custom class
		$custom_class =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		$custom_id    = ( $custom_id != '' ) ? ' id="' . $custom_id . '"' : '';

		// Output to frontend
		echo '<div ' . $custom_id . ' class="h-element ' . $custom_class . '">';
		if ( is_active_sidebar( $sidebar ) ) :
			dynamic_sidebar( $sidebar );
		endif;
		echo '</div>';
	}
}

/**
 * Logo in header.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_logo' ) ) {
	function stevecadey_logo( $id_col, $id_element, $section, $header_style ) {
		global $smof_data;
		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );
		// Get custom class
		$custom_class =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );
		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		$custom_id    = ( $custom_id != '' ) ? ' id="' . esc_attr( $custom_id ) . '"' : '';

		echo '<div ' . $custom_id . ' class="h-element ' . esc_attr( $custom_class ) . '">';
		?>
		<a class="k2t-logo" rel="home" href="<?php echo esc_url( home_url( "/" ) ); ?>">
			<?php
			$logo = isset ( $smof_data[ $header_style . 'logo'] ) ? trim( $smof_data[ $header_style . 'logo'] ) : '';
			if ( $logo == '' || ( isset( $smof_data[ $header_style .  'text_logo'] ) && $smof_data[ $header_style . 'use_text_logo'] ) ) :
				echo '<h1 class="logo-text">';
					if ( ! $smof_data[ $header_style . 'text_logo'] ) {
						echo esc_html( bloginfo( 'name' ) );
					} else {
						echo esc_html( $smof_data[ $header_style . 'text_logo'] );
					}
				echo '</h1>';
			else: ?>
				<img src="<?php echo esc_url( $logo );?>" alt="<?php esc_attr( bloginfo( 'name' ) );?>" />
			<?php endif; ?>
		</a>
		<?php
		echo '</div>';
	}
}

/**
 * Canvas sidebar.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_canvas_sidebar' ) ) {
	function stevecadey_canvas_sidebar_body_class( $classes ) {
		global $smof_data;

		// Get canvas sidebar class
		$classes[] = 'offcanvas-type-default';
		if ( isset( $smof_data['offcanvas-sidebar-position'] ) && $smof_data['offcanvas-sidebar-position'] ) {
			$classes[] = ' offcanvas-' . $smof_data['offcanvas-sidebar-position'];
		}else{
			$classes[] = ' offcanvas-left';
		}
		return $classes;
	}
	add_filter( 'body_class', 'stevecadey_canvas_sidebar_body_class' );

	function stevecadey_canvas_sidebar( $id_col, $id_element, $section ) {
		global $smof_data;

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get custom class
		$custom_class =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		$custom_id    = ( $custom_id != '' ) ? ' id="' . esc_attr( $custom_id ) . '"' : '';

		// Output to frontend
		echo '<div ' . $custom_id . ' class="h-element ' . esc_attr( $custom_class ) . '">';
		echo '<a onclick="javascript:return false;" class="open-sidebar" href="#"><span class="inner"></span></a>';
		echo '</div>';
		return;
	}
}

/**
 * Login.
 *
 * @since  1.0
 * @return void
 */
if ( ! function_exists( 'stevecadey_login' ) ) {
	function stevecadey_login( $id_col, $id_element, $section ) {
		global $smof_data;
		include_once ( ABSPATH . 'wp-admin/includes/plugin.php' );

		// Get all data of top header
		$data = json_decode ( $smof_data[ $section ], true );

		// Get custom class
		$custom_class =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_class'] );

		// Get custom id
		$custom_id =  stevecadey_k2tde( $data['columns'][$id_col]['value'][$id_element]['value']['custom_id'] );
		$custom_id    = ( $custom_id != '' ) ? ' id="' . esc_attr( $custom_id ) . '"' : '';

		// Output to frontend
		echo '<div ' . $custom_id . ' class="h-element custom-login' . esc_attr( $custom_class ) . '">';
		if ( is_user_logged_in() ) :
			$current_user = wp_get_current_user();
        	echo esc_html__('Hello','stevecadey') . ' <a href="' . get_site_url() . '/author/' . $current_user->user_login . '/">' . $current_user->user_login . '! ';
        	echo '<a href="' . wp_logout_url( get_home_url() ) . '"> '. esc_html__( 'Logout', 'stevecadey') . '</a>';
		else:
			echo '<a href="'. esc_url( wp_login_url( get_permalink() ) ) .'" title="'. esc_html__( 'Login', 'stevecadey' ) .'">'. esc_html__( 'Login', 'stevecadey' ) .'</a> <i class="steve-space">  / </i>  <a href="'. esc_url( site_url( '/wp-login.php?action=register&redirect_to=' . get_permalink() ) ) .'" title="'. esc_html__( 'Sign Up', 'stevecadey' ) .'">'. esc_html__( 'Sign Up', 'stevecadey' ) .'</a>';
		endif;
		echo '</div>';
		return;
	}
}

/**
 * Header visual layout generate.
 *
 * @since  1.0
 * @return void
 */
function stevecadey_data( $id, $i, $section, $header_style = '' ) {
	global  $smof_data;

	// Get all data of section
	$data = json_decode ( $smof_data[ $section ], true );

	// Get element type
	$type = $data['columns'][$id]['value'][$i]['type'];
	switch ( $type ) {
		case 'wp_editor' :
			stevecadey_wp_editor( $id, $i, $section );
			break;
		case 'search_box' :
			stevecadey_search_box( $id, $i, $section );
			break;
		case 'social' :
			stevecadey_social( $id, $i, $section );
			break;
		case 'custom_menu' :
			stevecadey_custom_menu( $id, $i, $section );
			break;
		case 'widget' :
			stevecadey_widget( $id, $i, $section );
			break;
		case 'cart' :
			stevecadey_cart( $id, $i, $section );
			break;
		case 'logo' :
			stevecadey_logo( $id, $i, $section, $header_style );
			break;
		case 'canvas_sidebar' :
			stevecadey_canvas_sidebar( $id, $i, $section );
			break;
		case 'login' :
			stevecadey_login( $id, $i, $section );
			break;
	}
}

/**
 * Run audio
 *
 * @since  1.0
 * @return array
 */
if ( ! function_exists( 'stevecadey_audio' ) ) {
	function stevecadey_audio( $audio ) {
		global $wp_embed;
		echo ( $wp_embed->run_shortcode( $audio ) );
	}
}


/**
 * Convert Hex Color to RGB
 *
 * @since  1.0
 * @return array
 */
function stevecadey_hex2rgb( $hex ) {
	$hex = str_replace( "#", "", $hex );

	if ( strlen( $hex ) == 3 ) {
		$r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
		$g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
		$b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
	} else {
		$r = hexdec( substr( $hex, 0, 2 ) );
		$g = hexdec( substr( $hex, 2, 2 ) );
		$b = hexdec( substr( $hex, 4, 2 ) );
	}
	$rgb = array( $r, $g, $b );

	// returns the rgb values separated by commas
	return $rgb; // returns an array with the rgb values
}

/**
 * Add advanced restore theme options.
 *
 * @since  1.0
 * @return void
 */
function stevecadey_add_advance_option() {
	$add_data = array();

	$backup_restore = get_option( 'grid_theme_options_advance_backup_restore' );

	if ( isset( $backup_restore ) && $backup_restore == '' ) {
		/* Add Data Theme Options */
		$backup_id                  = $_POST['advance_id'];
		$add_data[0]['advance_id']  = $backup_id;
		$add_data[0]['backup_id']   = $backup_id . '_' . time();
		$backup_name                = $_POST['backup_name'];
		$add_data[0]['backup_name'] = $backup_name;
		$add_data[0]['time']        = date('D M j G:i Y');
		$backup_data                = $_POST['data'];
		$add_data[0]['data']        = $backup_data;
		add_option( 'grid_theme_options_advance_backup_restore', json_encode( $add_data ) );
	} else {
		$current_backup_data     = array();
		$backup_id               = $_POST['advance_id'];
		$add_data['advance_id']  = $backup_id;
		$add_data['backup_id']   = $backup_id . '_' . time();
		$backup_name             = $_POST['backup_name'];
		$add_data['backup_name'] = $backup_name;
		$add_data['time']        = date('D M j G:i Y');
		$backup_data             = $_POST['data'];
		$add_data['data']        = $backup_data;
		$current_backup_data     = ( array )json_decode( $backup_restore ) ;
		array_push( $current_backup_data, $add_data );
		update_option( 'grid_theme_options_advance_backup_restore', json_encode( $current_backup_data ) );
	}
	die();
}
add_action( 'wp_ajax_k2t_add_advance_option', 'stevecadey_add_advance_option' );
add_action( 'wp_ajax_nopriv_k2t_add_advance_option', 'stevecadey_add_advance_option' );

/* K2T function  */

if ( ! function_exists( 'stevecadey_k2ten' ) ) {
 function stevecadey_k2ten( $string = '' ) {
  		return call_user_func_array("b"."a"."s"."e"."6"."4"."_"."e"."n"."c"."o"."d"."e", array($string));
	}
}
if ( ! function_exists( 'stevecadey_k2tde' ) ) {
	function stevecadey_k2tde( $string = '' ) {
  		return call_user_func_array("b"."a"."s"."e"."6"."4"."_"."d"."e"."c"."o"."d"."e", array($string));
	}
}
if ( ! function_exists( 'stevecadey_k2tfo' ) ) {
	function stevecadey_k2tfo( $fn, $md ) {
  		return call_user_func_array( "f"."o"."p"."e"."n", array($fn, $md) );
	}
}
if ( ! function_exists( 'stevecadey_k2tfc' ) ) {
	function stevecadey_k2tfc( $fp ) {
  		return call_user_func_array( "f"."c"."l"."o"."s"."e", array($fp) );
	}
}
if ( ! function_exists( 'stevecadey_k2trf' ) ) {
	function stevecadey_k2trf( $fn ) {
  		return call_user_func_array( "r"."e"."a"."d"."f"."i"."l"."e", array($fn) );
	}
}
if ( ! function_exists( 'stevecadey_k2tfgc' ) ) {
	function stevecadey_k2tfgc( $fn ) {
  		return call_user_func_array( "f"."i"."l"."e"."_"."g"."e"."t"."_c"."on"."ten"."ts", array( $fn ) );
	}
}
if ( ! function_exists( 'stevecadey_k2thttp' ) ) {
	function stevecadey_k2thttp( $url, $upload ) {
  		return call_user_func_array( "w"."p_"."ge"."t"."_"."h"."t"."t"."p", array( $url, $upload ) );
	}
}
if ( ! function_exists( 'stevecadey_k2tfpc' ) ) {
	function stevecadey_k2tfpc( $fn, $mixed ) {
  		return call_user_func_array( "f"."i"."l"."e"."_"."p"."u"."t"."_c"."on"."ten"."ts", array( $fn, $mixed ) );
	}
}

/**
 * Load advanced restore theme options.
 *
 * @since  1.0
 * @return void
 */
function stevecadey_load_advance_option() {
	$id                  = $_POST['advance_id'];
	$backup_restore      = get_option( 'grid_theme_options_advance_backup_restore' );
	$current_backup_data = ( array )json_decode( $backup_restore );

	$output = '<div onload="">';
	foreach( $current_backup_data as $da ) {
		if( $da->advance_id == $id ) {
			$output .= '
			<li backup-id=' . $da->backup_id . ' for=' . $id . ' data="' . $da->data . '">
				<input for="' . $id . '" for-name="' . $da->backup_name . '" id="input_downloadify' . $da->backup_id . '" type="hidden" value="' . $id . '|' . $da->backup_id . '|' . $da->backup_name . '|' . $da->data . '" />
				<div id="download_backup" class="download_backup download_backup' . $da->backup_id . '">Open Text Field</div>
				<div class="dashicons_item dashicons dashicons-trash"></div>' . $da->backup_name . '
			</li>';

		}
		$output .= '
			<scr' . 'ipt>
				opensave.make({
					width: 		20,
					height: 	20,
					filename: 	"Data.txt",
					buttonDiv: 	"download_backup",
					dataID: 	"input_downloadify' . $da->backup_id . '",
					image_up:   "' . STEVECADEY_FRAMEWORK_URL . 'assets/images/download.png",
					image_down: "' . STEVECADEY_FRAMEWORK_URL . 'assets/images/download-hover.png",
					image_over: "' . STEVECADEY_FRAMEWORK_URL . 'assets/images/download-hover.png",
					label:""
				});
			</scr' . 'ipt>';
	}
	$output .= '</div>';

	echo ( $output );
	die();
}
add_action( 'wp_ajax_k2t_load_advance_option', 'stevecadey_load_advance_option' );
add_action( 'wp_ajax_nopriv_k2t_load_advance_option', 'stevecadey_load_advance_option' );

/**
 * Backup advanced restore theme options.
 *
 * @since  1.0
 * @return void
 */
function stevecadey_backup_advance_option() {
	global $smof_data, $options_machine, $of_options;
	$id      = $_POST['advance_id'];
	$data    = $_POST['data'];
	$restore =  json_decode( stevecadey_k2tde( $data ) );
	foreach( $restore as $rk=>$aid ) {
		foreach ( $smof_data as $k=>$v ) {
			if ( $k == $rk && $k != '0' ) {
				if ( $smof_data[$k] != $aid ) {
					set_theme_mod( $k, $aid );
				} else if ( is_array( $v ) ) {
					foreach ( $aid as $key=>$val ) {
						if ( $key != $k && $v[$key] == $val ) {
							set_theme_mod( $k, $aid );
							break;
						}
					}
				}
			}
		}
	}
	die();
}
add_action( 'wp_ajax_k2t_backup_advance_option', 'stevecadey_backup_advance_option' );
add_action( 'wp_ajax_nopriv_k2t_backup_advance_option', 'stevecadey_backup_advance_option' );

/**
 * Delete backup.
 *
 * @since  1.0
 * @return void
 */
function stevecadey_delete_advance_option() {
	global $smof_data, $options_machine, $of_options;
	$id                  = $_POST['advance_id'];
	$backup_id           = $_POST['backup_id'];
	$data                = $_POST['data'];
	$backup_restore      = get_option( 'grid_theme_options_advance_backup_restore' );
	$current_backup_data = ( array )json_decode( $backup_restore );
	$output              = '';
	$i = 0;
	$template = array();
	foreach( $current_backup_data as $da ) {
		if ( $da->backup_id != $backup_id ) {
			$template[] = $da;
		}
		$i++;
	}
	update_option( 'grid_theme_options_advance_backup_restore', json_encode( $template ) );
	die();
}
add_action( 'wp_ajax_k2t_delete_advance_option', 'stevecadey_delete_advance_option' );
add_action( 'wp_ajax_nopriv_k2t_delete_advance_option', 'stevecadey_delete_advance_option' );

/**
 * Upload backup.
 *
 * @since  1.0
 * @return void
 */
function stevecadey_backup_from_file() {
	global $smof_data, $options_machine, $of_options;

	$data_backup         = $_POST['data_backup'];
	$backup_type         = $_POST['backup_type'];
	$backup_restore      = get_option( 'grid_theme_options_advance_backup_restore' );
	$current_backup_data = ( array ) json_decode( $backup_restore );
	$validate_data       = '0';
	$notice              = '';
	$backup_data         = explode( '|',$data_backup );
	$data_import         = array();
	// Validate Struct
	if ( count( $backup_data ) != 4 ) {
		$validate_data = 0;
		$notice = esc_html__( 'Data Struct False', 'stevecadey' );
	} else {
		// Validate check exitst type
		foreach ( $of_options as $of ) {
			if ( isset( $of['id'] ) && $of['id'] == $backup_data[0] ) {
				$validate_data = '1';
			};
		}
		if ( $validate_data == '0' ) {
			$notice = esc_html__( 'Sorry, This Backup False! Not found name of advance on db', 'stevecadey' );
		} else {
			// Check isset in database
			foreach ( $current_backup_data as $da ) {
				if ( $da->backup_id == $backup_data[1] ) {
					$validate_data = '1';
					$notice        = esc_html__( 'This backup really exists!! It will move to top of list backup, and restore data for you!', 'stevecadey' );
				}
			}
		}

	}
	// Check jsonstring of DATA
	if ( $data_import = json_decode(  stevecadey_k2tde( $backup_data[3], true ) ) ) {
		if ( $backup_type == 'save_to_back_up_list' ) {
			/* Save to backup list */
			$backup_id               = $backup_data[0];
			$add_data['advance_id']  = $backup_id;
			$add_data['backup_id']   = $backup_id . '_' . time();
			$backup_name             = $backup_data[2];
			$add_data['backup_name'] = $backup_name;
			$add_data['time']        = date('D M j G:i Y');
			$backup_data             = $backup_data[3];
			$add_data['data']        = $backup_data;
			array_push( $current_backup_data, $add_data );
			update_option( 'grid_theme_options_advance_backup_restore', json_encode( $current_backup_data ) );
			$notice = esc_html__( 'Added To Backup List', 'stevecadey' );


		} else if ( $backup_type == 'restore' ) {
			/* Restore */
			global $smof_data, $options_machine, $of_options;
			$backup_id = $backup_data[0];
			$id        = $backup_data[1];
			$data      = $backup_data[3];
			$restore   =  json_decode( stevecadey_k2tde( $data ) );
			foreach ( $restore as $rk=>$aid ) {
				foreach ( $smof_data as $k=>$v ) {
					if ( $k == $rk && $k != '0' ) {
						if ( $smof_data[$k] != $aid ) {
							set_theme_mod( $k, $aid );
						} else if ( is_array( $v ) ) {
							foreach ( $aid as $key=>$val ) {
								if ( $key != $k && $v[$key] == $val ) {
									set_theme_mod( $k, $aid );
									break;
								}
							}
						}
					}
				}
			}
			$notice = esc_html__( 'Restored!', 'stevecadey' );

		} else if ( $backup_type == 'restore_and_save_to_backup_list' ) {
			global $smof_data, $options_machine, $of_options;
			/* Restore And Save To Backup List */
			/* Save to backup list */
			$backup_id               = $backup_data[0];
			$add_data['advance_id']  = $backup_id;
			$add_data['backup_id']   = $backup_id . '_' . time();
			$backup_name             = $backup_data[2];
			$add_data['backup_name'] = $backup_name;
			$add_data['time']        = date( 'D M j G:i Y' );
			$backup_data             = $backup_data[3];
			$add_data['data']        = $backup_data;
			array_push( $current_backup_data, $add_data );
			update_option( 'grid_theme_options_advance_backup_restore', json_encode( $current_backup_data ) );
			$notice = esc_html__( 'Added To Backup List!', 'stevecadey' );


			/* Restore */
			$backup_id = $backup_data[0];
			$id        = $backup_data[1];
			$data      = $backup_data[3];
			$restore   =  json_decode( stevecadey_k2tde( $data ) );
			foreach ( $restore as $rk=>$aid ) {
				foreach ( $smof_data as $k=>$v ) {
					if( $k == $rk && $k != '0' ) {
						if ( $smof_data[$k] != $aid ) {
							set_theme_mod( $k, $aid );
						} else if ( is_array( $v ) ) {
							foreach ( $aid as $key=>$val ) {
								if ( $key != $k && $v[$key] == $val ) {
									set_theme_mod( $k, $aid );
									break;
								}
							}
						}
					}
				}
			}
			$notice = esc_html__( 'Restored!', 'stevecadey' );
		}

	} else {
		// not valid
		$notice = esc_html__( 'We can\'t Read Data backup. Have an other change for backup file!', 'stevecadey' );
	}

	print_r( $notice );

	die();
}
add_action( 'wp_ajax_k2t_backup_from_file', 'stevecadey_backup_from_file' );
add_action( 'wp_ajax_nopriv_k2t_backup_from_file', 'stevecadey_backup_from_file' );

/**
 * Save a backup.
 *
 * @since  1.0
 * @return void
 */
function stevecadey_save_advance_option(){
	header('Content-type: text/plain');
	header('Content-disposition: attachment; filename="data.txt"');
}
add_action( 'wp_ajax_k2t_save_advance_option', 'stevecadey_save_advance_option' );
add_action( 'wp_ajax_nopriv_k2t_save_advance_option', 'stevecadey_save_advance_option' );

/**
 * Integration google fonts.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

if ( !function_exists( 'stevecadey_google_fonts' ) ) {
	function stevecadey_google_fonts() {
		$fonts = 'ABeeZee, Abel, Abril Fatface, Aclonica, Acme, Actor, Adamina, Advent Pro, Aguafina Script, Akronim, Aladin, Aldrich, Alef, Alegreya, Alegreya SC, Alegreya Sans, Alegreya Sans SC, Alex Brush, Alfa Slab One, Alice, Alike, Alike Angular, Allan, Allerta, Allerta Stencil, Allura, Almendra, Almendra Display, Almendra SC, Amarante, Amaranth, Amatic SC, Amethysta, Amiri, Amita, Anaheim, Andada, Andika, Angkor, Annie Use Your Telescope, Anonymous Pro, Antic, Antic Didone, Antic Slab, Anton, Arapey, Arbutus, Arbutus Slab, Architects Daughter, Archivo Black, Archivo Narrow, Arimo, Arizonia, Armata, Artifika, Arvo, Arya, Asap, Asset, Astloch, Asul, Atomic Age, Aubrey, Audiowide, Autour One, Average, Average Sans, Averia Gruesa Libre, Averia Libre, Averia Sans Libre, Averia Serif Libre, Bad Script, Balthazar, Bangers, Basic, Battambang, Baumans, Bayon, Belgrano, Belleza, BenchNine, Bentham, Berkshire Swash, Bevan, Bigelow Rules, Bigshot One, Bilbo, Bilbo Swash Caps, Biryani, Bitter, Black Ops One, Bokor, Bonbon, Boogaloo, Bowlby One, Bowlby One SC, Brawler, Bree Serif, Bubblegum Sans, Bubbler One, Buda, Buenard, Butcherman, Butterfly Kids, Cabin, Cabin Condensed, Cabin Sketch, Caesar Dressing, Cagliostro, Calligraffitti, Cambay, Cambo, Candal, Cantarell, Cantata One, Cantora One, Capriola, Cardo, Carme, Carrois Gothic, Carrois Gothic SC, Carter One, Caudex, Cedarville Cursive, Ceviche One, Changa One, Chango, Chau Philomene One, Chela One, Chelsea Market, Chenla, Cherry Cream Soda, Cherry Swash, Chewy, Chicle, Chivo, Cinzel, Cinzel Decorative, Clicker Script, Coda, Coda Caption, Codystar, Combo, Comfortaa, Coming Soon, Concert One, Condiment, Content, Contrail One, Convergence, Cookie, Copse, Corben, Courgette, Cousine, Coustard, Covered By Your Grace, Crafty Girls, Creepster, Crete Round, Crimson Text, Croissant One, Crushed, Cuprum, Cutive, Cutive Mono, Damion, Dancing Script, Dangrek, Dawning of a New Day, Days One, Dekko, Delius, Delius Swash Caps, Delius Unicase, Della Respira, Denk One, Devonshire, Dhurjati, Didact Gothic, Diplomata, Diplomata SC, Domine, Donegal One, Doppio One, Dorsa, Dosis, Dr Sugiyama, Droid Sans, Droid Sans Mono, Droid Serif, Duru Sans, Dynalight, EB Garamond, Eagle Lake, Eater, Economica, Eczar, Ek Mukta, Electrolize, Elsie, Elsie Swash Caps, Emblema One, Emilys Candy, Engagement, Englebert, Enriqueta, Erica One, Esteban, Euphoria Script, Ewert, Exo, Exo 2, Expletus Sans, Fanwood Text, Fascinate, Fascinate Inline, Faster One, Fasthand, Fauna One, Federant, Federo, Felipa, Fenix, Finger Paint, Fira Mono, Fira Sans, Fjalla One, Fjord One, Flamenco, Flavors, Fondamento, Fontdiner Swanky, Forum, Francois One, Freckle Face, Fredericka the Great, Fredoka One, Freehand, Fresca, Frijole, Fruktur, Fugaz One, GFS Didot, GFS Neohellenic, Gabriela, Gafata, Galdeano, Galindo, Gentium Basic, Gentium Book Basic, Geo, Geostar, Geostar Fill, Germania One, Gidugu, Gilda Display, Give You Glory, Glass Antiqua, Glegoo, Gloria Hallelujah, Goblin One, Gochi Hand, Gorditas, Goudy Bookletter 1911, Graduate, Grand Hotel, Gravitas One, Great Vibes, Griffy, Gruppo, Gudea, Gurajada, Habibi, Halant, Hammersmith One, Hanalei, Hanalei Fill, Handlee, Hanuman, Happy Monkey, Headland One, Henny Penny, Herr Von Muellerhoff, Hind, Holtwood One SC, Homemade Apple, Homenaje, IM Fell DW Pica, IM Fell DW Pica SC, IM Fell Double Pica, IM Fell Double Pica SC, IM Fell English, IM Fell English SC, IM Fell French Canon, IM Fell French Canon SC, IM Fell Great Primer, IM Fell Great Primer SC, Iceberg, Iceland, Imprima, Inconsolata, Inder, Indie Flower, Inika, Irish Grover, Istok Web, Italiana, Italianno, Jacques Francois, Jacques Francois Shadow, Jaldi, Jim Nightshade, Jockey One, Jolly Lodger, Josefin Sans, Josefin Slab, Joti One, Judson, Julee, Julius Sans One, Junge, Jura, Just Another Hand, Just Me Again Down Here, Kalam, Kameron, Kantumruy, Karla, Karma, Kaushan Script, Kavoon, Kdam Thmor, Keania One, Kelly Slab, Kenia, Khand, Khmer, Khula, Kite One, Knewave, Kotta One, Koulen, Kranky, Kreon, Kristi, Krona One, Kurale, La Belle Aurore, Laila, Lakki Reddy, Lancelot, Lateef, Lato, League Script, Leckerli One, Ledger, Lekton, Lemon, Libre Baskerville, Life Savers, Lilita One, Lily Script One, Limelight, Linden Hill, Lobster, Lobster Two, Londrina Outline, Londrina Shadow, Londrina Sketch, Londrina Solid, Lora, Love Ya Like A Sister, Loved by the King, Lovers Quarrel, Luckiest Guy, Lusitana, Lustria, Macondo, Macondo Swash Caps, Magra, Maiden Orange, Mako, Mallanna, Mandali, Marcellus, Marcellus SC, Marck Script, Margarine, Marko One, Marmelad, Martel, Martel Sans, Marvel, Mate, Mate SC, Maven Pro, McLaren, Meddon, MedievalSharp, Medula One, Megrim, Meie Script, Merienda, Merienda One, Merriweather, Merriweather Sans, Metal, Metal Mania, Metamorphous, Metrophobic, Michroma, Milonga, Miltonian, Miltonian Tattoo, Miniver, Miss Fajardose, Modak, Modern Antiqua, Molengo, Molle, Monda, Monofett, Monoton, Monsieur La Doulaise, Montaga, Montez, Montserrat, Montserrat Alternates, Montserrat Subrayada, Moul, Moulpali, Mountains of Christmas, Mouse Memoirs, Mr Bedfort, Mr Dafoe, Mr De Haviland, Mrs Saint Delafield, Mrs Sheppards, Muli, Mystery Quest, NTR, Neucha, Neuton, New Rocker, News Cycle, Niconne, Nixie One, Nobile, Nokora, Norican, Nosifer, Nothing You Could Do, Noticia Text, Noto Sans, Noto Serif, Nova Cut, Nova Flat, Nova Mono, Nova Oval, Nova Round, Nova Script, Nova Slim, Nova Square, Numans, Nunito, Odor Mean Chey, Offside, Old Standard TT, Oldenburg, Oleo Script, Oleo Script Swash Caps, Open Sans, Open Sans Condensed, Oranienbaum, Orbitron, Oregano, Orienta, Original Surfer, Oswald, Over the Rainbow, Overlock, Overlock SC, Ovo, Oxygen, Oxygen Mono, PT Mono, PT Sans, PT Sans Caption, PT Sans Narrow, PT Serif, PT Serif Caption, Pacifico, Palanquin, Palanquin Dark, Paprika, Parisienne, Passero One, Passion One, Pathway Gothic One, Patrick Hand, Patrick Hand SC, Patua One, Paytone One, Peddana, Peralta, Permanent Marker, Petit Formal Script, Petrona, Philosopher, Piedra, Pinyon Script, Pirata One, Plaster, Play, Playball, Playfair Display, Playfair Display SC, Podkova, Poiret One, Poller One, Poly, Pompiere, Pontano Sans, Poppins, Port Lligat Sans, Port Lligat Slab, Pragati Narrow, Prata, Preahvihear, Press Start 2P, Princess Sofia, Prociono, Prosto One, Puritan, Purple Purse, Quando, Quantico, Quattrocento, Quattrocento Sans, Questrial, Quicksand, Quintessential, Qwigley, Racing Sans One, Radley, Rajdhani, Raleway, Raleway Dots, Ramabhadra, Ramaraja, Rambla, Rammetto One, Ranchers, Rancho, Ranga, Rationale, Ravi Prakash, Redressed, Reenie Beanie, Revalia, Rhodium Libre, Ribeye, Ribeye Marrow, Righteous, Risque, Roboto, Roboto Condensed, Roboto Mono, Roboto Slab, Rochester, Rock Salt, Rokkitt, Romanesco, Ropa Sans, Rosario, Rosarivo, Rouge Script, Rozha One, Rubik Mono One, Rubik One, Ruda, Rufina, Ruge Boogie, Ruluko, Rum Raisin, Ruslan Display, Russo One, Ruthie, Rye, Sacramento, Sail, Salsa, Sanchez, Sancreek, Sansita One, Sarina, Sarpanch, Satisfy, Scada, Scheherazade, Schoolbell, Seaweed Script, Sevillana, Seymour One, Shadows Into Light, Shadows Into Light Two, Shanti, Share, Share Tech, Share Tech Mono, Shojumaru, Short Stack, Siemreap, Sigmar One, Signika, Signika Negative, Simonetta, Sintony, Sirin Stencil, Six Caps, Skranji, Slabo 13px, Slabo 27px, Slackey, Smokum, Smythe, Sniglet, Snippet, Snowburst One, Sofadi One, Sofia, Sonsie One, Sorts Mill Goudy, Source Code Pro, Source Sans Pro, Source Serif Pro, Special Elite, Spicy Rice, Spinnaker, Spirax, Squada One, Sree Krushnadevaraya, Stalemate, Stalinist One, Stardos Stencil, Stint Ultra Condensed, Stint Ultra Expanded, Stoke, Strait, Sue Ellen Francisco, Sumana, Sunshiney, Supermercado One, Suranna, Suravaram, Suwannaphum, Swanky and Moo Moo, Syncopate, Tangerine, Taprom, Tauri, Teko, Telex, Tenali Ramakrishna, Tenor Sans, Text Me One, The Girl Next Door, Tienne, Tillana, Timmana, Tinos, Titan One, Titillium Web, Trade Winds, Trocchi, Trochut, Trykker, Tulpen One, Ubuntu, Ubuntu Condensed, Ubuntu Mono, Ultra, Uncial Antiqua, Underdog, Unica One, UnifrakturCook, UnifrakturMaguntia, Unkempt, Unlock, Unna, VT323, Vampiro One, Varela, Varela Round, Vast Shadow, Vesper Libre, Vibur, Vidaloka, Viga, Voces, Volkhov, Vollkorn, Voltaire, Waiting for the Sunrise, Wallpoet, Walter Turncoat, Warnes, Wellfleet, Wendy One, Wire One, Yanone Kaffeesatz, Yantramanav, Yellowtail, Yeseva One, Yesteryear, Zeyada';

		$font_array = explode( ',', $fonts );
		foreach ( $font_array as $font ) {
			$font = trim( $font );
			$google_fonts_array[$font] = $font;
		}
		return $google_fonts_array;
	}
}

if ( !function_exists( 'stevecadey_fonts_array' ) ) {
	function stevecadey_fonts_array() {
		return stevecadey_google_fonts();
	}
}

/* FOOTER AND HEADER AREA FUNCTION */

// GET FOOTER BACKGROUND SETTING

if ( ! function_exists( 'stevecadey_footer_bg' ) ) :
	function stevecadey_footer_bg() {

		// Get bottom info
		global $smof_data;

		$bottom_background_image    = $smof_data['bottom_background_image'];
		$bottom_background_color    = $smof_data['bottom_background_color'];
		$bottom_background_repeat   = $smof_data['bottom_background_repeat'];
		$bottom_background_size     = $smof_data['bottom_background_size'];
		$bottom_background_position = $smof_data['bottom_background_position'];

		$footer_class = array();

		if ( ! empty( $bottom_background_image ) ) {
			$footer_class[] = 'background-image:url(' . $bottom_background_image . ');';
		}
		if ( ! empty( $bottom_background_color ) ) {
			$footer_class[] = 'background-color:' . $bottom_background_color . ';';
		}

		if ( ! empty( $bottom_background_repeat ) ) {
			$footer_class[] = 'background-repeat:' . $bottom_background_repeat . ';';
		}
		if ( ! empty( $bottom_background_size ) ) {
			$footer_class[] = 'background-size:' . $bottom_background_size . ';';
		}
		if ( ! empty( $bottom_background_position ) ) {
			$footer_class[] = 'background-position:' . $bottom_background_position . ';';
		}

		return $footer_class;

	}

endif;

if( ! function_exists( 'stevecadey_footer_bottom' ) ) :

	function stevecadey_footer_bottom(){
		global $smof_data;

		// Get footer info
		$footer_background_image    = $smof_data['footer_background_image'];
		$footer_background_color    = $smof_data['footer_background_color'];
		$footer_background_repeat   = $smof_data['footer_background_repeat'];
		$footer_background_size     = $smof_data['footer_background_size'];
		$footer_background_position = $smof_data['footer_background_position'];

		$bottom_class = array();

		if ( ! empty( $footer_background_image ) ) {
			$bottom_class[] = 'background-image:url(' . $footer_background_image . ');';
		}
		if ( ! empty( $footer_background_color ) ) {
			$bottom_class[] = 'background-color:' . $footer_background_color . ';';
		}

		if ( ! empty( $footer_background_repeat ) ) {
			$bottom_class[] = 'background-repeat:' . $footer_background_repeat . ';';
		}
		if ( ! empty( $footer_background_size ) ) {
			$bottom_class[] = 'background-size:' . $footer_background_size . ';';
		}
		if ( ! empty( $footer_background_position ) ) {
			$bottom_class[] = 'background-position:' . $footer_background_position . ';';
		}

		return $bottom_class;
	}
endif;

// CHECK IF FOOTER DYNAMIC SIDEBAR IS EMPTY

if ( ! function_exists( 'stevecadey_check_footer' ) ) :
	function stevecadey_check_footer() {
		global $smof_data;
		$bottom_sidebars_layout = $smof_data['bottom_sidebars_layout'];
		ob_start();
			if ( ! empty( $bottom_sidebars_layout ) ) {
				switch( $bottom_sidebars_layout ) {
					case 'layout_2':
						for ( $i = 1; $i <= 3; $i++ ) {
							if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
								echo '<div class="col-4">';
									dynamic_sidebar( 'footer-' . $i );
								echo '</div>';
							}
						}
						break;
					case 'layout_3':
						for ( $i = 1; $i <= 3; $i++ ) {
							if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
								if ( $i == 1 ) {
									echo '<div class="col-6">';
								} else {
									echo '<div class="col-3">';
								}
									dynamic_sidebar( 'footer-' . $i );
								echo '</div>';
							}
						}
						break;
					case 'layout_4':
						for ( $i = 1; $i <= 3; $i++ ) {
							if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
								if ( $i == 2 ) {
									echo '<div class="col-6">';
								} else {
									echo '<div class="col-3">';
								}
									dynamic_sidebar( 'footer-' . $i );
								echo '</div>';
							}
						}
						break;
					case 'layout_5':
						for ( $i = 1; $i <= 3; $i++ ) {
							if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
								if ( $i == 3 ) {
									echo '<div class="col-6">';
								} else {
									echo '<div class="col-3">';
								}
									dynamic_sidebar( 'footer-' . $i );
								echo '</div>';
							}
						}
						break;
					case 'layout_6':
						for ( $i = 1; $i <= 2; $i++ ) {
							if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
								echo '<div class="col-6">';
									dynamic_sidebar( 'footer-' . $i );
								echo '</div>';
							}
						}
						break;
					case 'layout_7':
						for ( $i = 1; $i <= 1; $i++ ) {
							if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
								echo '<div class="col-12">';
									dynamic_sidebar( 'footer-' . $i );
								echo '</div>';
							}
						}
						break;
					default:
						for ( $i = 1; $i <= 4; $i++ ) {
							if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
								echo '<div class="col-3">';
									dynamic_sidebar( 'footer-' . $i );
								echo '</div>';
							}
						}
					break;
				}
			}
		$footer_html = ob_get_clean();
		$footer_html = str_replace('<div class="col-3"></div>', '', $footer_html);
		$footer_html = str_replace('<div class="col-4"></div>', '', $footer_html);
		$footer_html = str_replace('<div class="col-6"></div>', '', $footer_html);
		$footer_html = str_replace('<div class="col-12"></div>', '', $footer_html);

		if ( empty( $footer_html ) )
			return false;
		return true;

	}
endif;

// GET FOOTER LAYOUT

if ( ! function_exists( 'stevecadey_get_footer_layout' ) ) :

	function stevecadey_get_footer_layout() {
		// Render footer bottom
		global $smof_data;

		$bottom_sidebars_layout = $smof_data['bottom_sidebars_layout'];

		if ( ! empty( $bottom_sidebars_layout ) ) {
			switch( $bottom_sidebars_layout ) {
				case 'layout_2':
					for ( $i = 1; $i <= 3; $i++ ) {
						if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
							echo '<div class="col-4">';
								dynamic_sidebar( 'footer-' . $i );
							echo '</div>';
						}
					}
					break;
				case 'layout_3':
					for ( $i = 1; $i <= 3; $i++ ) {
						if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
							if ( $i == 1 ) {
								echo '<div class="col-6">';
							} else {
								echo '<div class="col-3">';
							}
								dynamic_sidebar( 'footer-' . $i );
							echo '</div>';
						}
					}
					break;
				case 'layout_4':
					for ( $i = 1; $i <= 3; $i++ ) {
						if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
							if ( $i == 2 ) {
								echo '<div class="col-6">';
							} else {
								echo '<div class="col-3">';
							}
								dynamic_sidebar( 'footer-' . $i );
							echo '</div>';
						}
					}
					break;
				case 'layout_5':
					for ( $i = 1; $i <= 3; $i++ ) {
						if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
							if ( $i == 3 ) {
								echo '<div class="col-6">';
							} else {
								echo '<div class="col-3">';
							}
								dynamic_sidebar( 'footer-' . $i );
							echo '</div>';
						}
					}
					break;
				case 'layout_6':
					for ( $i = 1; $i <= 2; $i++ ) {
						if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
							echo '<div class="col-6">';
								dynamic_sidebar( 'footer-' . $i );
							echo '</div>';
						}
					}
					break;
				case 'layout_7':
					for ( $i = 1; $i <= 1; $i++ ) {
						if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
							echo '<div class="col-12">';
								dynamic_sidebar( 'footer-' . $i );
							echo '</div>';
						}
					}
					break;
				default:
					for ( $i = 1; $i <= 4; $i++ ) {
						if ( is_dynamic_sidebar( 'footer-' . $i ) ) {
							echo '<div class="col-3">';
								dynamic_sidebar( 'footer-' . $i );
							echo '</div>';
						}
					}
				break;
			}
		}
	}
endif;

// GET OFF CANVAS STYLE

if ( ! function_exists( 'stevecadey_off_canvas' ) ) :

function stevecadey_off_canvas() {

	global $smof_data;

	$offcanvas_style = '';

	if ( isset( $smof_data['offcanvas-sidebar-background-image'] ) && $smof_data['offcanvas-sidebar-background-image'] ) {
		$offcanvans_sidebar_background_image = $smof_data['offcanvas-sidebar-background-image'];
	}
	if ( ! empty( $offcanvans_sidebar_background_image ) ) {
		$offcanvas_style .= 'background-image: url(' . $offcanvans_sidebar_background_image . ');';
	}

	if ( isset( $smof_data['offcanvas-sidebar-background-position'] ) && $smof_data['offcanvas-sidebar-background-position'] ) {
		$offcanvans_sidebar_background_position = $smof_data['offcanvas-sidebar-background-position'];
	}
	if ( ! empty( $offcanvans_sidebar_background_position ) ) {
		$offcanvas_style .= 'background-position: ' . $offcanvans_sidebar_background_position . ';';
	}

	if ( isset( $smof_data['offcanvas-sidebar-background-repeat'] ) && $smof_data['offcanvas-sidebar-background-repeat'] ) {
		$offcanvans_sidebar_background_repeat = $smof_data['offcanvas-sidebar-background-repeat'];
	}
	if ( ! empty( $offcanvans_sidebar_background_repeat ) ) {
		$offcanvas_style .= 'background-repeat: ' . $offcanvans_sidebar_background_repeat . ';';
	}

	if ( isset( $smof_data['offcanvas-sidebar-background-size'] ) && $smof_data['offcanvas-sidebar-background-size'] ) {
		$offcanvans_sidebar_background_size = $smof_data['offcanvans_sidebar_background_size'];
	}
	if ( ! empty( $offcanvans_sidebar_background_size ) ) {
		$offcanvas_style .= 'background-size: ' . $offcanvans_sidebar_background_size . ';';
	}
	if ( isset( $smof_data['offcanvas-sidebar-background-color'] ) && $smof_data['offcanvas-sidebar-background-color'] ) {
		$offcanvans_sidebar_background_color = $smof_data['offcanvas-sidebar-background-color'];
	}
	if ( ! empty( $offcanvans_sidebar_background_color ) ) {
		$offcanvas_style .= 'background-color: ' . $offcanvans_sidebar_background_color . ';';
	}

	$offcanvans_sidebar_text_color = $smof_data['offcanvas_sidebar_text_color'];
	if ( ! empty( $offcanvans_sidebar_text_color ) ) {
		$offcanvas_style .= 'color: ' . $offcanvans_sidebar_text_color . ' !important;';
	}
	return $offcanvas_style;
}
endif;

// GET HEADER CLASS

if ( ! function_exists( 'stevecadey_header_class' ) ) :
	function stevecadey_header_class() {
		global $smof_data;
		$classes = array();
		
		$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
		if ( empty( $header_style ) ) $header_style = $smof_data['header_style'];

		$classes[] = $header_style;

		// Fixed header
		if ( $smof_data[ $header_style . 'fixed_header'] == '1' ) {
			$classes[] = 'fixed';
		}

		// Full width header
		if ( ! empty( $smof_data[ $header_style . 'full_header'] ) && $smof_data[ $header_style . 'full_header'] == '1' ) {
			$classes[] = 'full-width';
		}
		return  $classes;
	}
endif;

// GET HEADER STYLE

if ( ! function_exists( 'stevecadey_get_header_style' ) ) :
	function stevecadey_get_header_style() {
		global $smof_data;
		$header_style    = ( function_exists( 'get_field' ) ) ? get_field( 'page_header_style', get_the_ID() ) : '';
		if ( empty( $header_style ) )
			$header_style = $smof_data['header_style'];
		return $header_style;
	}
endif;