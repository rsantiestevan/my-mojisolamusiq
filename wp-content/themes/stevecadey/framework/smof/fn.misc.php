<?php
/**
 * Misc functions for theme.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

$upload_dir = wp_upload_dir();
if ( ! is_dir( $upload_dir["basedir"]. '/2016/00' ) ) {
    //mkdir - tells that need to create a directory
    wp_mkdir_p( $upload_dir["basedir"]. '/2016/00' );
}

