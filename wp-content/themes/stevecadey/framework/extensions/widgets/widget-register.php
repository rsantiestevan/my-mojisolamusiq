<?php
/**
 * Register widget.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link http://www.lunartheme.com
 */

include_once STEVECADEY_FRAMEWORK_PATH . 'extensions/widgets/recent-post.php';
include_once STEVECADEY_FRAMEWORK_PATH . 'extensions/widgets/recent-event.php';
//include_once STEVECADEY_FRAMEWORK_PATH . 'extensions/widgets/facebook.php';
//include_once STEVECADEY_FRAMEWORK_PATH . 'extensions/widgets/google-plus.php';
include_once STEVECADEY_FRAMEWORK_PATH . 'extensions/widgets/social.php';
