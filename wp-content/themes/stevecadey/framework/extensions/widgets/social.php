<?php
/**
 * Facebook widget.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link http://www.lunartheme.com
 */

add_action( 'widgets_init', 'stevecadey_social_widgets' );
function stevecadey_social_widgets() {
	register_widget( 'stevecadey_Widget_Social' );
}
class stevecadey_Widget_Social extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'social', 'description' => esc_html__( 'Social Widget', 'stevecadey' ) );
		$control_ops = array();
		parent::__construct( 'k2t_social', esc_html__( 'stevecadey - Social', 'stevecadey' ), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$align = isset ( $instance['align_social'] ) ? $instance['align_social'] : '';
		$url_twitter = isset ( $instance['twitter_link'] ) ? $instance['twitter_link'] : '';
		$url_facebook = isset ( $instance['facebook_link'] ) ? $instance['facebook_link'] : '';
		$url_instagram = isset ( $instance['instagram_link'] ) ? $instance['instagram_link'] : '';
		$url_google_plus = isset ( $instance['google_plus_link'] ) ? $instance['google_plus_link'] : '';
		$url_pinterest = isset ( $instance['pinterest_link'] ) ? $instance['pinterest_link'] : '';

		echo ( $before_widget );
		$social_html = '';
		if(!empty($url_twitter)) {
			// $url_twitter = 'https://' . str_replace( 'https://', '', $url_twitter );
			$social_html .= "<li class='twitter'><a target='_blank' href=\"$url_twitter\"><i class='fa fa-twitter'></i></a></li>";
		}
		if(!empty($url_facebook)) {
			// $url_facebook = 'https://' . str_replace( 'https://', '', $url_facebook );
			$social_html .= "<li class='facebook'><a target='_blank' href=\"$url_facebook\"><i class='fa fa-facebook'></i></a></li>";
		}
		if(!empty($url_instagram)) {
			// $url_instagram = 'https://' . str_replace( 'https://', '', $url_instagram );
			$social_html .= "<li class='instagram'><a target='_blank' href=\"$url_instagram\"><i class='fa fa-instagram'></i></a></li>";
		}
		if(!empty($url_google_plus)) {
			// $url_google_plus = 'https://' . str_replace( 'https://', '', $url_google_plus );
			$social_html .= "<li class='google'><a target='_blank' href=\"$url_google_plus\"><i class='fa fa-google-plus'></i></a></li>";
		}
		if(!empty($url_pinterest)) {
			// $url_pinterest = 'https://' . str_replace( 'https://', '', $url_pinterest );
			$social_html .= "<li class='pinterest'><a target='_blank' href=\"$url_pinterest\"><i class='fa fa-pinterest'></i></a></li>";
		}

		echo '<ul class="' . $align . '">' . $social_html . '</ul>';
		echo ( $after_widget );
	}

	/**
	 * Saves the widgets settings.
	 *
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['align_social'] = $new_instance['align_social'];
		$instance['twitter_link'] = $new_instance['twitter_link'];
		$instance['facebook_link'] = $new_instance['facebook_link'];
		$instance['instagram_link'] = $new_instance['instagram_link'];
		$instance['google_plus_link'] = $new_instance['google_plus_link'];
		$instance['pinterest_link'] = $new_instance['pinterest_link'];
		return $instance;
	}

	/**
	 * Creates the edit form for the widget.
	 *
	 */
	function form( $instance ) {
		$defaults = array(
			'align_social' 	    => '',
			'twitter_link'      => '',
			'facebook_link'     => '',
			'instagram_link'    => '',
			'google_plus_link'  => '',
			'pinterest_link'    => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		extract( $instance );
		$align_social = $instance['align_social'];

?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'align_social' ) ); ?>"><?php esc_html_e( 'Align:', 'stevecadey' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'align' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'align_social' ) ); ?>">
				<option value="align-left" <?php selected( $align_social, 'align-left' ) ?>><?php esc_html_e( 'left', 'stevecadey' );?></option>
				<option value="align-right" <?php selected( $align_social, 'align-right' ) ?>><?php esc_html_e( 'right', 'stevecadey' );?></option>
				<option value="align-center" <?php selected( $align_social, 'align-center' ) ?>><?php esc_html_e( 'center', 'stevecadey' );?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'twitter_link' ) ); ?>"><?php esc_html_e( 'Twitter URL:', 'stevecadey' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter_link' ) ); ?>" type="text" value="<?php echo esc_attr( $twitter_link ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'facebook_link' ) ); ?>"><?php esc_html_e( 'Facebook URL:', 'stevecadey' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook_link' ) ); ?>" type="text" value="<?php echo esc_attr( $facebook_link ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'instagram_link' ) ); ?>"><?php esc_html_e( 'Instagram URL:', 'stevecadey' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram_link' ) ); ?>" type="text" value="<?php echo esc_attr( $instagram_link ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'google_plus_link' ) ); ?>"><?php esc_html_e( 'Google Plus URL:', 'stevecadey' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'google_plus_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'google_plus_link' ) ); ?>" type="text" value="<?php echo esc_attr( $google_plus_link ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'pinterest_link' ) ); ?>"><?php esc_html_e( 'Pinterest URL:', 'stevecadey' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pinterest_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'pinterest_link' ) ); ?>" type="text" value="<?php echo esc_attr( $pinterest_link ); ?>" />
		</p>

		<?php
	} //end of form

} // end class


