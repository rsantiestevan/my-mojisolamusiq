<?php
/**
 * Recent Event widget.
 *
 * @package stevecadey
 * @author  Lunartheme
 * @link http://www.lunartheme.com
 */

add_action( 'widgets_init', 'steve_recent_event_load_widgets' );
function steve_recent_event_load_widgets() {
	register_widget( 'stevecadey_Widget_Recent_Event' );
}
class stevecadey_Widget_Recent_Event extends WP_Widget {

	function __construct() {
		$widget_ops  = array( 'classname' => 'k2t_widget_recent_event', 'description' => '' );
		$control_ops = array( 'width' => 250, 'height' => 350 );
		parent::__construct( 'k2t_recent_event', esc_html__( 'stevecadey - Recent Event', 'stevecadey' ), $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		global $post;
		echo ( $before_widget );
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		if ( ! empty( $title ) ) {
			echo ( $before_title );
			echo esc_html($title) ;
			echo ( $after_title );
		}
		
		// Load parameter
		$order         = isset( $instance['order'] ) ? $instance['order'] : '';
		$limit         = isset( $instance['limit'] ) ? $instance['limit'] : '';
		$display_date  = isset( $instance['display_date'] ) ? $instance['display_date'] : '';
		$display_location  = isset( $instance['display_location'] ) ? $instance['display_location'] : '';


		// Enqueue
		if ( ! empty( $limit ) ) $args['posts_per_page'] = $limit;
		// Load data
		$args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'post_type'   => 'post-k-event',
			'orderby'     => 'post_date',
			'posts_per_page' => $limit,
		);

		$recent_posts = get_posts( $args );
		if ( count( $recent_posts ) > 0 ) {
			echo '<div class="k2t-related-event clearfix">';
			foreach ( $recent_posts as $post ) {
				$event_start_date = (function_exists('get_field')) ? get_field('event_start_date', get_the_ID()) : ''; $event_start_date = empty($event_start_date) ? '' : $event_start_date;

				$new_start_date = date_i18n('M d', strtotime( $event_start_date ) );

				$event_address = (function_exists('get_field')) ? get_field('event_address', get_the_ID()) : ''; $event_address = empty($event_address) ? '' : $event_address;

				$thumb_html = '';
				if( has_post_thumbnail( get_the_ID() ) ){
					$thumb_html = get_the_post_thumbnail(get_the_ID(), 'thumb_event_related', array('alt' => trim(get_the_title())));
				}else{
					$thumb_html = '<img src="' . plugin_dir_url( __FILE__ ) . 'images/thumb-400x256.png" alt="'.trim(get_the_title()).'" />';
				}
				$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'post' );

				setup_postdata( $post );
				
				?>
				<div class="related-event-inner clearfix">
					<article class="related-event">			
						<div class="related-inner">
							<div class="related-thumb">
								<div class="event-date">
									<?php if ( $display_date == 'show' && !empty($event_start_date)) : ?>
										<?php echo '<span class="event-m">' . date_i18n('M d', strtotime( $new_start_date ) ) . '</span>'; ?>
									<?php endif;?>									
								</div>
								<a title="<?php the_title(); ?>" class="image-link" href="<?php the_permalink(get_the_ID())?>">
									<i class="recent-e-mask"></i>
									<?php echo ( $thumb_html );?>
								</a>

							</div>

							<div class="related-meta">
								<h5 class="event-item-title">
									<a href="<?php the_permalink(get_the_ID())?>" title="<?php the_title()?>"><?php the_title();?></a>
								</h5>

								<!-- Date & Time -->

								<?php if ( $display_date == 'show' && !empty($event_start_date)) :
									$time_up  = date_i18n( 'g:i A', strtotime( $event_start_date ) );
								?>

								<!-- Time -->

								<div class="event-time">
									<span><i class="zmdi zmdi-time"></i></span>
									<?php
										echo '<span>' . $time_up . '</span>';
									?>
								</div>

								<?php endif;?>
								<!-- END Time -->
								<!-- Address -->

								<?php if ( $display_location == 'show' && !empty($event_address) ) : ?>
									<span><i class="zmdi zmdi-pin"></i><?php echo esc_html($event_address) ?></span>
								<?php endif;?>

							</div><!-- .related-meta -->

						</div><!-- .related-inner -->
					</article>
				</div>
				<?php
			}
			echo '</div>';
		}
		echo ( $after_widget );
		wp_reset_postdata();
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		return $new_instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' => esc_html__( 'Recent Event', 'stevecadey' ), 'limit' => 3, 'order' => 'desc', 'display_date' => 'show' );
		$instance = wp_parse_args( (array) $instance, $defaults );?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'stevecadey' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
    	<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"><?php esc_html_e( 'Limit:', 'stevecadey' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['limit'] ); ?>" />
		</p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'display_date' ) ); ?>"><?php esc_html_e( 'Display Date:', 'stevecadey' ); ?></label>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'display_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'display_date' ) ); ?>">
                <option <?php selected( $instance['display_date'], 'show' ) ?> value="show"><?php esc_html_e( 'Show', 'stevecadey' );?></option>
                <option <?php selected( $instance['display_date'], 'hided' ) ?> value="hided"><?php esc_html_e( 'Hide', 'stevecadey' );?></option>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'display_location' ) ); ?>"><?php esc_html_e( 'Display Location:', 'stevecadey' ); ?></label>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'display_location' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'display_location' ) ); ?>">
                <option <?php selected( $instance['display_location'], 'show' ) ?> value="show"><?php esc_html_e( 'Show', 'stevecadey' );?></option>
                <option <?php selected( $instance['display_location'], 'hided' ) ?> value="hided"><?php esc_html_e( 'Hide', 'stevecadey' );?></option>
            </select>
        </p>
		<?php
	}
}
?>