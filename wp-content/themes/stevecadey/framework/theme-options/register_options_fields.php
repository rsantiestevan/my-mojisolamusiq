<?php

// Disable some theme options by id if not required
if( ! function_exists( 'k2t_disable_of_options' ) ){
	function k2t_disable_of_options($options = array()){
		global $of_options, $of_options_disable;
		$of_options_disable = array(
		);

		if( !empty($options) ){
			$options =& $of_options;
		}

		foreach( $of_options_disable as $op_id ){
			if( is_string( $op_id ) && isset( $of_options[$op_id] ) ){
				$of_options[$op_id]['disable'] = true;
			}
		}

		return $options;
	}

	add_filter('optionsframework_machine_before', 'k2t_disable_of_options');
}

if ( ! function_exists( 'k2t_render_titlebar_options' ) ) {
	function k2t_render_titlebar_options( $pre, $of_options ) {
		/* Titlebar */
		$of_options[] = array( 'name' => esc_html__( 'Titlebar', 'stevecadey' ),
			'type' => 'info',
			'std'  => esc_html__( 'Titlebar', 'stevecadey' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Show / Hide titlebar', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . '_display_titlebar',
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array(  $pre . '_titlebar_font_size', $pre . '_titlebar_color', $pre . '_pading_top', $pre . '_pading_bottom', $pre . '_background_color', $pre . '_background_image', $pre . '_background_position', $pre . '_background_size', $pre . '_background_repeat', $pre . '_background_parallax', $pre . '_titlebar_overlay_opacity', $pre . '_titlebar_clipmask_opacity', $pre . '_titlebar_custom_content',  ),
			),
		);

		$of_options[] = array( 'name' => esc_html__( 'Display Meta', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . '_display_meta',
			'desc' => esc_html__( 'Show or hide all meta', 'stevecadey' ),
			'logicstic' => true,
		);

		$of_options[] = array( 'name' => esc_html__( 'Display Excerpt', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . '_display_excerpt',
			'desc' => esc_html__( 'Show or hide excerpt', 'stevecadey' ),
			'logicstic' => true,
		);

		$of_options[] = array( 'name' => esc_html__( 'Display breadcrumb', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . '_display_breadcum',
			'desc' => esc_html__( 'Show or hide breadcrumb', 'stevecadey' ),
			'logicstic' => true,
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar title font size', 'stevecadey' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . '_titlebar_font_size',
			'desc' => esc_html__( 'Unit: px.', 'stevecadey' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar title color', 'stevecadey' ),
			'type' => 'color',
			'std'  => '',
			'id'   => $pre . '_titlebar_color',
		);

		$of_options[] = array( 'name' => esc_html__( 'Padding top', 'stevecadey' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . '_pading_top',
			'desc' => esc_html__( 'Unit: px. Eg: 10px;', 'stevecadey' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Padding bottom', 'stevecadey' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . '_pading_bottom',
			'desc' => esc_html__( 'Unit: px. Eg: 10px;', 'stevecadey' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Background color', 'stevecadey' ),
			'type' => 'color',
			'std'  => '',
			'id'   => $pre . '_background_color',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background image', 'stevecadey' ),
			'type' => 'media',
			'std'  => get_template_directory_uri() . '/assets/img/bgmasonry.jpg',
			'id'   => $pre . '_background_image',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background position', 'stevecadey' ),
			'type' => 'select',
			'std'  => 'left',
			'options' => array(
				'left top'      => esc_html__( 'Left Top', 'stevecadey' ),
				'left center'   => esc_html__( 'Left Center', 'stevecadey' ),
				'left bottom'   => esc_html__( 'Left Bottom', 'stevecadey' ),
				'right top'     => esc_html__( 'Right Top', 'stevecadey' ),
				'right center'  => esc_html__( 'Right Center', 'stevecadey' ),
				'right bottom'  => esc_html__( 'Right Bottom', 'stevecadey' ),
				'center top'    => esc_html__( 'Center Top', 'stevecadey' ),
				'center center' => esc_html__( 'Center Center', 'stevecadey' ),
				'center bottom' => esc_html__( 'Center Bottom', 'stevecadey' ),
			),
			'id'   => $pre . '_background_position',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background size', 'stevecadey' ),
			'type' => 'select',
			'std'  => 'cover',
			'options' => array(
				'inherit' 		=> esc_html__( 'Inherit', 'stevecadey' ),
				'cover'    		=> esc_html__( 'Cover', 'stevecadey' ),
				'contain'  		=> esc_html__( 'Contain', 'stevecadey' ),
				'full'  		=> esc_html__( '100%', 'stevecadey' ),
			),
			'id'   => $pre . '_background_size',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background repeat', 'stevecadey' ),
			'type' => 'select',
			'std'  => 'no-repeat',
			'options' => array(
				'no_repeat' => esc_html__( 'No Repeat', 'stevecadey' ),
				'repeat'    => esc_html__( 'Repeat', 'stevecadey' ),
				'repeat_x'  => esc_html__( 'Repeat X', 'stevecadey' ),
				'repeat_y'  => esc_html__( 'Repeat Y', 'stevecadey' ),
			),
			'id'   => $pre . '_background_repeat',
		);

		$of_options[] = array( 'name' => esc_html__( 'Background parallax', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . '_background_parallax',
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar overlay opacity', 'stevecadey' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => $pre . '_titlebar_overlay_opacity',
		);

		$of_options[] = array( 'name'   => esc_html__( 'Overlay background color', 'stevecadey' ),
			'id'   => $pre . '_overlay_background_color',
			'type' => 'color',
			'std'  => '',
		);

		$of_options[] = array( 'name' => esc_html__( 'Titlebar clipmask opacity', 'stevecadey' ),
			'type' => 'sliderui',
			'min'  => 0,
			'max'  => 10,
			'std'  => 0,
			'step' => 1,
			'id'   => $pre . '_titlebar_clipmask_opacity',
		);
		$of_options[] = array( 'name' => esc_html__( 'Pattern image', 'stevecadey' ),
			'type' => 'media',
			'id'   => $pre . 'titlebar_clipmask_opacity',
			'std'  => '',
			'desc' => esc_html__( '', 'stevecadey' ),
		);
		$of_options[] = array( 'name' => esc_html__( 'Titlebar custom content', 'stevecadey' ),
			'type' => 'textarea',
			'std'  => '',
			'id'   => $pre . '_titlebar_custom_content',
		);
		return $of_options;
	}
}

if ( ! function_exists( 'k2t_render_header_style' ) ) {
	function k2t_render_header_style( $pre, $of_options ) {

		$of_options[] = array( 'name' => esc_html__( 'Fixed header?', 'stevecadey' ),
		 	'type' => 'switch',
		 	'std'  => false,
		 	'id'   => $pre . 'fixed_header',
		 	'desc' => esc_html__( 'If the setting is enabled, the body section will be displayed under the header. If the setting is disabled, the body section will be displayed above the header. In the 2nd case, you can make the header transparent to create a very nice style', 'stevecadey' ),
		 	'k2t_logictic' => array(
				'0' => array( $pre . 'sticky_menu', $pre . 'smart_sticky', $pre . 'absolute' ),
				'1' => array(),
			),
		 );
		$of_options[] = array( 'name' => esc_html__( 'Sticky menu?', 'stevecadey' ),
			'type' => 'select',
			'options' => array(
				''        => esc_html__( 'None', 'stevecadey' ),
				'sticky_top' => esc_html__( 'Sticky menu on top header', 'stevecadey' ),
				'sticky_mid' => esc_html__( 'Sticky menu on middle header', 'stevecadey' ),
				'sticky_bot' => esc_html__( 'Sticky menu on bottom header', 'stevecadey' ),
			),
			'std'  => '',
			'id'   => $pre . 'sticky_menu',
			'desc' => esc_html__( 'Enable this setting so that the header section and menus inlcuded in the header are sticky', 'stevecadey' ),
		);

		$of_options[] = array(  'name' => esc_html__( 'Smart sticky menu', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'smart_sticky',
			'desc' => esc_html__( 'Turn ON to enable sticky menu, it will always stay in your page when scrolling to top and disappear when scrolling down.', 'stevecadey' ),
		);


		$of_options[] = array( 'name' => esc_html__( 'Header Layouts', 'stevecadey' ),
			'type' => 'info',
			'std'  => esc_html__( 'Header Layouts', 'stevecadey' ),
			'id'   => $pre . 'header_layouts',
		);

		$of_options[] = array(  'name' => esc_html__( 'Fullwidth header layout', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'full_header',
			'desc' => esc_html__( 'Turn it ON if you want to set full width header.', 'stevecadey' ),
		);

		/* Visual Header */
		$of_options[] = array( 'name' => esc_html__( 'Top Header Section', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'use_top_header',
			'desc' => esc_html__( 'Show or hide top header layout.', 'stevecadey' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( $pre . 'header_section_1' ),
			),
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '',
			'id'   => $pre . 'header_section_1',
			'desc' => '',

		);

		$of_options[] = array( 'name' => esc_html__( 'Middle Header Section', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'use_mid_header',
			'desc' => esc_html__( 'Show or hide middle header layout.', 'stevecadey' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( $pre . 'header_section_2' ),
			),
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '{"name":"header_section_2","setting":{"bg_image":"","bg_color":"","opacity":"","fixed_abs":"fixed","custom_css":""},"columns_num":2,"htmlData":"","columns":[{"id":1,"percent":"2","value":[{"id":"1425696862388","type":"logo","value":{"custom_class":"","custom_id":""}}]},{"id":2,"value":[{"id":"1434273437540","type":"custom_menu","value":{"menu_id":"QWxsIFBhZ2Vz","custom_class":"","custom_id":""}}],"percent":"10"}]}',
			'id'   => $pre . 'header_section_2',
			'desc' => '',
		);

		$of_options[] = array( 'name' => esc_html__( 'Bottom Header Section', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'use_bot_header',
			'desc' => esc_html__( 'Show or hide middle header layout.', 'stevecadey' ),
			'k2t_logictic' => array(
				'0' => array( ),
				'1' => array( $pre . 'header_section_3' ),
			),
		);

		$of_options[] = array(
			'type' => 'k2t_header_option',
			'std'  => '',
			'id'   => $pre . 'header_section_3',
			'desc' => '',
		);

		/* Logo */
		$of_options[] = array( 'name' => esc_html__( 'Logo', 'stevecadey' ),
			'type' => 'info',
			'std'  => esc_html__( 'Logo', 'stevecadey' ),
			'id'   => $pre . 'header_logo',
		);

		$of_options[] = array( 'name' => esc_html__( 'Use text logo', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'use_text_logo',
			'desc' => esc_html__( 'Turn it ON if you want to use text logo instead of image logo.', 'stevecadey' ),
			'logicstic' => true,
		);

		$of_options[] = array( 'name' => esc_html__( 'Text logo', 'stevecadey' ),
			'type' => 'text',
			'std'  => '',
			'id'   => $pre . 'text_logo',
			'desc' => '',
			'conditional_logic' => array(
				$pre .'field'    => 'use_text_logo',
				$pre .'value'    => 'switch_1',
			),
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo', 'stevecadey' ),
			'type' => 'media',
			'id'   => $pre . 'logo',
			'std'  => get_template_directory_uri() . '/assets/img/logo.png',
			'desc' => esc_html__( 'The logo size in our demo is 116x33px. Please use jpg, jpeg, png or gif image for best performance.', 'stevecadey' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Retina logo', 'stevecadey' ),
			'type' => 'media',
			'id'   => $pre . 'retina_logo',
			'std'  => get_template_directory_uri() . '/assets/img/logo.png',
			'desc' => esc_html__( '2x times your logo dimension.', 'stevecadey' ),
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin top (px)', 'stevecadey' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo_margin_top',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 16,
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin right (px)', 'stevecadey' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo_margin_right',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 0,
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin bottom (px)', 'stevecadey' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo_margin_bottom',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 14,
		);

		$of_options[] = array( 'name' => esc_html__( 'Logo margin left (px)', 'stevecadey' ),
			'type' => 'sliderui',
			'id'   => $pre . 'logo_margin_left',
			'min'  => 0,
			'max'  => 200,
			'step' => 1,
			'std'  => 0,
		);
		return $of_options;
	}
}

// Social Share Function
if ( ! function_exists( 'stevecadey_social_share_options' ) ) {
	function stevecadey_social_share_options( $pre, $of_options ) {
		/* Titlebar */
		$of_options[] = array( 'name' => esc_html__( 'Show Facebook Share', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social_share_facebook',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Twitter Share', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social_share_twitter',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Instagram Share', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social_share_instagram',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Google Plus Share', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social_share_google',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Linkedin Share', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social_share_linkedin',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Tumblr Share', 'stevecadey' ),
			'type' => 'switch',
			'std'  => true,
			'id'   => $pre . 'social_share_tumblr',
		);
		$of_options[] = array( 'name' => esc_html__( 'Show Email Share', 'stevecadey' ),
			'type' => 'switch',
			'std'  => false,
			'id'   => $pre . 'social_share_email',
		);
		return $of_options;
	}
}



// Add some theme options if needs
if( ! function_exists('k2t_add_of_options') ){
	function k2t_add_of_options($options = array()){
		$add_options = array(
			// option array to add here
		);

		foreach($add_options as $option){
			if( !empty($option['id']) && !isset($options[$option['id']]) )
				$options[$option['id']] = $option;
			else{
				$options[] = $option;
			}
		}

		return $options;
	}

	add_filter('optionsframework_machine_before', 'k2t_add_of_options', 9);
}

//================= Register theme option's fields =======================

/*___________________________________________________________________________________*/
/* General */
/*___________________________________________________________________________________*/

$of_options = array(
	array( 'name' => esc_html__( 'General', 'stevecadey' ),
		'type' => 'heading',
		'icon' => '<i class="zmdi zmdi-settings"></i>',
	)
);

$of_options[] = array( 'name' => esc_html__( 'General', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'General', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide page loader', 'stevecadey' ),
	'type' => 'switch',
	'std'  => false,
	'id'   => 'pageloader',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Logo For Default Login Page', 'stevecadey' ),
	'type' => 'media',
	'id'   => 'login_logo',
	'std'  => get_template_directory_uri() . '/assets/img/logo.png',
	'desc' => esc_html__( 'The logo size in our demo is 90x60px. Please use jpg, jpeg, png or gif image for best performance.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'API Key', 'stevecadey' ),
	'type' => 'text',
	'std'  => 'AIzaSyBXKYIE3Y7I20NLhQcfUhAcodjxlqI9xWM',
	'id'   => 'key_map',
	'desc' => esc_html__( 'Please enter Google Map API Key for Contact page', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Sidebar width', 'stevecadey' ),
	'type' => 'sliderui',
	'id'   => 'sidebar_width',
	'min'  => 0,
	'max'  => 100,
	'step' => 1,
	'std'  => 25,
	'desc' => esc_html__( 'Unit: %', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Header Code', 'stevecadey' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'header_code',
	'desc' => esc_html__( 'You can load Google fonts here.', 'stevecadey' ),
	'is_js_editor' => '1'
);

$of_options[] = array( 'name' => esc_html__( 'Footer Code', 'stevecadey' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'footer_code',
	'desc' => esc_html__( 'You can fill footer code.', 'stevecadey' ),
	'is_js_editor' => '1'
);

$of_options[] = array( 'name' => esc_html__( 'Custom CSS', 'stevecadey' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'custom_css',
	'desc' => esc_html__( 'If you know a little about CSS, you can write your custom CSS here. Do not edit CSS files (it will be lost when you update this theme).', 'stevecadey' ),
	'is_css_editor' => '1'
);

/* Icons */
$of_options[] = array( 'name' => esc_html__( 'Icons', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Icons', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Favicon', 'stevecadey' ),
	'type' => 'media',
	'id'   => 'favicon',
	'std'  => get_template_directory_uri() . '/assets/img/favicon.png',
	'desc' => esc_html__( 'Favicon is a small icon image at the topbar of your browser. Should be 16x16px or 32x32px image (png, ico...)', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPhone icon (57x57px)', 'stevecadey' ),
	'type' => 'media',
	'id'   => 'apple_iphone_icon',
	'std'  => get_template_directory_uri() . '/assets/img/iphone.png',
	'desc' => esc_html__( 'Similar to the Favicon, the <strong> iPhone icon</strong> is a file used for a web page icon on the  iPhone. When someone bookmarks your web page or adds your web page to their home screen, this icon is used. If this file is not found, these  products will use the screen shot of the web page, which often looks like no more than a white square.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPhone retina icon (114x114px)', 'stevecadey' ),
	'type' => 'media',
	'id'   => 'apple_iphone_retina_icon',
	'std'  => get_template_directory_uri() . '/assets/img/iphone_retina.png',
	'desc' => esc_html__( 'The same as  iPhone icon but for Retina iPhone.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPad icon (72x72px)', 'stevecadey' ),
	'type' => 'media',
	'id'   => 'apple_ipad_icon',
	'std'  => get_template_directory_uri() . '/assets/img/ipad.png',
	'desc' => esc_html__( 'The same as  iPhone icon but for iPad.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'IPad Retina icon (144x144px)', 'stevecadey' ),
	'type' => 'media',
	'id'   => 'apple_ipad_retina_icon',
	'std'  => get_template_directory_uri() . '/assets/img/ipad_retina.png',
	'desc' => esc_html__( 'The same as  iPhone icon but for Retina iPad.', 'stevecadey' ),
);

/*___________________________________________________________________________________*/
/* Header
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Header', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-border-top"></i>',
);

$of_options[] = array(  'name' => esc_html__( 'Header style', 'stevecadey' ),
			'type' => 'select',
			'std'  => 'h1_header_',
			'id'   => 'header_style',
			'options' => array(
				'h1_header_'        	=> esc_html__( 'Header style 1', 'stevecadey' ),
				'h2_header_'        	=> esc_html__( 'Header style 2', 'stevecadey' ),
			),
			'hidden_option_for_all_section' => true,
			'hidden_option_array' => array( 'h1_header_', 'h2_header_','h3_header_', 'h4_header_' ),
			'k2t_logictic' => array(
				'h1_header_' => array( 'h1_header_' ),
				'h2_header_' => array( 'h2_header_'),
			),
		);
/* Header */
$of_options = k2t_render_header_style( 'h1_header_', $of_options);
$of_options = k2t_render_header_style( 'h2_header_', $of_options);

/*___________________________________________________________________________________*/
/* Footer
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Footer', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-border-bottom"></i>'
);

$of_options[] = array( 'name'   => esc_html__( 'Show/Hide "Go to top"', 'stevecadey' ),
	'id'   => 'footer_gototop',
	'type' => 'switch',
	'std'  => true,
);

/* Widget area */
$of_options[] = array( 'name' => esc_html__( 'Widget area', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Main Footer', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Sidebars layout', 'stevecadey' ),
	'type' => 'select',
	'id'   => 'bottom_sidebars_layout',
	'options' => array(
		'layout_1' => esc_html__( '1/4 1/4 1/4 1/4', 'stevecadey' ),
		'layout_2' => esc_html__( '1/3 1/3 1/3', 'stevecadey' ),
		'layout_3' => esc_html__( '1/2 1/4 1/4', 'stevecadey' ),
		'layout_4' => esc_html__( '1/4 1/2 1/4', 'stevecadey' ),
		'layout_5' => esc_html__( '1/4 1/4 1/2', 'stevecadey' ),
		'layout_6' => esc_html__( '1/2 1/2', 'stevecadey' ),
		'layout_7' => esc_html__( '1', 'stevecadey' ),
	),
	'std'  => 'layout_1',
	'desc' => esc_html__( 'Select sidebars layout', 'stevecadey' ),
);

$of_options[] = array( 'name'   => esc_html__( 'Background color', 'stevecadey' ),
	'id'   => 'bottom_background_color',
	'type' => 'color',
	'std'  => '',
);

$of_options[] = array( 'name'   => esc_html__( 'Background image', 'stevecadey' ),
	'id'   => 'bottom_background_image',
	'type' => 'upload',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background position?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'stevecadey' ),
		'left top'      => esc_html__( 'Left Top', 'stevecadey' ),
		'left center'   => esc_html__( 'Left Center', 'stevecadey' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'stevecadey' ),
		'right top'     => esc_html__( 'Right Top', 'stevecadey' ),
		'right center'  => esc_html__( 'Right Center', 'stevecadey' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'stevecadey' ),
		'center top'    => esc_html__( 'Center Top', 'stevecadey' ),
		'center center' => esc_html__( 'Center Center', 'stevecadey' ),
		'center bottom' => esc_html__( 'Center Bottom', 'stevecadey' ),
	),
	'std'  => '',
	'id'   => 'bottom_background_position',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background repeat?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'stevecadey' ),
		'no_repeat' => esc_html__( 'No repeat', 'stevecadey' ),
		'repeat'    => esc_html__( 'Repeat', 'stevecadey' ),
		'repeat_x'  => esc_html__( 'Repeat X', 'stevecadey' ),
		'repeat_y'  => esc_html__( 'Repeat Y', 'stevecadey' ),
	),
	'std'  => '',
	'id'  => 'bottom_background_repeat',
	'desc'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background size?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''        => esc_html__( 'None', 'stevecadey' ),
		'auto'    => esc_html__( 'Auto', 'stevecadey' ),
		'cover'   => esc_html__( 'Cover', 'stevecadey' ),
		'contain' => esc_html__( 'Contain', 'stevecadey' ),
	),
	'std'  => '',
	'id'   => 'bottom_background_size',
	'desc' => '',
);

/* ============== Footer bottom ==================== */
$of_options[] = array( 'name' => esc_html__( 'Footer', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Bottom Footer', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Footer Logo', 'stevecadey' ),
	'type' => 'media',
	'id'   => 'footer_logo',
	'std'  => get_template_directory_uri() . '/assets/img/logo.png',
	'desc' => esc_html__( 'The logo size in our demo is 116x33px. Please use jpg, jpeg, png or gif image for best performance.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide Footer Bottom Menu', 'stevecadey' ),
	'id'   => 'footer_bottom_menu',
	'type' => 'switch',
	'std'  => true,
);

$of_options[] = array( 'name'   => esc_html__( 'Background color', 'stevecadey' ),
	'id'   => 'footer_background_color',
	'type' => 'color',
	'std'  => '',
);

$of_options[] = array( 'name'   => esc_html__( 'Background image', 'stevecadey' ),
	'id'   => 'footer_background_image',
	'type' => 'upload',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background position?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'stevecadey' ),
		'left top'      => esc_html__( 'Left Top', 'stevecadey' ),
		'left center'   => esc_html__( 'Left Center', 'stevecadey' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'stevecadey' ),
		'right top'     => esc_html__( 'Right Top', 'stevecadey' ),
		'right center'  => esc_html__( 'Right Center', 'stevecadey' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'stevecadey' ),
		'center top'    => esc_html__( 'Center Top', 'stevecadey' ),
		'center center' => esc_html__( 'Center Center', 'stevecadey' ),
		'center bottom' => esc_html__( 'Center Bottom', 'stevecadey' ),
	),
	'std'  => '',
	'id'   => 'footer_background_position',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background repeat?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'stevecadey' ),
		'no_repeat' => esc_html__( 'No repeat', 'stevecadey' ),
		'repeat'    => esc_html__( 'Repeat', 'stevecadey' ),
		'repeat_x'  => esc_html__( 'Repeat X', 'stevecadey' ),
		'repeat_y'  => esc_html__( 'Repeat Y', 'stevecadey' ),
	),
	'std'  => '',
	'id'  => 'footer_background_repeat',
	'desc'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Background size?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''        => esc_html__( 'None', 'stevecadey' ),
		'auto'    => esc_html__( 'Auto', 'stevecadey' ),
		'cover'   => esc_html__( 'Cover', 'stevecadey' ),
		'contain' => esc_html__( 'Contain', 'stevecadey' ),
	),
	'std'  => '',
	'id'   => 'footer_background_size',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Footer copyright text', 'stevecadey' ),
	'type' => 'textarea',
	'id'   => 'footer_copyright_text',
	'std'  => '',
	'desc' => esc_html__( 'HTML and shortcodes are allowed.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Footer Bottom Social', 'stevecadey' ),
	'type' => 'info',
	'std'  => 'Footer Bottom Social',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide Bottom Footer Social', 'stevecadey' ),
	'id'   => 'bottom_footer_social',
	'type' => 'switch',
	'std'  => true,
);

$of_options[] = array( 'name' => esc_html__( 'Facebook', 'stevecadey' ),
	'id'   => 'bottom_footer_fb',
	'type' => 'text',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Twitter', 'stevecadey' ),
	'type' => 'text',
	'std'  => '',
	'id'   => 'bottom_footer_tw',
);

$of_options[] = array( 'name' => esc_html__( 'Instagram', 'stevecadey' ),
	'type' => 'text',
	'std'  => '',
	'id'   => 'bottom_footer_ins',
);

$of_options[] = array( 'name' => esc_html__( 'Google Plus', 'stevecadey' ),
	'type' => 'text',
	'id'   => 'bottom_footer_googleplus',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Youtube', 'stevecadey' ),
	'type' => 'text',
	'id'   => 'bottom_footer_youtube',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Pinterest', 'stevecadey' ),
	'type' => 'text',
	'id'   => 'bottom_footer_pinterest',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Tumblr', 'stevecadey' ),
	'type' => 'text',
	'id'   => 'bottom_footer_tumblr',
	'std'  => '',
);


/*___________________________________________________________________________________*/
/* Offcanvas sidebar
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-border-right"></i>'
);

$of_options[] = array( 'name'   => esc_html__( 'Show/Hide Offcanvas sidebar', 'stevecadey' ),
	'id'   => 'offcanvas_turnon',
	'type' => 'switch',
	'std'  => true,
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar position', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		'right'    => esc_html__( 'Right', 'stevecadey' ),
		'left'      => esc_html__( 'Left', 'stevecadey' ),
	),
	'std'  => '',
	'id'   => 'offcanvas_sidebar_position',
	'desc' => '',
);

// Get all sidebar
$sidebars = array();
$widget_list = wp_get_sidebars_widgets();
if ( count( $widget_list ) > 0 ){
	foreach ( $widget_list as $sidebar => $val ) {
		$sidebars[$sidebar] = $sidebar;
	}
}
$of_options[] = array( 'name' => esc_html__( 'Shown sidebar?', 'stevecadey' ),
	'type' => 'select',
	'options' => $sidebars,
	'id' => 'offcanvas_sidebar',
);

$of_options[] = array( 'name' => esc_html__( 'Background setting', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Background setting', 'stevecadey' ),
);

$of_options[] = array( 'name'   => esc_html__( 'Offcanvas sidebar background image', 'stevecadey' ),
	'id'   => 'offcanvas_sidebar_background_image',
	'type' => 'upload',
	'std'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background position?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'stevecadey' ),
		'left top'      => esc_html__( 'Left Top', 'stevecadey' ),
		'left center'   => esc_html__( 'Left Center', 'stevecadey' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'stevecadey' ),
		'right top'     => esc_html__( 'Right Top', 'stevecadey' ),
		'right center'  => esc_html__( 'Right Center', 'stevecadey' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'stevecadey' ),
		'center top'    => esc_html__( 'Center Top', 'stevecadey' ),
		'center center' => esc_html__( 'Center Center', 'stevecadey' ),
		'center bottom' => esc_html__( 'Center Bottom', 'stevecadey' ),
	),
	'std'  => '',
	'id'   => 'offcanvas_sidebar_background_position',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background repeat?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''    => esc_html__( 'None', 'stevecadey' ),
		'no_repeat' => esc_html__( 'No repeat', 'stevecadey' ),
		'repeat'    => esc_html__( 'Repeat', 'stevecadey' ),
		'repeat_x'  => esc_html__( 'Repeat X', 'stevecadey' ),
		'repeat_y'  => esc_html__( 'Repeat Y', 'stevecadey' ),
	),
	'std'  => '',
	'id'  => 'offcanvas_sidebar_background_repeat',
	'desc'  => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background size?', 'stevecadey' ),
	'type'    => 'select',
	'options' => array(
		''        => esc_html__( 'None', 'stevecadey' ),
		'auto'    => esc_html__( 'Auto', 'stevecadey' ),
		'cover'   => esc_html__( 'Cover', 'stevecadey' ),
		'contain' => esc_html__( 'Contain', 'stevecadey' ),
	),
	'std'  => '',
	'id'   => 'offcanvas_sidebar_background_size',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar background color', 'stevecadey' ),
	'type' => 'color',
	'id' => 'offcanvas_sidebar_background_color',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar text color', 'stevecadey' ),
	'type' => 'color',
	'id' => 'offcanvas_sidebar_text_color',
);

$of_options[] = array( 'name' => esc_html__( 'Offcanvas sidebar custom css', 'stevecadey' ),
	'type' => 'textarea',
	'std'  => '',
	'id'   => 'offcanvas_sidebar_custom_css',
);

/*___________________________________________________________________________________*/
/* Layout
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Layout', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-view-module"></i>'
);
/* Layout */
$of_options[] = array( 'name' => esc_html__( 'Layout', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Layout', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Content Width (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 1170,
	'min'  => 940,
	'max'  => 1200,
	'step' => 10,
	'id'   => 'use_content_width',
	'desc' => esc_html__( 'You can choose content width in the range from 940px to 1200px.', 'stevecadey' ),
);

$of_options = k2t_render_titlebar_options( 'page', $of_options );

/*___________________________________________________________________________________*/
/* Styling
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Style', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-view-module"></i>'
);

$of_options[] = array( 'name' => esc_html__( 'Primary Color', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Primary Color', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Primary color', 'stevecadey' ),
	'type' => 'select',
	'id'   => 'theme_primary_color',
	'std'  => 'light',
	'options' => array(
		'default'   => esc_html__( 'Default', 'stevecadey' ),
		'custom' 	=> esc_html__( 'Custom', 'stevecadey' ),
	),
	'k2t_logictic' => array(
		'default' 	=> array( ),
		'blue' 		=> array( ),
		'green' 	=> array( ),
		'red' 		=> array( ),
		'orange' 	=> array( ),
		'brown' 	=> array( ),
		'custom' 	=> array( 'primary_color' ),
	),
);

$of_options[] = array( 'name' => esc_html__( 'Primary Color Custom', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'primary_color',
	'desc' => esc_html__( 'Primary color is the main color of site.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Heading color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'heading_color',
	'desc' => esc_html__( 'Heading color', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Text color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'text_color',
	'desc' => esc_html__( 'Text color', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Links', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Links', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Link color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'link_color',
);

$of_options[] = array( 'name' => esc_html__( 'Link hover color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'link_hover_color',
);

$of_options[] = array( 'name' => esc_html__( 'Footer color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'footer_color',
);

$of_options[] = array( 'name' => esc_html__( 'Footer link color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'footer_link_color',
);

$of_options[] = array( 'name' => esc_html__( 'Menu colors', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Menu colors', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Main menu color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'main_menu_color',
);

$of_options[] = array( 'name' => esc_html__( 'Sub menu color', 'stevecadey' ),
	'type' => 'color',
	'std'  => '',
	'id'   => 'sub_menu_color',
);


/*___________________________________________________________________________________*/
/* Typography
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Typography', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-format-color-text"></i>'
);

$of_options[] = array( 'name' => esc_html__( 'Font family', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Font family', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Body font', 'stevecadey' ),
	'desc' => esc_html__( 'You can choose a normal font or Google font.', 'stevecadey' ),
	'id'   => 'body_font',
	'std'  => 'Montserrat',
	'type' => 'select_google_font',
	'preview'  => array(
		'text' => 'This is the preview!', //this is the text from preview box
		'size' => '16px' //this is the text size from preview box
	),
	'options' => stevecadey_fonts_array(),
);

$of_options[] = array( 'name' => esc_html__( 'Heading font', 'stevecadey' ),
	'desc' => esc_html__( 'You can choose a normal font or Google font', 'stevecadey' ),
	'id'   => 'heading_font',
	'std'  => 'News Cycle',
	'type' => 'select_google_font',
	'preview' => array(
		'text' => 'This is the preview!', //this is the text from preview box
		'size' => '60px' //this is the text size from preview box
	),
	'options' => stevecadey_fonts_array(),
);

$of_options[] = array( 'name' => esc_html__( 'Navigation font', 'stevecadey' ),
	'desc' => esc_html__( 'You can choose a normal font or Google font', 'stevecadey' ),
	'id'   => 'mainnav_font',
	'std'  => 'Montserrat',
	'type' => 'select_google_font',
	'preview' => array(
		'text' => 'This is the preview!', //this is the text from preview box
		'size' => '30px' //this is the text size from preview box
	),
	'options' => stevecadey_fonts_array(),
);

$of_options[] = array( 'name' => esc_html__( 'General font size', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'General font size', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Body font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 16,
	'min'  => 8,
	'max'  => 28,
	'step' => 1,
	'id'   => 'body_size',
);

$of_options[] = array( 'name' => esc_html__( 'Main navigation font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 14,
	'min'  => 9,
	'max'  => 24,
	'step' => 1,
	'id'   => 'mainnav_size',
);

$of_options[] = array( 'name' => esc_html__( 'Submenu of Main navigation font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 14,
	'min'  => 9,
	'max'  => 24,
	'step' => 1,
	'id'   => 'submenu_mainnav_size',
);

$of_options[] = array( 'name' => esc_html__( 'Titlebar title font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 60,
	'min'  => 14,
	'max'  => 120,
	'step' => 1,
	'id'   => 'titlebar_font_size',
);

$of_options[] = array( 'name' => esc_html__( 'Headings font size', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Headings font size', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'H1 font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 60,
	'min'  => 20,
	'max'  => 80,
	'step' => 1,
	'id'   => 'h1_size',
);

$of_options[] = array( 'name' => esc_html__( 'H2 font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 36,
	'min'  => 16,
	'max'  => 64,
	'step' => 1,
	'id'   => 'h2_size',
);

$of_options[] = array( 'name' => esc_html__( 'H3 font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 30,
	'min'  => 12,
	'max'  => 48,
	'step' => 1,
	'id'   => 'h3_size',
);

$of_options[] = array( 'name' => esc_html__( 'H4 font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 24,
	'min'  => 8,
	'max'  => 32,
	'step' => 1,
	'id'   => 'h4_size',
);

$of_options[] = array( 'name' => esc_html__( 'H5 font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 22,
	'min'  => 8,
	'max'  => 30,
	'step' => 1,
	'id'   => 'h5_size',
);

$of_options[] = array( 'name' => esc_html__( 'H6 font size (px)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 18,
	'min'  => 8,
	'max'  => 30,
	'step' => 1,
	'id'   => 'h6_size',
);

$of_options[] = array( 'name' => esc_html__( 'Font type', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Font type', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Navigation text transform', 'stevecadey' ),
	'desc' => esc_html__( 'Select navigation text transform.', 'stevecadey' ),
	'type' => 'select',
	'id'   => 'mainnav_text_transform',
	'std'  => 'uppercase',
	'options' => array (
		'none'       => esc_html__( 'None', 'stevecadey' ),
		'capitalize' => esc_html__( 'Capitalize', 'stevecadey' ),
		'uppercase'  => esc_html__( 'Uppercase', 'stevecadey' ),
		'lowercase'  => esc_html__( 'Lowercase', 'stevecadey' ),
		'inherit'    => esc_html__( 'Inherit', 'stevecadey' ),
	),
);

$of_options[] = array( 'name' => esc_html__( 'Navigation font weight', 'stevecadey' ),
	'desc' => esc_html__( 'Select navigation font weight.', 'stevecadey' ),
	'type' => 'select',
	'id'   => 'mainnav_font_weight',
	'std'  => '400',
	'options' => array (
		'100' => esc_html__( '100', 'stevecadey' ),
		'200' => esc_html__( '200', 'stevecadey' ),
		'300' => esc_html__( '300', 'stevecadey' ),
		'400' => esc_html__( '400', 'stevecadey' ),
		'500' => esc_html__( '500', 'stevecadey' ),
		'600' => esc_html__( '600', 'stevecadey' ),
		'700' => esc_html__( '700', 'stevecadey' ),
		'800' => esc_html__( '800', 'stevecadey' ),
		'900' => esc_html__( '900', 'stevecadey' ),
	),
);

/*___________________________________________________________________________________*/
/* Blog
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Blog', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-format-align-left"></i>'
);

$of_options[] = array( 'name' => esc_html__( 'Blog layout', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Blog layout', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Blog layout', 'stevecadey' ),
	'type' => 'select',
	'id'   => 'blog_layout',
	'options' => array (
		'right_sidebar' => esc_html__( 'Right Sidebar (default)', 'stevecadey' ),
		'left_sidebar'  => esc_html__( 'Left Sidebar', 'stevecadey' ),
		'no_sidebar'    => esc_html__( 'No Sidebar', 'stevecadey' ) ),
	'std'  => 'right_sidebar',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Blog sidebar', 'stevecadey' ),
	'type' => 'text',
	'std'  => '',
	'id'   => 'blog_custom_sidebar',
	'desc'   => '',
);

$of_options[] = array( 'name' => esc_html__( 'Blog style', 'stevecadey' ),
	'type' => 'select',
	'id'   => 'blog_style',
	'options' => array (
		'large'    => esc_html__( 'Large', 'stevecadey' ),
		'grid'     => esc_html__( 'Grid', 'stevecadey' ),
		'medium'   => esc_html__( 'Medium', 'stevecadey' ),
		'masonry'  => esc_html__( 'Masonry', 'stevecadey' ),
	),
	'std'       => 'large',
	'desc'      => esc_html__( 'Select blog style.', 'stevecadey' ),
	'logicstic' => true,
);
$of_options[] = array( 'name' => esc_html__( 'Blog animation', 'stevecadey' ),
	'type'      => 'select',
	'id'        => 'blog_anm',
	'type'	    => 'switch',
	'desc'      => esc_html__( '', 'stevecadey' ),
	'k2t_logictic' => array(
		'0' => array( ),
		'1' => array( 'blog_type_anm', 'blog_anm_delay' ),
	),
);
$of_options[] = array( 'name' => esc_html__( 'Animation type', 'stevecadey' ),
	'type'      => 'select',
	'id'        => 'blog_type_anm',
	'type'	    => 'select',
	'desc'      => esc_html__( '', 'stevecadey' ),
	'options'   => array(
		'bounce' 			=> esc_html__( 'bounce' , 'stevecadey' ),
		'flash'  			=> esc_html__( 'flash' , 'stevecadey' ),
		'pulse'  			=> esc_html__( 'pulse' , 'stevecadey' ),
		'rubberBand'  		=> esc_html__( 'rubberBand' , 'stevecadey' ),
		'shake'				=> esc_html__( 'shake' , 'stevecadey' ),
		'swing'				=> esc_html__( 'swing' , 'stevecadey' ),
		'tada'				=> esc_html__( 'tada' , 'stevecadey' ),
		'wobble'			=> esc_html__( 'wobble' , 'stevecadey' ),
		'bounceIn'			=> esc_html__( 'bounceIn' , 'stevecadey' ),
		'bounceInDown'		=> esc_html__( 'bounceInDown' , 'stevecadey' ),
		'bounceInLeft'		=> esc_html__( 'bounceInLeft' , 'stevecadey' ),
		'bounceInRight'		=> esc_html__( 'bounceInRight' , 'stevecadey' ),
		'bounceInUp'		=> esc_html__( 'bounceInUp' , 'stevecadey' ),
		'fadeIn'			=> esc_html__( 'fadeIn' , 'stevecadey' ),
		'fadeInDown'		=> esc_html__( 'fadeInDown' , 'stevecadey' ),
		'fadeInDownBig'		=> esc_html__( 'fadeInDownBig' , 'stevecadey' ),
		'fadeInLeft'		=> esc_html__( 'fadeInLeft' , 'stevecadey' ),
		'fadeInLeftBig'		=> esc_html__( 'fadeInLeftBig' , 'stevecadey' ),
		'fadeInRight'		=> esc_html__( 'fadeInRight' , 'stevecadey' ),
		'fadeInRightBig'	=> esc_html__( 'fadeInRightBig' , 'stevecadey' ),
		'fadeInUp'			=> esc_html__( 'fadeInUp' , 'stevecadey' ),
		'fadeInUpBig'		=> esc_html__( 'fadeInUpBig' , 'stevecadey' ),
		'flip'				=> esc_html__( 'flip' , 'stevecadey' ),
		'flipInX'			=> esc_html__( 'flipInX' , 'stevecadey' ),
		'flipInY'			=> esc_html__( 'flipInY' , 'stevecadey' ),
		'lightSpeedIn'		=> esc_html__( 'lightSpeedIn' , 'stevecadey' ),
		'rotateIn'			=> esc_html__( 'rotateIn' , 'stevecadey' ),
		'rotateInDownLeft'	=> esc_html__( 'rotateInDownLeft' , 'stevecadey' ),
		'rotateInDownRight'	=> esc_html__( 'rotateInDownRight' , 'stevecadey' ),
		'rotateInUpLeft'	=> esc_html__( 'rotateInUpLeft' , 'stevecadey' ),
		'rotateInUpRight'	=> esc_html__( 'rotateInUpRight' , 'stevecadey' ),
		'rollIn'			=> esc_html__( 'rollIn' , 'stevecadey' ),
		'zoomIn'			=> esc_html__( 'zoomIn' , 'stevecadey' ),
		'zoomInDown'		=> esc_html__( 'zoomInDown' , 'stevecadey' ),
		'zoomInLeft'		=> esc_html__( 'zoomInLeft' , 'stevecadey' ),
		'zoomInRight'		=> esc_html__( 'zoomInRight' , 'stevecadey' ),
		'zoomInUp'			=> esc_html__( 'zoomInUp' , 'stevecadey' ),
	),
);
$of_options[] = array( 'name' => esc_html__( 'Blog animation', 'stevecadey' ),
	'type'      => 'select',
	'id'        => 'blog_anm_delay',
	'type'	    => 'text',
	'std'		=> '2000',
	'desc'      => esc_html__( 'Numeric value only, 1000 = 1second.', 'stevecadey' ),
);

$of_options[] = array(  'name' => esc_html__( 'Columns', 'stevecadey' ),
	'type'    => 'select',
	'id'      => 'blog_masonry_column',
	'options' => array (
		'column_2' => esc_html__( '2 Columns', 'stevecadey' ),
		'column_3' => esc_html__( '3 Columns', 'stevecadey' ),
		'column_4' => esc_html__( '4 Columns', 'stevecadey' ),
		'column_5' => esc_html__( '5 Columns', 'stevecadey' )
	),
	'std'  => 'column_3',
	'desc' => esc_html__( 'Select column for layout masonry.', 'stevecadey' ),
	'conditional_logic' => array(
		'field'    => 'blog_style',
		'value'    => 'masonry',
	),
);
$of_options[] = array(  'name' => esc_html__( 'Columns', 'stevecadey' ),
	'type'    => 'select',
	'id'      => 'blog_grid_column',
	'options' => array (
		'column_2' => esc_html__( '2 Columns', 'stevecadey' ),
		'column_3' => esc_html__( '3 Columns', 'stevecadey' ),
		'column_4' => esc_html__( '4 Columns', 'stevecadey' ),
	),
	'std'  => 'column_2',
	'desc' => esc_html__( 'Select column for layout grid.', 'stevecadey' ),
	'conditional_logic' => array(
		'field'    => 'blog_style',
		'value'    => 'grid',
	),
);
$of_options[] = array(  'name' => esc_html__( 'Full width', 'stevecadey' ),
	'type' => 'switch',
	'std'  => false,
	'id'   => 'blog_masonry_full_width',
	'desc' => esc_html__( 'Enable full width layout for masonry blog.', 'stevecadey' ),
	'conditional_logic' => array(
		'field'    => 'blog_style',
		'value'    => 'masonry',
	),
);

$of_options[] = array( 'name' => esc_html__( 'Blog Options', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Blog Options', 'stevecadey' ),
);
$of_options[] = array( 'name' => esc_html__( 'Show or Hide excerpt', 'stevecadey' ),
	'type'    => 'switch',
	'id'      => 'blog_excerpt',
	'std'  	  => true,
	'desc' 	  => esc_html__( 'Show or Hide excerpt on blog archive.', 'stevecadey' ),
	'k2t_logictic' => array(
		true => array( 'blog_display', 'excerpt_length'),
	),
);
$of_options[] = array( 'name' => esc_html__( 'Content or excerpt', 'stevecadey' ),
	'type'    => 'select',
	'id'      => 'blog_display',
	'options' => array (
		'excerpts' => esc_html__( 'Excerpt', 'stevecadey' ),
		'contents' => esc_html__( 'Content', 'stevecadey' ) ),
	'std'  => 'excerpt',
	'desc' => esc_html__( 'Select display post contentor excerpt on the blog.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Excerpt length (words)', 'stevecadey' ),
	'type' => 'sliderui',
	'std'  => 25,
	'step' => 1,
	'min'  => 10,
	'max'  => 80,
	'id'   => 'excerpt_length',
);

$of_options[] = array(  'name' => esc_html__( 'Infinite Scroll', 'stevecadey' ),
	'type'    => 'select',
	'id'      => 'pagination_type',
	'options' => array (
		'pagination_number' => esc_html__( 'Pagination Number', 'stevecadey' ),
		'pagination_lite'   => esc_html__( 'Pagination Lite', 'stevecadey' ),
		'pagination_ajax'   => esc_html__( 'Pagination Ajax', 'stevecadey' ),
	),
	'std' => 'pagination_number'
);

$of_options[] = array(  'name' => esc_html__( 'Show/Hide categories filter', 'stevecadey' ),
	'type' => 'switch',
	'id'   => 'blog_categories_filter',
	'std'  => true,
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide title link?', 'stevecadey' ),
	'type' => 'switch',
	'id'   => 'blog_post_link',
	'std'  => true,
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide post date', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog_date',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide the number of comments', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog_number_comment',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide categories', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog_categories',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide author', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog_author',
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide "Reamore" link', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'blog_readmore',
);

$of_options = k2t_render_titlebar_options( 'blog', $of_options );


/*___________________________________________________________________________________*/
/* Single
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Single', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-receipt"></i>'
);

/* Featured Image */
$of_options[] = array( 'name' => esc_html__( 'Single Post Layout', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Single Post Layout', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Single post layout', 'stevecadey' ),
	'type' => 'select',
	'id'   => 'single_layout',
	'options' => array (
		'right_sidebar' => esc_html__( 'Right Sidebar (default)', 'stevecadey' ),
		'left_sidebar'  => esc_html__( 'Left Sidebar', 'stevecadey' ),
		'no_sidebar'    => esc_html__( 'No Sidebar', 'stevecadey' ) ),
	'std'  => 'right_sidebar',
	'desc' => '',
);

$of_options[] = array( 'name' => esc_html__( 'Single custom sidebar', 'stevecadey' ),
	'type' => 'text',
	'std'  => '',
	'id'   => 'single_custom_sidebar',
	'desc'   => '',
);

/* Meta */
$of_options[] = array( 'name' => esc_html__( 'Meta', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( 'Meta', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide Categories', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'display_categories',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display categories on single post', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide posted author', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'display_posted_author',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display posted author on single post', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide post date', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'display_post_date',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display post date on single post', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide tags', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'display_tags',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display tags on single post', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Show/Hide authorbox', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'display_authorbox',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display author box on single post', 'stevecadey' ),
);

$of_options[] = array(  'name' => esc_html__( 'Show/Hide related post', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'display_related_post',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display related post on single post', 'stevecadey' ),
);

$of_options[] = array(  'name' => esc_html__( 'Related post title', 'stevecadey' ),
	'type' => 'text',
	'std'  => 'You may also like',
	'id'   => 'single_related_post_title',
	'desc' => '',
);
$of_options[] = array( 'name' => esc_html__( 'Number of related post', 'stevecadey' ),
	'type' => 'text',
	'std'  => 3,
	'id'   => 'single_related_post_number',
	'desc' => esc_html__( 'Fill out _1 if you want to display ALL related post.', 'stevecadey' ),
);
$of_options[] = array( 'name' => esc_html__( 'Show/Hide comment form', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'single_commnet_form',
	'desc' => esc_html__( 'Turn OFF if you don\'t want to display comment form on single post', 'stevecadey' ),
);

$of_options = k2t_render_titlebar_options( 'single', $of_options );

/*___________________________________________________________________________________*/
/* WooCommerce
/*___________________________________________________________________________________*/

if ( class_exists('woocommerce') ) :
	$of_options[] = array( 'name' => esc_html__( 'WooCommerce', 'stevecadey' ),
		'type' => 'heading',
		'icon' => '<i class="zmdi zmdi-shopping-basket"></i>'
	);

	/* Shop Archive Page */
	$of_options[] = array( 'name' => esc_html__( 'Shop Archive', 'stevecadey' ),
		'type' => 'info',
		'std'  => esc_html__( 'Shop Archive', 'stevecadey' ),
	);

	$of_options[] = array( 'name' => esc_html__( 'Shop Layout', 'stevecadey' ),
		'type' => 'select',
		'std'  => 'no_sidebar',
		'options' => array(
			'right_sidebar' => esc_html__( 'Right Sidebar', 'stevecadey' ),
			'left_sidebar'  => esc_html__( 'Left Sidebar', 'stevecadey' ),
			'no_sidebar'    => esc_html__( 'No Sidebar', 'stevecadey' ),
		),
		'id' => 'shop_layout',
	);
	$of_options[] = array( 'name' => esc_html__( 'Shop style', 'stevecadey' ),
		'type' => 'select',
		'std'  => 'shop_grid',
		'options' => array(
			'shop_grid' => esc_html__( 'Shop Grid', 'stevecadey' ),
			'shop_listing'  => esc_html__( 'Shop Listing', 'stevecadey' ),
		),
		'id' => 'shop_style',
	);
	$of_options[] = array( 'name' => esc_html__( 'Show "sorting"?', 'stevecadey' ),
		'type' => 'switch',
		'std'  => true,
		'id'   => 'shop_display_sorting',
	);

	$of_options[] = array(  'name' => esc_html__( 'Show "result count"?', 'stevecadey' ),
		'type' => 'switch',
		'std'  => true,
		'id'   => 'shop_display_result_count',
	);

	$of_options[] = array( 'name' => esc_html__( 'Number of columns (default)', 'stevecadey' ),
		'type' => 'sliderui',
		'std'  => 4,
		'min'  => 1,
		'max'  => 4,
		'id'   => 'shop_column',
	);

	$of_options[] = array( 'name' => esc_html__( 'Number of products per page', 'stevecadey' ),
		'type' => 'text',
		'std'  => '',
		'id'   => 'shop_products_per_page',
		'desc' => esc_html__( 'Fill it _1 if you want to display all products.', 'stevecadey' ),
	);

	$of_options = k2t_render_titlebar_options( 'shop', $of_options );

	/* Single Product */
	$of_options[] = array( 'name' => esc_html__( 'Single Product', 'stevecadey' ),
		'type' => 'info',
		'std'  => esc_html__( 'Single Product', 'stevecadey' ),
	);

	$of_options[] = array(  'name' => esc_html__( 'Layout', 'stevecadey' ),
		'type' => 'select',
		'std'  => 'no_sidebar',
		'options' => array(
			'right_sidebar' => esc_html__( 'Right Sidebar', 'stevecadey' ),
			'left_sidebar'  => esc_html__( 'Left Sidebar', 'stevecadey' ),
			'no_sidebar'    => esc_html__( 'No Sidebar', 'stevecadey' ),
		),
		'id' => 'product_layout',
	);

	$of_options[] = array( 'name' => esc_html__( 'Show/Hide related products?', 'stevecadey' ),
		'type' => 'switch',
		'std'  => true,
		'id'   => 'product_single_display_related_products',
		'k2t_logictic' => array(
			'0' => array( ),
			'1' => array('product_related_products_column'),
		),
	);

	$of_options[] = array( 'name' => esc_html__( 'Column of related products', 'stevecadey' ),
		'type' => 'sliderui',
		'min'  => 2,
		'max'  => 4,
		'std'  => 3,
		'step' => 1,
		'id'   => 'product_related_products_column',
	);

endif; // end woocommerce option

/*___________________________________________________________________________________*/
/* 404 Page
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( '404 Page', 'stevecadey' ),
	'type' => 'heading',
	'icon' => '<i class="zmdi zmdi-alert-triangle"></i>'
);

$of_options[] = array( 'name' => esc_html__( '404 page', 'stevecadey' ),
	'type' => 'info',
	'std'  => esc_html__( '404 page', 'stevecadey' ),
);

// $of_options[] = array( 'name' => esc_html__( '404 Title', 'stevecadey' ),
// 	'type' => 'text',
// 	'std'  => 'Oops! Looks like something was broken.',
// 	'id'   => '404_title',
// 	'desc' => '',
// );

$of_options[] = array( 'name' => esc_html__( '404 Image', 'stevecadey' ),
	'type' => 'media',
	'std'  => get_template_directory_uri() . '/assets/img/file_not_found.png',
	'id'   => '404_image',
	'desc' => '',
);

// $of_options[] = array( 'name' => esc_html__( '404 Custom Text', 'stevecadey' ),
// 	'type' => 'textarea',
// 	'id'   => '404_text',
// );

/*___________________________________________________________________________________*/
/* Social Icons
/*___________________________________________________________________________________*/
$of_options[] = array( 'name' => esc_html__( 'Social', 'stevecadey' ),
	'type'  => 'heading',
	'icon'  => '<i class="zmdi zmdi-accounts"></i>'
);

$of_options[] = array( 'name' => esc_html__( 'Target', 'stevecadey' ),
	'type' => 'select',
	'std'  => '_blank',
	'options' => array(
		'_self'  => esc_html__( 'Same tab', 'stevecadey' ),
		'_blank' => esc_html__( 'New tab', 'stevecadey' ),
	),
	'id' => 'social_target',
);

$of_options[] = array( 'name' => esc_html__( 'Twitter username?', 'stevecadey' ),
	'type' => 'text',
	'std'  => 'lunartheme',
	'id'   => 'twitter_username',
	'desc' => esc_html__( 'Twitter username used for tweet share buttons.', 'stevecadey' ),
);

$of_options[] = array( 'name' => esc_html__( 'Icon title?', 'stevecadey' ),
	'type' => 'switch',
	'std'  => true,
	'id'   => 'social_title',
	'desc' => esc_html__( 'Turn it ON if you want to display social icon titles like Facebook, Google+, Twitter... when hover icons.', 'stevecadey' ),
);


foreach ( stevecadey_social_array() as $s => $c ):

	$of_options[] = array( 'name' => $c,
		'type' => 'text',
		'std'  => '',
		'id'   => 'social_' . $s,
	);

endforeach;


$of_options[] = array( 'name' => esc_html__( 'Social Share', 'stevecadey' ),
	'type'  => 'heading',
	'icon'  => '<i class="zmdi zmdi-accounts"></i>'
);

$of_options = stevecadey_social_share_options( '', $of_options );

/*___________________________________________________________________________________*/
/* One Click Install */
/*___________________________________________________________________________________*/
 $of_options[] = array( 'name'   => esc_html__( 'One Click Install', 'stevecadey' ),
 	'type'  => 'heading',
 	'icon'  => '<i class="zmdi zmdi-time"></i>'
 );
 $of_options[] = array( 'name'   => esc_html__( 'Transfer Theme Options Data', 'stevecadey' ),
 	'id'   => 'k2t_advance_backup',
 	'std'  => '',
 	'type' => 'k2t_advance_backup',
 );
