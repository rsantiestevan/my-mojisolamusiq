<?php
/**
 * The main template file.
 *
 * @package stevecadey
 * @author  LunarTheme
 * @link	http://www.lunartheme.com
 */

// Get theme options
if ( empty( $smof_data ) )
	$smof_data = of_get_options();

// blog setting

$archive_stt = array(
					'blog_layout'			=> '',
					'blog_style'			=> '',
					'blog_excerpt'			=> '',	// show or hide blog excerpt
					'blog_masonry_column'	=> '',
					'blog_grid_column'		=> '',
				);

// get data

foreach ($archive_stt as $key => $value) {
	if ( isset( $smof_data[$key] ) ) $archive_stt[$key] = $smof_data[$key];
}

// set up parent class

$stevecadey_classes = array();

$stevecadey_classes[] = 'b-' . $archive_stt['blog_style'];		// blog style

switch ( $archive_stt['blog_layout'] ) :			// sidebar class
	case 'right_sidebar':	$stevecadey_classes[] = 'right-sidebar'	;	break;
	case 'left_sidebar'	:	$stevecadey_classes[] = 'left-sidebar'		;	break;
	case 'no_sidebar'	:	$stevecadey_classes[] = 'no-sidebar'		;	break;
endswitch;

// If show excerpt we have class on top

$stevecadey_classes[] = $archive_stt['blog_excerpt'] ? 'show-excerpt' : '';

get_header(); ?>

	<div class="k2t-content <?php echo esc_attr( implode( ' ', $stevecadey_classes ) ); ?>">

		<div class="k2t-wrap">

			<main class="k2t-blog">
				<div class="k2t-blog-inner">
					<?php

						// wrap loop post for masonry and grid stye

						switch ( $archive_stt['blog_style'] ) :
							case 'masonry':
								echo '<div class="masonry-layout ' . esc_attr( $archive_stt['blog_masonry_column'] ) . ' ">'.
										'<div class="grid-sizer"></div>';
								break;

							case 'grid':
								echo '<div class="grid-layout clearfix ' . esc_attr( $archive_stt['blog_grid_column'] ) . ' ">';
								break;
						endswitch;

						// loop post

						if ( have_posts() ) :

							while ( have_posts() ) : the_post();

								switch ( $archive_stt['blog_style'] ) :
									case 'large':	// BLOG LARGE
										include STEVECADEY_TEMPLATE_PATH . 'blog/content-large.php';
										break;
									case 'medium':	// BLOG MEDIUM
										include STEVECADEY_TEMPLATE_PATH . 'blog/content-medium.php';
										break;
									default:		// BLOG MANSORY AND GRID
										include STEVECADEY_TEMPLATE_PATH . 'blog/content-grid.php';
										break;
								endswitch;

							endwhile;

						else :
							get_template_part( 'content', 'none' );
						endif;	// END HAVE POST

						// close wrap loop post for mansory and grid style

						if ( 'masonry' == $archive_stt['blog_style'] || 'grid' == $archive_stt['blog_style'] ) echo '</div>';

						// Navigation

						include_once STEVECADEY_TEMPLATE_PATH . 'navigation.php';

					?>
				</div>
			</main><!-- .k2t-main -->

			<?php  // get sidebar
				if ( 'no_sidebar' != $archive_stt['blog_layout'] ) get_sidebar();
			?>

		</div><!-- .k2t-wrap -->
	</div><!-- .k2t-content -->

<?php get_footer();
